/*=================================================================*/
/** @file D2Struct.hpp
 *  @brief Diablo II structures definitions.
 *
 *  (c) January 2002 - Joel Falcou for The Phrozen Keep.
 */
/*=================================================================*/

#ifndef __D2STRUCT_HPP__INCLUDED__
#define __D2STRUCT_HPP__INCLUDED__

/*=================================================================*/
/*    Common Structure.                                            */ 
/*=================================================================*/

struct D2Seed 
{
	DWORD lowSeed;
    DWORD highSeed;
};

struct FogBitMasks
{
	DWORD	Mask[32];
};

/*=================================================================*/
/*   Forward Declarations.                                         */
/*=================================================================*/

// Unit related
struct Unit;
struct D2UnitFindParam;
struct D2UnitsList;
struct D2UnitFind;

struct D2PlayerData;
struct D2MonsterData;
struct D2ObjectData;
struct D2ItemData;
struct D2MissileData;
struct D2TileData;
struct UnitAi;

struct Quest;
struct Waypoint;
struct LocInfo;
struct Node;
struct D2Inventory;
struct Skills;
struct D2SkillData;
struct Stats ;
struct D2Combat;
struct D2Game;
struct Room;
struct Client;
struct D2HirelingWrapper;

// Map & dungeons related.
// struct DrlgRoom2;
// struct DrlgLevel;
// struct DrlgMisc;
// struct DRLGAct;
// struct D2CoordStrc;

struct DRLGRoom;
struct DRLGCoords;
struct DRLGRoomTiles;
struct D2RoomCollisionStrc;
struct DRLGRoomEx;
struct DRLGUnits;
struct D2MapTileStrc;
struct DRLGLevel;
struct DRLGMazeRoom;
struct DRLGOutdoorRoom;
struct D2TileLibraryHashStrc;
struct D2RoomTileStrc;
struct D2PresetUnitStrc;
struct DRLGOrth;
struct DRLGCoordList;
struct DRLGMisc;
struct DRLGCoord;
struct DRLGPresetInfo;
struct DRLGOutdoorInfo;
struct DRLGTileInfo;
struct DRLGAct;
struct DRLGMiscData;
struct DRLGWarpStuff;
struct DRLGGrid;
struct DRLGVer;
struct DRLGRoomData;
struct DRLGEnv;
struct DRLGTileData;
struct D2TileLibraryEntryStrc;
struct D2GFXTileStrc;
struct D2TileLibraryHashNodeStrc;
struct D2TileLibraryBlockStrc;
struct D2TileRecordStrc;
struct D2TileLibraryHashRefStrc;
//struct D2TileLibraryHashNodeStrc;
struct D2TileLibraryStrc;
struct D2TileLibraryHeaderStrc;
struct DRLGDelete;
struct DRLGWarp;

// Old DLRG structs
struct RoomTile;
struct RoomTile2;
struct D2Path;
struct D2StaticPath;

// Automap related.
struct AutomapCell;
struct AutomapLayer;

// String Table realted
struct TblHeader;
struct TblNode;

// Shrines effect
struct StateEffect;

// Menus
struct Menu;
struct NpcMenuData;

// TXT tables
struct D2SkillsTXT;
struct D2SkillDescTXT;
struct D2BooksTXT;
struct D2ItemsTXT;
struct D2RunesTXT;
struct D2RuneStruct;
struct D2ShrinesTXT;
struct D2CharStatsTXTTXT;
struct D2ItemTypesTXT;
struct D2ItemStatCostTXT;
struct D2MissilesTXT;
struct D2LvlMazeTXT;
struct D2InventoryTXT;

// Cube Related
struct CubeInput;
struct CubeOutput;

// Object Related
struct Operate;

// Vendors
struct VendorLst;
struct VItems;

// Item
struct D2DisplayData;

// Room Structs
// struct Adjacent;
// struct DRLGRoomEx;
// struct DRLGRoom;

// Mem handling structs
struct D2PoolBlockStrc;
struct D2PoolStrc;
struct D2PoolBlockEntryStrc;
struct D2PoolManager;
struct D2MemoryManagerStrc;

// item related structs
struct D2StatListExStrc;

// Lightmap struct
struct D2LightMapStrc;

// States & Stats

struct D2Stats;
struct D2StatInfo;
struct StatsList;
struct D2StatsListEx;
struct D2Stats;
struct D2ItemEvent;
struct D2ClientItemEvent;
struct D2StatsList;

// Combat
struct D2DamageData;

// Quests
struct D2QuestDataEx;
struct D2QuestGUIDStrc;
struct D2QuestFlagStrc;
struct D2QuestTimerStrc;
struct D2QuestDataStrc;
struct D2QuestArgStrc;
struct D2QuestControlStrc;
struct DrlgLevel;
struct DrlgRoom2;
struct DrlgMisc;

// tcEx
struct D2TCExInfoStrc;
struct D2TCExShortStrc;


// Memory handling defines
#define MAX_POOL_NAME 32
#define MAX_POOLS 40
#define MAX_MANAGERS 8
#define MAX_POOL_OVERFLOW 1023

// Automap
#define AUTOMAP_CACHE_CELLS 512

// States
#define MAX_STATES 16384
#define STATE_INDEX_BITS 10
#define STATE_MAX_INDEX ((1 << STATE_INDEX_BITS) - 1)

// Levels
#define MAX_LEVELS 255

// Quests
#define MAX_QUEST_CALLBACKS	15


typedef bool (FASTCALL * QUESTACTIVE)  (D2QuestDataStrc* pQuest, int nNPC, Unit* pPlayer, D2QuestFlagStrc* pFlags, Unit* ptNPC); 
typedef void (FASTCALL * QUESTCALLBACK)(D2QuestDataStrc* pQuest, D2QuestArgStrc* pArgs);
typedef bool (FASTCALL * QUESTSTATUS)  (D2QuestDataStrc* pQuest, Unit* pPlayer, D2QuestFlagStrc* pGlobalFlags, D2QuestFlagStrc* pFlags, BYTE* pStatus);
typedef bool (FASTCALL * QUESTSEQ)     (D2QuestDataStrc* pQuest); //, D2UnitStrc* pPlayer, D2QuestFlagStrc*, D2QuestFlagStrc*, BYTE*);


/*=================================================================*/
/*    Specific Structure.                                          */ 
/*=================================================================*/

#include "D2TxtStruct.h"
#include "D2UnitStruct.hpp"
#include "D2DRLGStruct.hpp"
#include "d2customtxt.hpp"
#include "D2StringTblStruct.hpp"

#endif
