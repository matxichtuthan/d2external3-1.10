/*==============================================*/
/** @file d2wrapper.hpp
 *  @brief Main Diablo II extra DLL handler.
 *
 *  This is the header of the D2 Wrapper functions.
 */
/*==============================================*/

#ifndef __D2WRAPPER_HPP__INCLUDED__
#define __D2WRAPPER_HPP__INCLUDED__

#include "error.hpp"
#include "common.hpp"

/*==============================================*/
/**
 *  @brief Patch the functions memory address into a memory offset
 *
 *  F = Function Name
 *  A = Memory offset to copy to
 *
 */
/*==============================================*/
#define PATCH( F, A ) *((int*)A) = (int)&F;

/*==============================================*/
/**
 *  @brief Update memory location with a offset
 *
 *  F = Updated offset
 *  A = Memory offset to put the Updated offset
 *
 */
/*==============================================*/
#define UPDATE( F, A) *((int*)A) = (int)F;

/*==============================================*/
/**
 *  @brief Load a Diablo II DLL file into memory space.
 *
 *  Load a DLL called L into memory space.
 *  If the library can't be loaded, an error
 *  is thrown from file F.
 *
 */
/*==============================================*/

#define CALL(F, A) *(unsigned char*)A = 0xE8; *(unsigned long*)((unsigned long)A + 1) = (unsigned long)((unsigned long)F - ((unsigned long)A + sizeof(unsigned char) + sizeof(unsigned long)));

/*===========================================*/
// @brief Write a relative jump to a location
// F = Function being called
// A = Memory offset to put the call
/*===========================================*/

#define JUMP(F, A) *(unsigned char*)A = 0xE9; *(unsigned long*)((unsigned long)A + 1) = (unsigned long)((unsigned long)F - ((unsigned long)A + sizeof(unsigned char) + sizeof(unsigned long)));

/*
To push extra args, edit the dlls, or convert the edits to:
BYTE_PATCH(A,OP) *(unsigned char*)A = OP;
and do a byte byte, or word by word etc edits at run time
*/

#define LOAD_LIBRARY(L, F )										\
if( !LoadLibrary(L) )												\
{																	\
	log_error( F, " Failed to load library " L );				    \
}																	\
else																\
{																	\
	log_info( L " loaded succesully." );						    \
}																	\

/*==============================================*/
/**
 *  @brief Unprotect Diablo II DLL.
 *
 *  Change the protection scheme of a loaded
 *  DLL called L in memory space at address A+O
 *  to allow us to customize it.
 *  If the library can't be loaded, an error
 *  is thrown from file F.
 *
 */
/*==============================================*/

#define HOOK_LIBRARY( L, A, O, F )									\
if( !VirtualProtect((LPVOID)A, O, PAGE_EXECUTE_READWRITE, &dw) )	\
{																	\
	log_error( F, " Failed to hook " L );		                    \
}																	\
else																\
{																	\
	log_info( L " succesully hooked." );							\
}																	\

/*==============================================*/
/** 
 *  @brief DLL wrapper initialisation.
 *
 *  Initialise the D2External DLL by
 *  settign up all the needed part.
 *
 *  @param pLogfilePath : Filename for the debug 
 *  log file.
 *
 */
/*==============================================*/

void STDCALL init( const char* pLogfilePath );

/*==============================================*/
/** 
 *  @brief DLL wrapper clean-up.
 *
 *  Close and release all ressoruces taken by
 *  the D2External DLL.
 *
 */
/*==============================================*/

void STDCALL release();

/*==============================================*/
/** 
 *  @brief DLL wrapper hooking.
 *
 *  Hook the D2External.dll to the global memory 
 *  space of Diablo II by retrieving the HANDLE
 *  of Diablo II application.
 *
 *  @param pModule : handle to Diablo II.
 *
 */
/*==============================================*/

void STDCALL set_module( HANDLE pModule );

/*==============================================*/
/** 
 *  @brief DLL wrapper patching.
 *
 *  Patch the various Diablo II DLL by changing
 *  the address of calling routines.
 *
 */
/*==============================================*/

void STDCALL init_patch();

static HANDLE module_handle = INVALID_HANDLE_VALUE;
static char   application_path[MAX_PATH] = "";

#define _CRT_SECURE_NO_WARNINGS
#define _CRT_NON_CONFORMING_SWPRINTFS

__forceinline void D2FillMemWithNOP(DWORD dwAddress, int itercount);


/*
	Date: 08.04.12
	Author: Thomas Westman
	Function: Fills mem with nop
	Location: 
*/

__forceinline void D2FillMemWithNOP(DWORD dwAddress, int itercount)
{
	for (int i = 0; i < itercount; i++)
		 *(BYTE*)(dwAddress+i) = 0x90;
}

#endif