#include "Common.hpp"
#include "error.hpp"
#include "d2functions.hpp"
#include "d2customtxt.hpp"
#include "d2wrapper.hpp"
#include "Unit.hpp"

/*
	Date: 2017-07-10
	Author: Thomas Westman
	Function: Units_GetTargetUnit
	Location: D2Game.GetTargetUnit
*/

Unit* STDCALL Unit_GetTargetUnit (D2Game* ptGame, Unit* ptUnit)
{
  
	if (ptGame == NULL){ // Check if ptGame == NULL
		 log_message("Unit_GetTargetUnit(): ptGame == %d",ptGame);
		return 0;
	}	

		if (ptUnit == NULL){ // Check if ptUnit == NULL
		 log_message("Unit_GetTargetUnit(): ptUnit == %d",ptUnit);
		return 0;
	}	

  BOOL CheckState241 = D2CheckState(ptUnit, 241);
  BOOL CheckState247 = D2CheckState(ptUnit, 247);
  Unit* ptOldTarget = NULL;
  D2Path* ptPath = ptUnit->hPath;

  if (CheckState241 == TRUE || CheckState247 == TRUE)
  {
	  if (ptPath)
		  return ptPath->pTargetUnit;

	  return D2GetTargetUnitWrapper(ptGame, ptUnit);
  }
 
	return D2GetTargetUnitWrapper(ptGame, ptUnit);
}

/*
	Date: 2017-07-10
	Author: Thomas Westman
	Function: Units_GetTargetUnit
	Location: D2Game.GetTargetUnit
*/

Unit* STDCALL Unit_GetTargetUnitv2(D2Game* ptGame, Unit* ptUnit)
{
  
	if (ptGame == NULL){ // Check if ptGame == NULL
		 log_message("Unit_GetTargetUnitv2(): ptGame == %d",ptGame);
		return 0;
	}	

		if (ptUnit == NULL){ // Check if ptUnit == NULL
		 log_message("Unit_GetTargetUnitv2(): ptUnit == %d",ptUnit);
		return 0;
	}	

  BOOL CheckState241 = D2CheckState(ptUnit, 241);
  BOOL CheckState247 = D2CheckState(ptUnit, 247);
  Unit* ptOldTarget = NULL;
  D2Path* ptPath = ptUnit->hPath;

  if (CheckState241 == TRUE || CheckState247 == TRUE)
  {
	  if (ptPath)
		  return ptPath->pTargetUnit;

	  return D2GetTargetUnit(ptGame, ptUnit);
  }
 
	return D2GetTargetUnit(ptGame, ptUnit);
}