#ifndef __AI_HPP__INCLUDED
#define __AI_HPP__INCLUDED

typedef void (FASTCALL * MONAI)(D2Game* ptGame, Unit* ptMonster, AIParam* ptAIParam);

struct D2MonAIStrc
{
    DWORD   nAIType;		//+00
    MONAI   pfInitAI;		//+04
    MONAI   pfActiveAI;		//+08
    MONAI   pfAlteredAI;	//+0C
};

enum D2AITypes
{
	AI_DEFAULT		= 0,
    AI_BASIC		= 1,
    AI_ADVANCED		= 2,	
    AI_SPECIAL		= 3,    //Tenticle, Trap, Hydra, Vines, Raven, NPCBarb, Nilithak, Wussie, Ancient, BaalThrone & Shadows
	AI_MULTILAYER	= 4,
    AI_HIDDEN		= 5
};

extern const D2MonAIStrc gpfMonsterAI[];
extern const DWORD gdwfMonsterAI;

void FASTCALL AI_NONE(D2Game* ptGame, Unit* ptUnit, AIParam* ptAIParam);
void FASTCALL AI_IDLE(D2Game* ptGame, Unit* ptUnit, AIParam* ptAIParam);

#endif