/*=================================================================*/
/** @file Error.cpp
 *  @brief Error Logger implementation.
 *
 *  (c) January 2002 - Joel Falcou for The Phrozen Keep.
 */
/*=================================================================*/

#define _CRT_SECURE_NO_WARNINGS
#define _CRT_NON_CONFORMING_SWPRINTFS

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <wchar.h>
#include <wctype.h>

#include "common.hpp"
#include "d2functions.hpp"
#include "error.hpp"

void set_logfile( const char* pPath )
{
	strcpy( log_file, pPath );

	FILE* lLog = fopen( log_file, "w" );

	if( lLog != NULL )
    {
	    fclose(lLog);
	    log_init = true;
	}
}

void log_error( const char* pLocation, const char* pDesc )
{
	log_info("==============================================================");
	log_info("\tError trapped");
	log_message("\tFile: %s", pLocation);
	log_message("\tDescription: %s", pDesc);
	log_info("==============================================================");

    FogAssert( pDesc, pLocation, 666 );
	emergency_exit();
}


void log_info( const char* pMessage )
{
	if( log_init )
    {
	    FILE* lDebug = fopen( log_file, "a+" );

	    if( lDebug != NULL)
        {
	        fprintf( lDebug, pMessage );
	        fprintf( lDebug, "\n" );
	        fclose( lDebug );
        }
		else
        {
            log_init = false;
        }
	}
}


void log_message( const char* pFormat, ... )
{
	if( log_init )
    {
	    va_list lArgs;
	    va_start( lArgs, pFormat );

	    FILE *lDebug = fopen( log_file, "a+" );

	    if( lDebug != NULL )
        {
	        vfprintf( lDebug, pFormat, lArgs );

	        fprintf( lDebug, "\n" );
	        fclose( lDebug );

	        va_end(lArgs);
	    }
	}
}


void log_wmessage( const wchar_t* pFormat, ... )
{
	if( log_init )
    {
	    va_list lArgs;
	    va_start( lArgs, pFormat );

	    FILE *lDebug = fopen( log_file, "a+" );

	    if( lDebug != NULL )
        {
	        vfwprintf( lDebug, pFormat, lArgs );

	        fprintf( lDebug, "\n" );
	        fclose( lDebug );

	        va_end(lArgs);
	    }
	}
}

void emergency_exit()
{
	log_info("Performing emergency shutdown under unrecoverable conditions.");
	exit(0);
}

void d2_assert( bool pCondition, char* pLocation, char* pMessage, int pLineNbr )
{
    if( !pCondition )
    {
        FogAssert( pMessage, pLocation, pLineNbr );
    }
}
