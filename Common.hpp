/*=================================================================*/
/*  @file Custom.h
 *
 *
 *  (c) May 2003 - Allan Jensen.
 */
/*=================================================================*/

#ifndef __COMMON_HPP__INCLUDED__
#define __COMMON_HPP__INCLUDED__

#define STDCALL	__stdcall
#define FASTCALL __fastcall

#include <windows.h>
#include "D2Constants.hpp"
#include "D2Struct.hpp"

static DWORD CommonOffset	= (DWORD)LoadLibrary("D2Common.dll");
static DWORD GameOffset		= (DWORD)LoadLibrary("D2Game.dll");
static DWORD LangOffset		= (DWORD)LoadLibrary("D2Lang.dll");
static DWORD FogOffset		= (DWORD)LoadLibrary("Fog.dll");
static DWORD GfxOffset		= (DWORD)LoadLibrary("D2Gfx.dll");
static DWORD ClientOffset	= (DWORD)LoadLibrary("D2Client.dll");
static DWORD NetOffset		= (DWORD)LoadLibrary("D2Net.dll");
static DWORD LaunchOffset	= (DWORD)LoadLibrary("D2Launch.dll");
static DWORD WinOffset		= (DWORD)LoadLibrary("D2Win.dll");
static DWORD StormOffset	= (DWORD)LoadLibrary("Storm.dll");
static DWORD CmpOffset		= (DWORD)LoadLibrary("D2Cmp.dll");

// Memory handling defines
#define MAX_POOL_NAME 32
#define MAX_POOLS 40
#define MAX_MANAGERS 8
#define MAX_POOL_OVERFLOW 1023

// Automap
#define AUTOMAP_CACHE_CELLS 512

// States
#define MAX_STATES 16384
#define STATE_INDEX_BITS 10
#define STATE_MAX_INDEX ((1 << STATE_INDEX_BITS) - 1)

// Savefile
#define MAX_SAVEFILE_SIZE 8192 // 8192 extended 16000

// Levels
#define MAX_LEVELS 255

// Quests
#define MAX_QUEST_CALLBACKS	15

#define INVALID_GUID -1
#define INVALID_UNIT_ID -1
#define UNIT_INVALID -1
#define INVALID_SHRINE_ID -1

#define ARRAY(x) (sizeof(x) / sizeof(x[0]))
#define CHANCE_CALC	10000

// Packets
#define TOTALPACKETTYPES 180 // Max 255

DWORD STDCALL Code2Bin(const char* ptCode);
bool TestCode(DWORD dwCode, const char* code);
int GetRand(int nMaxNum);

// Units
#define MAXCLASSES 8 // original default 7

// Classes
class DynBook
{
	DWORD SaveTownID[5];
	BYTE TownFlag;
	BYTE LastVisitAct;
	BYTE TownDifficulty;
	DWORD SaveTownID_NM[5];
	DWORD SaveTownID_HELL[5];

	public:
		DynBook();
		~DynBook();
		void Init();
		
		void DynBook::SetTownID(BYTE Act, DWORD LevelID);
		DWORD DynBook::GetTownID(BYTE Act);
		void DynBook::SetTownFlag(BYTE Flag);
		BYTE DynBook::GetTownFlag();
		void DynBook::SetLastVisitAct(BYTE act);
		BYTE DynBook::GetLastVisitAct();
		void DynBook::SetCurrentTownDifficulty(BYTE Difficulty);
		BYTE DynBook::GetCurrentTownDifficulty();
		void DynBook::SetTownID_NM(BYTE Act, DWORD LevelID);
		DWORD DynBook::GetTownID_NM(BYTE Act);
		void DynBook::SetTownID_HELL(BYTE Act, DWORD LevelID);
		DWORD DynBook::GetTownID_HELL(BYTE Act);

};

extern DynBook* D2DynBook;

void initvar();

// Templates

template <class MemType> void readoffset(MemType* var, DWORD offset)
{
	*var = (*(MemType*)offset);
}

template <class MemType> void writeoffset(MemType var, DWORD offset)
{
	(*(MemType*)offset) = var;
}

// Datatable Functions

D2DataTables* D2GetDataTables();
D2ItemStatCostTXT* D2GetItemStatsTXT(int nStat);
D2StatesTXT* D2GetStatesTXT(int nState);
D2MissilesTXT* D2GetMissilesTXT(int MissileID);
D2MonstatsTXT* D2GetMonstatsTXT(int MonstatID);
D2CharStatsTXT* D2GetCharStatsTXT(int CharStatID);
D2SkillDescTXT* D2GetSkillDescTXT(int SkilldescID);
D2LevelsTXT* D2GetLevelsTXT(int LevelID);
D2Monstats2TXT* D2GetMonstats2TXT(int MonstatEx);

Unit* FASTCALL FindItemInPage(D2Inventory* ptInventory, DWORD dwCode,  BYTE bPage, Unit* ptOldItem = NULL);
Unit* FASTCALL FindItemInBelt(D2Inventory* ptInventory, DWORD dwCode);
void DASpawnItem(D2Game* ptGame, Unit* ptUnit, DWORD itemCode, DWORD ilvl, DWORD quality, DWORD page, BYTE CubeClientMsg, BYTE unidItem, BOOL nBeltInv = FALSE, BOOL notSellable = FALSE, BOOL nPlayerName = FALSE);
BOOL FASTCALL ChanceCalc(WORD Pct, DWORD Max = 0);

#endif
