#ifndef __UTILITY_HPP__INCLUDED__
#define __UTILITY_HPP__INCLUDED__

D2LightMapStrc* FASTCALL SetPlayerLightRadius(Unit* ptUnit, BOOL nFlag, WORD wLight, WORD w255, WORD wRed, WORD wGrn, WORD wBlu);

#endif
