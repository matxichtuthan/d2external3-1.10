/*=================================================================*/
/** @file Main.cpp
 *  @brief Main file of D2External.DLL
 *
 *  (c) January 2002 - Joel Falcou for The Phrozen Keep.
 */
/*=================================================================*/

#include "d2wrapper.hpp"

BOOL WINAPI DllMain(HANDLE hModule, DWORD dwReason, LPVOID lpReserved)
{
    set_module( hModule );

	switch(dwReason)
    {
		case DLL_PROCESS_ATTACH: 
		init("D2Revolution.log");
		break;

		case DLL_PROCESS_DETACH:
		release();
		break;
	}

	return TRUE;
}