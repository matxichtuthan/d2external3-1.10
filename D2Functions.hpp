/*==============================================*/
/** @file d2functions.hpp 
    @brief Exported Diablo II DLL functions.

    This file implements some common and useful
    function related to some Diablo II mechanisms.

 */ 
/*==============================================*/

#ifndef __D2FUNCTIONS_HPP__INCLUDED__
#define __D2FUNCTIONS_HPP__INCLUDED__

#include "common.hpp"
#include "d2struct.hpp"
#include "d2wrapper.hpp"

/*==============================================*/
/** 
 *  @brief stdcall function pointer declaration.
 *
 *  Declares the type for a STDCALL function pointer
 *  called N with arguments list A adn returning R.
 *
 */
/*==============================================*/

#define STDCALL_FUNCTION( R, N, A )  typedef R (_stdcall *N) A

/*==============================================*/
/**
 *  @brief fastcall function pointer declaration.
 *
 *  Declares the type for a FASTCALL function pointer
 *  called N with arguments list A adn returning R.
 *
 */
/*==============================================*/

#define FASTCALL_FUNCTION( R, N, A ) typedef R (_fastcall *N) A

/*==============================================*/
/**
 *  @brief cdecl function pointer declaration.
 *
 *  Declares the type for a CDECL function pointer
 *  called N with arguments list A adn returning R.
 *
 */
/*==============================================*/

#define CDECL_FUNCTION( R, N, A ) typedef R (_cdecl *N) A

/*=================================================================*/
/* D2 Functions prototypes CDECL                                   */
/*=================================================================*/

CDECL_FUNCTION(void,			D2Fog2727,			(const char* ptMessage, const char* ptLocation, DWORD line));
CDECL_FUNCTION(void,			D2Fog2728,			(const char* ptMessage, const char* ptLocation, DWORD line));
CDECL_FUNCTION(void,			D2L_SPrintF,		(DWORD wChar_Size, LPWSTR ptTo, LPCWSTR ptFrom, ...));


/*=================================================================*/
/* D2 hardcoded tables			                                   */
/*=================================================================*/


/*=================================================================*/
/* D2 Functions prototypes STDCALL                                 */
/*=================================================================*/

//D2Common
STDCALL_FUNCTION(BYTE,			D2Common2711,		(DWORD LevelID));
STDCALL_FUNCTION(Unit*,			D2Common272B,		(DRLGRoom* ptRoom, DWORD unitID, LvlWarpWrapper* ptLvlWarpWrapper));
//STDCALL_FUNCTION(void,			D2Common2733,		(DrlgRoom1* ptRoom, void* ptRoomList, void* nRoomsNear));
STDCALL_FUNCTION(void,			D2Common2733,		(DRLGRoom* ptRoom, void* ptRoomList, void* nRoomsNear));
//STDCALL_FUNCTION(void,			D2Common2733_2,		(DRLGRoom* ptRoom, void* ptRoomList, void* nRoomsNear));
//STDCALL_FUNCTION(DrlgRoom1*,	D2Common2739,		(DWORD x, DWORD y, BYTE Act));
STDCALL_FUNCTION(DRLGRoom*,		D2Common2739,		(DWORD x, DWORD y, BYTE Act));
//STDCALL_FUNCTION(Unit*,			D2Common2747,		(DrlgRoom1* ptRoom));
STDCALL_FUNCTION(Unit*,			D2Common2747,		(DRLGRoom* ptRoom));
//STDCALL_FUNCTION(DWORD,			D2Common2749,		(DrlgRoom1* ptRoom));
STDCALL_FUNCTION(DWORD,			D2Common2749,		(DRLGRoom* ptRoom));
//STDCALL_FUNCTION(DWORD,			D2Common2749_2,		(DRLGRoom* ptRoom));
STDCALL_FUNCTION(DWORD,			D2Common274A,		(DRLGRoom* ptRoom, DWORD nUnitID));
//STDCALL_FUNCTION(DWORD,			D2Common2752,		(DrlgRoom1* ptRoom, DWORD UnitType, DWORD UniqueID));
STDCALL_FUNCTION(DWORD,			D2Common2752,		(DRLGRoom* ptRoom, DWORD UnitType, DWORD UniqueID));
//STDCALL_FUNCTION(BOOL,			D2Common2762,		(DrlgRoom1* ptRoom));
STDCALL_FUNCTION(BOOL,			D2Common2762,		(DRLGRoom* ptRoom));
//STDCALL_FUNCTION(BOOL,			D2Common2762_2,		(DRLGRoom* ptRoom));
STDCALL_FUNCTION(DWORD,			D2Common2765,		(BYTE Act));
STDCALL_FUNCTION(void,			D2Common2776,		(DRLGRoom* ptRoom, DWORD nFrame));
STDCALL_FUNCTION(WORD,			D2Common2788,		(DRLGRoom* ptRoom, int TargetX, int TargetY, BYTE sizeX, BYTE sizeY, WORD fMask));
STDCALL_FUNCTION(DRLGRoom*,		D2Common2796,		(DRLGRoom* ptRoom, D2CoordStrc *ptCoordStrc, BYTE sizeX, WORD fMask, BOOL bUseDest)); 
STDCALL_FUNCTION(DRLGRoom*,		D2Common279A,		(DRLGRoom* pRoom, D2CoordStrc *ptDest, D2CoordStrc *ptSource, int nSize, WORD fMask, WORD fFieldMask, BOOL bUseDest)); 
STDCALL_FUNCTION(DWORD,			D2Common27AE,		(D2Path* hPath));
STDCALL_FUNCTION(void,			D2Common27B0,		(D2Path* ptPath, int nDirection64));
STDCALL_FUNCTION(void,			D2Common27B1,		(D2Path* hPath, DWORD nDirection));
STDCALL_FUNCTION(WORD,			D2Common27B2,		(D2Path* hPath));
STDCALL_FUNCTION(WORD,			D2Common27B3,		(D2Path* hPath));
STDCALL_FUNCTION(WORD,			D2Common27BF,		(D2Path* hPath));
STDCALL_FUNCTION(WORD,			D2Common27C0,		(D2Path* hPath));
STDCALL_FUNCTION(Unit*,			D2Common27C4,		(D2Path* hPath));
STDCALL_FUNCTION(void,			D2Common27C6,		(D2Path* hPath, DWORD nCollosionType));
STDCALL_FUNCTION(void,			D2Common27DB,		(D2Path* hPath, BOOL Rotate));
STDCALL_FUNCTION(void,			D2Common27E3,		(Unit* ptUnit, int nPattern));
STDCALL_FUNCTION(void,			D2Common27E6,		(Unit* ptUnit));
STDCALL_FUNCTION(DWORD,			D2Common27EC,		(DWORD AttackerOffsetX, DWORD AttackerOffsetY, DWORD DefenderOffsetX, DWORD DefenderOffsetY));
STDCALL_FUNCTION(void,			D2Common27EF,		(Unit* ptUnit, int nPathType));
//STDCALL_FUNCTION(void,			D2Common27F5,		(D2Path* hPath, Unit* ptMissile, DrlgRoom1* ptRoom, DWORD offsetX, DWORD offsetY));
STDCALL_FUNCTION(void,			D2Common27F5,		(D2Path* hPath, Unit* ptMissile, DRLGRoom* ptRoom, DWORD offsetX, DWORD offsetY));
STDCALL_FUNCTION(D2Inventory*,	D2Common2800,		(D2Inventory* ptInventory, Unit* ptItem));
STDCALL_FUNCTION(int,			D2Common2802,		(D2Inventory* ptInventory, Unit* ptItem, int nodePage));
STDCALL_FUNCTION(int,			D2Common280D,		(D2Inventory* ptInventory, Unit* ptItem, int bodylocation));
STDCALL_FUNCTION(Unit*,			D2Common2811,		(D2Inventory* ptInventory, DWORD BodyLoc));
STDCALL_FUNCTION(Unit*,			D2Common2812,		(D2Inventory* ptInventory));
STDCALL_FUNCTION(void,			D2Common2815,		(D2Inventory* ptInventory, Unit* ptItem));
STDCALL_FUNCTION(Unit*,			D2Common2816,		(D2Inventory* ptInventory));
STDCALL_FUNCTION(BOOL,			D2Common281D,		(D2Inventory* ptInventory, Unit* ptItem, DWORD *invMem));
STDCALL_FUNCTION(Unit*,			D2Common281F,		(D2Inventory* ptInventory, DWORD column));
STDCALL_FUNCTION(BOOL,			D2Common2820,		(D2Inventory* ptInventory, Unit* ptMouseItem, DWORD column, D2InvBeltItemStrc* ptInvBeltItemStrc));
STDCALL_FUNCTION(Unit*,			D2Common2825,		(D2Inventory* ptInventory));
STDCALL_FUNCTION(void,			D2Common282B,		(D2Inventory* ptInventory, Unit* ptItem));
STDCALL_FUNCTION(Unit*,			D2Common282D,		(D2Inventory* ptInventory, DWORD nItemType, DWORD nFlag));
STDCALL_FUNCTION(void,			D2Common2831,		(D2Inventory* ptInventory, Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common282C,		(D2Inventory* ptInventory));
STDCALL_FUNCTION(void,			D2Common2834,		(D2Inventory* ptInventory2, DWORD ptPlayerUniqueID));
STDCALL_FUNCTION(DWORD,			D2Common2835,		(D2Inventory* ptInventory2));
STDCALL_FUNCTION(void,			D2Common2836,		(D2Inventory* ptInventory, DWORD CorpseUniqueID, int unknown1, int unknown2));
STDCALL_FUNCTION(void,			D2Common2837,		(D2Inventory* ptInventory2, DWORD CorpseUniqueID, DWORD nFlag));
STDCALL_FUNCTION(Unit*,			D2Common2838,		(D2Inventory* ptInventory));	
STDCALL_FUNCTION(int,			D2Common2839,		(D2Inventory* ptInventory));
STDCALL_FUNCTION(int,			D2Common283B,		(Unit* ptPlayer, BYTE BodyLoc, Unit* ptItem, DWORD nSet));
STDCALL_FUNCTION(Unit*,			D2Common2840,		(Unit* ptItem));
STDCALL_FUNCTION(Unit*,			D2Common2841,		(Unit* ptItem));
STDCALL_FUNCTION(int,			D2Common2843,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2846,		(D2Inventory* ptInventory, Unit* ptItem));
STDCALL_FUNCTION(Unit*,			D2Common2849,		(Unit* ptBodyUniqueIDList));
STDCALL_FUNCTION(int,			D2Common284A,		(Unit* ptBodyUniqueIDList));
STDCALL_FUNCTION(Skills*,		D2Common2850,		(Unit* ptPlayer));
STDCALL_FUNCTION(Skills*,		D2Common2851,		(Unit* ptPlayer));
STDCALL_FUNCTION(Skills*,		D2Common2852,		(Unit* ptPlayer));
STDCALL_FUNCTION(Skills*,		D2Common2853,		(Unit* ptPlayer));
STDCALL_FUNCTION(void,			D2Common285C,		(Unit* ptUnit, void* ptOffsetStruct));
STDCALL_FUNCTION(BYTE,			D2Common2860,		(Unit* ptUnit));
STDCALL_FUNCTION(BYTE,			D2Common2861,		(Unit* ptUnit));
//STDCALL_FUNCTION(DrlgRoom1*,	D2Common2866,		(Unit* ptUnit));
STDCALL_FUNCTION(DRLGRoom*,		D2Common2866,		(Unit* ptUnit));
//STDCALL_FUNCTION(DRLGRoom*,		D2Common_2866,		(Unit* ptUnit));
STDCALL_FUNCTION(void,			D2Common2868,		(Unit* ptUnit, Unit* ptTarget));
STDCALL_FUNCTION(void,			D2Common286C,		(Unit* ptUnit, DWORD Mode));
STDCALL_FUNCTION(void,			D2Common286D,		(Unit* ptUnit));
STDCALL_FUNCTION(void,			D2Common286F,		(Unit* pUnit, DRLGRoom* pRoom, int nXpos, int nYpos));
STDCALL_FUNCTION(void,			D2Common2870,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common2875,		(Unit* ptUnit, int nFlags));
STDCALL_FUNCTION(BOOL,			D2Common287A,		(Unit* ptUnit, Unit* ptTargetUnit, int nType));
STDCALL_FUNCTION(int,			D2Common287F,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2880,		(Unit* ptPlayer));
STDCALL_FUNCTION(void,			D2Common2884,		(Unit* ptUnit, DWORD CurrentFrame));
STDCALL_FUNCTION(void,			D2Common2891,		(Unit* ptUnit));
STDCALL_FUNCTION(void,			D2Common2892,		(Unit* ptItem));
STDCALL_FUNCTION(BOOL,			D2Common2899,		(Unit* ptObject));
STDCALL_FUNCTION(D2ObjectsTXT*,	D2Common289A,		(Unit* ptObject));
STDCALL_FUNCTION(D2ShrinesTXT*,	D2Common289B,		(Unit* ptShrine));
STDCALL_FUNCTION(void,			D2Common289C,		(Unit* ptShrine, D2ShrinesTXT* ptShrineTXT));
STDCALL_FUNCTION(int,			D2Common289E,		(Unit* ptUnit, D2PathPointX* ptPathPointX, D2PathPointY* ptPathPointY));
STDCALL_FUNCTION(void,			D2Common28A1,		(D2PoolManager*	pMemPool, D2UnitFind* ptUnitFind, DRLGRoom* ptRoom, DWORD xPos, DWORD yPos, DWORD nRange, DWORD nFunc, D2UnitFindParam* ptUnitFindParam));
STDCALL_FUNCTION(void,			D2Common28A2,		(D2UnitFind* ptUnitFind));
STDCALL_FUNCTION(void,			D2Common28A3,		(D2UnitFind* ptUnitFind));
STDCALL_FUNCTION(void,			D2Common28AF,		(Unit* ptUnit, Unit* ptSourceUnit));
STDCALL_FUNCTION(void,			D2Common28B0,		(Unit* ptUnit, int nUnitType, int nUnitId));
STDCALL_FUNCTION(void,			D2Common28B6,		(Unit* ptUnit, Unit* ClientName));
STDCALL_FUNCTION(D2PlayerData*,	D2Common28B8,		(Unit* ptPlayer));
STDCALL_FUNCTION(DWORD,			D2Common28BB,		(Unit* ptPortal));
STDCALL_FUNCTION(void,			D2Common28BC,		(Unit* ptPortal, DWORD nFlag));
STDCALL_FUNCTION(DWORD,			D2Common28BF,		(Unit* ptPlayer));
STDCALL_FUNCTION(DWORD,			D2Common28C0,		(Unit* ptPlayer));
STDCALL_FUNCTION(DWORD,			D2Common28C1,		(Unit* ptPlayer, DWORD lodFlag));
STDCALL_FUNCTION(void,			D2Common28C5,		(Unit* ptUnit, int overlay, BOOL bOn));
STDCALL_FUNCTION(DWORD,			D2Common28C7,		(Unit* ptUnit));
STDCALL_FUNCTION(void,			D2Common28D7,		(Unit* ptUnit, Unit* ptItem));
STDCALL_FUNCTION(int,			D2Common28D9,		(Unit* ptUnit));
STDCALL_FUNCTION(void,			D2Common28DC,		(Unit* ptUnit));
STDCALL_FUNCTION(BOOL,			D2Common28DF,		(StatsList* pStatlist, DWORD dwStat, DWORD dwValue, DWORD dwParam));
STDCALL_FUNCTION(void,			D2Common28E0,		(StatsList* ptStats, DWORD statID, DWORD statValue, DWORD statIndex));
STDCALL_FUNCTION(void,			D2Common28E0_2,		(D2StatsList* ptStats, DWORD statID, DWORD statValue, DWORD statIndex));
STDCALL_FUNCTION(void,			D2Common28E1,		(StatsList* ptStats, WORD wStat, DWORD dwValue, DWORD skillID));
STDCALL_FUNCTION(int,			D2Common28E2,		(StatsList* ptStats, DWORD dwStat, DWORD dwLayer));
STDCALL_FUNCTION(StatsList*,	D2Common28E6,		(void* Memory, DWORD uk1, DWORD uk2, DWORD uk3, DWORD uniqueID));
STDCALL_FUNCTION(DWORD,			D2Common28E9,		(StatsList* ptStats));
STDCALL_FUNCTION(void,			D2Common28EA,		(Unit* ptUnit, StatsList* ptStatsList));
STDCALL_FUNCTION(void,			D2Common28EB,		(Unit* ptUnit, StatsList* ptStatsList, BYTE set));
STDCALL_FUNCTION(void,			D2Common28EC,		(StatsList* ptStats, DWORD EndFrame));
STDCALL_FUNCTION(void,			D2Common28ED,		(StatsList* ptStats, DWORD func));
STDCALL_FUNCTION(void,			D2Common28EE,		(StatsList* ptStatsList, DWORD StateID));
STDCALL_FUNCTION(StatsList*,	D2Common28F0,		(Unit* ptUnit, int StateID));
STDCALL_FUNCTION(StatsList*,	D2Common28F4,		(Unit* ptItem, DWORD uk1, DWORD flags));
STDCALL_FUNCTION(void,			D2Common28F5,		(StatsList* ptStats));
STDCALL_FUNCTION(DWORD,			D2Common28F6,		(Unit* ptUnit, DWORD StatID, int on));
STDCALL_FUNCTION(int,			D2Common28F7,		(Unit* ptUnit, DWORD StatID));
STDCALL_FUNCTION(void,			D2Common28F8,		(Unit* ptUnit, DWORD State, DWORD Set));
STDCALL_FUNCTION(DWORD*,		D2Common28FC,		(Unit* ptUnit));
STDCALL_FUNCTION(DWORD*,		D2Common28FE,		(Unit* ptUnit));
STDCALL_FUNCTION(DWORD,			D2Common2901,		(DWORD StateID));
STDCALL_FUNCTION(void,			D2Common2915,		(Unit* ptUnit, DWORD StatID, int Value, int TableIndex));
STDCALL_FUNCTION(void,			D2Common2916,		(Unit* ptUnit, DWORD StatID, int Value, int TableIndex));
STDCALL_FUNCTION(int,			D2Common2917,		(Unit* ptUnit, DWORD StatID, int TableIndex));
STDCALL_FUNCTION(int,			D2Common2918,		(Unit* ptUnit, DWORD StatID, int TableIndex));
STDCALL_FUNCTION(int,			D2Common2919,		(Unit* ptUnit, DWORD StatID, int TableIndex));
STDCALL_FUNCTION(void,			D2Common291D,		(Unit* ptFillerItem, Unit* ptSocketItem));
STDCALL_FUNCTION(int,			D2Common2927,		(Unit* ptItem, DWORD uk));
STDCALL_FUNCTION(DWORD,			D2Common2942,		(Unit* ptUnit));
STDCALL_FUNCTION(DWORD,			D2Common2944,		(Unit* ptUnit));
STDCALL_FUNCTION(DWORD,			D2Common2945,		(Unit* ptUnit));
STDCALL_FUNCTION(DWORD,			D2Common2946,		(Unit* ptUnit));
STDCALL_FUNCTION(DWORD,			D2Common2947,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common294F,		());
STDCALL_FUNCTION(void,			D2Common2950,		(DWORD GameType, DWORD Uk1, DWORD Uk2));
STDCALL_FUNCTION(void*,			D2Common2952,		(WORD unused, const char* ptFilename, BINField* ptFields, DWORD* ptRecordCount, WORD recordLength));
STDCALL_FUNCTION(void,			D2Common2953,		(void* ptBinFile));
STDCALL_FUNCTION(HirelingTXT*,	D2Common2957,		(DWORD	gameVersion, DWORD nPlayerLevel, DWORD nHirelingLevel));
STDCALL_FUNCTION(HirelingTXT*,	D2Common295B,		(DWORD	gameVersion, DWORD vendorID, BYTE Difficulty, BYTE Act));
STDCALL_FUNCTION(DWORD,			D2Common295E,		(WORD MissileID, DWORD SkillLevel));
STDCALL_FUNCTION(D2ItemsTXT*,	D2Common2968,		(DWORD ItemNbr));
STDCALL_FUNCTION(D2ItemsTXT*,	D2Common2969,		(DWORD ItemCode, int* ptRetNum));
STDCALL_FUNCTION(DWORD,			D2Common296A,		(DWORD ItemCode));
STDCALL_FUNCTION(D2GemTXT*,		D2Common2978,		(DWORD GemOffset));
STDCALL_FUNCTION(D2BooksTXT*,	D2Common297A,		(DWORD BookNbr));
STDCALL_FUNCTION(D2RuneStruct*,	D2Common297B,		());
STDCALL_FUNCTION(D2ShrinesTXT*,	D2Common2980,		(DWORD ShrineNbr));
STDCALL_FUNCTION(DWORD,			D2Common2981,		());
STDCALL_FUNCTION(D2ObjectsTXT*,	D2Common2982,		(int ObjectID));
STDCALL_FUNCTION(DWORD,			D2Common2984,		(DWORD PlayerClass, DWORD targetLevel));
STDCALL_FUNCTION(DWORD,			D2Common2985,		(DWORD dwPlayerClass));
STDCALL_FUNCTION(DWORD,			D2Common2986,		(DWORD dwPlayerClass, DWORD Experience));
STDCALL_FUNCTION(D2LevelsTXT*,	D2Common2987,		(DWORD nLevelID));
STDCALL_FUNCTION(void,			D2Common298E,		(int nRow, int nResMode, D2InvBelt* invBelt));
STDCALL_FUNCTION(void,			D2Common298F,		(int nBeltIndex, int nResMode, D2InvBoxStrc* pBuffer, int nSlot));
STDCALL_FUNCTION(D2DifficultyLevelsTXT*, D2Common299F, (BYTE Difficulty));
STDCALL_FUNCTION(D2TCExShortStrc*,	D2Common29A3,		(DWORD TCIndex, DWORD clvl));
STDCALL_FUNCTION(SuperUniquesTXT*, D2Common29AC,	(DWORD hcIdx));
STDCALL_FUNCTION(void,			D2Common29BF,		(D2PoolManager* MemoryPool, Unit* ptUnit));
STDCALL_FUNCTION(BYTE,			D2Common29C1,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common29C2,		(Unit* ptItem, int bodylocation));
STDCALL_FUNCTION(D2Seed*,		D2Common29C3,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common29C4,		(Unit* ptItem));
STDCALL_FUNCTION(int,			D2Common29C5,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common29C6,		(Unit* ptItem, int nSeed));
STDCALL_FUNCTION(DWORD,			D2Common29C7,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common29C8,		(Unit* ptItem, DWORD Quality));
STDCALL_FUNCTION(WORD,			D2Common29CC,		(Unit* ptItem, int AffixIndex, int AffixSlot));
STDCALL_FUNCTION(WORD,			D2Common29CD,		(Unit* ptItem, int nSlot));
STDCALL_FUNCTION(WORD,			D2Common29CE,		(Unit* ptItem, int AffixIndex, int AffixSlot));
STDCALL_FUNCTION(DWORD,			D2Common29D3,		(Unit* ptItem, DWORD dwFlag, LPCSTR file, DWORD line));
STDCALL_FUNCTION(void,			D2Common29D4,		(Unit* ptItem, DWORD dwFlag, DWORD dwAction));
STDCALL_FUNCTION(DWORD,			D2Common29D5,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common29D7,		(Unit* ptItem, int nflag, int set));
STDCALL_FUNCTION(DWORD,			D2Common29DD,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common29DE,		(Unit* ptItem, DWORD iLvl));
STDCALL_FUNCTION(BYTE,			D2Common29DF,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common29E0,		(Unit* ptItem, DWORD dwPage));
STDCALL_FUNCTION(BOOL,			D2Common29EB,		(Unit* ptItem, DWORD dwTypeID));
STDCALL_FUNCTION(DWORD,			D2Common29EC,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common29ED,		(Unit* ptItem, DWORD nUnitID));
STDCALL_FUNCTION(void,			D2Common29EE,		(Unit* ptItem, DWORD PlayerID));
STDCALL_FUNCTION(BYTE,			D2Common29F3,		(Unit* ptItem));
STDCALL_FUNCTION(BOOL,			D2Common29F4,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common29FA,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common29FE,		(Unit* ptItem, int* bodyloc, int* mem));
STDCALL_FUNCTION(DWORD,			D2Common29FF,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A00,		(DWORD ItemNbr));
STDCALL_FUNCTION(BOOL,			D2Common2A04,		(Unit* ptItem, Unit* ptPlayer, DWORD uk1, DWORD uk2, DWORD uk3, DWORD uk4));
STDCALL_FUNCTION(BOOL,			D2Common2A07,		(Unit* ptItem));
STDCALL_FUNCTION(BOOL,			D2Common2A0D,		(Unit* ptItem));
STDCALL_FUNCTION(int,			D2Common2A0F,		(Unit* ptItem));
STDCALL_FUNCTION(int,			D2Common2A10,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A13,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A17,		(Unit* ptPlayer, Unit* ptItem, BYTE Difficulty, void* ptQuest, DWORD VendorID, DWORD TransactionType));
STDCALL_FUNCTION(int,			D2Common2A18,		(Unit* ptPlayer));
STDCALL_FUNCTION(BOOL,			D2Common2A19,		(Unit* ptItem));
STDCALL_FUNCTION(int,			D2Common2A2B,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A32,		(DWORD nUnitID));
STDCALL_FUNCTION(int,			D2Common2A34,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A3A,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A3B,		(Unit* ptItem));
STDCALL_FUNCTION(BYTE,			D2Common2A3D,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A40,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A45,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A46,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A47,		(Unit* ptItem));
STDCALL_FUNCTION(BOOL,			D2Common2A4A,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A4E,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A4F,		(Unit* ptItem));
STDCALL_FUNCTION(DWORD,			D2Common2A50,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common2A66,		(Unit* ptItem, DWORD dwPage));
STDCALL_FUNCTION(LPWSTR,		D2Common2A67,		(int uk1, int uk2, Unit* ptItem, D2GemTXT* ptGem, int modType, int uk3));
STDCALL_FUNCTION(void,			D2Common2A71,		(Unit* ptItem));
STDCALL_FUNCTION(void,			D2Common2A73,		(Unit* ptSocketItem, Unit* ptFillerItem, DWORD uk1));
STDCALL_FUNCTION(DWORD,			D2Common2A74,		(Unit* ptItem, ItemMod* ptProperty, int one));
STDCALL_FUNCTION(void,			D2Common2A7C,		(Unit* ptItem, DWORD version));
STDCALL_FUNCTION(DWORD,			D2Common2A7D,		(D2Game* ptGame, Unit* ptPlayer, DWORD nVendorID, BYTE nDiffinculty, void* ptQuest, DWORD FuncAddr));
STDCALL_FUNCTION(DWORD,			D2Common2A7F,		(Unit* ptItem, D2QuantityStrc* QuantityStrc));
STDCALL_FUNCTION(DWORD,			D2Common2A80,		(Unit* ptItem));
STDCALL_FUNCTION(D2HoverText*,	D2Common2A8C,		(void* pMempool, char* pBuffer, DWORD dwFrame));
STDCALL_FUNCTION(void,			D2Common2A8D,		(void* pMempool, void* pMsgBox));
STDCALL_FUNCTION(WORD,			D2Common2A9B,		(nTextList* hTextList, DWORD uk1));
STDCALL_FUNCTION(void,			D2Common2AA0,		(D2Seed* ptSeed));
STDCALL_FUNCTION(int,			D2Common2AA8,		(int nSeed));
STDCALL_FUNCTION(int,			D2Common2ABA,		(int nSkillID));
STDCALL_FUNCTION(void,			D2Common2ABC,		(Unit* ptUnit, int nSkillID));
STDCALL_FUNCTION(BOOL,			D2Common2ABD,		(Unit* ptUnit));
STDCALL_FUNCTION(void,			D2Common2AC2,		(Unit* ptUnit));
STDCALL_FUNCTION(void,			D2Common2AD2,		(Unit* ptUnit, DWORD SkillID, DWORD dwOwnerGUID)); // old last param DWORD skill level that was wrong
STDCALL_FUNCTION(WORD,			D2Common2AD3,		(Skills* ptSkill, LPCSTR file, DWORD line));
STDCALL_FUNCTION(int,			D2Common2AD4,		(Unit* ptUnit, Skills* ptSkill));
STDCALL_FUNCTION(DWORD,			D2Common2AE0,		(Skills* ptSkill));
STDCALL_FUNCTION(DWORD,			D2Common2AE1,		(Skills* ptSkill));
STDCALL_FUNCTION(DWORD,			D2Common2AE2,		(Skills* ptSkill));
STDCALL_FUNCTION(void,			D2Common2AE4,		(Skills* ptSkill, DWORD offsetX));
STDCALL_FUNCTION(void,			D2Common2AE5,		(Skills* ptSkill, DWORD offsetY));
STDCALL_FUNCTION(void,			D2Common2AE6,		(Skills* ptSkill, DWORD nUnitID));
STDCALL_FUNCTION(void,			D2Common2AE8,		(Skills* ptSkill, DWORD skillFlag));
STDCALL_FUNCTION(int,			D2Common2AE9,		(Skills* ptSkill));
STDCALL_FUNCTION(Skills*,		D2Common2AC8,		(Unit* ptPlayer, DWORD SkillID));
STDCALL_FUNCTION(void,			D2Common2AC9,		(Unit* ptUnit, int nSkill, int nBaseLV, int nBonusLV, char* szFile, int nLine));
STDCALL_FUNCTION(DWORD,			D2Common2ACB,		(Skills* ptSkill, DWORD* pGUID, int* pSkill, int* pLevel, int* pCharges));
STDCALL_FUNCTION(DWORD,			D2Common2ACD,		(Skills* ptSkill));
STDCALL_FUNCTION(void,			D2Common2AD1,		(Unit* ptPlayer, int nSkillID, DWORD dwOwnerGUID));
STDCALL_FUNCTION(DWORD,			D2Common2AD8,		(Unit* ptPlayer, Skills* ptSkill, BOOL bIncludeBonus));
STDCALL_FUNCTION(void,			D2Common2ADE,		(Skills* ptSkill, DWORD mode));
STDCALL_FUNCTION(DWORD,			D2Common2ADF,		(Skills* ptSkill));
STDCALL_FUNCTION(int,			D2Common2AEA,		(DWORD SkillID));
STDCALL_FUNCTION(int,			D2Common2AEB,		(Unit* ptUnit, DWORD SkillID));
STDCALL_FUNCTION(int,			D2Common2AEC,		(Unit* ptUnit, DWORD SkillID));
STDCALL_FUNCTION(int,			D2Common2AED,		(Unit* ptUnit, DWORD SkillID));
STDCALL_FUNCTION(DWORD,			D2Common2AF7,		(Unit* ptUnit, WORD SkillID, DWORD sLvl, DWORD uk));
STDCALL_FUNCTION(DWORD,			D2Common2AF9,		(WORD SkillID, DWORD sLvl));
STDCALL_FUNCTION(DWORD,			D2Common2AFA,		(Unit* ptPlayer, DWORD SkillID, DWORD sLvl, int uk2));
STDCALL_FUNCTION(DWORD,			D2Common2AFB,		(Unit* ptPlayer, DWORD SkillID, DWORD sLvl, int uk2));
STDCALL_FUNCTION(DWORD,			D2Common2AFC,		(Unit* ptPlayer, DWORD SkillID, DWORD sLvl, int uk2));
STDCALL_FUNCTION(DWORD,			D2Common2AFD,		(Unit* ptPlayer, DWORD SkillID, DWORD sLvl, int uk2));
STDCALL_FUNCTION(DWORD,			D2Common2AFE,		(Unit* ptAttacker, WORD SkillID, DWORD sLvl, int uk));
STDCALL_FUNCTION(int,			D2Common2B03,		(Skills* ptSkill));
STDCALL_FUNCTION(void,			D2Common2B04,		(Skills* ptSkill, int nQuantity));
STDCALL_FUNCTION(BOOL,			D2Common2B09,		(Unit* ptCorpse, DWORD dwArg));
STDCALL_FUNCTION(BOOL,			D2Common2B0D,		(Unit* ptTarget));
STDCALL_FUNCTION(int,			D2Common2B0F,		(Unit* ptUnit, Unit* ptItem, Skills* ptSkills, int nType));
STDCALL_FUNCTION(void,			D2Common2B16,		(Unit* ptUnit, int nSkillID, int nSkillLV));
STDCALL_FUNCTION(void,			D2Common2B7B,		(Unit* ptMissile, WORD MissileID));
STDCALL_FUNCTION(void,			D2Common2B7D,		(Unit* ptMissile, int nRange));
STDCALL_FUNCTION(void,			D2Common2B84,		(Unit* ptMissile, int nStatus));
STDCALL_FUNCTION(int,			D2Common2B85,		(Unit* ptMissile));
STDCALL_FUNCTION(int,			D2Common2B1A,		(DWORD sLvl, DWORD SkillID));
STDCALL_FUNCTION(BOOL,			D2Common2B32,		(Unit* ptUnit));
STDCALL_FUNCTION(BOOL,			D2Common2B33,		(Unit* ptUnit));
STDCALL_FUNCTION(DWORD,			D2Common2B4B,		(Unit* ptHireling));
STDCALL_FUNCTION(DWORD,			D2Common2B34,		(DWORD uk1, Unit* ptUnit));
STDCALL_FUNCTION(BOOL,			D2Common2B38,		(Unit* ptUnit));
STDCALL_FUNCTION(BOOL,			D2Common2B3B,		(Unit* ptUnit));
STDCALL_FUNCTION(Unit*,			D2Common2B50,		(Unit* ptTarget, DWORD xPos, DWORD yPos, DWORD dwArg, DWORD nFunc));
STDCALL_FUNCTION(BOOL,			D2Common2B63,		(void* ptQuest, DWORD QuestID, DWORD QuestState));
STDCALL_FUNCTION(void,			D2Common2B64,		(void* ptQuest, DWORD QuestID, DWORD QuestState));
STDCALL_FUNCTION(void,			D2Common2B65,		(void* ptQuest, DWORD QuestID, DWORD QuestState));
STDCALL_FUNCTION(DWORD,			D2Common2B70,		(Unit* ptMissile));
STDCALL_FUNCTION(void,			D2Common2B71,		(Unit* ptMissile, DWORD nTotalFrames));
STDCALL_FUNCTION(int,			D2Common2B72,		(Unit* ptMissile));
STDCALL_FUNCTION(void,			D2Common2B73,		(Unit* ptMissile, DWORD nCurrentFrame));
STDCALL_FUNCTION(short,			D2Common2B74,		(Unit* ptMissile));
STDCALL_FUNCTION(short,			D2Common2B75,		(Unit* ptMissile));
STDCALL_FUNCTION(WORD,			D2Common2B77,		(Unit* ptMissile));
STDCALL_FUNCTION(void,			D2Common2B80,		(Unit* ptMissile, DWORD nActivateFrame));
STDCALL_FUNCTION(DWORD,			D2Common2B87,		(Unit* ptMissile));
STDCALL_FUNCTION(void,			D2Common2B8B,		(void* ptHistory, DWORD WaypointID));
STDCALL_FUNCTION(DWORD,			D2Common2B90,		(DWORD TownID, int *mem));
STDCALL_FUNCTION(void,			D2Common2BD1,		(void* ptDamageData, Unit* ptAttacker, BOOL bMissile, Unit* ptMissile, DWORD MissileLevel));
STDCALL_FUNCTION(void,			D2Common2BD2,		(Unit* ptAttacker, Unit* ptMissile, void* ptDamageData, DWORD MissileLevel));
STDCALL_FUNCTION(DWORD,			D2Common2BEF,		(DWORD LevelID, BYTE Act, DWORD nFlag));
STDCALL_FUNCTION(Unit*,			D2Common2BFB,		(void* pPool, int nType));
STDCALL_FUNCTION(void,			D2Common2BFD,		(DWORD nUnitID, BOOL nGameFlag, BYTE nDifficulty, DWORD nUnitLevel, DWORD set, D2SourceDmgStrc *ptSourceDmgStrc));
STDCALL_FUNCTION(int,			D2Common2C06,		(Unit* pUnit, int nStatID, D2StatsCompbound* ptStatsCompbound, int nArrayMembers));
STDCALL_FUNCTION(DWORD,			D2Common2C0C,		(Unit* ptPlayer, DWORD Calc, DWORD SkillID, DWORD SkillLevel));	
STDCALL_FUNCTION(void,			D2Common2C1C,		(int nZero, int nZero2, Unit* ptUnit, AffixTXT* pAffix, int nZero3, int nZero4, ItemMod* ModData, int nZero5, DWORD fStatlist, int nZero6));	
STDCALL_FUNCTION(BOOL,			D2Common2C1D,		(Unit* pItem, int nSkill, int nLevel, int* pCurrentCharges, StatsList** pListBuffer));	
STDCALL_FUNCTION(BOOL,			D2Common2C1F,		(StatsList* pStatlist, short nStat, int nValue, WORD nParam, Unit* pItem));	
STDCALL_FUNCTION(int,			D2Common2C24,		(Unit* ptPlayer, Unit* ptItem, DWORD Calc));
STDCALL_FUNCTION(BOOL,			D2Common2C27,		(Unit* ptPlayer, DWORD nFlag));



//D2Client
STDCALL_FUNCTION(void,			D2Client6FAABF30,	());
STDCALL_FUNCTION(void,			D2Client6FAF65C0,	());
STDCALL_FUNCTION(Unit*,			D2Client6FB283D0,	());
STDCALL_FUNCTION(DWORD,			D2Client6FB283E0,	());
STDCALL_FUNCTION(DWORD,			D2Client6FB57BC0,   ());
STDCALL_FUNCTION(DWORD,			D2Client6FB57BD0,	());
STDCALL_FUNCTION(Unit*,			D2Client6FB575E0,	());
STDCALL_FUNCTION(void,			D2Client6FB57500,	(sWinMessage *ptWinMsg));
STDCALL_FUNCTION(DWORD,			D2Client6FAB5750,	());
STDCALL_FUNCTION(void,			D2Client6FB57480,	(sWinMessage *ptWinMsg));
STDCALL_FUNCTION(void,			D2Client6FAF7740,	());
STDCALL_FUNCTION(void,			D2Client6FAF75A0,	());
STDCALL_FUNCTION(BOOL,			D2Client6FAFC0B0,	());
STDCALL_FUNCTION(BOOL,			D2Client6FAFC130,	());

STDCALL_FUNCTION(void,			D2Client6FAEED80,	(sWinMessage *ptWinMsg));
STDCALL_FUNCTION(void,			D2Client6FAEEEA0,	(sWinMessage *ptWinMsg));
STDCALL_FUNCTION(void,			D2Client6FAEEFD0,	(sWinMessage *ptWinMsg));
STDCALL_FUNCTION(void,			D2Client6FAEF130,	(sWinMessage *ptWinMsg));

// D2Game
STDCALL_FUNCTION(Unit*,			D2Game6FCBC900,     (D2Game* ptGame, Unit* ptPlayer));
STDCALL_FUNCTION(void,			D2Game6FD1CB30,		(D2PlayerData* ptPlayerData, char* nPlayerName));
STDCALL_FUNCTION(char*,			D2Game6FD1D660,		(D2PlayerData* ptPlayerData, DWORD nFlag));
STDCALL_FUNCTION(BOOL,			D2Game6FCBD820,		(Unit* pPlayer, int* pInteractType, DWORD* pInteractID));
STDCALL_FUNCTION(void,			D2Game6FD1CACF,		(char* pBuffer, char* szFormat,...));

//Fog
STDCALL_FUNCTION(void,			D2Fog2729,			(BOOL bMinimized, LPCSTR lpszErrFile, DWORD ErrLine));
STDCALL_FUNCTION(void,			D2Fog272D,			(LPCSTR ptszMsg, ...));
STDCALL_FUNCTION(void,			D2Fog278E,			(D2BitBuffer* pBuffer, BYTE* pBitData, int nSize));
STDCALL_FUNCTION(int,			D2Fog278F,			(D2BitBuffer* pBuffer));
STDCALL_FUNCTION(void,			D2Fog2790,			(D2BitBuffer* pBuffer, int nValue, int nBits));
STDCALL_FUNCTION(int,			D2Fog2791,			(D2BitBuffer* pBuffer, int nBitsToRead));
STDCALL_FUNCTION(int,			D2Fog2792,			(D2BitBuffer* pBuffer, int nBitsToRead));
STDCALL_FUNCTION(void*,			D2Fog27E3,			(LPCSTR ptszFile, DWORD dwLine));
STDCALL_FUNCTION(DWORD,			D2Fog27E9,			(void* ptBin, LPCSTR ptText, DWORD column));
STDCALL_FUNCTION(DWORD,			D2Fog27F3,			());
STDCALL_FUNCTION(size_t,		D2Fog280C,			(DWORD dwScan));




//D2Lang
STDCALL_FUNCTION(WORD,			D2Lang10013,		(LPSTR ptStringRef, LPWSTR* ptReturnStr));
STDCALL_FUNCTION(void,			D2Lang10006,		(LPSTR ptLang, DWORD Zero));
STDCALL_FUNCTION(DWORD,			D2Lang10007,		());

//D2Gfx
STDCALL_FUNCTION(int,			D2Gfx2713,			());
STDCALL_FUNCTION(int,			D2Gfx2715,			());
STDCALL_FUNCTION(void,			D2Gfx2749,			(int nXStart, int nYStart, int nXEnd, int nYEnd, BYTE nColor, BYTE nAlpha));

STDCALL_FUNCTION(HWND,			D2Gfx10027,			());
// STDCALL_FUNCTION(void,          D2Gfx10072,         (void* ImageStructure, DWORD Xpos, DWORD Ypos, DWORD unknown, DWORD DrawWindowType, DWORD unknown2));  old args
STDCALL_FUNCTION(void,          D2Gfx2758,         (D2GFXDataStrc* pData, int nXpos, int nYpos, DWORD dwGamma, int nDrawMode, BYTE* pPalette)); 
STDCALL_FUNCTION(void,          D2Gfx275A,         (D2GFXDataStrc* pData, int nXpos, int nYpos, int nSkipLines, int nDrawLines, int nDrawMode)); 

//D2Net
STDCALL_FUNCTION(DWORD,			D2Net2715,			(DWORD IndexNbr, void* ptData, DWORD dwSize));
STDCALL_FUNCTION(BOOL,			D2Net2716,			(BOOL bZero, int nClientID, void* ptPacket, size_t nSize));


//D2Storm
STDCALL_FUNCTION(D2Game*,		D2Storm401,			(DWORD size, char* szDataLabel, DWORD uk1, DWORD uk2)); 
STDCALL_FUNCTION(void,			D2Storm511,			(sWinMessage* msg));


//D2Win
STDCALL_FUNCTION(DWORD,			D2Win10034,			(DWORD red, DWORD green, DWORD blue));
STDCALL_FUNCTION(void,			D2Win10152,			(DWORD* ptGrid, DWORD nSetGridXMax));
STDCALL_FUNCTION(void,			D2Win10153,			(DWORD* ptGrid, DWORD nSetGridYMax));
STDCALL_FUNCTION(void,			D2Win10156,			(DWORD* ptGrid, DWORD nBitmask));
STDCALL_FUNCTION(DWORD*,		D2Win10161,			(DWORD nClassID, DWORD nGameflag, DWORD nGameflag1, DWORD nGameflag2));


//D2Cmp
STDCALL_FUNCTION(D2GFXCellStrc*,	D2Cmp2734,			(D2GFXDataStrc* ptGFXDataStrc));
STDCALL_FUNCTION(DWORD,				D2Cmp2735,			(D2GFXCellStrc* ptGfxCellStrc));


/*=================================================================*/
/* D2 Functions prototypes FASTCALL                                */
/*=================================================================*/

//D2Common
FASTCALL_FUNCTION(D2LevelDefsTXT*,	D2Common271A,	(DWORD LevelID));
FASTCALL_FUNCTION(void,			D2Common2AA1,		(D2Seed* pSeed, int nLoSeed));
FASTCALL_FUNCTION(Skills*,		D2Common2AC5,		(Unit* ptPlayer, int SkillID, DWORD dwGUID));
FASTCALL_FUNCTION(Skills*,		D2Common2AC6,		(Unit* ptPlayer, int SkillID));
FASTCALL_FUNCTION(void,			D2Common2AC7,		(Unit* ptPlayer, int SkillID, LPCSTR lpszErrFile, DWORD ErrLine));
FASTCALL_FUNCTION(D2SkillsTXT*,	D2Common2AD6,		(Skills* ptSkills));
FASTCALL_FUNCTION(BOOL,			D2Common2B4A,		(DWORD gameVersion, Unit* ptPlayer, DWORD nSeed, DWORD dwGUID, DWORD nDifficulty, int* mem));
FASTCALL_FUNCTION(DWORD,		D2Common2B4E,		(DWORD gameVersion, DWORD act, D2Hireling* ptD2Hireling));
FASTCALL_FUNCTION(DWORD,		D2Common6FD52410,	(BYTE* ptBinLine, LPCSTR ptLine, DWORD Offset, DWORD unknown));
FASTCALL_FUNCTION(DWORD,		D2Common6FD52910,	(BYTE* ptBinLine, LPCSTR ptLine, DWORD Offset, DWORD unknown));
FASTCALL_FUNCTION(DWORD,		D2Common6FDB1130,	(Unit* righthandItem, Unit* lefthandItem, D2SkillsTXT* ptSkillsTXT, DWORD WeaponType));
FASTCALL_FUNCTION(DWORD,		D2Common6FDB1580,	(Unit* ptPlayer, Skills* ptSkill));


//D2Game

FASTCALL_FUNCTION(Unit*,		D2Game6FC33BE0,		(Client* ptClient));
FASTCALL_FUNCTION(BOOL,			D2Game6FC34020,		(int nClient, BYTE* pBuffer, size_t nSize, size_t nBufferSize, DWORD dwSave, DWORD dwUnused, DWORD dwUnusedEx));
FASTCALL_FUNCTION(BYTE*,		D2Game6FC34170,		(Client* ptClient));
FASTCALL_FUNCTION(void,			D2Game6FC34280,		(Client* ptClient));
FASTCALL_FUNCTION(void,			D2Game6FC34300,		(Client* ptClient));
FASTCALL_FUNCTION(Unit*,		D2Game6FC345A0,		(Client* ptClient));
FASTCALL_FUNCTION(void,			D2Game6FC34690,		(Client* ptClient, char* szBuffer));
FASTCALL_FUNCTION(void,			D2Game6FC346A0,		(Client* ptClient, Unit** ppContainer));
FASTCALL_FUNCTION(void,			D2Game6FC346B0,		(Client* ptClient, void* dwRealm));
FASTCALL_FUNCTION(void,			D2Game6FC34840,		(D2Game* ptGame, Unit* ptUnit, DWORD nEvent, DWORD set));
FASTCALL_FUNCTION(void,			D2Game6FC351B0,		(D2Game* ptGame, Unit* ptUnit, int nEvent, int dwFrame, DWORD dwArg, DWORD dwArgEx));
FASTCALL_FUNCTION(void,			D2Game6FC3C710,		(Client* ptClient, void* pData, DWORD size));
FASTCALL_FUNCTION(void,			D2Game6FC3C920,		(Client* ptClient, DWORD uk1, DWORD unitID, DWORD uniqueID, DWORD uk2, DWORD pointX, DWORD pointY, DWORD uk3, DWORD ExitWalkY));
FASTCALL_FUNCTION(void,			D2Game6FC3DB50,		(Client* ptClient, Unit* ptPlayer, DWORD skillID, DWORD sLvl, DWORD uk1));
FASTCALL_FUNCTION(void,			D2Game6FC3DBE0,		(Client* ptClient, DWORD nUnitType, DWORD nUniqueID, int nSkillID, DWORD nQuantity));
FASTCALL_FUNCTION(void,			D2Game6FC3E200,		(Client* ptClient, Unit* ptSummon));
FASTCALL_FUNCTION(void,			D2Game6FC3E9D0,		(Client* ptClient, Unit* ptPlayer, Unit* ptItem, DWORD dwCmd));
FASTCALL_FUNCTION(void,			D2Game6FC3EC20,		(Client* ptClient, Unit* ptItem, DWORD unknown1_1, DWORD StatID, DWORD Value, DWORD unknown2_0));
FASTCALL_FUNCTION(void,			D2Game6FC3EDC0,		(Client* ptClient, Unit* ptItem, DWORD dwSpellIcon, DWORD unknown1_0, DWORD unknown2_1, DWORD dwSkillId));
FASTCALL_FUNCTION(void,			D2Game6FC3F520,		(Client* ptClient, D2ItemDropStrc* ptUpdateItemParam));
FASTCALL_FUNCTION(void,			D2Game6FC3F9B0,		(D2Game* ptGame, DWORD ptPlayerUniqueID, DWORD ptDeadBodyUniqueID, int Set));
FASTCALL_FUNCTION(void,			D2Game6FC3FB30,		(Client* ptClient, DWORD ErrLine, DWORD ResurrectCost));
FASTCALL_FUNCTION(void,			D2Game6FC3FC20,		(Client* pClient, Unit* pUnit, int nState, DWORD dwLength));
FASTCALL_FUNCTION(void,			D2Game6FC40170,		(D2Game* ptGame, Unit* ptUnit, DWORD null));
FASTCALL_FUNCTION(void,			D2Game6FC401F0,		(D2Game* ptGame, Unit* ptSummon, DWORD uk1, DWORD nTargetID));
FASTCALL_FUNCTION(void,			D2Game6FC42310,		(D2Game* ptGame, Unit* ptFillerItem, Unit* ptUnit, BOOL nWeaponSwapFlag));
FASTCALL_FUNCTION(void,			D2Game6FC43340,		(D2Game* ptGame, Unit* ptAttacker, DWORD nUniqueID, int* mem));
FASTCALL_FUNCTION(BOOL,			D2Game6FC44410,		(char* lpszErrFile, DWORD ErrLine, D2Game* ptGame, Unit* ptPlayer, DWORD ItemUniqueID, DWORD XOffset, DWORD YOffset, DWORD Unknown1_1, DWORD Unknown2_1, DWORD Unknown3_0));
FASTCALL_FUNCTION(void,			D2Game6FC44A90,		(D2Game* ptGame, Unit* ptPlayer, DWORD nZero));
FASTCALL_FUNCTION(DWORD,		D2Game6FC45060,		(D2Game* ptGame, Unit* ptPlayer, DWORD uniqueID, BYTE bodyloc, BYTE set, int* mem));
FASTCALL_FUNCTION(void,			D2Game6FC45300,		(Unit* ptPlayer));
FASTCALL_FUNCTION(void,			D2Game6FC471F0,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptItem, DWORD zero));
FASTCALL_FUNCTION(void,			D2Game6FC47C90,		(Unit* ptPlayer, DWORD SkillID));
FASTCALL_FUNCTION(BOOL,			D2Game6FC47D30,		(D2Game* ptGame, Unit* ptPlayer, DWORD ItemUniqueID, DWORD XOffset, DWORD YOffset, void* ptUnknown));
FASTCALL_FUNCTION(BOOL,			D2Game6FC48940,		(D2Game* ptGame, Unit* ptPlayer, DWORD ItemUniqueID, DWORD XOffset, BYTE set, int* mem)); 
FASTCALL_FUNCTION(void,			D2Game6FC49670,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptItem));
FASTCALL_FUNCTION(BOOL,			D2Game6FC497E0,		(D2Game* ptGame, Unit* ptUnit, DWORD SourceUniqueID, DWORD TargetUniqueID, int* mem, DWORD uk1, DWORD uk2, DWORD uk3));
FASTCALL_FUNCTION(DWORD,		D2Game6FC4A4B0,		(D2Game* ptGame, Unit* ptPlayer));
FASTCALL_FUNCTION(BOOL,			D2Game6FC4AD80,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptCorpse));
FASTCALL_FUNCTION(void,			D2Game6FC4B240,		(D2Game* ptGame, Unit* ptUnit));
FASTCALL_FUNCTION(BOOL,			D2Game6FC4B9D0,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptItem));
FASTCALL_FUNCTION(void,			D2Game6FC4BB90,		(D2Game* ptGame, Unit* ptUnit, BOOL nFlag));
FASTCALL_FUNCTION(void,			D2Game6FC4BD50,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptItem));
FASTCALL_FUNCTION(void,			D2Game6FC4BE80,		(Unit* ptUnit, D2Game* ptGame, DWORD dwItemGUID, DRLGRoom* ptRoom));
FASTCALL_FUNCTION(DRLGRoom*,	D2Game6FC4BF00,		(DRLGRoom* ptRoom, D2CoordStrc* ptCoordStrc, D2CoordStrc* ptReturnCoords, DWORD dwArg)); // int* mem
FASTCALL_FUNCTION(DWORD,		D2Game6FC4E520,		(D2Game* ptGame, ItemOperate* itemOP, D2ItemsTXT* ptItemsTXT, DWORD set));
FASTCALL_FUNCTION(void,			D2Game6FC4FEC0,		(D2Game* ptGame, Unit* ptUnit, DWORD dwQuality, void* ptOffsets, DWORD un1_0, DWORD un2_neg, DWORD un3_0));
FASTCALL_FUNCTION(Unit*,		D2Game6FC4ED80,		(D2Game* ptGame, D2ItemDropStrc *ptItemDrop, BOOL bSetSeed)); 
FASTCALL_FUNCTION(Unit*,		D2Game6FC501A0,		(Unit* ptPlayer, DWORD itemCode, D2Game* ptGame, DWORD nMode, DWORD quality, BOOL bForbidEthereal, BOOL bForbidSockets, DWORD Lvl, BOOL bSetSeed, DWORD dwSeed, DWORD dwItemSeed));	
FASTCALL_FUNCTION(void,			D2Game6FC50320,		(D2Game* ptGame, Unit* ptItem));
FASTCALL_FUNCTION(Unit*,		D2Game6FC503A0,		(Unit* ptPlayer, Unit* ptItem, BOOL nFlag));
FASTCALL_FUNCTION(DWORD,		D2Game6FC504F0,		(Unit* ptUnit, DWORD UnitType));
FASTCALL_FUNCTION(Unit*,		D2Game6FC51070,		(D2Game* ptGame, Unit* ptItem, Unit* ptPlayer, DWORD unknown2_1));
FASTCALL_FUNCTION(void,			D2Game6FC512C0,		(D2Game* ptGame, Unit* ptItem));
FASTCALL_FUNCTION(void,			D2Game6FC51310,		(D2Game* ptGame, Unit* ptItem));
FASTCALL_FUNCTION(void,			D2Game6FC51360,		(D2Game* ptGame, Unit* ptChar, Unit* ptNPC, D2TCExShortStrc* ptClass, int nQuality, int ilvl, int nNoDropOverride, DWORD z2, DWORD z3, DWORD z4));
//FASTCALL_FUNCTION(void,			D2Game6FC52260,		(D2Game* ptGame, DWORD uk1, Unit* ptItem, DrlgRoom1* ptRoom, DWORD pointX, DWORD pointY));
FASTCALL_FUNCTION(void,			D2Game6FC52260,		(D2Game* ptGame, DWORD uk1, Unit* ptItem, DRLGRoom* ptRoom, DWORD pointX, DWORD pointY));
FASTCALL_FUNCTION(void,			D2Game6FC52410,		(Unit* ptItem, D2ItemDropStrc* ptItemDrop));
FASTCALL_FUNCTION(WORD,			D2Game6FC52920,		(Unit* ptItem, DWORD dwArg1, DWORD dwArg2, DWORD dwArg3, DWORD dwArg4, int nSlot));
FASTCALL_FUNCTION(DWORD,		D2Game6FC54240,		(Unit* ptItem, DWORD dwArg));
FASTCALL_FUNCTION(void,			D2Game6FC552F0,		(D2Game* ptGame, Unit* ptUnit, DWORD dwMissileGUID));
FASTCALL_FUNCTION(Unit*,		D2Game6FC55360,		(D2Game* ptGame, D2MissileInit* ptMissileCombat));
FASTCALL_FUNCTION(void,			D2Game6FC55F80,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptMissile, Unit* ptDefender, D2DamageData* ptDamageData));
FASTCALL_FUNCTION(void,			D2Game6FC568F0,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptTargetUnit, DWORD srvmissilea, WORD SkillID, DWORD TargetUnitID, DWORD uk1, DWORD uk2, DWORD uk3));
FASTCALL_FUNCTION(void,			D2Game6FC61270,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptUnit, DWORD ErrLine, int* mem, BOOL nFlag));
FASTCALL_FUNCTION(void,			D2Game6FC61490,		(D2Game* ptGame, Unit* ptPlayer, DWORD UnitType));
FASTCALL_FUNCTION(void,			D2Game6FC61610,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptPet, DWORD nLevel));
FASTCALL_FUNCTION(void,			D2Game6FC61AB0,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptNpc, NpcInteract* ptInteract));
FASTCALL_FUNCTION(void,			D2Game6FC62E70,		(Unit* ptAttacker, Unit* ptDefender, D2DamageData* ptDamageData));
FASTCALL_FUNCTION(void,			D2Game6FC56480,		(D2Game* ptGame, Unit* ptMissile, Unit* ptDefender, D2DamageData* ptDamageData));
FASTCALL_FUNCTION(void,			D2Game6FC56D50,		(D2Game* ptGame, Unit* ptMissile));
FASTCALL_FUNCTION(void,			D2Game6FC5FAD0,		(D2Game* ptGame, Unit* ptMissile, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(DWORD,		D2Game6FC60220,		(Unit* ptUnit));
FASTCALL_FUNCTION(void,			D2Game6FC60270,		(Unit* ptUnit, DWORD nFlag, DWORD nSet));
FASTCALL_FUNCTION(void,			D2Game6FC602A0,		(D2Game* ptGame, DRLGRoom* ptRoom, Unit* ptUnit, DWORD dwGUID));
//FASTCALL_FUNCTION(Unit*,		D2Game6FC66560,		(D2Game* ptGame, DrlgRoom1* ptRoom, DWORD unitID, DWORD pointX, DWORD pointY, DWORD uk1));
FASTCALL_FUNCTION(Unit*,		D2Game6FC66560,		(D2Game* ptGame, DRLGRoom* ptRoom, DWORD unitID, DWORD pointX, DWORD pointY, DWORD uk1));
FASTCALL_FUNCTION(D2MonRegStrc*, D2Game6FC67F90,	(D2MonRegStrc* ptMonRegList, DWORD nMonRegIdx));
//FASTCALL_FUNCTION(Unit*,		D2Game6FC6A030,		(D2Game* ptGame, DrlgRoom1* ptRoom, DWORD* list, DWORD pointX, DWORD pointY, DWORD unitID, DWORD uk1, DWORD uk2, DWORD uk3));
FASTCALL_FUNCTION(Unit*,		D2Game6FC6A030,		(D2Game* ptGame, DRLGRoom* ptRoom, DWORD* list, DWORD pointX, DWORD pointY, DWORD unitID, DWORD uk1, DWORD uk2, DWORD uk3));
FASTCALL_FUNCTION(void,			D2Game6FC6AD50,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(DWORD,		D2Game6FC62F50,		(Unit* ptUnit));
FASTCALL_FUNCTION(DWORD,		D2Game6FC6AD10,		(D2Game* ptGame, Unit* ptMonster));
FASTCALL_FUNCTION(void,			D2Game6FC6AD90,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(int,			D2Game6FC6AF70,		(int orgValue, int BonusValue, int Percent));
FASTCALL_FUNCTION(void,			D2Game6FC6BA70,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6BB80,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6C4F0,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6B610,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6DCE0,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6E240,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6E390,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6E410,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6DD20,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6DFA0,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6E700,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6BC10,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6C710,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6C740,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6CDB0,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6CE50,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6B030,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6BDD0,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6DCB0,		(D2Game* ptGame, Unit* ptUnit,DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6CD30,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6BF90,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6CD60,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6D030,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6D060,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6D800,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6C340,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6B8C0,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6B210,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6B3A0,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6B3E0,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6B4B0,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6E070,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6CF10,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6D8B0,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6D1C0,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6CB40,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6DFC0,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6C9E0,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6DDE0,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6CAB0,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6D690,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6CD30,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6CF90,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6D440,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6DA40,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6B5D0,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC6E730,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
//FASTCALL_FUNCTION(Unit*,		D2Game6FC6F690,		(D2Game* ptGame, DrlgRoom1* ptRoom, DWORD pointX, DWORD pointY, DWORD hcIdx));
FASTCALL_FUNCTION(Unit*,		D2Game6FC6F690,		(D2Game* ptGame, DRLGRoom* ptRoom, DWORD pointX, DWORD pointY, DWORD hcIdx));
//FASTCALL_FUNCTION(Unit*,		D2Game6FC6F690_2,	(D2Game* ptGame, DRLGRoom* ptRoom, DWORD pointX, DWORD pointY, DWORD hcIdx));
FASTCALL_FUNCTION(void,			D2Game6FC6FF10,		(D2Game* ptGame, Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC75B40,		(D2ObjectOperateStrc* ptParams, DWORD monID, DWORD arg));
FASTCALL_FUNCTION(void,			D2Game6FC70600,		(D2Game* ptGame, Unit* ptObject, int nObjectGUID, DRLGRoom* ptRoom, int nXpos, int nYpos));
FASTCALL_FUNCTION(void,			D2Game6FC6AFF0,		(Unit* ptUnit, DWORD monUmod, BOOL bUnique));
FASTCALL_FUNCTION(void,			D2Game6FC76030,		(Operate* ptOperate, DWORD OperateNbr));
FASTCALL_FUNCTION(void,			D2Game6FC764B0,		(D2ObjectOperateStrc* pParams, short nTrap));
FASTCALL_FUNCTION(void,			D2Game6FC766B0,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC766F0,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC76730,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC76790,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC767F0,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC76850,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC76880,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC76910,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(Unit*,		D2Game6FC76A60,		(D2Game* ptGame, Unit* ptPlayer, DWORD nItemID, Unit* ptGem));
FASTCALL_FUNCTION(void,			D2Game6FC76BC0,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC76ED0,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC770D0,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC773B0,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC77690,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(DWORD,		D2Game6FC77750,		(D2ShrinesTXT* ptShrineTXT, WORD statID, DWORD Value, Unit* ptPlayer));
FASTCALL_FUNCTION(void,			D2Game6FC779C0,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC77AE0,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC77BA0,		(D2ObjectOperateStrc* ptShrineOp, D2ShrinesTXT* ptShrineTXT));
FASTCALL_FUNCTION(void,			D2Game6FC789C0,		(D2Game* ptGame, Unit* ptAttacker, DWORD nUnitType, DWORD nUniqueID, int* mem));
FASTCALL_FUNCTION(void,			D2Game6FC7A140,		(D2Game* ptGame, Unit* ptUnit, DWORD itemCode, DWORD dwArg));
FASTCALL_FUNCTION(Unit*,		D2Game6FC7A220,		(D2ObjectOperateStrc* ptParams, int nQuality));
FASTCALL_FUNCTION(DWORD,		D2Game6FC7AA20,		(D2ObjectOperateStrc* ptParams));
FASTCALL_FUNCTION(void,			D2Game6FC7CA10,		(Unit* ptAttacker, DWORD nFlag));
FASTCALL_FUNCTION(void,			D2Game6FC7B550,		(D2Game* pGame, Unit* pPlayer, int dwGUID));
FASTCALL_FUNCTION(BOOL,			D2Game6FC7BD50,		(Unit* ptAttacker));
FASTCALL_FUNCTION(void,			D2Game6FC7DBF0,		(D2Game* pGame, Unit* pPlayer, Unit* ptUnit, DWORD petType, D2HirelingWrapper *ptHirelingWrapper, DWORD nLevel));
FASTCALL_FUNCTION(Unit*,		D2Game6FC7C7B0,		(Unit* ptAttacker));
FASTCALL_FUNCTION(void,			D2Game6FC7C900,		(Unit* ptAttacker, BOOL nFlag));
FASTCALL_FUNCTION(void,			D2Game6FC7D260,		(D2Game* pGame, Unit* pPlayer, DWORD dwGUID, DWORD dwUK));
FASTCALL_FUNCTION(void,			D2Game6FC7D7A0,		(D2Game* pGame, Unit* pPlayer, Unit* pPet, int nPetType, int nPetCount));
FASTCALL_FUNCTION(void,			D2Game6FC7DF40,		(D2Game* ptGame, Unit* pPlayer));
FASTCALL_FUNCTION(DWORD,		D2Game6FC7E7C0,		(D2Game* ptGame, Unit* ptPlayer, DWORD HirelingMemAdress, DWORD unused));
FASTCALL_FUNCTION(Unit*,		D2Game6FC7E8B0,		(D2Game* ptGame, Unit* ptPlayer, int nPetType, BOOL bNoScan));
FASTCALL_FUNCTION(void,			D2Game6FC817D0,		(D2Game* ptGame, Unit* ptPlayer, DWORD uk1, DWORD uk2, DWORD pointX, DWORD pointY, DWORD uk3));
FASTCALL_FUNCTION(void,			D2Game6FC81A00,		(D2Game* ptGame, Unit* ptPlayer, Skills* ptSkills, DWORD nMode, DWORD nUnitType, DWORD nUniqueID, DWORD nFlag));
FASTCALL_FUNCTION(void,			D2Game6FC822D0,		(Unit* ptPlayer, DWORD statID, DWORD value, Unit* ptUnit));
FASTCALL_FUNCTION(D2QuestDataStrc*,	D2Game6FC93B90,		(D2Game* ptGame, DWORD QuestID));
FASTCALL_FUNCTION(void,			D2Game6FC93DC0,		(D2Game* ptGame));
FASTCALL_FUNCTION(void,			D2Game6FC867C0,		(D2Game* ptGame, Unit* ptPlayer, DWORD townID, DWORD unitType));
FASTCALL_FUNCTION(DWORD,		D2Game6FC898F0,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptItem, Unit* ptSocketItem));
FASTCALL_FUNCTION(BOOL,			D2Game6FC8A500,		(D2Game* ptGame, Unit* ptPlayer, const char* szName, DWORD dwArg));
FASTCALL_FUNCTION(DWORD,		D2Game6FC8C9D0,		(D2Game* pGame, Client* pClient, const char* szName, Unit** ppPlayer, DWORD dw1, DWORD dw2, DWORD dw3));
FASTCALL_FUNCTION(void,			D2Game6FC8D880,		(D2Game* ptGame, Unit* ptPlayer, BYTE** ppSection, BYTE* pSaveFileEnd, DWORD dwVersion));
FASTCALL_FUNCTION(DWORD,		D2Game6FC8D940,		(D2Game* ptGame, Unit* ptPlayer, BYTE* pBuffer, size_t* pSize, size_t nBufferSize, BOOL bInteracting, DWORD dwArg));
FASTCALL_FUNCTION(WORD,			D2Game6FC8FE40,		(D2Game* ptGame, DWORD Lvl, WORD ItemType));
FASTCALL_FUNCTION(void,			D2Game6FC92890,		(Unit* ptPlayer, DWORD statID, DWORD nValue));
FASTCALL_FUNCTION(BOOL,			D2Game6FC937A0,		(D2Game* ptGame, Unit* ptPlayer));
FASTCALL_FUNCTION(void,			D2Game6FC94520,		(D2Game* ptGame, Unit* ptCorpse, Unit* ptNewItem));
FASTCALL_FUNCTION(BOOL,			D2Game6FC95490,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptNPC));
FASTCALL_FUNCTION(void,			D2Game6FC95DF0,		(void* ptGame, Unit* ptChar, DWORD itemCode, DWORD ilvl, DWORD quality, DWORD one));
FASTCALL_FUNCTION(void,			D2Game6FC960C0,		(D2Game* ptGame, Unit* ptPlayer, Unit *ptVendor, DWORD townID, DWORD unitType));
FASTCALL_FUNCTION(void,			D2Game6FC942D0,		(D2QuestArgStrc* pArgs, bool bCheckActive, bool bCheckAct));
FASTCALL_FUNCTION(void,			D2Game6FC94CF0,		(D2QuestDataStrc* ptQuestDataStrc, BYTE QuestState, LPCSTR lpszErrFile, DWORD ErrLine));
FASTCALL_FUNCTION(BOOL,			D2Game6FC97400,		(D2Game* ptGame, Unit* ptPlayer, DWORD sourceLevelID, DWORD destLevelID));
FASTCALL_FUNCTION(void,			D2Game6FC98F50,		(D2Game* ptGame, Unit* ptPlayer));
FASTCALL_FUNCTION(void,			D2Game6FC96E20,		(Unit* ptPlayer, Unit* ptPtObject, DWORD stringkey));
FASTCALL_FUNCTION(BOOL,			D2Game6FCBA8E0,		(Unit* ptOwner, Unit* ptPlayer, DWORD nFlag));
FASTCALL_FUNCTION(void,			D2Game6FCFB070,		(D2Game* ptGame, Unit* ptPlayer, Unit* TargetUnit, DWORD Skill_ID, DWORD Skill_Level));
FASTCALL_FUNCTION(void,			D2Game6FCBAEE0,		(D2Game* ptGame, Unit* ptUnit));
FASTCALL_FUNCTION(void,			D2Game6FCBB440,		(D2Game* ptGame, Unit* ptTarget, Unit* ptRoomTile));
FASTCALL_FUNCTION(void,			D2Game6FCBB630,		(Unit* ptPlayer, DRLGAct* ptAct));
//FASTCALL_FUNCTION(Unit*,		D2Game6FCBB6C0,		(int UnitType, DWORD UnitID, int PointX2, int PointY2, D2Game* ptGame, DrlgRoom1* ptRoom, int Unknown2, int Mode, int Unknown3));
FASTCALL_FUNCTION(Unit*,		D2Game6FCBB6C0,		(int UnitType, DWORD UnitID, int PointX2, int PointY2, D2Game* ptGame, DRLGRoom* ptRoom, int Unknown2, int Mode, int Unknown3));
//FASTCALL_FUNCTION(Unit*,		D2Game6FCBB6C0_2,	(int UnitType, DWORD UnitID, int PointX2, int PointY2, D2Game* ptGame, DRLGRoom* ptRoom, int Unknown2, int Mode, int Unknown3));
FASTCALL_FUNCTION(Unit*,		D2Game6FCBBB00,		(D2Game* ptGame, DWORD UnitType, DWORD UniqueID));
FASTCALL_FUNCTION(Unit*,		D2Game6FCBBB70,		(D2Game* ptGame, Unit* ptMissile));
FASTCALL_FUNCTION(void,			D2Game6FCBC280,		(Unit* ptDeadBody, Client* ptClient));
FASTCALL_FUNCTION(Client*,		D2Game6FCBC2E0,		(Unit* ptPlayer, char* lpszErrFile, DWORD ErrLine));
FASTCALL_FUNCTION(void,			D2Game6FCBC480,		(Unit* ptPlayer_1, DWORD SoundNbr, Unit* ptPlayer_2));
FASTCALL_FUNCTION(Unit*,		D2Game6FCBC7E0,		(D2Game* ptGame, Unit* ptPlayer));
FASTCALL_FUNCTION(void,			D2Game6FCBBCB0,		(Unit* ptUnit, int nXpos, int nYpos, D2Game* ptGame, DRLGRoom* ptRoom, BOOL bTrue));
FASTCALL_FUNCTION(Unit*,		D2Game6FCBCB30,		(D2Game* ptGame, Unit* ptObject));
FASTCALL_FUNCTION(void,			D2Game6FCBCD70,		(D2Game* ptGame, DWORD nFunc, Unit* ptUnit));
FASTCALL_FUNCTION(Unit*,		D2Game6FCBD4D0,		(D2Game* ptGame, Unit* ptPlayer));
FASTCALL_FUNCTION(D2Game*,		D2Game6FCBD730,		(Unit* ptUnit));
FASTCALL_FUNCTION(DWORD,		D2Game6FCBD7F0,		(Unit* ptObject));
FASTCALL_FUNCTION(BOOL,			D2Game6FCBD8B0,		(Unit* ptUnit));
FASTCALL_FUNCTION(BOOL,			D2Game6FCBD900,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptTargetUnit));
FASTCALL_FUNCTION(BOOL,			D2Game6FCBDC60,		(D2Game* ptGame, Unit* ptUnit, Unit* ptTargetUnit));
FASTCALL_FUNCTION(void,			D2Game6FCBDD30,		(Unit* pUnit, BYTE nAlignment, BOOL bUpdateRegion));
FASTCALL_FUNCTION(BOOL,			D2Game6FCBDFE0,		(D2Game* ptGame, Unit* ptPlayer, DRLGRoom* ptRoom, DWORD pointX, DWORD pointY, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(int,			D2Game6FCBE420,		(Unit* ptAttacker, BOOL DualWield, DWORD dw_01, DWORD dw_02, DWORD dw_03, int DamagePercent, int PhysicalDamage, DWORD SourceDmg));
FASTCALL_FUNCTION(BOOL,			D2Game6FCBF400,		(DWORD MonTypeItem, DWORD MonTypeMonster));
FASTCALL_FUNCTION(void,			D2Game6FCBFE90,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender, BOOL bMissile, D2DamageData* ptDamageData));
FASTCALL_FUNCTION(void,			D2Game6FCC0940,		(Unit* ptAttacker, Unit* ptDefender, DWORD uk1));
FASTCALL_FUNCTION(void,			D2Game6FCC0F10,		(D2Game* ptGame, Unit* ptDefender, Unit* ptAttacker, BOOL bPetKill));
FASTCALL_FUNCTION(void,			D2Game6FCC1260,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender, D2DamageData* ptDamageData));
FASTCALL_FUNCTION(D2DamageData*, D2Game6FCC1D70,	(Unit* ptPlayer, Unit* ptTargetUnit));
FASTCALL_FUNCTION(DWORD,		D2Game6FCCAF30,		(Unit* ptUnit));
FASTCALL_FUNCTION(void,			D2Game6FCCEE40,		(D2Game* ptGame, Unit* ptUnit));
FASTCALL_FUNCTION(void,			D2Game6FCCEF70,		(D2Game* ptGame, Unit* ptUnit, CmdCurrent* ptCmdCurrent));
FASTCALL_FUNCTION(void,			D2Game6FCCF050,		(D2Game* ptGame, Unit* ptUnit, CmdCurrent* ptCmdCurrent));
FASTCALL_FUNCTION(Unit*,		D2Game6FCCF320,		(Unit* ptMinion));
FASTCALL_FUNCTION(void,			D2Game6FCCF270,		(D2Game* ptGame, Unit* ptMonster, DWORD dwOwnerGUID, int nOwnerType, BOOL bBossInit, BOOL bBoneWallInit));
FASTCALL_FUNCTION(void,			D2Game6FCCF5C0,		(Unit* ptUnit, DWORD monUmod, DWORD Set, DWORD funcAdress));
FASTCALL_FUNCTION(Unit*,		D2Game6FCCFD40,		(D2Game* ptGame, Unit* ptPet, UnitAi* hControl, UnitnFlag* ptUnitnFlag, UnitInDistance* ptUnitInDistance));
FASTCALL_FUNCTION(Unit*,		D2Game6FCCFD70,		(D2Game* ptGame, Unit* ptNpc, DWORD* mem));
FASTCALL_FUNCTION(void,			D2Game6FCCFEA0,		(D2Game* ptGame, Unit* ptUnit, DWORD AnimationMode, Unit* ptTargetUnit));
FASTCALL_FUNCTION(void,			D2Game6FCCFF20,		(D2Game* ptGame, Unit* ptUnit, short skillID, Unit* ptTargetUnit, DWORD funcAddr, DWORD nFlags));
FASTCALL_FUNCTION(void,			D2Game6FCD00A0,		(D2Game* ptGame, Unit* ptUnit, DWORD stalltime));
FASTCALL_FUNCTION(void,			D2Game6FCD01B0,		(Unit* ptUnit, DWORD unitType, DWORD velocity, DWORD uk1));
FASTCALL_FUNCTION(void,			D2Game6FCD0220,		(D2Game* ptGame, Unit* ptUnit, Unit* ptTargetUnit, DWORD nVelocity));
FASTCALL_FUNCTION(void,			D2Game6FCD03F0,		(D2Game* ptGame, Unit* ptUnit, Unit* ptTargetUnit));
FASTCALL_FUNCTION(void,			D2Game6FCD0410,		(D2Game* ptGame, Unit* ptPet, Unit* ptTargetUnit, DWORD nTiles));
FASTCALL_FUNCTION(void,			D2Game6FCD0530,		(D2Game* ptGame, Unit* ptUnit, Unit* ptTargetUnit, DWORD nTiles));
FASTCALL_FUNCTION(BOOL,			D2Game6FCD0560,		(D2Game* ptGame, Unit* ptUnit, Unit* ptTargetUnit, BYTE WalkDistance, DWORD check));
FASTCALL_FUNCTION(void,			D2Game6FCD0840,		(D2Game* ptGame, Unit* ptUnit, DWORD nTiles));
FASTCALL_FUNCTION(void,			D2Game6FCD09D0,		(D2Game* ptGame, Unit* ptUnit, Unit* ptTargetUnit, DWORD nTiles));
FASTCALL_FUNCTION(void,			D2Game6FCD0E80,		(D2Game* ptGame, Unit* ptUnit, Unit* ptTargetUnit, DWORD nTiles, DWORD uk1));
FASTCALL_FUNCTION(int,			D2Game6FD15580,		(Unit* ptUnit, DWORD nUnitType, DWORD SkillID, DWORD sLvl, D2SummonArg* ptSummonArg, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(CmdCurrent*,	D2Game6FCCEEF0,		(Unit* ptUnit));
FASTCALL_FUNCTION(void,			D2Game6FCE2BA0,		(D2Game* ptGame, Unit* ptOwner, Unit* ptPet, int eMotionType, BOOL bRun, int nSpeed, BYTE bSteps));
FASTCALL_FUNCTION(void,			D2Game6FCE3740,		(D2Game* ptGame, Unit* ptPet, AIParam* ptAIParam));
FASTCALL_FUNCTION(BOOL,			D2Game6FCE34E0,		(D2Game* ptGame, Unit* ptUnit, Unit* ptTargetEnemy, Unit* ptMinionOwner, DWORD uk1, AIParam* ptAIParam, DWORD uk2, DWORD uk3));
FASTCALL_FUNCTION(void,			D2Game6FCF07D0,		(D2Game* pGame, Unit* pMonster, UnitAi* pAIGeneral, int nAISpecialState));
FASTCALL_FUNCTION(DWORD,		D2Game6FCF1210,		(Unit* ptUnit, Unit* ptTargetUnit));
FASTCALL_FUNCTION(DWORD,		D2Game6FCF13B0,		(Unit* ptUnit, int xOffset, int yOffset));
FASTCALL_FUNCTION(BOOL,			D2Game6FCF14D0,		(Unit* ptPet, Unit* ptTargetUnit));
FASTCALL_FUNCTION(void,			D2Game6FCF1E80,		(D2Game* ptGame, Unit* ptUnit, Minion* ptMinion, DWORD funcAddr, DWORD nFlag));
FASTCALL_FUNCTION(Unit*,		D2Game6FCF2110,		(D2Game* ptGame, Unit* ptUnit, UnitAi* ptUnitAi, void* mem1, void* mem2));
FASTCALL_FUNCTION(Unit*,		D2Game6FCF2CC0,		(D2Game* ptGame, Unit* ptUnit, int TargetDistance, void* args));
FASTCALL_FUNCTION(void,			D2Game6FCF2E20,		(Unit* ptUnit, DWORD set));
FASTCALL_FUNCTION(BOOL,			D2Game6FCF2E70,		(Unit* ptUnit));
FASTCALL_FUNCTION(void,			D2Game6FCF2F30,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender, WORD skillID, DWORD Distance, DWORD Duration));
FASTCALL_FUNCTION(int,			D2Game6FCF5E20,		(Unit* ptAttacker, WORD skillID));
FASTCALL_FUNCTION(int,			D2Game6FCF5EE0,		(Unit* ptAttacker, WORD skillID, DWORD sLvl));
FASTCALL_FUNCTION(void,			D2Game6FC7EC00,		(D2Game* ptGame, Unit* ptUnit));
FASTCALL_FUNCTION(void,			D2Game6FC7EA50,		(D2Game* ptGame, Unit* ptPlayer, DWORD nAct));
FASTCALL_FUNCTION(BOOL,			D2Game6FCF8330,		(D2Game* ptGame, Unit* ptUnit, Unit* ptAttacker, int MissileID, DWORD NbrMissiles, DWORD Range, WORD skillID, DWORD sLvl));
FASTCALL_FUNCTION(void,			D2Game6FCFE200,		(Unit* ptAttacker, StatsList* ptStatsList, D2SkillsTXT* ptSkillTXT, WORD skillID, DWORD sLvl));
FASTCALL_FUNCTION(void,			D2Game6FCFEDD0,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender, DWORD currentFrame, WORD skillID, DWORD sLvl));
FASTCALL_FUNCTION(void,			D2Game6FCFF2E0,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender, DWORD eLen, WORD skillID, DWORD sLvl));
FASTCALL_FUNCTION(void,			D2Game6FCFE0E0,		(Unit* ptAttacker, StatsList* ptStatsList, D2SkillsTXT* ptSkillTXT, WORD skillID, DWORD sLvl));
FASTCALL_FUNCTION(BOOL,			D2Game6FD03E40,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptItem, Unit* ptTargetItem, DWORD X_Offset, DWORD Y_Offset));
FASTCALL_FUNCTION(void,			D2Game6FD0C2E0,		(Unit* ptAttacker, Unit* ptSummon));
FASTCALL_FUNCTION(void,			D2Game6FD0C3A0,		(Unit* ptAttacker, Unit* ptPet, DWORD unitLevel, BOOL bShield, DWORD dwArg));
FASTCALL_FUNCTION(void,			D2Game6FD0C530,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptSummon, DWORD SkillID, DWORD SkillLevel, DWORD uk1));
FASTCALL_FUNCTION(void,			D2Game6FD0CB10,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptSummon, DWORD nUnitType, DWORD SkillLevel));
FASTCALL_FUNCTION(unsigned int,	D2Game6FD10200,		(D2Game* ptGame, Unit* ptAttacker, DWORD xOffset, DWORD yOffset, DWORD Range, void* mem, BOOL skipFilter));
FASTCALL_FUNCTION(void,			D2Game6FD10C90,		(Unit* ptAttacker, DWORD ManaCost));
FASTCALL_FUNCTION(int,			D2Game6FD02E10,		(D2Game* ptGame, Unit* ptPlayer, Unit* ptItem, Unit* ptTargetItem, DWORD X_Offset, DWORD Y_Offset, DWORD uk1));
FASTCALL_FUNCTION(void,			D2Game6FD0FE50,		(D2Game* ptGame, Unit* ptAttacker, DWORD FuncAdress, D2AuraStateEffect* ptAuraStateEffect));
FASTCALL_FUNCTION(void,			D2Game6FD0FE80,		(D2Game* ptGame, Unit* ptAttacker, DWORD X_Offset, DWORD Y_Offset, DWORD Range, DWORD Filter, DWORD Func, D2AuraStateEffect* ptAuraStateEffect, DWORD uk1, LPCSTR file, DWORD line));
FASTCALL_FUNCTION(void,			D2Game_6FD0FE80,	(D2Game* ptGame, Unit* ptAttacker, DWORD X_Offset, DWORD Y_Offset, DWORD Range, DWORD Filter, DWORD Func, D2LifeAndManaLeechSkillParam* ptLifeAndManaLeechSkillParam, DWORD uk1, LPCSTR file, DWORD line));
FASTCALL_FUNCTION(int,			D2Game6FD10360,		(D2Game* ptGame, DWORD AuraFilter, Unit* ptAttacker, DWORD AuraRange, DWORD FuncAdress, CurseParam* ptCurseParam));
FASTCALL_FUNCTION(void,			D2Game6FD10E50,		(Unit* ptPlayer, DWORD StateID, StatsList* ptStatsList));
FASTCALL_FUNCTION(void,			D2Game6FD00370,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender, DWORD sLvl));
FASTCALL_FUNCTION(void,			D2Game6FD025E0,		(Unit* ptPlayer, int ManaCost));
FASTCALL_FUNCTION(void,			D2Game6FD11C90,		(Unit* ptUnit, DWORD StateID, DWORD set));
FASTCALL_FUNCTION(void,			D2Game6FD11D90,		(Unit* ptAttacker, D2DamageData* ptDamageData, DWORD eLen, WORD SkillID));
FASTCALL_FUNCTION(DWORD,		D2Game6FD12950,		(D2Game* ptGame, Unit* ptPlayer, DWORD dwSkillID, DWORD dwSkillLvl, DWORD Unknown1_1));
FASTCALL_FUNCTION(DWORD,		D2Game6FD12BA0,		(D2Game* ptGame, Unit* ptPlayer, DWORD dwSkillID, DWORD dwSkillLvl, DWORD Unknown1_1, DWORD ClassSkill, DWORD Unknown3_0));
//FASTCALL_FUNCTION(Unit*,		D2Game6FD13DF0,		(D2Game* ptGame, Unit* ptPlayer, DrlgRoom1* ptRoom, DWORD XOffset, DWORD YOffset, DWORD DestLvlID, Unit* ptSrcPortal, DWORD ObjectID, BOOL bPermanent));
FASTCALL_FUNCTION(Unit*,		D2Game6FD13DF0,		(D2Game* ptGame, Unit* ptPlayer, DRLGRoom* ptRoom, DWORD XOffset, DWORD YOffset, DWORD DestLvlID, Unit* ptSrcPortal, DWORD ObjectID, BOOL bPermanent));
//FASTCALL_FUNCTION(Unit*,		D2Game6FD13DF0_2,	(D2Game* ptGame, Unit* ptPlayer, DRLGRoom* ptRoom, DWORD XOffset, DWORD YOffset, DWORD DestLvlID, Unit* ptSrcPortal, DWORD ObjectID, BOOL bPermanent));
FASTCALL_FUNCTION(void,			D2Game6FD13260,		(D2Game* ptGame, Unit* ptPlayer, DWORD dwSkillID, DWORD dwSkillLvl, DWORD Unknown1_0));
FASTCALL_FUNCTION(void,			D2Game6FD136E0,		(Unit* ptPlayer, DWORD dwSkillID));
FASTCALL_FUNCTION(DWORD,		D2Game6FC802F0,		(D2Game* ptGame, int bodyloc, Unit* ptItem, Unit* ptPlayer));
FASTCALL_FUNCTION(void,			D2Game6FC80F80,		(D2Game* ptGame, Unit* ptUnit, DWORD dwArg, DWORD dwArgEx));
FASTCALL_FUNCTION(WORD,			D2Game6FC33540,		(Unit* ptItem));
FASTCALL_FUNCTION(BYTE,			D2Game6FC33890,		(Client* ptClient));
FASTCALL_FUNCTION(DWORD,		D2Game6FC339A0,		(Client* ptClient));
FASTCALL_FUNCTION(WORD,			D2Game6FC35170,		(D2Game* pGame, Unit* pUnit, int nEvent));
FASTCALL_FUNCTION(WORD,			D2Game6FC33510,		(Unit* ptItem));
FASTCALL_FUNCTION(void,			D2Game6FCC1AC0,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender, DWORD set));
FASTCALL_FUNCTION(WORD,			D2Game6FCC2300,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender, DWORD hitBonus));
FASTCALL_FUNCTION(void,			D2Game6FCC2420,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender, D2DamageData* ptDamageData, DWORD SrcDam)); 
FASTCALL_FUNCTION(void,			D2Game6FCC2C70,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender)); 
FASTCALL_FUNCTION(void,			D2Game6FCC34A0,		(D2Game* ptGame, Unit* ptPlayer, DWORD Experience)); 
FASTCALL_FUNCTION(void,			D2Game6FCC36D0,		(D2Game* ptGame, Unit* ptUnit, DWORD Set, WORD StateID)); 
FASTCALL_FUNCTION(void,			D2Game6FCC3790,		(D2Game* ptGame, DWORD nEvent, Unit* ptAttacker, Unit* ptDefender, D2DamageData* ptDamageData)); 
FASTCALL_FUNCTION(void,			D2Game6FCC58E0,		(Unit* pUnit, Client* pClient)); 
FASTCALL_FUNCTION(void,			D2Game6FCC5F00,		(Unit* ptUnit, Client* ptClient, BOOL bPlayer)); 
FASTCALL_FUNCTION(void,			D2Game6FCC6080,		(Unit* ptUnit, Client* ptClient)); 
FASTCALL_FUNCTION(void,			D2Game6FC3C410,		(D2Game* ptGame, Unit* ptPlayer, WORD LevelID, DWORD uk1));
FASTCALL_FUNCTION(void,			D2Game6FC60CD0,		(D2Game* ptGame, Unit* ptMonster));
//FASTCALL_FUNCTION(void,			D2Game6FC60E90,		(D2Game* ptGame, RoomTile2* ptRoomTile2, DrlgRoom1* ptRoom, Unit* ptUnit));
FASTCALL_FUNCTION(void,			D2Game6FC60E90,		(D2Game* ptGame, RoomTile2* ptRoomTile2, DRLGRoom* ptRoom, Unit* ptUnit));
FASTCALL_FUNCTION(BOOL,			D2Game6FC68630,		(D2Game* ptGame, Unit* ptUnit, short skillID, Unit* targetUnit, DWORD funcAddr, DWORD nFlags));
FASTCALL_FUNCTION(void,			D2Game6FCC6270,		(Unit* ptUnit, DWORD value));
FASTCALL_FUNCTION(DWORD,		D2Game6FC7EDF0,     (Unit* ptPlayer, DWORD statsID));
FASTCALL_FUNCTION(StatsList*,	D2Game6FD10EC0,     (D2CurseState* ptCurseState));
FASTCALL_FUNCTION(DWORD,		D2Game6FD13220,     (D2Game* ptGame, Unit* ptUnit, D2SkillsTXT* ptSkillTXT, WORD skillID, DWORD sLvl));
FASTCALL_FUNCTION(void,			D2Game6FD14170,     (D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender, WORD MissileID, WORD SkillID, DWORD SkillLevel, DWORD MissileRange));
FASTCALL_FUNCTION(void,			D2Game6FD14EC0,		(Unit* ptAttacker, D2DamageData* ptDamageData, WORD SkillID, DWORD SkillLevel));
FASTCALL_FUNCTION(int,			D2Game6FD155E0,		(D2Game* ptGame, Unit* ptAttacker, Unit* ptDefender, D2SkillsTXT* ptSkillTXT, WORD SkillID, DWORD sLvl, D2DamageData* ptDamageData));
FASTCALL_FUNCTION(void,			D2Game6FD15940,		(D2Game* ptGame, Unit* ptAttacker, WORD SkillID, DWORD sLvl, WORD MissileID));
FASTCALL_FUNCTION(int,			D2Game6FD1C8F0,		(int nValue1, int nValue2, int nValue3, int nValue4));
FASTCALL_FUNCTION(int,			D2Game6FD1C9A0,		(int nValue1, int nValue2, int nPercent, DWORD nBool));
FASTCALL_FUNCTION(void,			D2Game6FD1CAA0,		(DWORD nSize));
FASTCALL_FUNCTION(Unit*,		D2Game6FD115E0,		(D2Game* ptGame, Unit* ptAttacker, WORD SkillID, DWORD sLvl, WORD MissileID, DWORD offsetX, DWORD offsetY));
FASTCALL_FUNCTION(BOOL,			D2Game6FD123D0,		(D2Game* ptGame, Unit* ptPlayer, WORD Skill_ID, DWORD TargetUnitID));
FASTCALL_FUNCTION(Unit*,		D2Game6FD14430,		(D2Game* ptGame, D2SummonArg* ptSummonArg));
FASTCALL_FUNCTION(void,			D2Game6FD14C30,		(Unit* ptUnit));
FASTCALL_FUNCTION(void,			D2Game6FD14DD0,		(Unit* ptAttacker, D2DamageData* ptDamageData, WORD SkillID, DWORD sLvl));
FASTCALL_FUNCTION(void,			D2Game6FD156A0,		(D2Game* ptGame, Unit* ptUnit, WORD AuraEvent, WORD SkillID, DWORD sLvl, DWORD AuraFunc, DWORD Set, WORD StateID));
FASTCALL_FUNCTION(void,			D2Game6FD156F0,		(D2Game* ptGame, Unit* ptAttacker, WORD Skill_ID, DWORD sLvl, int srvmissilea));
FASTCALL_FUNCTION(BOOL,			D2Game6FCFBE80,		(D2Game* ptGame, Unit* ptPlayer, WORD Skill_ID, DWORD TargetUnitID));
FASTCALL_FUNCTION(void,			D2Game6FCF6D70,		(D2Game* ptGame, Unit* ptAttacker, DWORD dwLoSeed, int nMissileID, int nXpos, int nYpos, WORD wSkillID, WORD wSkillLV, int nbrMissiles));
FASTCALL_FUNCTION(BOOL,			D2Game6FC6AC70,		(Unit* ptUnit, DWORD nFlag));
FASTCALL_FUNCTION(void,			D2Game6FC6E780,		(D2Game* pGame, Unit* pBoss, Unit* pMissile, int nEvent));
FASTCALL_FUNCTION(void,			D2Game6FC6E940,		(D2Game* pGame, Unit* ptBoss, DWORD dwArg));
FASTCALL_FUNCTION(void,			D2Game6FC6F440,		(D2Game* pGame, DRLGRoom* pRoom, RMCoordList* pList, Unit* ptBoss, int nFailSafeMinion, int nMin, int nMax));
FASTCALL_FUNCTION(DWORD,		D2Game6FCF0E40,		(D2Game* pGame, Unit* ptUnit, DWORD mem));
FASTCALL_FUNCTION(BOOL,			D2Game6FCFD9B0,		(D2Game* ptGame, Unit* ptPlayer, WORD Skill_ID, DWORD TargetUnitID));
FASTCALL_FUNCTION(void,			D2Game6FCFD3C0,		(D2Game* ptGame, Unit* ptPlayer, Skills* ptSkill));
FASTCALL_FUNCTION(void,			D2Game6FD14C60,		(Unit* ptUnit, DWORD skillID, DWORD skillLevel, DWORD unitType));
FASTCALL_FUNCTION(void,			D2Game6FD13800,		(Unit* ptUnit, DWORD uk1, DWORD skillID, DWORD uk2));
FASTCALL_FUNCTION(Unit*,		D2Game6FD13B20,		(D2Game* ptGame, Unit* ptPortal, int nDestLevel, int nSourceLevel));
FASTCALL_FUNCTION(void,			D2Game6FCCFFB0,		(D2Game* ptGame, Unit* ptUnit, BYTE mode, short skillID, Unit* ptTargetUnit, DWORD pointX, DWORD pointY));
FASTCALL_FUNCTION(Unit*,		D2Game6FC68D70,		(D2Game* ptGame, Unit* ptUnit, DWORD unitID, DWORD monMode, DWORD SearchRadius, DWORD MonSpawnFlags));
FASTCALL_FUNCTION(void,			D2Game6FC3F3B0,		(Client* ptClient, BYTE msg, DWORD uk, DWORD value, Unit* ptHireling, DWORD uk3));
FASTCALL_FUNCTION(void,			D2Game6FCC7FA0,		(D2Game* ptGame, Unit* ptUnit, Unit* ptVendor, DWORD* uk));
FASTCALL_FUNCTION(D2Hireling*,	D2Game6FCCB910,		(D2Game* ptGame, Unit* ptVendor, int* uk));
FASTCALL_FUNCTION(void,			D2Game6FCCC7C0,		(D2Game* ptGame, Unit* ptUnit, DWORD nLevel));
FASTCALL_FUNCTION(size_t,		D2Game6FD1D70D,		(const void * ptr, size_t size, size_t count, FILE * stream));
FASTCALL_FUNCTION(int,			D2Game6FD1D690,		(FILE * stream));
FASTCALL_FUNCTION(FILE*,		D2Game6FD1D877,		(const char * filename, const char * mode));

//D2Client

FASTCALL_FUNCTION(D2LightMapStrc*, D2Client6FAA4280, (Unit* ptUnit, BOOL nFlag, WORD wLight, WORD w255, WORD wRed, WORD wGrn, WORD wBlu));
FASTCALL_FUNCTION(void,			D2Client6FAA43A0,	(D2LightMapStrc* ptLightMapStrc, DWORD LightRadiusEx));
FASTCALL_FUNCTION(BOOL,			D2Client6FAA8B30,	());
FASTCALL_FUNCTION(DWORD,		D2Client6FAA9AC0,	());
FASTCALL_FUNCTION(DWORD,		D2Client6FAABF80,	());
FASTCALL_FUNCTION(DWORD,		D2Client6FAABFF0,	());
FASTCALL_FUNCTION(BOOL,			D2Client6FAAC020,	());
FASTCALL_FUNCTION(BOOL,			D2Client6FAAC080,	());
FASTCALL_FUNCTION(BYTE,			D2Client6FAAC090,	());
FASTCALL_FUNCTION(void,			D2Client6FAAD990,	(BYTE MsgNum, WORD Msg));
FASTCALL_FUNCTION(void,			D2Client6FAADA20,	(BYTE MsgNum, DWORD Msg));
FASTCALL_FUNCTION(void,			D2Client6FAADA40,	(BYTE MsgNum, DWORD Msg, DWORD dwUniqueID));
FASTCALL_FUNCTION(void,			D2Client6FAADA70,	(char nType, int nSubType, int nNPCGUID, DWORD nPlayerGUID)); // Menu* ActiveNpcMenu
FASTCALL_FUNCTION(void,			D2Client6FABABC0,	(Unit* ptUnit, WORD MissileID, WORD SkillID, DWORD Slvl));
FASTCALL_FUNCTION(char*,		D2Client6FAB0C00,	(Unit* ptChar));
FASTCALL_FUNCTION(DWORD,		D2Client6FAB5A80,	());
FASTCALL_FUNCTION(void,			D2Client6FAB5A90,	());
FASTCALL_FUNCTION(BOOL,			D2Client6FABA950,	(Unit* ptUnit, D2PathPointX* ptPathPointX, D2PathPointY* ptPathPointY));
FASTCALL_FUNCTION(void*,		D2Client6FABC6B0,	(Unit* ptCltPlayer, DWORD SkillID, DWORD SkillLvl, DWORD unknown2_0));
FASTCALL_FUNCTION(void,			D2Client6FABD4D0,	(Unit* ptAttacker, Unit* ptUnit, DWORD MissileID, int uk1, WORD SkillID, DWORD sLvl, DWORD Range));
FASTCALL_FUNCTION(Unit*,		D2Client6FACA1D0,	(Unit* ptAttacker, WORD SkillID, DWORD sLvl));
FASTCALL_FUNCTION(void,			D2Client6FAC8940,	(Unit* ptAttacker, DWORD SkillID, DWORD bBool, DWORD SkillXPos, DWORD SkillYPos, DWORD TargetUnitID));
FASTCALL_FUNCTION(void,			D2Client6FACFCD0,	(DWORD Xpos, DWORD Ypos, DWORD Unknown, LPCWSTR ptSkillTabString, short int color));
FASTCALL_FUNCTION(DWORD,		D2Client6FAD10D0,	(DWORD attackRating));
FASTCALL_FUNCTION(void,			D2Client6FADD360,	(Unit* ptItem, wchar_t* pBuffer, int nSize));
FASTCALL_FUNCTION(DWORD,		D2Client6FAD4B60,	(DWORD nGUID, BOOL bState));
FASTCALL_FUNCTION(LPCWSTR,		D2Client6FAD56B0,	(DWORD nGUID, BOOL bState));
FASTCALL_FUNCTION(Menu*,		D2Client6FAD6BE0,	(DWORD X, DWORD Y, DWORD func1, DWORD func2, DWORD un1_0, DWORD un2_0, DWORD AutoSize, DWORD Width, DWORD Heigth, DWORD un6_0, DWORD un7_1));
FASTCALL_FUNCTION(void,			D2Client6FAD6EB0,	(Menu* hMenu));
FASTCALL_FUNCTION(void,			D2Client6FAD6DC0,	());
FASTCALL_FUNCTION(void,			D2Client6FAF9CB0,	(DWORD button, BOOL nFlag));
FASTCALL_FUNCTION(void,			D2Client6FAFA230,	(Unit* ptUnit, int nFlag, BOOL bState));
FASTCALL_FUNCTION(void,			D2Client6FAFA6E0,	(DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(BOOL,			D2Client6FAFB200,	(Unit* ptItem, BOOL bUnk, int* pCost, wchar_t* pBuffer, size_t nSize));
FASTCALL_FUNCTION(void,			D2Client6FAFB6E0,	(Unit* ptUnit, int nFlag, BOOL bState));
FASTCALL_FUNCTION(void,			D2Client6FAFB710,	());
FASTCALL_FUNCTION(void,			D2Client6FAFBB10,	());
FASTCALL_FUNCTION(DWORD,		D2Client6FAFBB50,	(VendorNPCData* pGUID));
FASTCALL_FUNCTION(void,			D2Client6FAFBB60,	(DWORD nUnitUnID));
FASTCALL_FUNCTION(void,			D2Client6FAFE850,	());
FASTCALL_FUNCTION(void,			D2Client6FAFF0B0,	());
FASTCALL_FUNCTION(void,			D2Client6FAFFE60,	());
FASTCALL_FUNCTION(DWORD,		D2Client6FAFF2D0,	(DWORD stat));
FASTCALL_FUNCTION(void,			D2Client6FAFF480,	());
FASTCALL_FUNCTION(void,			D2Client6FAFFCA0,	());
FASTCALL_FUNCTION(BOOL,			D2Client6FAFFF90,	(int XMouseCoord, int YMouseCoord));
FASTCALL_FUNCTION(void,			D2Client6FB001B0,	());
FASTCALL_FUNCTION(void,			D2Client6FB002B0,	());
FASTCALL_FUNCTION(void,			D2Client6FB00990,	());
FASTCALL_FUNCTION(void,			D2Client6FB00C10,	());
FASTCALL_FUNCTION(void,			D2Client6FAD7150,	(Menu* hMenu, DWORD un1_1));
FASTCALL_FUNCTION(void*,		D2Client6FAD7180,	(Menu* hMenu, LPCWSTR ptMenuString, DWORD dwPixel, DWORD dwUnknown, DWORD dwColor, DWORD dwFontType, DWORD dwFunc, DWORD dwSelectable));
FASTCALL_FUNCTION(void,			D2Client6FAE8F10,	());
FASTCALL_FUNCTION(void,			D2Client6FAE9690,	(int nButtonNbrPos));
FASTCALL_FUNCTION(void,			D2Client6FAE96D0,	(int nButtonNbrPos));
FASTCALL_FUNCTION(void,			D2Client6FAE9710,	(int nButtonNbrPos));
FASTCALL_FUNCTION(void,			D2Client6FAE9750,	(int nButtonNbrPos));
FASTCALL_FUNCTION(void,			D2Client6FAE9790,	(int nButtonNbrPos));
FASTCALL_FUNCTION(BOOL,			D2Client6FAEB910,	());
FASTCALL_FUNCTION(DWORD,		D2Client6FAEB930,	());
FASTCALL_FUNCTION(BOOL,			D2Client6FAEB940,	());
FASTCALL_FUNCTION(void,			D2Client6FAEC510,	());
FASTCALL_FUNCTION(void,			D2Client6FAED1C0,	());
FASTCALL_FUNCTION(void,			D2Client6FAF3160,	(Unit* ptItem, wchar_t* pBuffer, int nSize, BOOL bTrue, BOOL bFalse));
FASTCALL_FUNCTION(DWORD,		D2Client6FAF3950,	());
FASTCALL_FUNCTION(void,			D2Client6FAF3DC0,	());
FASTCALL_FUNCTION(void,			D2Client6FAF6340,	());
FASTCALL_FUNCTION(DWORD,		D2Client6FB10970,	(Unit* ptCltPlayer, LPWSTR ptTo, D2SkillDescTXT* ptSkillDescTXT, DWORD SkillID, DWORD uk1, DWORD uk2, DWORD uk3, DWORD color)); 
FASTCALL_FUNCTION(DWORD,		D2Client6FB16200,	(Unit* ptPlayer, DWORD selectedSkill));
FASTCALL_FUNCTION(void,			D2Client6FB177C0,	(LPCWSTR ptSkillTabString, DWORD Ypos));
FASTCALL_FUNCTION(void,			D2Client6FB18AC0,	(D2CellFileStrc* ptCells, D2SkillsTXT* ptSkillsTXT, DWORD dwGUID, int nValue, int nColor, int nXpos, int nYpos, BOOL bLeft));
FASTCALL_FUNCTION(D2CellFileStrc*,	D2Client6FB18700,	(D2SkillsTXT* ptSkillsTXT));
FASTCALL_FUNCTION(int,			D2Client6FB18770,	(Unit* ptPlayer, Skills* ptSKills));
FASTCALL_FUNCTION(void,			D2Client6FB18790,	(D2SkillsTXT* ptSkillsTXT, int nColor, int nQuantity, int nXpos, int nYpos));
FASTCALL_FUNCTION(void,			D2Client6FB18940,	(D2SkillsTXT* ptSkillsTXT, int nColor, int nLevel, int nCharges, int nXpos, int nYpos));
FASTCALL_FUNCTION(void,			D2Client6FB1CBD0,	(DWORD func1, DWORD func2));
FASTCALL_FUNCTION(void,			D2Client6FB1CC20,	(Unit* ptUnit, DWORD ptText, int zero));
FASTCALL_FUNCTION(nTextList*,	D2Client6FB1E6B0,	());
FASTCALL_FUNCTION(void,			D2Client6FB1E710,	());
FASTCALL_FUNCTION(DWORD,		D2Client6FB20600,	(Unit* ptUnit, DWORD stringKey));
FASTCALL_FUNCTION(DWORD,		D2Client6FB23260,	(DWORD UImsg, DWORD un1_0, DWORD un2_0));
FASTCALL_FUNCTION(DWORD*,		D2Client6FB24110,	());
FASTCALL_FUNCTION(void,			D2Client6FB25A10,	());
FASTCALL_FUNCTION(void,			D2Client6FB25BA0,	(DWORD ActiveNpcUniqueID));
FASTCALL_FUNCTION(Unit*,		D2Client6FB269F0,	(DWORD nUniqueID, DWORD nUnitType));
FASTCALL_FUNCTION(Unit*,		D2Client6FB26A60,	(DWORD nUniqueID, DWORD nUnitType));
//FASTCALL_FUNCTION(DrlgRoom1*,	D2Client6FB26C90,	(DWORD SizeX, DWORD SizeY));
FASTCALL_FUNCTION(DRLGRoom*,	D2Client6FB26C90,	(DWORD SizeX, DWORD SizeY));
FASTCALL_FUNCTION(int,			D2Client6FB284A0,	(Unit* ptUnit));
//FASTCALL_FUNCTION(DrlgRoom1*,	D2Client6FB29370,	());
FASTCALL_FUNCTION(DRLGRoom*,	D2Client6FB29370,	());
FASTCALL_FUNCTION(void,			D2Client6FB238B0,	());
FASTCALL_FUNCTION(Unit*,		D2Client6FB26AD0,	(Unit* ptMissile));
FASTCALL_FUNCTION(LPWSTR,		D2Client6FB297F0,	(Unit* ptNpc));
FASTCALL_FUNCTION(BOOL,			D2Client6FB29D80,	(Unit* ptAttacker, Unit* ptDefender));
FASTCALL_FUNCTION(void,			D2Client6FB2E780,	(DWORD unitID, BYTE set, DWORD statID, DWORD value, DWORD table_index));
FASTCALL_FUNCTION(BOOL,			D2Client6FB2FB90,	(Unit* ptItem));
FASTCALL_FUNCTION(Unit*,		D2Client6FB3E070,	(D2MissileInit* ptMissileInit));
FASTCALL_FUNCTION(DWORD,		D2Client6FB464A0,	(Unit* ptNpc, DWORD randNr));
FASTCALL_FUNCTION(void,			D2Client6FB46930,	(Unit* ptObject, DWORD nLightRadius, DWORD nRed, DWORD nGreen, DWORD nBlue));
FASTCALL_FUNCTION(void,			D2Client6FB478D0,	(Unit* ptObject, DWORD nUniqueID, DWORD nMode));
//FASTCALL_FUNCTION(void,			D2Client6FB2A8E0,	(Unit* ptItem, DrlgRoom1* ptRoom, DWORD posX, WORD posY, WORD uk1, DWORD uk2));
FASTCALL_FUNCTION(void,			D2Client6FB2A8E0,	(Unit* ptItem, DRLGRoom* ptRoom, DWORD posX, WORD posY, WORD uk1, DWORD uk2));
FASTCALL_FUNCTION(Unit*,		D2Client6FB296A0,	(Unit* ptAttacker));
FASTCALL_FUNCTION(void,			D2Client6FB296E0,	(Unit* ptAttacker));
FASTCALL_FUNCTION(void,			D2Client6FB4EE00,	(DWORD UnitType, Unit* ptUnit, Operate3* ptOperate, DWORD Act));
FASTCALL_FUNCTION(void,			D2Client6FB4F410,	(Unit* ptUnit, DWORD action));
FASTCALL_FUNCTION(void,			D2Client6FB50370,	(Unit* ptUnit));
FASTCALL_FUNCTION(void,			D2Client6FB55820,	(DWORD Sound, Unit* ptUnit, DWORD un3_0, DWORD un4_0, DWORD un5_0));
FASTCALL_FUNCTION(void,			D2Client6FB5B0F0,	(int Xpos, int Ypos, int width, int height, int color, int drawType)); 
FASTCALL_FUNCTION(void,			D2Client6FB5A320,	(Unit* ptItem, int nXpos, int nYpos));
FASTCALL_FUNCTION(void,			D2Client6FB1C600,	(LPWSTR text, int Color));
FASTCALL_FUNCTION(D2CellFileStrc*, D2Client6FB20100,	(LPSTR ptPath));
FASTCALL_FUNCTION(void,			D2Client6FB20430,	(wchar_t* pBuffer, DWORD nQuality));
FASTCALL_FUNCTION(void,			D2Client6FB2B360,	(Unit* ptUnit));
FASTCALL_FUNCTION(void,			D2Client6FB454A0,	(Unit* ptUnit, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(void,			D2Client6FB45300,	(Unit* ptUnit, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(void,			D2Client6FB456D0,	(Unit* ptUnit, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(void,			D2Client6FB45B20,	(Unit* ptUnit, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(void,			D2Client6FB45B60,	(Unit* ptUnit, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(void,			D2Client6FB45E10,	(Unit* ptUnit, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(void,			D2Client6FB46110,	(Unit* ptUnit, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(void,			D2Client6FB45800,	(Unit* ptUnit, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(void,			D2Client6FB45910,	(Unit* ptUnit, DWORD uk1, DWORD uk2));
FASTCALL_FUNCTION(D2CellFileStrc*,	D2Client6FAA1000,	(LPSTR ptPathandFilename, DWORD uk1));
FASTCALL_FUNCTION(void,			D2Client6FAA1140,	(D2CellFileStrc* ptCellFileStrc));
FASTCALL_FUNCTION(int,			D2Client6FABF670,	(Unit* ptAttacker, WORD SkillID));
FASTCALL_FUNCTION(DWORD,		D2Client6FABF730,	(Unit* ptAttacker, WORD SkillID, DWORD sLvl));
FASTCALL_FUNCTION(void,			D2Client6FAC85F0,	(Unit* ptAttacker, WORD SkillID, DWORD sLvl, DWORD PosX, DWORD PosY, DWORD nTargetUnitID));
FASTCALL_FUNCTION(void,			D2Client6FABDB00,	(Unit* ptAttacker, int MissileID, DWORD NbrMissiles, DWORD Range, WORD SkillID, DWORD sLvl));
FASTCALL_FUNCTION(BOOL,			D2Client6FB23230,	(int nToggle));
FASTCALL_FUNCTION(void,			D2Client6FB35D10,	(Unit* ptMissile, DWORD xOffset, DWORD yOffset, DWORD ChitPar1, WORD MissileID, WORD skillID, DWORD sLvl, DWORD cHitPar2, DWORD uk1));
FASTCALL_FUNCTION(void,			D2Client6FB4D3A0,	(Unit* ptUnit, int nState));
FASTCALL_FUNCTION(void,			D2Client6FB4D920,	(Unit* ptUnit, int nState));
FASTCALL_FUNCTION(void,			D2Client6FB4DA00,	(Unit* ptUnit, int nState));
FASTCALL_FUNCTION(void,			D2Client6FB4DB40,	(Unit* pUnit, DWORD dwColorTint, BOOL bVanish));
FASTCALL_FUNCTION(void,			D2Client6FB4D640,	(Unit* ptUnit, int nState));
FASTCALL_FUNCTION(StatsList*,	D2Client6FB4D830,	(Unit* ptUnit, StatsList* ptStatlist, WORD wState, WORD wStat, int nValue, int nParam));
FASTCALL_FUNCTION(void,			D2Client6FB5BA70,	(Unit* ptAttacker, int prgOverlay, DWORD uk1, DWORD uk2, DWORD uk3, DWORD uk4, DWORD uk5, DWORD uk6));
FASTCALL_FUNCTION(void,			D2Client6FB5BEC0,	(Unit* ptObect, DWORD set, int prgOverlay));
FASTCALL_FUNCTION(void,			D2Client6FB5C080,	(Unit* ptObect, DWORD dwArg));
FASTCALL_FUNCTION(wchar_t*,		D2Client6FB5EF30,	(wchar_t* pReturn));
FASTCALL_FUNCTION(void,			D2Client6FAF7950,	(DWORD dwUniqueID));

//Fog
FASTCALL_FUNCTION(void*,		D2Fog273A,			(DWORD dwMemSize, LPCSTR lpszErrFile, DWORD ErrLine, DWORD null));
FASTCALL_FUNCTION(void,			D2Fog273B,			(void* ptMemLoc, LPCSTR lpszErrFile, DWORD ErrLine, DWORD null));
// FASTCALL_FUNCTION(void*,		D2Fog273C,			(void* pMemPool, size_t nSize, LPCSTR lpszErrFile, DWORD ErrLine, DWORD null));
FASTCALL_FUNCTION(void*,		D2Fog273C,			(D2PoolManager* pMemPool, size_t nSize, LPCSTR lpszErrFile, DWORD ErrLine, DWORD null));
FASTCALL_FUNCTION(void*,		D2Fog273D,			(D2PoolManager* ptMemPool, size_t nSize, LPCSTR lpszErrFile, DWORD ErrLine, DWORD null));
FASTCALL_FUNCTION(void*,		D2Fog273E,			(void* pMemPool, void* Memory, LPCSTR lpszErrFile, DWORD ErrLine));
//FASTCALL_FUNCTION(void,		D2Fog273E,			(void* pMemPool, void* Memory, LPCSTR lpszErrFile, DWORD ErrLine));
FASTCALL_FUNCTION(void,			D2Fog2783,			(LPSTR ptPath, DWORD dwFlag));




//D2Lang
FASTCALL_FUNCTION(LPWSTR,		D2Lang10003,		(LPCSTR ptString));
FASTCALL_FUNCTION(LPWSTR,		D2Lang10004,		(WORD dwIndexNum));
FASTCALL_FUNCTION(DWORD,		D2L_UnicodeWidth,	(DWORD MemLoc, WORD Size));
FASTCALL_FUNCTION(DWORD,		D2L_ToUnicode,		(void* ptToLoc, DWORD ptFromLoc, DWORD Size));
FASTCALL_FUNCTION(LPWSTR,		D2L_StrCat,			(LPWSTR ptToString, LPCWSTR ptFromString));
FASTCALL_FUNCTION(void,			D2L_StrCpy,			(LPWSTR ptTo, LPCWSTR ptFrom));
FASTCALL_FUNCTION(void,			D2L_Win2Unicode,	(LPWSTR ptTo, LPCSTR ptFrom, DWORD wCharSize));
FASTCALL_FUNCTION(LPWSTR,		D2Lang6FC12E60,		(void* ptFile, void* ptMemLoc, DWORD dwIndexNum ));
FASTCALL_FUNCTION(TblHeader*,	D2Lang6FC13640,		(LPCSTR lpszFileName));
FASTCALL_FUNCTION(WORD,			D2Lang6FC13A90,		(void* ptFile, void* ptMemLoc, LPSTR String, LPWSTR RetString));

// D2Win
FASTCALL_FUNCTION(void,			D2Win10051,			(D2MenuObject* ptMenuObject, LPWSTR ptFrom, DWORD ncolor, DWORD nzero, DWORD nCenter));
FASTCALL_FUNCTION(void,			D2Win10117,			(const wchar_t* pString, int nXpos, int nYpos, int nColour, int nTransTbl));
FASTCALL_FUNCTION(DWORD,		D2Win10121,			(const wchar_t* pString));
FASTCALL_FUNCTION(int,			D2Win10127,			(int FontType));
FASTCALL_FUNCTION(void,			D2Win10129,			(LPWSTR ptTo, DWORD xPos, DWORD yPos, DWORD color, DWORD trans));		
FASTCALL_FUNCTION(DWORD*,		D2Win10131,			(LPWSTR ptTo, DWORD xPos, DWORD yPos));
FASTCALL_FUNCTION(D2MPQArchiveStrc*, D2Win6F8B2399,	 (const char* szFileName, const char* szModule, const char* szIdentifier, DWORD dwUnused, BOOL bCDDrive, D2MPQArchiveStrc::t_LoadFail pfLoadFail, DWORD dwPriority));
FASTCALL_FUNCTION(void,			D2Win10047,			(D2MenuObject* ptMenuObject));	
FASTCALL_FUNCTION(void,			D2Win10046,			(D2MenuObject* ptMenuObject, DWORD* nTitleList, DWORD nGameFlags));

// D2Net
FASTCALL_FUNCTION(size_t,		D2Net272E,			(void* ptPacket, size_t nSize, size_t* ptSize));
FASTCALL_FUNCTION(void,			D2Net6FC01810,		(void* ptPacket, size_t nSize));


// D2Launch
FASTCALL_FUNCTION(void,			D2Launch6FA14520,	(LPWSTR* nTextBuffert, DWORD nClassID));
FASTCALL_FUNCTION(void,			D2Launch6FA23890,	(D2LaunchPlayerInit* ptPlayerInit, WORD nGameFlags, BYTE nClass, LPCSTR* charName));


/*=================================================================*/
/* D2 Functions pointers		                                   */
/*=================================================================*/

//D2Common
static D2Common2711		D2GetActfromLevelID		= (D2Common2711)		(0x35300 + CommonOffset);	// D2Common.10001 - replaced
static D2Common271A		D2GetLeveldefsBin		= (D2Common271A)		(0x20D90 + CommonOffset);	// D2Common.10010
static D2Common272B		D2GetDestRoomTile		= (D2Common272B)		(0x4BA20 + CommonOffset);	// D2Common.10027
static D2Common2733		D2GetRoomList			= (D2Common2733)		(0x4BC10 + CommonOffset);	// D2Common.10035
//static D2Common2733_2	D2GetRoomListv2			= (D2Common2733_2)		(0x4BC10 + CommonOffset);	// D2Common.10035
static D2Common2739		D2FindFreeRoom			= (D2Common2739)		(0x4C240 + CommonOffset);	// D2Common.10041
static D2Common2747		D2GetPresetUnitsFromRoom = (D2Common2747)		(0x4C5C0 + CommonOffset);	// D2Common.10047
static D2Common2749		D2GetLevelIDFromRoom	= (D2Common2749)		(0x4C7A0 + CommonOffset);	// D2Common.10057
//static D2Common2749_2	D2GetLevelIDFromRoomV2	= (D2Common2749_2)		(0x4C7A0 + CommonOffset);	// D2Common.10057
static D2Common274A		D2GetLevelIDFromUnitID	= (D2Common274A)		(0x4C7C0 + CommonOffset);	// D2Common.10058
static D2Common2752		D2RemoveUnit			= (D2Common2752)		(0x4C860 + CommonOffset);	// D2Common.10066
static D2Common2762		D2CheckIfTown			= (D2Common2762)		(0x4D0E0 + CommonOffset);	// D2Common.10082
//static D2Common2762_2	D2CheckIfTownV2			= (D2Common2762_2)		(0x4D0E0 + CommonOffset);	// D2Common.10082
static D2Common2765		D2GetTownLvlFromAct		= (D2Common2765)		(0x4D140 + CommonOffset);	// D2Common.10085	
static D2Common2776		D2RoomSetFrame			= (D2Common2776)		(0x4D540 + CommonOffset);	// D2Common.10102	
static D2Common2788		D2CheckRect				= (D2Common2788)		(0x018C0 + CommonOffset);	// D2Common.10120	
static D2Common2796		D2GetSpawnPoint			= (D2Common2796)		(0x06280 + CommonOffset);	// D2Common.10134
static D2Common279A		D2GetFreeRoomCoords		= (D2Common279A)		(0x062E0 + CommonOffset);	// D2Common.10138
static D2Common27AE		D2GetPathDirection		= (D2Common27AE)		(0x69BF0 + CommonOffset);	// D2Common.10158
static D2Common27B0		D2SetUnitDirection		= (D2Common27B0)		(0x69C20 + CommonOffset);	// D2Common.10160
static D2Common27B1		D2SetPathDirection		= (D2Common27B1)		(0x69C90 + CommonOffset);	// D2Common.10161
static D2Common27B2		D2GetXOffsetFromPath	= (D2Common27B2)		(0x69CB0 + CommonOffset);	// D2Common.10162
static D2Common27B3		D2GetYOffsetFromPath	= (D2Common27B3)		(0x69CF0 + CommonOffset);	// D2Common.10163
static D2Common27BF		D2GetFirstPointXFromPath = (D2Common27BF)		(0x69DE0 + CommonOffset);   // D2Common.10175
static D2Common27C0		D2GetFirstPointYFromPath = (D2Common27C0)		(0x69DF0 + CommonOffset);   // D2Common.10176
static D2Common27C4		D2GetTargetUnitFromPath = (D2Common27C4)		(0x69FA0 + CommonOffset);	// D2Common.10180
static D2Common27C6		PathSetCollisionType	= (D2Common27C6)		(0x69FE0 + CommonOffset);	// D2Common.10182
static D2Common27DB		D2SetPathRotateFlag		= (D2Common27DB)		(0x6A430 + CommonOffset);	// D2Common.10203
static D2Common27E3		D2SetUnitCollidePattern = (D2Common27E3)		(0x6A580 + CommonOffset);	// D2Common.10211
static D2Common27E6		D2Common_10214			= (D2Common27E6)		(0x692F0 + CommonOffset);	// D2Common.10214
static D2Common27EC		D2GetDistanceToTarget	= (D2Common27EC)		(0x6A6A0 + CommonOffset);	// D2Common.10220
static D2Common27EF		D2CreatePath			= (D2Common27EF)		(0x68450 + CommonOffset);	// D2Common.10223
static D2Common27F5		D2_Common10229			= (D2Common27F5)		(0x6D590 + CommonOffset);	// D2Common.10229
static D2Common2800		D2InitInventory			= (D2Common2800)		(0x4E4C0 + CommonOffset);	// D2Common.10240
static D2Common2802		D2AddItemToInventory	= (D2Common2802)		(0x4E6A0 + CommonOffset);	// D2Common.10242
static D2Common280D		D2EquipUnit				= (D2Common280D)		(0x4FAB0 + CommonOffset);	// D2Common.10253
static D2Common2811		D2GetItemInBodyLoc		= (D2Common2811)		(0x4FAE0 + CommonOffset);	// D2Common.10257
static D2Common2812     D2GetEquipedWeapon      = (D2Common2812)		(0x4FC60 + CommonOffset);	// D2Common.10258
static D2Common2815		D2SetMouseItem			= (D2Common2815)		(0x4FF20 + CommonOffset);	// D2Common.10261
static D2Common2816		D2GetMouseItem			= (D2Common2816)		(0x4FF80 + CommonOffset);	// D2Common.10262
static D2Common281D		D2CheckItemInbelt		= (D2Common281D)		(0x50340 + CommonOffset);	// D2Common.10269
static D2Common281F		D2GetItemFromBelt		= (D2Common281F)		(0x50550 + CommonOffset);	// D2Common.10271
static D2Common2820		D2CheckBeltItem			= (D2Common2820)		(0x50590 + CommonOffset);	// D2Common.10272
static D2Common2825		D2GetInventoryItem		= (D2Common2825)		(0x4E7A0 + CommonOffset);	// D2Common.10277
static D2Common282B		D2SetLodFlagForCorpse	= (D2Common282B)		(0x509F0 + CommonOffset);	// D2Common.10283 D2UpdateTrade (nec's name)
static D2Common282C		D2GetUsedSockets		= (D2Common282C)		(0x50AC0 + CommonOffset);	// D2Common.10284
static D2Common282D		D2GetGemFromInv			= (D2Common282D)		(0x50AE0 + CommonOffset);	// D2Common.10285
static D2Common2831     D2SetWeaponGUID         = (D2Common2831)		(0x50ED0 + CommonOffset);	// D2Common.10289
static D2Common2834		D2AddPlayerUniqueIDtoInventory2	= (D2Common2834) (0x51140 + CommonOffset);	// D2Common.10292
static D2Common2835		D2GetUnIDFromInv2 		= (D2Common2835)		(0x51160 + CommonOffset);	// D2Common.10293
static D2Common2836		D2AddCorpseUnIDtoPtInv	= (D2Common2836)        (0x51190 + CommonOffset);   // D2Common.10294 
static D2Common2837		D2RemoveLastCorpse		= (D2Common2837)		(0x51210 + CommonOffset);	// D2Common.10295
static D2Common2838		D2GetCorpse				= (D2Common2838)		(0x51290 + CommonOffset);	// D2Common.10296
static D2Common2839		D2GetNextCorpseUniqueID = (D2Common2839)		(0x512B0 + CommonOffset);	// D2Common.10297
static D2Common283B		D2Common10299			= (D2Common283B)		(0x51B60 + CommonOffset);	// D2Common.10299	
static D2Common2840		D2GetNextInventoryItem	= (D2Common2840)		(0x52080 + CommonOffset);	// D2Common.10304
static D2Common2841		D2CheckInventoryItem	= (D2Common2841)		(0x520C0 + CommonOffset);	// D2Common.10305
static D2Common2843		D2GetNodePage			= (D2Common2843)		(0x52100 + CommonOffset);	// D2Common.10307
static D2Common2846		D2CheckifItemInInventory = (D2Common2846)		(0x52140 + CommonOffset);	// D2Common.10310
static D2Common2849		D2GetFirstBodyUniqueID	= (D2Common2849)		(0x512D0 + CommonOffset);	// D2Common.10313
static D2Common284A		D2GetNextBodyUniqueID	= (D2Common284A)		(0x6FEA0 + CommonOffset);   // D2Common.10314
static D2Common2850		D2GetStartSkill			= (D2Common2850)		(0x7D570 + CommonOffset);   // D2Common.10320
static D2Common2851		D2GetLeftSkill			= (D2Common2851)		(0x7D5B0 + CommonOffset);	// D2Common.10321
static D2Common2852		D2GetRightSkill			= (D2Common2852)		(0x7D5F0 + CommonOffset);	// D2Common.10322
static D2Common2853		D2GetCurrentSkill		= (D2Common2853)		(0x7D670 + CommonOffset);	// D2Common.10323
static D2Common285C		D2GetMapOffsetsFromUnit	= (D2Common285C)		(0x7DD10 + CommonOffset);	// D2Common.10332
static D2Common2860		D2GetSizeXofUnit		= (D2Common2860)		(0x7D900 + CommonOffset);	// D2Common.10336
static D2Common2861		D2GetSizeYofUnit		= (D2Common2861)		(0x7DA00 + CommonOffset);	// D2Common.10337
static D2Common2866		D2GetRoomFromUnit		= (D2Common2866)		(0x7E270 + CommonOffset);	// D2C�mmon.10342
//static D2Common_2866	D2GetRoomFromUnitv2		= (D2Common_2866)		(0x7E270 + CommonOffset);	// D2C�mmon.10342 new room struct
static D2Common2868		D2SetTargetPath			= (D2Common2868)		(0x7E2D0 + CommonOffset);	// D2C�mmon.10344
static D2Common286C		D2SetUnitMode			= (D2Common286C)		(0x7EA60 + CommonOffset);	// D2Common.10348
static D2Common286D		D2UpdateObjectFrame		= (D2Common286D)		(0x7E510 + CommonOffset);	// D2Common.10349
static D2Common286F		D2ObjectUpdateField		= (D2Common286F)		(0x7E060 + CommonOffset);	// D2Common.10351
static D2Common2870		D2SetNoCollision		= (D2Common2870)		(0x7DEC0 + CommonOffset);	// D2Common.10352
static D2Common2875		D2RefreshUnit2			= (D2Common2875)		(0x7EB80 + CommonOffset);	// D2Common.10357  
static D2Common287A		D2CheckLineOfSight		= (D2Common287A)		(0x814C0 + CommonOffset);	// D2Common.10362  
static D2Common287F		D2GetNbrofBeltRows		= (D2Common287F)		(0x801F0 + CommonOffset);	// D2Common.10367
static D2Common2880		D2GetUnitPercentHealth	= (D2Common2880)		(0x80260 + CommonOffset);	// D2Common.10368
static D2Common2884		D2SetCurrentFrame		= (D2Common2884)		(0x7EE20 + CommonOffset);	// D2Common.10372
static D2Common2891		D2Units_Refresh			= (D2Common2891)		(0x7D120 + CommonOffset);	// D2Common.10385
static D2Common2892		D2RemoveUnitFromRoom	= (D2Common2892)		(0x7D300 + CommonOffset);	// D2Common.10386
static D2Common2899		D2CheckObjSubClass		= (D2Common2899)		(0x7FC90 + CommonOffset);	// D2Common.10393
static D2Common289A		D2GetObjTXT				= (D2Common289A)		(0x7FCB0 + CommonOffset);	// D2Common.10394
static D2Common289B		D2GetShrineTXTFromUnit	= (D2Common289B)		(0x7FD00 + CommonOffset);	// D2Common.10395 
static D2Common289C		D2SetShrineTXTToUnit	= (D2Common289C)		(0x7FD50 + CommonOffset);	// D2Common.10396 
static D2Common289E		D2GetRangeFromXY		= (D2Common289E)		(0x82F50 + CommonOffset);	// D2Common.10398
static D2Common28A1		D2CreateUnitFind		= (D2Common28A1)		(0x7C990 + CommonOffset);	// D2Common.10401
static D2Common28A2		D2RemoveUnitFind		= (D2Common28A2)		(0x7CA50 + CommonOffset);	// D2Common.10402
static D2Common28A3		D2GetUnitsList			= (D2Common28A3)		(0x7CA80 + CommonOffset);	// D2Common.10403
static D2Common28AF		D2SetSourceUnit			= (D2Common28AF)		(0x7FFE0 + CommonOffset);	// D2Common.10415
static D2Common28B0		D2StatsSetLinkInfo		= (D2Common28B0)		(0x7FF20 + CommonOffset);	// D2Common.10416
static D2Common28B6		D2AddPlayerDatatoUnit   = (D2Common28B6)		(0x804A0 + CommonOffset);	// D2Common.10422
static D2Common28B8		D2GetPlayerData			= (D2Common28B8)		(0x805B0 + CommonOffset);	// D2Common.10424
static D2Common28BB		D2GetPortalFlags		= (D2Common28BB)		(0x80700 + CommonOffset);	// D2Common.10427
static D2Common28BC		D2SetPortalFlags		= (D2Common28BC)		(0x80760 + CommonOffset);	// D2Common.10428
static D2Common28BF		D2GetDefenseRating		= (D2Common28BF)		(0x808B0 + CommonOffset);	// D2Common.10431
static D2Common28C0		D2GetAttackRating		= (D2Common28C0)		(0x80AC0 + CommonOffset);	// D2Common.10432
static D2Common28C1		D2GetBlockRating		= (D2Common28C1)		(0x80B60 + CommonOffset);	// D2Common.10433
static D2Common28C5		D2CastOverlay			= (D2Common28C5)		(0x80160 + CommonOffset);	// D2Common.10437
static D2Common28C7     D2GetMaxGold            = (D2Common28C7)		(0x81D90 + CommonOffset);	// D2Common.10439
static D2Common28D7     D2SetPlayerWeaponGUID   = (D2Common28D7)		(0x82530 + CommonOffset);	// D2Common.10455
static D2Common28D9		D2GetUnitAnimDirection	= (D2Common28D9)		(0x7D520 + CommonOffset);	// D2Common.10457
static D2Common28DC		D2CreateObjectPath		= (D2Common28DC)		(0x82780 + CommonOffset);	// D2Common.10460
static D2Common28DF		D2SetStatInStatList		= (D2Common28DF)		(0x777B0 + CommonOffset);	// D2Common.10463
static D2Common28E0		D2AddToStat				= (D2Common28E0)		(0x77560 + CommonOffset);	// D2Common.10464
static D2Common28E0_2	D2AddToStat_2			= (D2Common28E0_2)		(0x77560 + CommonOffset);	// D2Common.10464
static D2Common28E1		D2AddStatToStatsListEx	= (D2Common28E1)		(0x77910 + CommonOffset);	// D2Common.10465
static D2Common28E2		D2GetStatFromStatListEx	= (D2Common28E2)		(0x77D40 + CommonOffset);	// D2Common.10466
static D2Common28E6		D2Common10470			= (D2Common28E6)		(0x77140 + CommonOffset);	// D2Common.10470
static D2Common28E9		D2GetEndFrameFromState  = (D2Common28E9)		(0x77320 + CommonOffset);	// D2Common.10473
static D2Common28EA		D2ExpireStatList		= (D2Common28EA)		(0x791C0 + CommonOffset);	// D2Common.10474
static D2Common28EB		D2SetStatListToItem		= (D2Common28EB)		(0x77340 + CommonOffset);	// D2Common.10475
static D2Common28EC		D2UpdateStateEndTime	= (D2Common28EC)		(0x77300 + CommonOffset);	// D2Common.10476
static D2Common28ED		D2AddFiletoStatsList    = (D2Common28ED)		(0x78CA0 + CommonOffset);	// D2Common.10477
static D2Common28EE		D2AddStateIDtoStatsList = (D2Common28EE)		(0x772C0 + CommonOffset);	// D2Common.10478
static D2Common28F0		D2GetStatsList			= (D2Common28F0)		(0x781E0 + CommonOffset);	// D2Common.10480
static D2Common28F4		D2GetAffixStats			= (D2Common28F4)		(0x782C0 + CommonOffset);	// D2Common.10484
static D2Common28F5		D2FreeStats				= (D2Common28F5)		(0x77030 + CommonOffset);	// D2Common.10485
static D2Common28F6		D2CheckAndSetUnitState	= (D2Common28F6)		(0x74560 + CommonOffset);	// D2Common.10486
static D2Common28F7		D2CheckState			= (D2Common28F7)		(0x745A0 + CommonOffset);	// D2Common.10487
static D2Common28F8		D2StatesTogglegfx		= (D2Common28F8)		(0x745F0 + CommonOffset);	// D2Common.10488
static D2Common28FC		D2StatesGetListGFXFlags = (D2Common28FC)		(0x74760 + CommonOffset);	// D2Common.10492
static D2Common28FE		D2StatesGetListFlags	= (D2Common28FE)		(0x748F0 + CommonOffset);	// D2Common.10494
static D2Common2901		D2GetMaxDurationFromState = (D2Common2901)		(0x74E80 + CommonOffset);	// D2Common.10497
static D2Common2915		D2SetUnitStat			= (D2Common2915)		(0x77AB0 + CommonOffset);	// D2Common.10517
static D2Common2916		D2AddUnitStat			= (D2Common2916)		(0x77B00 + CommonOffset);	// D2Common.10518
static D2Common2917		D2GetUnitStat			= (D2Common2917)		(0x77C30 + CommonOffset);	// D2Common.10519
static D2Common2918		D2GetStatSigned			= (D2Common2918)		(0x77E30 + CommonOffset);	// D2Common.10520
static D2Common2919		D2GetUnitBaseStat		= (D2Common2919)		(0x77B30 + CommonOffset);	// D2Common.10521
static D2Common291D		D2Common10525			= (D2Common291D)		(0x78F30 + CommonOffset);	// D2Common.10525
static D2Common2927		D2Common10535			= (D2Common2927)		(0x783A0 + CommonOffset);	// D2Common.10535
static D2Common2942		D2GetAlignment			= (D2Common2942)		(0x78620 + CommonOffset);	// D2Common.10562
static D2Common2944		D2GetMaxHealth			= (D2Common2944)		(0x791D0 + CommonOffset);	// D2Common.10564
static D2Common2945		D2GetMaxMana			= (D2Common2945)		(0x792C0 + CommonOffset);	// D2Common.10565	
static D2Common2946		D2GetMaxStamina			= (D2Common2946)		(0x793B0 + CommonOffset);	// D2Common.10566	
static D2Common2947		D2GetMaxDurability		= (D2Common2947)		(0x794A0 + CommonOffset);	// D2Common.10567
static D2Common294F		D2UnloadAllBinImages	= (D2Common294F)		(0x10150 + CommonOffset);	// D2Common.10575
static D2Common2950		D2InitCreateBinFiles	= (D2Common2950)		(0x104B0 + CommonOffset);	// D2Common.10576
static D2Common2952		D2CompileText			= (D2Common2952)		(0x0FD70 + CommonOffset);	// D2Common.10578
static D2Common2953		D2UnloadBinImage		= (D2Common2953)		(0x10110 + CommonOffset);	// D2Common.10579
static D2Common2957		D2GetHireTXTFromLevel	= (D2Common2957)		(0x2B1D0 + CommonOffset);	// D2Common.10583
static D2Common295B		D2GetHirestatTXT		= (D2Common295B)		(0x2B3A0 + CommonOffset);	// D2Common.10587
static D2Common295E		D2GetMissileRange		= (D2Common295E)		(0x24BE0 + CommonOffset);	// D2Common.10590
static D2Common2968		D2GetItemTXTFromNbr		= (D2Common2968)		(0x17680 + CommonOffset);	// D2Common.10600
static D2Common2969		D2GetItemTXTFromCode	= (D2Common2969)		(0x176D0 + CommonOffset);	// D2Common.10601
static D2Common296A		D2GetItemIDfromCode		= (D2Common296A)		(0x17720 + CommonOffset);	// D2Common.10602
static D2Common2978		D2GetptGem				= (D2Common2978)		(0x1BB00 + CommonOffset);	// D2Common.10616
static D2Common297A		D2GetBookPt				= (D2Common297A)		(0x1BD30 + CommonOffset);	// D2Common.10618
static D2Common297B		D2GetRuneStruct			= (D2Common297B)		(0x1E9E0 + CommonOffset);	// D2Common.10619
static D2Common2980		D2GetShrineTXT			= (D2Common2980)		(0x32000 + CommonOffset);	// D2Common.10624 is this really right? disabled as dubplicated. but crash then used
static D2Common2981		D2GetShrineRecordNbr	= (D2Common2981)		(0x32070 + CommonOffset);	// D2Common.10625
static D2Common2982		D2GetObjectsTXT			= (D2Common2982)		(0x318F0 + CommonOffset);	// D2Common.10626
static D2Common2984		D2GetLevelThreshold		= (D2Common2984)		(0x096B0 + CommonOffset);	// D2Common.10628
static D2Common2985		D2GetMaxLevelFromExpTable = (D2Common2985)		(0x096E0 + CommonOffset);	// D2Common.10629
static D2Common2986		D2GetLevelFromExp		= (D2Common2986)		(0x09710 + CommonOffset);	// D2Common.10630
static D2Common2987		D2GetLevelTXT			= (D2Common2987)		(0x203C0 + CommonOffset);	// D2Common.10631
static D2Common298E		D2RefreshBelt			= (D2Common298E)		(0x093B0 + CommonOffset);	// D2Common.10638
static D2Common298F		D2GetBeltBox			= (D2Common298F)		(0x09420 + CommonOffset);	// D2Common.10639
static D2Common299F		D2GetDifficultyTXT		= (D2Common299F)		(0x0E4D0 + CommonOffset);	// D2Common.10655
static D2Common29A3		D2GetTCFromID			= (D2Common29A3)		(0x28DF0 + CommonOffset);	// D2Common.10659
static D2Common29AC		D2GetSuperUniquesInfo	= (D2Common29AC)		(0x2A440 + CommonOffset);	// D2Common.10668
static D2Common29BF		D2CreateItemData		= (D2Common29BF)		(0x58380 + CommonOffset);	// D2Common.10687
static D2Common29C1		D2GetBodyLoc			= (D2Common29C1)		(0x58430 + CommonOffset);	// D2Common.10689
static D2Common29C2		D2SetBodyloc			= (D2Common29C2)		(0x58450 + CommonOffset);	// D2Common.10690
static D2Common29C3		D2GetItemSeed			= (D2Common29C3)		(0x58470 + CommonOffset);	// D2Common.10691
static D2Common29C4		D2InitItemSeed			= (D2Common29C4)		(0x58490 + CommonOffset);	// D2Common.10692
static D2Common29C5		D2GetItemLowSeed		= (D2Common29C5)		(0x584B0 + CommonOffset);	// D2Common.10693
static D2Common29C6		D2AssignItemStartSeed	= (D2Common29C6)		(0x584D0 + CommonOffset);	// D2Common.10694
static D2Common29C7		D2GetQuality			= (D2Common29C7)		(0x58550 + CommonOffset);	// D2Common.10695
static D2Common29C8		D2SetQuality			= (D2Common29C8)		(0x585B0 + CommonOffset);	// D2Common.10696
static D2Common29CC		D2AssignPrefix			= (D2Common29CC)		(0x585D0 + CommonOffset);	// D2Common.10700
static D2Common29CD		D2GetSuffix				= (D2Common29CD)		(0x58650 + CommonOffset);	// D2Common.10701
static D2Common29CE		D2AssignSuffix			= (D2Common29CE)		(0x58680 + CommonOffset);   // D2Common.10702
static D2Common29D3		D2TestFlags				= (D2Common29D3)		(0x58750 + CommonOffset);	// D2Common.10707
static D2Common29D4		D2SetFlags				= (D2Common29D4)		(0x58780 + CommonOffset);	// D2Common.10708
static D2Common29D5		D2GetFlags				= (D2Common29D5)		(0x587C0 + CommonOffset);	// D2Common.10709
static D2Common29D7		D2SetLodFlags			= (D2Common29D7)		(0x58810 + CommonOffset);	// D2Common.10711
static D2Common29DD		D2GetiLvl				= (D2Common29DD)		(0x58870 + CommonOffset);	// D2Common.10717
static D2Common29DE		D2SetiLvl				= (D2Common29DE)		(0x588B0 + CommonOffset);	// D2Common.10718
static D2Common29DF		D2GetPage				= (D2Common29DF)		(0x588E0 + CommonOffset);	// D2Common.10719
static D2Common29E0		D2SetPage				= (D2Common29E0)		(0x58900 + CommonOffset);	// D2Common.10720
static D2Common29EB		D2CheckItemType			= (D2Common29EB)		(0x5DCE0 + CommonOffset);	// D2Common.10731
static D2Common29EC		D2GetPlayerIDFromItem	= (D2Common29EC)		(0x5DEE0 + CommonOffset);	// D2Common.10732
static D2Common29ED		D2ItemSetClass			= (D2Common29ED)		(0x5DF60 + CommonOffset);	// D2Common.10733
static D2Common29EE		D2SetPlayerIDonItem		= (D2Common29EE)		(0x5E0A0 + CommonOffset);	// D2Common.10734
static D2Common29F3		D2GetClassfromItemCode	= (D2Common29F3)		(0x5E390 + CommonOffset);	// D2Common.10739
static D2Common29F4		D2CheckQuestItem		= (D2Common29F4)		(0x5E580 + CommonOffset);	// D2Common.10740
static D2Common29FA		D2GetItemCode			= (D2Common29FA)		(0x59370 + CommonOffset);	// D2Common.10746
static D2Common29FE		D2SetBodyLocFromComponent	= (D2Common29FE)	(0x59540 + CommonOffset);	// D2Common.10750
static D2Common29FF		D2GetItemType			= (D2Common29FF)		(0x595D0 + CommonOffset);	// D2Common.10751
static D2Common2A00		D2GetItemTypeFromNbr	= (D2Common2A00)		(0x59640 + CommonOffset);	// D2Common.10752
static D2Common2A04		D2CheckRequirements     = (D2Common2A04)		(0x59740 + CommonOffset);	// D2Common.10756
static D2Common2A07		D2CheckThrowable        = (D2Common2A07)		(0x5A530 + CommonOffset);	// D2Common.10759
static D2Common2A0D		D2CheckIfTwoHanded		= (D2Common2A0D)		(0x5A700 + CommonOffset);	// D2Common.10765
static D2Common2A0F		D2Stackable				= (D2Common2A0F)		(0x5A750 + CommonOffset);	// D2Common.10767
static D2Common2A10		D2Beltable				= (D2Common2A10)		(0x5A7A0 + CommonOffset);	// D2Common.10768
static D2Common2A13		D2CheckItemUsable		= (D2Common2A13)		(0x5AA00 + CommonOffset);	// D2Common.10771
static D2Common2A17     D2GetVendorTransactionCost    = (D2Common2A17)  (0x5CDE0 + CommonOffset);	// D2Common.10775
static D2Common2A18		D2GetNbrOfUdfItems		= (D2Common2A18)		(0x5FF00 + CommonOffset);	// D2Common.10776
static D2Common2A19		D2CheckRepairable       = (D2Common2A19)		(0x58A70 + CommonOffset);   // D2Common.10777
static D2Common2A2B		D2GetMaxStack			= (D2Common2A2B)		(0x5CE50 + CommonOffset);	// D2Common.10795
static D2Common2A32		D2GetItemBitfield		= (D2Common2A32)		(0x5DE10 + CommonOffset);	// D2Common.10802
static D2Common2A34		D2GetSpellIcon			= (D2Common2A34)		(0x5D0F0 + CommonOffset);	// D2Common.10804
static D2Common2A3A		D2HasInv				= (D2Common2A3A)		(0x5D360 + CommonOffset);	// D2Common.10810
static D2Common2A3B		D2CheckDurability		= (D2Common2A3B)		(0x5D390 + CommonOffset);	// D2Common.10811
static D2Common2A3D		D2GetStaffmod			= (D2Common2A3D)		(0x5D3F0 + CommonOffset);	// D2Common.10813
static D2Common2A40		D2GetSockets			= (D2Common2A40)		(0x5D580 + CommonOffset);	// D2Common.10816
static D2Common2A45		D2IsSocketFiller		= (D2Common2A45)		(0x5D9D0 + CommonOffset);	// D2Common.10821
static D2Common2A46		D2IsRuneWordComplete	= (D2Common2A46)		(0x5D9E0 + CommonOffset);	// D2Common.10822
static D2Common2A47		D2GetWeaponClass		= (D2Common2A47)		(0x5E410 + CommonOffset);	// D2Common.10823
static D2Common2A4A		D2CheckQuality			= (D2Common2A4A)		(0x5E550 + CommonOffset);	// D2Common.10826
static D2Common2A4E		D2CheckItemImbue		= (D2Common2A4E)		(0x5EEA0 + CommonOffset);	// D2Common.10830
static D2Common2A4F		D2CheckItemSocket		= (D2Common2A4F)		(0x5F260 + CommonOffset);	// D2Common.10831
static D2Common2A50		D2CheckItemPersonalize	= (D2Common2A50)		(0x5F080 + CommonOffset);	// D2Common.10832
static D2Common2A66		D2SetOrgPage			= (D2Common2A66)		(0x58980 + CommonOffset);	// D2Common.10854
static D2Common2A67		D2GetGemDesc			= (D2Common2A67)		(0x55430 + CommonOffset);	// D2Common.10855
static D2Common2A71		D2SetEtheral			= (D2Common2A71)		(0x558D0 + CommonOffset);	// D2Common.10865
static D2Common2A73		D2Common10867			= (D2Common2A73)		(0x559F0 + CommonOffset);	// D2Common.10867
static D2Common2A74		D2SetItemMod			= (D2Common2A74)		(0x55F90 + CommonOffset);	// D2Common.10868
static D2Common2A7C		D2SetVersion		    = (D2Common2A7C)		(0x600B0 + CommonOffset);	// D2Common.10876
static D2Common2A7D		D2GetRepairCost			= (D2Common2A7D)		(0x5F490 + CommonOffset);	// D2Common.10877 
static D2Common2A7F     D2_Common10879          = (D2Common2A7F)		(0x602B0 + CommonOffset);	// D2Common.10879
static D2Common2A80		D2IsEtheral				= (D2Common2A80)		(0x60340 + CommonOffset);	// D2Common.10880
static D2Common2A8C		D2UnitHoverMsg			= (D2Common2A8C)		(0x83BF0 + CommonOffset);	// D2Common.10892
static D2Common2A8D		D2FreeUnitMsgBox		= (D2Common2A8D)		(0x83C80 + CommonOffset);	// D2Common.10893
static D2Common2A9B		D2GetNPCTextMessage		= (D2Common2A9B)		(0x83850 + CommonOffset);	// D2Common.10907
static D2Common2AA0		D2SetStartSeed			= (D2Common2AA0)		(0x6EAB0 + CommonOffset);	// D2Common.10912
static D2Common2AA1		D2InitLowSeed			= (D2Common2AA1)		(0x6EAC0 + CommonOffset);	// D2Common.10913
static D2Common2AA8		D2RollSeed				= (D2Common2AA8)		(0x6EA80 + CommonOffset);	// D2Common.10920
static D2Common2ABA		D2GetSkillState			= (D2Common2ABA)		(0x6EB10 + CommonOffset);	// D2Common.10938
static D2Common2ABC		D2RefreshSkill			= (D2Common2ABC)		(0x6F8C0 + CommonOffset);	// D2Common.10940
static D2Common2ABD		D2RefreshPassiveSkills	= (D2Common2ABD)		(0x6FB40 + CommonOffset);	// D2Common.10941
static D2Common2AC2     D2InitPlayerSkills		= (D2Common2AC2)		(0x6FD10 + CommonOffset);	// D2Common.10946
static D2Common2AC5		D2GetSkill				= (D2Common2AC5)		(0x6FF40 + CommonOffset);	// D2Common.10949
static D2Common2AC6		D2GetPtSkill			= (D2Common2AC6)		(0x6FF80 + CommonOffset);	// D2Common.10950
static D2Common2AC7		D2RemoveSkill			= (D2Common2AC7)		(0x6FFD0 + CommonOffset);	// D2Common.10951
static D2Common2AC8		D2CreateSkill			= (D2Common2AC8)		(0x70320 + CommonOffset);	// D2Common.10952 D2GetSkillEx
static D2Common2AC9		D2SetSkillLevel			= (D2Common2AC9)		(0x704D0 + CommonOffset);	// D2Common.10953
static D2Common2ACB		D2GetSkillData          = (D2Common2ACB)		(0x708F0 + CommonOffset);	// D2Common.10955
static D2Common2ACD     D2GetSkillOwnerGUID     = (D2Common2ACD)		(0x708C0 + CommonOffset);	// D2Common.10957
static D2Common2AD1     D2SetLeftActiveSkill	= (D2Common2AD1)		(0x709A0 + CommonOffset);	// D2Common.10961
static D2Common2AD2		D2SetRightActiveSkill	= (D2Common2AD2)		(0x70A30 + CommonOffset);	// D2Common.10962 D2AddSkillToUnit
static D2Common2AD3		D2GetSkillIDFromPtSkill	= (D2Common2AD3)		(0x70AC0 + CommonOffset);	// D2Common.10963
static D2Common2AD4		D2SkillGetUseState      = (D2Common2AD4)		(0x70B70 + CommonOffset);	// D2Common.10964
static D2Common2AD6		D2_GetSkillsTXT			= (D2Common2AD6)		(0x71540 + CommonOffset);	// D2Common.10966
static D2Common2AD8		D2GetCurrentSkillLvl	= (D2Common2AD8)		(0x71700 + CommonOffset);	// D2Common.10968
static D2Common2ADE		D2SetSkillMode			= (D2Common2ADE)		(0x718B0 + CommonOffset);	// D2Common.10974
static D2Common2ADF		D2GetSkillMode			= (D2Common2ADF)		(0x718D0 + CommonOffset);	// D2Common.10975
static D2Common2AE0		D2GetSkillOffsetX		= (D2Common2AE0)		(0x719F0 + CommonOffset);	// D2Common.10976	
static D2Common2AE1		D2GetSkillOffsetY		= (D2Common2AE1)		(0x71A20 + CommonOffset);	// D2Common.10977
static D2Common2AE2		D2GetTargetUnitID		= (D2Common2AE2)		(0x71A50 + CommonOffset);	// D2Common.10978
static D2Common2AE4		D2SetOffsetXToSkill		= (D2Common2AE4)		(0x71AB0 + CommonOffset);	// D2Common.10980
static D2Common2AE5		D2SetOffsetYToSkill		= (D2Common2AE5)		(0x71AE0 + CommonOffset);	// D2Common.10981
static D2Common2AE6		D2SetUnitIDToSkill		= (D2Common2AE6)		(0x71B10 + CommonOffset);	// D2Common.10982
static D2Common2AE8		D2SetSkillFlag			= (D2Common2AE8)		(0x71B70 + CommonOffset);	// D2Common.10984
static D2Common2AE9		D2GetSkillFlag			= (D2Common2AE9)		(0x71BA0 + CommonOffset);	// D2Common.10985
static D2Common2AEA		D2GetSkillReq			= (D2Common2AEA)		(0x71BC0 + CommonOffset);	// D2Common.10986
static D2Common2AEB		D2GetCurrentSkillReq	= (D2Common2AEB)		(0x71C00 + CommonOffset);	// D2Common.10987
static D2Common2AEB		D2CheckPrevSkillEnabled	= (D2Common2AEC)		(0x71C80 + CommonOffset);	// D2Common.10988
static D2Common2AEB		D2CheckSkillStatReq		= (D2Common2AED)		(0x71FC0 + CommonOffset);	// D2Common.10989
static D2Common2AF7		D2GetHitChanceFromSkill = (D2Common2AF7)		(0x72110 + CommonOffset);	// D2Common.10999
static D2Common2AF9		D2GetManaCostFromSkills = (D2Common2AF9)		(0x72280 + CommonOffset);	// D2Common.11001
static D2Common2AFA		D2GetminDmgCalc			= (D2Common2AFA)		(0x72390 + CommonOffset);	// D2Common.11002
static D2Common2AFB		D2GetmaxDmgCalc			= (D2Common2AFB)		(0x725D0 + CommonOffset);	// D2Common.11003
static D2Common2AFC		D2GetEminCalc			= (D2Common2AFC)		(0x72810 + CommonOffset);	// D2Common.11004
static D2Common2AFD		D2GetEmaxCalc			= (D2Common2AFD)		(0x72B00 + CommonOffset);	// D2Common.11005
static D2Common2AFE		D2GetElen				= (D2Common2AFE)		(0x72CA0 + CommonOffset);	// D2Common.11006
static D2Common2B03		D2GetSkillQty			= (D2Common2B03)		(0x72F40 + CommonOffset);	// D2Common.11011
static D2Common2B04		D2SetSkillQty			= (D2Common2B04)		(0x72F70 + CommonOffset);	// D2Common.11012
static D2Common2B09		D2CheckIfCorpseConsumable = (D2Common2B09)		(0x733A0 + CommonOffset);	// D2Common.11017
// D2Common2B0E same function as D2CheckUnitInRange
static D2Common2B0D		D2CheckUnitInRange		= (D2Common2B0D)		(0x73520 + CommonOffset);	// D2Common.11019/11021/11022
static D2Common2B0F		D2GetWeaponMastery		= (D2Common2B0F)		(0x736D0 + CommonOffset);	// D2Common.11023
static D2Common2B16		D2AddGeneralSkill		= (D2Common2B16)		(0x717A0 + CommonOffset);   // D2Common.11030
static D2Common2B1A		D2RandSkillsPar1andPar2	= (D2Common2B1A)		(0x73B00 + CommonOffset);	// D2Common.11034
static D2Common2B32		D2CheckDemon			= (D2Common2B32)		(0x657D0 + CommonOffset);	// D2Common.11058
static D2Common2B33		D2CheckUndead			= (D2Common2B33)		(0x65830 + CommonOffset);	// D2Common.11059
static D2Common2B34		D2CheckBoss				= (D2Common2B34)		(0x658A0 + CommonOffset);	// D2Common.11060
static D2Common2B38		D2CheckUnitDead			= (D2Common2B38)		(0x65900 + CommonOffset);	// D2Common.11064
static D2Common2B3B		D2CheckHireling			= (D2Common2B3B)		(0x66730 + CommonOffset);	// D2Common.11067	
static D2Common2B4A		D2SetMercStats			= (D2Common2B4A)		(0x64C10 + CommonOffset);	// D2Common.11082
static D2Common2B4B		D2CalcMercResCost		= (D2Common2B4B)		(0x65220 + CommonOffset);	// D2Common.11083
static D2Common2B4E		D2GetHirelingUniqueID	= (D2Common2B4E)		(0x651C0 + CommonOffset);	// D2Common.11086
static D2Common2B50		D2GetUnitFromTargetXY	= (D2Common2B50)		(0x7C840 + CommonOffset);	// D2Common.11088
static D2Common2B63		D2GetQuestState			= (D2Common2B63)		(0x6E800 + CommonOffset);	// D2Common.11107
static D2Common2B64		D2SetQuestState			= (D2Common2B64)		(0x6E850 + CommonOffset);	// D2Common.11108
static D2Common2B65		D2ClearQuestState		= (D2Common2B65)		(0x6E890 + CommonOffset);	// D2Common.11109
static D2Common2B70		D2GetSkillLevelFromptMissile = (D2Common2B70)	(0x7A020 + CommonOffset);	// D2Common.11120
static D2Common2B71		D2SetMissilenTotalFrames = (D2Common2B71)		(0x7A0A0 + CommonOffset);	// D2Common.11121
static D2Common2B72		D2GetMissileMaxFrame	= (D2Common2B72)		(0x7A0E0 + CommonOffset);	// D2Common.11122
static D2Common2B73		D2SetMissilenCurrentFrame = (D2Common2B73)		(0x7A100 + CommonOffset);	// D2Common.11123
static D2Common2B74		D2GetCurrentFrameFromMissile = (D2Common2B74)	(0x7A140 + CommonOffset);	// D2Common.11124
static D2Common2B75		D2GetnFrameFromMissile  = (D2Common2B75)		(0x7A160 + CommonOffset);	// D2Common.11125
static D2Common2B77		D2GetSkillIDFromptMissile = (D2Common2B77)		(0x7A080 + CommonOffset);	// D2Common.11127
static D2Common2B7B		D2SetnStreamMissile		= (D2Common2B7B)		(0x7A2B0 + CommonOffset);	// D2Common.11131
static D2Common2B7D		D2SetnStreamRange		= (D2Common2B7D)		(0x7A300 + CommonOffset);	// D2Common.11133
static D2Common2B80		D2SetMissileActivateFrame = (D2Common2B80)		(0x7A390 + CommonOffset);	// D2Common.11136
static D2Common2B84		D2MissilesSetStatus		= (D2Common2B84)		(0x7A490 + CommonOffset);	// D2Common.11140
static D2Common2B85		D2MissilesGetStatus		= (D2Common2B85)		(0x7A4B0 + CommonOffset);	// D2Common.11141
static D2Common2B87		D2GetpCoordFromMissile	= (D2Common2B87)		(0x7A4F0 + CommonOffset);	// D2Common.11143
static D2Common2B8B		D2SetpHistory			= (D2Common2B8B)		(0x83E80 + CommonOffset);	// D2Common.11147
static D2Common2B90		D2VerifyWaypoint		= (D2Common2B90)		(0x83D90 + CommonOffset);	// D2Common.11152
static D2Common2BD1		D2MissileDmgPreCalc		= (D2Common2BD1)		(0x7A5B0 + CommonOffset);	// D2Common.11217
static D2Common2BD2		D2MissileDmg			= (D2Common2BD2)		(0x7B2E0 + CommonOffset);	// D2Common.11218
static D2Common2BEF		D2GetMonLvlFromLevels	= (D2Common2BEF)		(0x704F0 + CommonOffset);	// D2Common.11247
static D2Common2BFB		D2CreateUnit			= (D2Common2BFB)		(0x7D6B0 + CommonOffset);	// D2Common.11259
static D2Common2BFD		D2calcSourceDmg			= (D2Common2BFD)		(0x27BD0 + CommonOffset);	// D2Common.11261
static D2Common2C06		D2CreateCompoundStatArray = (D2Common2C06)		(0x79D20 + CommonOffset);	// D2Common.11270	
static D2Common2C0C		D2GetSkillCalc			= (D2Common2C0C)		(0x6F7E0 + CommonOffset);	// D2Common.11276
static D2Common2C1C		D2AssignProperty		= (D2Common2C1C)		(0x58160 + CommonOffset);	// D2Common.11292
static D2Common2C1D     D2GetChargedSkill       = (D2Common2C1D)		(0x526C0 + CommonOffset);	// D2Common.11293
static D2Common2C1F     D2SetItemCharges		= (D2Common2C1F)		(0x77A90 + CommonOffset);	// D2Common.11295
static D2Common2C24		D2GetItemCalc			= (D2Common2C24)		(0x58300 + CommonOffset);	// D2Common.11300
static D2Common2C27     D2CheckStateMask        = (D2Common2C27)		(0x75740 + CommonOffset);	// D2Common.11303
static D2Common6FD52410	D2ParseCubeInput		= (D2Common6FD52410)	(0x12410 + CommonOffset);	// D2Common.6FD52410
static D2Common6FD52910	D2ParseCubeOutput		= (D2Common6FD52910)	(0x12910 + CommonOffset);	// D2Common.6FD52910
static D2Common6FDB1130 D2CheckSkilleType		= (D2Common6FDB1130)	(0x71130 + CommonOffset);	// D2Common.6FDB1130
static D2Common6FDB1580 D2GetSkillLvlFromItems	= (D2Common6FDB1580)	(0x71580 + CommonOffset);	// D2Common.6FDB1580





//D2Game #6FC30000

static D2Game6FC33510	D2GetOffsetXFromUnit	 = (D2Game6FC33510)		(0x03510 + GameOffset); // D2Game.6FC33510
static D2Game6FC33540	D2GetOffsetYFromUnit	 = (D2Game6FC33540)		(0x03540 + GameOffset); // D2Game.6FC33540
static D2Game6FC33890	D2GetClientAct			 = (D2Game6FC33890)		(0x03890 + GameOffset); // D2Game.6FC33890
static D2Game6FC339A0	D2GetClientIndex		 = (D2Game6FC339A0)		(0x03890 + GameOffset); // D2Game.6FC339A0
static D2Game6FC33BE0	D2GetClientName			 = (D2Game6FC33BE0)		(0x03BE0 + GameOffset); // D2Game.6FC33BE0
static D2Game6FC34020	D2ClientAttachSaveFile	 = (D2Game6FC34020)		(0x04020 + GameOffset); // D2Game.6FC34020
static D2Game6FC34170	D2GetSaveGame			 = (D2Game6FC34170)		(0x04170 + GameOffset); // D2Game.6FC34170
static D2Game6FC34280	D2FreeSaveGame			 = (D2Game6FC34280)		(0x04280 + GameOffset); // D2Game.6FC34280
static D2Game6FC34300	D2SetClientSaveloadComplete = (D2Game6FC34280)		(0x04300 + GameOffset); // D2Game.6FC34300
static D2Game6FC345A0   D2ReturnPtClient504		 = (D2Game6FC345A0)		(0x045A0 + GameOffset); // D2Game.6FC345A0
static D2Game6FC34690   D2GetAccountname		 = (D2Game6FC34690)		(0x04690 + GameOffset); // D2Game.6FC34690
static D2Game6FC346A0	D2GetClientContainer	 = (D2Game6FC346A0)		(0x046A0 + GameOffset); // D2Game.6FC346A0
static D2Game6FC346B0	D2GetRealm				 = (D2Game6FC346B0)		(0x046B0 + GameOffset); // D2Game.6FC346B0
static D2Game6FC34840	D2SetServerEvent		 = (D2Game6FC34840)		(0x04840 + GameOffset); // D2Game.6FC34840
static D2Game6FC35170	D2EVENTS_GetEventFrame	 = (D2Game6FC35170)		(0x05170 + GameOffset); // D2Game.6FC35170
static D2Game6FC351B0	D2EventsQuickSetEvent	 = (D2Game6FC351B0)		(0x051B0 + GameOffset); // D2Game.6FC351B0
static D2Game6FC3C410	D2ChangeLevel			 = (D2Game6FC3C410)		(0x0C410 + GameOffset); // D2Game.6FC3C410
static D2Game6FC3C710	D2SendPacket			 = (D2Game6FC3C710)		(0x0C710 + GameOffset); // D2Game.6FC3C710
static D2Game6FC3C920	D2WarpClient			 = (D2Game6FC3C920)		(0x0C920 + GameOffset); // D2Game.6FC3C920
static D2Game6FC3DB50   D2SetClientSkillLevel	 = (D2Game6FC3DB50)		(0x0DB50 + GameOffset); // D2Game.6FC3DB50
static D2Game6FC3DBE0	D2UpdateClientSkillQty   = (D2Game6FC3DBE0)		(0x0DBE0 + GameOffset); // D2Game.6FC3DBE0
static D2Game6FC3E200	D2SetSummonIcon			 = (D2Game6FC3E200)		(0x0E200 + GameOffset); // D2Game.6FC3E200
static D2Game6FC3E9D0	D2UpdateClientItem		 = (D2Game6FC3E9D0)		(0x0E9D0 + GameOffset); // D2Game.6FC3E9D0
static D2Game6FC3EC20	D2SetClientUnitStat		 = (D2Game6FC3EC20)		(0x0EC20 + GameOffset); // D2Game.6FC3EC20
static D2Game6FC3EDC0	D2ChangeItemMode		 = (D2Game6FC3EDC0)		(0x0EDC0 + GameOffset); // D2Game.6FC3EDC0
static D2Game6FC3F3B0	D2SendHirelingOKMsg		 = (D2Game6FC3F3B0)		(0x0F3B0 + GameOffset); // D2Game.6FC3F3B0
static D2Game6FC3F520	UpdateTransitionToClient = (D2Game6FC3F520)		(0x0F520 + GameOffset); // D2Game.6FC3F520
static D2Game6FC3F9B0	D2MakeBodyActive		 = (D2Game6FC3F9B0)		(0x0F9B0 + GameOffset); // D2Game.6FC3F9B0
static D2Game6FC3FB30	D2AddRessurectCost	     = (D2Game6FC3FB30)		(0x0FB30 + GameOffset); // D2Game.6FC3FB30
static D2Game6FC3FC20	D2StatesSendStateOn		 = (D2Game6FC3FC20)		(0x0FC20 + GameOffset); // D2Game.6FC3FC20
static D2Game6FC40170	D2CreateUnitPath		 = (D2Game6FC40170)		(0x10170 + GameOffset); // D2Game.6FC40170
static D2Game6FC401F0	D2UpdateSummonAI		 = (D2Game6FC401F0)		(0x101F0 + GameOffset); // D2Game.6FC401F0
static D2Game6FC42310	D2AddGemEffect			 = (D2Game6FC42310)		(0x12310 + GameOffset); // D2Game.6FC42310
static D2Game6FC43340	D2PickItemFromGround	 = (D2Game6FC43340)		(0x13340 + GameOffset); // D2Game.6FC43340
static D2Game6FC44410	D2PlaceItem				 = (D2Game6FC44410)		(0x14410 + GameOffset); // D2Game.6FC44410
static D2Game6FC44A90   D2UpdateUnitItems        = (D2Game6FC44A90)		(0x14A90 + GameOffset); // D2Game.6FC44A90
// static D2Game6FC45060	D2EquipItemOnPlayer		 = (D2Game6FC45060)		(0x15060 + GameOffset); // D2Game.6FC45060 replaced by Items_EquipItemOnPlayer
static D2Game6FC45300	D2SetEquippedItemSkills  = (D2Game6FC45300)		(0x15300 + GameOffset); // D2Game.6FC45300
static D2Game6FC471F0	D2RemoveItem			 = (D2Game6FC471F0)		(0x171F0 + GameOffset); // D2Game.6FC471F0
static D2Game6FC47C90	D2UpdateSkillIcon		 = (D2Game6FC47C90)		(0x17C90 + GameOffset); // D2Game.6FC47C90
static D2Game6FC47D30	D2UseItem				 = (D2Game6FC47D30)		(0x17D30 + GameOffset); // D2Game.6FC47D30
static D2Game6FC48940	D2GameAddItemtoBelt		 = (D2Game6FC48940)		(0x18940 + GameOffset); // D2Game.6FC48940
static D2Game6FC49670	D2IdentifyItem			 = (D2Game6FC49670)		(0x19670 + GameOffset); // D2Game.6FC49670
static D2Game6FC497E0	D2ServerInsertSocket	 = (D2Game6FC497E0)		(0x197E0 + GameOffset); // D2Game.6FC497E0
static D2Game6FC4A4B0	D2DoKeyCheck			 = (D2Game6FC4A4B0)		(0x1A4B0 + GameOffset); // D2Game.6FC4A4B0
static D2Game6FC4AD80	D2AddItemsFromCorpse	 = (D2Game6FC4AD80)		(0x1AD80 + GameOffset); // D2Game.6FC4AD80
static D2Game6FC4B240	D2UpdatePlayerVitals	 = (D2Game6FC4B240)		(0x1B240 + GameOffset); // D2Game.6FC4A4B0
static D2Game6FC4B9D0	D2MoveItemToCursor		 = (D2Game6FC4B9D0)		(0x1B9D0 + GameOffset); // D2Game.6FC4B9D0
static D2Game6FC4BB90	D2UpdatePlayerItems		 = (D2Game6FC4BB90)		(0x1BB90 + GameOffset); // D2Game.6FC4BB90
static D2Game6FC4BD50	D2ResetCharges			 = (D2Game6FC4BD50)		(0x1BD50 + GameOffset); // D2Game.6FC4BD50
static D2Game6FC4BE80	D2ItemInit				 = (D2Game6FC4BE80)		(0x1BE80 + GameOffset); // D2Game.6FC4BE80
static D2Game6FC4BF00	D2FindRoomFromOffsetXY	 = (D2Game6FC4BF00)		(0x1BF00 + GameOffset); // D2Game.6FC4BF00
static D2Game6FC4E520	D2AddAutoMod			 = (D2Game6FC4E520)		(0x1E520 + GameOffset); // D2Game.6FC4E520
static D2Game6FC4ED80   D2CreateItem			 = (D2Game6FC4ED80)		(0x1ED80 + GameOffset); // D2Game.6FC4ED80 
static D2Game6FC4FEC0	D2DropAtUnit			 = (D2Game6FC4FEC0)		(0x1FEC0 + GameOffset); // D2Game.6FC4FEC0
static D2Game6FC501A0	D2CreateItem_Wrapper	 = (D2Game6FC501A0)		(0x201A0 + GameOffset); // D2Game.6FC501A0	
static D2Game6FC50320	D2PlayerDeleteItem		 = (D2Game6FC50320)		(0x20320 + GameOffset); // D2Game.6FC50320 
static D2Game6FC503A0   D2_Game6FC503A0          = (D2Game6FC503A0)		(0x203A0 + GameOffset); // D2Game.6FC503A0 
static D2Game6FC504F0	D2GetUnitLevel			 = (D2Game6FC504F0)		(0x204F0 + GameOffset); // D2Game.6FC504F0
static D2Game6FC51070	D2ItemDuplicate			 = (D2Game6FC51070)		(0x21070 + GameOffset); // D2Game.6FC51070
static D2Game6FC512C0	D26FC512C0				 = (D2Game6FC512C0)		(0x212C0 + GameOffset); // D2Game.6FC512C0
static D2Game6FC51310	D26FC51310				 = (D2Game6FC51310)		(0x21310 + GameOffset); // D2Game.6FC51310
static D2Game6FC51360	D2DropTC				 = (D2Game6FC51360)		(0x21360 + GameOffset); // D2Game.6FC51360
static D2Game6FC52260	D2ServerDropUnitOnGround = (D2Game6FC52260)		(0x22260 + GameOffset); // D2Game.6FC52260
static D2Game6FC52410	D2AssignStaffmod		 = (D2Game6FC52410)		(0x22410 + GameOffset); // D2Game.6FC52410
static D2Game6FC52920	D2GetAffixIndex			 = (D2Game6FC52920)		(0x22920 + GameOffset); // D2Game.6FC52920
static D2Game6FC54240	D2GetItemSuffixID		 = (D2Game6FC54240)		(0x24240 + GameOffset); // D2Game.6FC54240
static D2Game6FC552F0	D2MissileUnitInit		 = (D2Game6FC552F0)		(0x252F0 + GameOffset); // D2Game.6FC552F0
static D2Game6FC55360	D2ReleaseMissile		 = (D2Game6FC55360)		(0x25360 + GameOffset); // D2Game.6FC55360		
static D2Game6FC55F80	D2InitDamageMissileData  = (D2Game6FC55F80)		(0x25F80 + GameOffset); // D2Game.6FC55F80
static D2Game6FC56480	D2CalcMissileElemDmg	 = (D2Game6FC56480)		(0x26480 + GameOffset); // D2Game.6FC56480
static D2Game6FC568F0	D2LaunchCorpseExpMissile = (D2Game6FC568F0)		(0x268F0 + GameOffset); // D2Game.6FC568F0
static D2Game6FC56D50	D2_Game6FC56D50			 = (D2Game6FC56D50)		(0x26D50 + GameOffset); // D2Game.6FC56D50
static D2Game6FC5FAD0	D2_Game6FC5FAD0			 = (D2Game6FC5FAD0)		(0x2FAD0 + GameOffset); // D2Game.6FC5FAD0
static D2Game6FC60220	D2GetMonRegIdx			 = (D2Game6FC60220)		(0x30220 + GameOffset); // D2Game.6FC60220
static D2Game6FC60270	D2SetSummonerFlag		 = (D2Game6FC60270)		(0x30270 + GameOffset); // D2Game.6FC60270
static D2Game6FC602A0	D2InitMonster			 = (D2Game6FC602A0)		(0x302A0 + GameOffset); // D2Game.6FC602A0
static D2Game6FC60CD0	D2AI_DelayThink			 = (D2Game6FC60CD0)		(0x30CD0 + GameOffset); // D2Game.6FC60CD0
static D2Game6FC60E90	D2_D2Game6FC60E90		 = (D2Game6FC60E90)		(0x30E90 + GameOffset); // D2Game.6FC60E90
static D2Game6FC61270   D2RemoveHireling		 = (D2Game6FC61270)		(0x31270 + GameOffset); // D2Game.6FC61270
static D2Game6FC61490	D2SetUnitBaseStats		 = (D2Game6FC61490)		(0x31490 + GameOffset); // D2Game.6FC61490
static D2Game6FC61610	D2HireMerc				 = (D2Game6FC61610)		(0x31610 + GameOffset); // D2Game.6FC61610
static D2Game6FC61AB0	D2NPCInteract			 = (D2Game6FC61AB0)		(0x31AB0 + GameOffset); // D2Game.6FC61AB0
static D2Game6FC62E70   D2MonsterSetCritDmg		 = (D2Game6FC62E70)		(0x32E70 + GameOffset); // D2Game.6FC62E70
static D2Game6FC62F50	D2GameMaxHealth			 = (D2Game6FC62F50)		(0x32F50 + GameOffset); // D2Game.6FC62F50
static D2Game6FC66560	D2PresetUnit			 = (D2Game6FC66560)		(0x36560 + GameOffset); // D2Game.6FC66560
static D2Game6FC67F90	D2GetMonRegion			 = (D2Game6FC67F90)		(0x37F90 + GameOffset); // D2Game.6FC67F90
static D2Game6FC68630	D2CheckSkillTarget		 = (D2Game6FC68630)		(0x38630 + GameOffset); // D2Game.6FC68630	
static D2Game6FC68D70	D2SpawnSingleMonster	 = (D2Game6FC68D70)		(0x38D70 + GameOffset); // D2Game.6FC68D70
static D2Game6FC6A030	D2RandomUnit			 = (D2Game6FC6A030)		(0x3A030 + GameOffset); // D2Game.6FC6A030
static D2Game6FC6AC70	D2GetTypeFlag			 = (D2Game6FC6AC70)		(0x3AC70 + GameOffset); // D2Game.6FC6AC70
static D2Game6FC6AD10	D2GetUniqueNbr			 = (D2Game6FC6AD10)		(0x3AD10 + GameOffset); // D2Game.6FC6AD10
static D2Game6FC6AD50	MonUmod_Rndname_Init	 = (D2Game6FC6AD50)		(0x3AD50 + GameOffset); // D2Game.6FC6AD50
static D2Game6FC6AD90	MonUmod_AddHpXP_Init	 = (D2Game6FC6AD90)		(0x3AD90 + GameOffset); // D2Game.6FC6AD90
static D2Game6FC6AF70	D2CalcDivValue			 = (D2Game6FC6AF70)		(0x3AF70 + GameOffset); // D2Game.6FC6AF70	
static D2Game6FC6AFF0	MonUmod_AddLevel_Init	 = (D2Game6FC6AFF0)		(0x3AFF0 + GameOffset); // D2Game.6FC6AFF0
static D2Game6FC6B030	MonUmod_Champion_Init	 = (D2Game6FC6B030)		(0x3B030 + GameOffset); // D2Game.6FC6B030
static D2Game6FC6B210	MonUmod_Ghostly_Init	 = (D2Game6FC6B210)		(0x3B210 + GameOffset); // D2Game.6FC6B210
static D2Game6FC6B3A0	MonUmod_Fanatic_Init	 = (D2Game6FC6B3A0)		(0x3B3A0 + GameOffset); // D2Game.6FC6B3A0
static D2Game6FC6B3E0	MonUmod_Possessed_Init	 = (D2Game6FC6B3E0)		(0x3B3E0 + GameOffset); // D2Game.6FC6B3E0
static D2Game6FC6B4B0	MonUmod_Berserk_Init	 = (D2Game6FC6B4B0)		(0x3B4B0 + GameOffset); // D2Game.6FC6B4B0
static D2Game6FC6B5D0	MonUmod_AlwaysRun_Init	 = (D2Game6FC6B5D0)		(0x3B5D0 + GameOffset); // D2Game.6FC6B5D0
static D2Game6FC6B610	MonUmod_Resistance_Init	 = (D2Game6FC6B610)		(0x3B610 + GameOffset); // D2Game.6FC6B610
static D2Game6FC6B8C0	MonUmod_TeleHeal_Init	 = (D2Game6FC6B8C0)		(0x3B8C0 + GameOffset); // D2Game.6FC6B8C0
static D2Game6FC6BA70	MonUmod_Strong_Init		 = (D2Game6FC6BA70)		(0x3BA70 + GameOffset); // D2Game.6FC6BA70
static D2Game6FC6BB80	MonUmod_Fast_Init		 = (D2Game6FC6BB80)		(0x3BB80 + GameOffset); // D2Game.6FC6BB80
static D2Game6FC6BC10	MonUmod_FireDmg_Init	 = (D2Game6FC6BC10)		(0x3BC10 + GameOffset); // D2Game.6FC6BC10
static D2Game6FC6BDD0	MonUmod_ChargedBolt_Init = (D2Game6FC6BDD0)		(0x3BDD0 + GameOffset); // D2Game.6FC6BDD0
static D2Game6FC6BF90	MonUmod_Nova_Init 		 = (D2Game6FC6BF90)		(0x3BF90 + GameOffset); // D2Game.6FC6BF90
static D2Game6FC6C340	MonUmod_ManaSteal_Init	 = (D2Game6FC6C340)		(0x3C340 + GameOffset); // D2Game.6FC6C340
static D2Game6FC6C4F0	MonUmod_Amp_Dmg			 = (D2Game6FC6C4F0)		(0x3C4F0 + GameOffset); // D2Game.6FC6C4F0
static D2Game6FC6C710	MonUmod_FireExp_Mode	 = (D2Game6FC6C710)		(0x3C710 + GameOffset); // D2Game.6FC6C710
static D2Game6FC6C740	MonUmod_FireExp			 = (D2Game6FC6C740)		(0x3C740 + GameOffset); // D2Game.6FC6C740
static D2Game6FC6C9E0   MonUmod_goboom_callback  = (D2Game6FC6C9E0)		(0x3C9E0 + GameOffset); // D2Game.6FC6C9E0
static D2Game6FC6CAB0	MonUmod_lightningdeath_callback = (D2Game6FC6CAB0) (0x3CAB0 + GameOffset); // D2Game.6FC6CAB0
static D2Game6FC6CB40	MonUmod_chargebolt_callback = (D2Game6FC6CB40)	(0x3CB40 + GameOffset); // D2Game.6FC6CB40
static D2Game6FC6CD30	MonUmod_ChargedBolt		 = (D2Game6FC6CD30)		(0x3CD30 + GameOffset); // D2Game.6FC6CD30
static D2Game6FC6CD60	MonUmod_Cold_Callback	 = (D2Game6FC6CD60)		(0x3CD60 + GameOffset); // D2Game.6FC6CD60
static D2Game6FC6CDB0	MonUmod_Nova			 = (D2Game6FC6CDB0)		(0x3CDB0 + GameOffset); // D2Game.6FC6CDB0
static D2Game6FC6CE50	MonUmod_PartyDead_Mode	 = (D2Game6FC6CE50)		(0x3CE50 + GameOffset); // D2Game.6FC6CE50
static D2Game6FC6CF10	MonUmod_hireable_Init	 = (D2Game6FC6CF10)		(0x3CF10 + GameOffset); // D2Game.6FC6CF10
static D2Game6FC6CF90	MonUmod_hireable_attack  = (D2Game6FC6CF90)		(0x3CF90 + GameOffset); // D2Game.6FC6CF90
static D2Game6FC6D030	MonUmod_ScarabBolt_Mode	 = (D2Game6FC6D030)		(0x3D030 + GameOffset); // D2Game.6FC6D030
static D2Game6FC6D060	MonUmod_ScarabBolt		 = (D2Game6FC6D060)		(0x3D060 + GameOffset); // D2Game.6FC6D060
static D2Game6FC6D1C0	MonUmod_spectralhit_Init = (D2Game6FC6D1C0)		(0x3D1C0 + GameOffset); // D2Game.6FC6D1C0
static D2Game6FC6D440	MonUmod_spectralhit_attack = (D2Game6FC6D440)   (0x3D440 + GameOffset); // D2Game.6FC6D440
static D2Game6FC6D690	MonUmod_thief_misshit    = (D2Game6FC6D690)		(0x3D690 + GameOffset); // D2Game.6FC6D690
static D2Game6FC6D800	MonUmod_QuestCheck		 = (D2Game6FC6D800)		(0x3D800 + GameOffset); // D2Game.6FC6D800
static D2Game6FC6D8B0	MonUmod_poisonhit_Init	 = (D2Game6FC6D8B0)		(0x3D8B0 + GameOffset); // D2Game.6FC6D8B0
static D2Game6FC6DA40	MonUmod_multishot_attack = (D2Game6FC6DA40)		(0x3DA40 + GameOffset); // D2Game.6FC6DA40 
static D2Game6FC6DCB0	MonUmod_ChargedBolt_Mode = (D2Game6FC6DCB0)		(0x3DCB0 + GameOffset); // D2Game.6FC6DCB0
static D2Game6FC6DCE0	MonUmod_Nova_Mode		 = (D2Game6FC6DCE0)		(0x3DCE0 + GameOffset); // D2Game.6FC6DCE0
static D2Game6FC6DD20	MonUmod_AIAfterDeath_Mode = (D2Game6FC6DD20)	(0x3DD20 + GameOffset); // D2Game.6FC6DD20
static D2Game6FC6DDE0	MonUmod_ai_after_death_callback = (D2Game6FC6DDE0) (0x3DDE0 + GameOffset); // D2Game.6FC6DDE0
static D2Game6FC6DFA0	MonUmod_Shatter			 = (D2Game6FC6DFA0)		(0x3DFA0 + GameOffset); // D2Game.6FC6DFA0
static D2Game6FC6DFC0	MonUmod_killself_callback = (D2Game6FC6DFC0)	(0x3DFC0 + GameOffset); // D2Game.6FC6DFC0
static D2Game6FC6E070	MonUmod_spcdamage_Init	 = (D2Game6FC6E070)		(0x3E070 + GameOffset); // D2Game.6FC6E070
static D2Game6FC6E240	MonUmod_firespike_callback = (D2Game6FC6E240)	(0x3E240 + GameOffset); // D2Game.6FC6E240
static D2Game6FC6E390	MonUmod_Suicide_Mode	 = (D2Game6FC6E390)		(0x3E390 + GameOffset); // D2Game.6FC6E390
static D2Game6FC6E410	MonUmod_Suicide			 = (D2Game6FC6E410)		(0x3E410 + GameOffset); // D2Game.6FC6E410
static D2Game6FC6E700	MonUmod_Worms			 = (D2Game6FC6E700)		(0x3E700 + GameOffset); // D2Game.6FC6E700
static D2Game6FC6E730	MonUmod_AlwaysRun		 = (D2Game6FC6E730)		(0x3E730 + GameOffset); // D2Game.6FC6E730
static D2Game6FC6E780	BOSSES_IterateUMods		 = (D2Game6FC6E780)		(0x3E780 + GameOffset); // D2Game.6FC6E780
static D2Game6FC6E940	D2D2Game_6FC6E940		 = (D2Game6FC6E940)		(0x3E940 + GameOffset); // D2Game.6FC6E940
static D2Game6FC6F440	D2SpawnMinions			 = (D2Game6FC6F440)		(0x3F440 + GameOffset); // D2Game.6FC6F440
static D2Game6FC6F690	D2SpawnSuperUnique		 = (D2Game6FC6F690)		(0x3F690 + GameOffset); // D2Game.6FC6F690
//static D2Game6FC6F690_2	D2SpawnSuperUniqueV2	 = (D2Game6FC6F690_2)	(0x3F690 + GameOffset); // D2Game.6FC6F690
static D2Game6FC6FF10	D2SetMonUMod			 = (D2Game6FC6FF10)		(0x3FF10 + GameOffset); // D2Game.6FC6FF10
static D2Game6FC75B40	D2SpawnTrapMonster		 = (D2Game6FC75B40)		(0x45B40 + GameOffset); // D2Game.6FC75B40
static D2Game6FC70600	D2ObjectInit			 = (D2Game6FC70600)		(0x40600 + GameOffset); // D2Game.6FC70600
static D2Game6FC76030	D2OperateFN4			 = (D2Game6FC76030)		(0x46030 + GameOffset); // D2Game.6FC76030
static D2Game6FC764B0	D2SetTrapCallback		 = (D2Game6FC764B0)		(0x464B0 + GameOffset); // D2Game.6FC764B0
static D2Game6FC766B0	D2SHRINE_RestoreLife	 = (D2Game6FC766B0)		(0x466B0 + GameOffset); // D2Game.6FC766B0	
static D2Game6FC766F0	D2SHRINE_RestoreMana	 = (D2Game6FC766F0)		(0x466F0 + GameOffset); // D2Game.6FC766F0	
static D2Game6FC76730	D2SHRINE_Refreshing		 = (D2Game6FC76730)		(0x46730 + GameOffset); // D2Game.6FC76730
static D2Game6FC76790	D2SHRINE_LifeExchange	 = (D2Game6FC76790)		(0x46790 + GameOffset); // D2Game.6FC76790
static D2Game6FC767F0	D2SHRINE_ManaExchange	 = (D2Game6FC767F0)		(0x467F0 + GameOffset); // D2Game.6FC767F0
static D2Game6FC76850	D2SHRINE_Enirhs			 = (D2Game6FC76850)		(0x46850 + GameOffset); // D2Game.6FC767F0	
static D2Game6FC76880	D2SHRINE_Portal			 = (D2Game6FC76880)		(0x46880 + GameOffset); // D2Game.6FC767F0
static D2Game6FC76910	D2SHRINE_Gem			 = (D2Game6FC76910)		(0x46910 + GameOffset); // D2Game.6FC76910
static D2Game6FC76A60	D2SHRINE_UpgradeGem		 = (D2Game6FC76A60)		(0x46A60 + GameOffset); // D2Game.6FC76910
static D2Game6FC76BC0	D2SHRINE_Storm			 = (D2Game6FC76BC0)		(0x46BC0 + GameOffset); // D2Game.6FC76BC0
static D2Game6FC76ED0	D2SHRINE_Warping		 = (D2Game6FC76ED0)		(0x46ED0 + GameOffset); // D2Game.6FC76BC0
static D2Game6FC770D0	D2SHRINE_Exploding		 = (D2Game6FC770D0)		(0x470D0 + GameOffset); // D2Game.6FC770D0
static D2Game6FC773B0	D2SHRINE_Poison			 = (D2Game6FC773B0)		(0x473B0 + GameOffset); // D2Game.6FC773B0
static D2Game6FC77690	D2SHRINE_Combat			 = (D2Game6FC77690)		(0x47690 + GameOffset); // D2Game.6FC77690
static D2Game6FC77750	D2SHRINE_StatPercentValue = (D2Game6FC77750)	(0x47750 + GameOffset); // D2Game.6FC77750
static D2Game6FC779C0	D2SHRINE_Stamina		 = (D2Game6FC779C0)		(0x479C0 + GameOffset); // D2Game.6FC779C0
static D2Game6FC77AE0	D2SHRINE_ApplyStat		 = (D2Game6FC77AE0)		(0x47AE0 + GameOffset); // D2Game.6FC77AE0
static D2Game6FC77BA0	D2SHRINE_Allskill		 = (D2Game6FC77BA0)		(0x47BA0 + GameOffset); // D2Game.6FC77BA0
static D2Game6FC789C0	D2PickupItemFromObject   = (D2Game6FC789C0)		(0x489C0 + GameOffset); // D2Game.6FC789C0
static D2Game6FC7A140	D2DropTCLine			 = (D2Game6FC7A140)		(0x4A140 + GameOffset); // D2Game.6FC7A140
static D2Game6FC7A220	D2DropChestTC			 = (D2Game6FC7A220)		(0x4A220 + GameOffset); // D2Game.6FC7A220
static D2Game6FC7AA20	D2GetTrapMonsterID		 = (D2Game6FC7AA20)		(0x4AA20 + GameOffset); // D2Game.6FC7AA20
static D2Game6FC7CA10	D2MergeUnitStatsList	 = (D2Game6FC7CA10)		(0x4CA10 + GameOffset); // D2Game.6FC7CA10
static D2Game6FC7B550	D2InitUnit				 = (D2Game6FC7B550)		(0x4B550 + GameOffset); // D2Game.6FC7B550
static D2Game6FC7BD50	D2CheckInventorySpace	 = (D2Game6FC7BD50)		(0x4BD50 + GameOffset); // D2Game.6FC7BD50
static D2Game6FC7DBF0	D2Game_6FC7DBF0			 = (D2Game6FC7DBF0)		(0x4DBF0 + GameOffset); // D2Game.6FC7DBF0
static D2Game6FC7C7B0	D2GetAttackerWeapon		 = (D2Game6FC7C7B0)		(0x4C7B0 + GameOffset); // D2Game.6FC7C7B0
static D2Game6FC7C900	D2CheckAttackerWeapon	 = (D2Game6FC7C900)		(0x4C900 + GameOffset); // D2Game.6FC7C900
static D2Game6FC7D260	D2_Game6FC7D260			 = (D2Game6FC7D260)		(0x4D260 + GameOffset); // D2Game.6FC7D260
static D2Game6FC7D7A0	D2PETSAdd				 = (D2Game6FC7D7A0)		(0x4D7A0 + GameOffset); // D2Game.6FC7D7A0
static D2Game6FC7DF40	D2RefreshSkills			 = (D2Game6FC7DF40)		(0x4DF40 + GameOffset); // D2Game.6FC7DF40
static D2Game6FC7E7C0	D2UpdateStatHireling	 = (D2Game6FC7E7C0)		(0x4E7C0 + GameOffset); // D2Game.6FC7E7C0
static D2Game6FC7E8B0	D2GetPetFromPlayer		 = (D2Game6FC7E8B0)		(0x4E8B0 + GameOffset); // D2Game.6FC7E8B0
static D2Game6FC7EDF0	D2ServerSetStat			 = (D2Game6FC7EDF0)		(0x4EDF0 + GameOffset); // D2Game.6FC7EDF0
static D2Game6FC802F0	D2RemoveItemFromBodyLoc	 = (D2Game6FC802F0)		(0x502F0 + GameOffset); // D2Game.6FC802F0
static D2Game6FC80F80   D2RegenHealthStaMana     = (D2Game6FC80F80)		(0x50F80 + GameOffset); // D2Game.6FC80F80
static D2Game6FC817D0	D2WarpServer			 = (D2Game6FC817D0)		(0x517D0 + GameOffset); // D2Game.6FC817D0
static D2Game6FC81A00	D2SetStartMode			 = (D2Game6FC81A00)		(0x51A00 + GameOffset); // D2Game.6FC81A00
static D2Game6FC822D0	D2SendStat				 = (D2Game6FC822D0)		(0x522D0 + GameOffset); // D2Game.6FC822D0
static D2Game6FC867C0	D2PlayerChangeAct		 = (D2Game6FC867C0)		(0x567C0 + GameOffset); // D2Game.6FC867C0
static D2Game6FC898F0	D2PlayerSetItemsOnGameInit = (D2Game6FC898F0)	(0x598F0 + GameOffset); // D2Game.6FC898F0
static D2Game6FC8A500	D2_SaveFile				 = (D2Game6FC8A500)		(0x5A500 + GameOffset); // D2Game.6FC8A500
static D2Game6FC8C9D0	D2_LoadFile				 = (D2Game6FC8C9D0)		(0x5C9D0 + GameOffset); // D2Game.6FC8C9D0
static D2Game6FC8D880	D2SaveIronGolem			 = (D2Game6FC8D880)		(0x5D880 + GameOffset); // D2Game.6FC8D880
static D2Game6FC8D940	D2CreateSaveFile		 = (D2Game6FC8D940)		(0x5D940 + GameOffset); // D2Game.6FC8D940
static D2Game6FC8FE40	D2GetRndItemFromType	 = (D2Game6FC8FE40)		(0x5FE40 + GameOffset); // D2Game.6FC8FE40
static D2Game6FC92890   D2PayRepairCost			 = (D2Game6FC92890)     (0x62890 + GameOffset); // D2Game.6FC92890
static D2Game6FC937A0	D2_UnknownCheck			 = (D2Game6FC937A0)		(0x637A0 + GameOffset); // D2Game.6FC937A0
static D2Game6FC93B90	D2GetQuestDataFromQuestID = (D2Game6FC93B90)		(0x63B90 + GameOffset); // D2Game.6FC93B90
static D2Game6FC93DC0	D2QuestInit				 = (D2Game6FC93DC0)		(0x63DC0 + GameOffset); // D2Game.6FC93DC0			
static D2Game6FC94520	D2Game_6FC94520			 = (D2Game6FC94520)		(0x64520 + GameOffset); // D2Game.6FC94520
static D2Game6FC95490	D2QuestActiveCycler		 = (D2Game6FC95490)		(0x65490 + GameOffset); // D2Game.6FC95490
static D2Game6FC95DF0	D2SpawnItem				 = (D2Game6FC95DF0)		(0x65DF0 + GameOffset); // D2Game.6FC95DF0
static D2Game6FC960C0	D2HirelingChangeAct		 = (D2Game6FC960C0)		(0x660C0 + GameOffset); // D2Game.6FC960C0
static D2Game6FC942D0	D2QuestEventCallback	 = (D2Game6FC942D0)		(0x642D0 + GameOffset); // D2Game.6FC942D0
static D2Game6FC94CF0	D2SetQuestStateWrapper	 = (D2Game6FC94CF0)		(0x64CF0 + GameOffset); // D2Game.6FC94CF0
static D2Game6FC97400	D2CheckifLevelBlocked	 = (D2Game6FC97400)		(0x67400 + GameOffset); // D2Game.6FC97400
static D2Game6FC98F50	D2AddClientItem			 = (D2Game6FC98F50)		(0x68F50 + GameOffset); // D2Game.6FC98F50
static D2Game6FC96E20	D2SendQuestScrollPacket  = (D2Game6FC96E20)		(0x66E20 + GameOffset); // D2Game.6FC96E20
static D2Game6FCBA8E0	D2CheckLootCorpse		 = (D2Game6FCBA8E0)		(0x8A8E0 + GameOffset); // D2Game.6FCBA8E0
static D2Game6FCBAEE0	D2ForceSpawn			 = (D2Game6FCBAEE0)		(0x8AEE0 + GameOffset); // D2Game.6FCBAEE0
static D2Game6FCBB440	D2WarpPlayer			 = (D2Game6FCBB440)		(0x8B440 + GameOffset); // D2Game.6FCBB440
static D2Game6FCBB630	D2ModifySeedValues		 = (D2Game6FCBB630)		(0x8B630 + GameOffset); // D2Game.6FCBB630
static D2Game6FCBB6C0	D2SpawnBody				 = (D2Game6FCBB6C0)		(0x8B6C0 + GameOffset); // D2Game.6FCBB6C0
//static D2Game6FCBB6C0_2	D2SpawnBodyV2			 = (D2Game6FCBB6C0_2)	(0x8B6C0 + GameOffset); // D2Game.6FCBB6C0
static D2Game6FCBBB00	D2GetServerUnit			 = (D2Game6FCBBB00)		(0x8BB00 + GameOffset); // D2Game.6FCBBB00
static D2Game6FCBBB70	D2GetMissileOwner		 = (D2Game6FCBBB70)		(0x8BB70 + GameOffset); // D2Game.6FCBBB70
static D2Game6FCBC280   D2AddClientToPlayerData	 = (D2Game6FCBC280)		(0x8C280 + GameOffset); // D2Game.6FCBC280
static D2Game6FCBC2E0	D2GetClient				 = (D2Game6FCBC2E0)		(0x8C2E0 + GameOffset); // D2Game.6FCBC2E0
static D2Game6FCBC480	D2FireSound				 = (D2Game6FCBC480)		(0x8C480 + GameOffset); // D2Game.6FCBC480
static D2Game6FCBC7E0   D2GetTargetUnit			 = (D2Game6FCBC7E0)		(0x8C7E0 + GameOffset); // D2Game.6FCBC7E0
static D2Game6FCBC900	D2GetTargetUnitWrapper	 = (D2Game6FCBC900)		(0x8C900 + GameOffset); // D2Game.6FCBC900
static D2Game6FCBBCB0	D2SyncUnitWithClient	 = (D2Game6FCBBCB0)		(0x8BCB0 + GameOffset); // D2Game.6FCBBCB0
static D2Game6FCBCB30	D2GetDestObjUnit		 = (D2Game6FCBCB30)		(0x8CB30 + GameOffset); // D2Game.6FCBCB30
static D2Game6FCBCD70	D2IteratePlayers		 = (D2Game6FCBCD70)		(0x8CD70 + GameOffset); // D2Game.6FCBCD70
static D2Game6FCBD4D0   D2GetInteractUnit		 = (D2Game6FCBD4D0)		(0x8D4D0 + GameOffset); // D2Game.6FCBD4D0
static D2Game6FCBD730	D2GetptGamefromUnit		 = (D2Game6FCBD730)		(0x8D730 + GameOffset); // D2Game.6FCBD730
static D2Game6FCBD7F0	D2CheckIfSparklyChest	 = (D2Game6FCBD7F0)		(0x8D7F0 + GameOffset); // D2Game.6FCBD7F0
static D2Game6FCBD8B0	D2_Game6FCBD8B0			 = (D2Game6FCBD8B0)		(0x8D8B0 + GameOffset); // D2Game.6FCBD8B0
static D2Game6FCBD900	D2CheckTargetUnitStatus	 = (D2Game6FCBD900)		(0x8D900 + GameOffset); // D2Game.6FCBD900
static D2Game6FCBDC60	D2CheckOwnerFromTarget	 = (D2Game6FCBDC60)		(0x8DC60 + GameOffset); // D2Game.6FCBDC60
static D2Game6FCBDD30	D2AI_SetAlignment		 = (D2Game6FCBDD30)		(0x8DD30 + GameOffset); // D2Game.6FCBDD30
static D2Game6FCBDFE0	D2WarpCharacter			 = (D2Game6FCBDFE0)		(0x8DFE0 + GameOffset); // D2Game.6FCBDFE0	
static D2Game6FCBF400	D2CheckMonType			 = (D2Game6FCBF400)		(0x8F400 + GameOffset); // D2Game.6FCBF400	
static D2Game6FCBE420	D2CalcPhysicalDmg		 = (D2Game6FCBE420)		(0x8E420 + GameOffset); // D2Game.6FCBDFE0	
static D2Game6FCBFE90	D2CalcMissileDamage		 = (D2Game6FCBFE90)		(0x8FE90 + GameOffset); // D2Game.6FCBFE90
static D2Game6FCC0940	D2SetColdState			 = (D2Game6FCC0940)		(0x90940 + GameOffset); // D2Game.6FCC0940
static D2Game6FCC0F10	D2KillMonster			 = (D2Game6FCC0F10)		(0x90F10 + GameOffset); // D2Game.6FCC0F10
static D2Game6FCC1260	D2ExecuteMissileDamage	 = (D2Game6FCC1260)		(0x91260 + GameOffset); // D2Game.6FCC1260
static D2Game6FCC1AC0	D2ExecuteMeleeDamage	 = (D2Game6FCC1AC0)		(0x91AC0 + GameOffset); // D2Game.6FCC1AC0
static D2Game6FCC1D70	D2GetptDamageData		 = (D2Game6FCC1D70)		(0x91D70 + GameOffset); // D2Game.6FCC1D70
static D2Game6FCC2300	D2ProcessAttack			 = (D2Game6FCC2300)		(0x92300 + GameOffset); // D2Game.6FCC2300
static D2Game6FCC2420	D2CalcMeleeDamage		 = (D2Game6FCC2420)		(0x92420 + GameOffset); // D2Game.6FCC2420
static D2Game6FCC2C70   D2MonsterDeath			 = (D2Game6FCC2C70)     (0x92C70 + GameOffset); // D2Game.6FCC2C70
static D2Game6FCC34A0	D2SetExperienceFromCorpse = (D2Game6FCC34A0)	(0x934A0 + GameOffset); // D2Game.6FCC34A0
static D2Game6FCC36D0	D2SetAuraTargetEvent	 = (D2Game6FCC36D0)		(0x936D0 + GameOffset); // D2Game.6FCC36D0
static D2Game6FCC3790	D2ItemEvent				 = (D2Game6FCC3790)		(0x93790 + GameOffset); // D2Game.6FCC3790
static D2Game6FCC58E0	D2SendUnitStates		 = (D2Game6FCC58E0)		(0x958E0 + GameOffset); // D2Game.6FCC58E0
static D2Game6FCC5F00	D2SendState				 = (D2Game6FCC5F00)		(0x95F00 + GameOffset); // D2Game.6FCC5F00
static D2Game6FCC6080	D2UpdateUnit			 = (D2Game6FCC6080)		(0x96080 + GameOffset); // D2Game.6FCC6080
static D2Game6FCC6270	D2SetUnitFlag			 = (D2Game6FCC6270)		(0x96270 + GameOffset); // D2Game.6FCC6270
static D2Game6FCC7FA0	D2BuyHireling			 = (D2Game6FCC7FA0)		(0x97FA0 + GameOffset); // D2Game.6FCC7FA0
static D2Game6FCCAF30	D2Game_6FCCAF30			 = (D2Game6FCCAF30)		(0x9AF30 + GameOffset); // D2Game.6FCCAF30	
static D2Game6FCCB910	D2_Game6FCCB910			 = (D2Game6FCCB910)		(0x9B910 + GameOffset); // D2Game.6FCCB910
static D2Game6FCCC7C0	D2RefreshVendors		 = (D2Game6FCCC7C0)		(0x9C7C0 + GameOffset); // D2Game.6FCCC7C0
static D2Game6FCCEE40	D2Game_6FCCEE40			 = (D2Game6FCCEE40)		(0x9EE40 + GameOffset); // D2Game.6FCCEE40
static D2Game6FCCEEF0	D2_AI_GetCmdCurrent		 = (D2Game6FCCEEF0)		(0x9EEF0 + GameOffset); // D2Game.6FCCEEF0
static D2Game6FCCEF70   D2CreatepCmdCurrent		 = (D2Game6FCCEF70)		(0x9EF70 + GameOffset); // D2Game.6FCCEF70
static D2Game6FCCF050	D2Game_6FCCF050			 = (D2Game6FCCF050)		(0x9F050 + GameOffset); // D2Game.6FCCF050
static D2Game6FCCF270	D2SetAIInfo				 = (D2Game6FCCF270)		(0x9F270 + GameOffset); // D2Game.6FCCF270
static D2Game6FCCF320	D2GetMinionOwner		 = (D2Game6FCCF320)		(0x9F320 + GameOffset); // D2Game.6FCCF320
static D2Game6FCCF5C0	D2_Game6FCCF5C0			 = (D2Game6FCCF5C0)		(0x9F5C0 + GameOffset); // D2Game.6FCCF5C0
static D2Game6FCCFD40	D2GetNextUnitFromPet     = (D2Game6FCCFD40)		(0x9FD40 + GameOffset); // D2Game.6FCCFD40
static D2Game6FCCFD70	D2CycleQuest_Wrapper	 = (D2Game6FCCFD70)		(0x9FD70 + GameOffset); // D2Game.6FCCFD70
static D2Game6FCCFEA0	D2_AI_Attack			 = (D2Game6FCCFEA0)		(0x9FEA0 + GameOffset); // D2Game.6FCCFEA0
static D2Game6FCCFF20	D2_Ressurect_Target		 = (D2Game6FCCFF20)		(0x9FF20 + GameOffset); // D2Game.6FCCFF20
static D2Game6FCCFFB0	D2MonUseSkill			 = (D2Game6FCCFFB0)		(0x9FFB0 + GameOffset); // D2Game.6FCCFFB0
static D2Game6FCD00A0	D2_AI_Stall_Time		 = (D2Game6FCD00A0)		(0xA00A0 + GameOffset); // D2Game.6FCD00A0
static D2Game6FCD01B0	D2_AI_Change_Velocity	 = (D2Game6FCD01B0)		(0xA01B0 + GameOffset); // D2Game.6FCD01B0
static D2Game6FCD0220	D2_AI_Approach2_Target	 = (D2Game6FCD0220)		(0xA0220 + GameOffset); // D2Game.6FCD0220
static D2Game6FCD03F0	D2_AI_Approach_Target	 = (D2Game6FCD03F0)		(0xA03F0 + GameOffset); // D2Game.6FCD03F0
static D2Game6FCD0410	D2_AI_Set_Walk_Path		 = (D2Game6FCD0410)		(0xA0410 + GameOffset); // D2Game.6FCD0410
static D2Game6FCD0530	D2_AI_Follow_Target		 = (D2Game6FCD0530)		(0xA0530 + GameOffset); // D2Game.6FCD0530
static D2Game6FCD0560	D2_AI_Circle_Target		 = (D2Game6FCD0560)		(0xA0560 + GameOffset); // D2Game.6FCD0560
static D2Game6FCD0840	D2_AI_Random_Walk		 = (D2Game6FCD0840)		(0xA0840 + GameOffset); // D2Game.6FCD0840	
static D2Game6FCD09D0	D2_AI_Walk_Away_From_Target = (D2Game6FCD09D0)	(0xA09D0 + GameOffset); // D2Game.6FCD09D0
static D2Game6FCD0E80	D2_AI_Walk_Away_From_Enemy = (D2Game6FCD0E80)	(0xA0E80 + GameOffset); // D2Game.6FCD0E80
static D2Game6FCE2BA0	D2_AI_PetMove			 = (D2Game6FCE2BA0)		(0xB2BA0 + GameOffset); // D2Game.6FCE2BA0
static D2Game6FCE3740	D2_AI_Melee_Decision	 = (D2Game6FCE3740)		(0xB3740 + GameOffset); // D2Game.6FCE3740
static D2Game6FCE34E0	D2RangeDecision			 = (D2Game6FCE34E0)		(0xB34E0 + GameOffset); // D2Game.6FCE34E0
static D2Game6FCF07D0	D2AI_Init				 = (D2Game6FCF07D0)		(0xC07D0 + GameOffset); // D2Game.6FCF07D0
static D2Game6FCF0E40	D2TeleHealSkillUse		 = (D2Game6FCF0E40)		(0xC0E40 + GameOffset); // D2Game.6FCF0E40
static D2Game6FCF1210	D2GetDistanceFromUnit	 = (D2Game6FCF1210)		(0xC1210 + GameOffset); // D2Game.6FCF1210
static D2Game6FCF13B0	D2_AI_CheckRangeFromXY   = (D2Game6FCF13B0)		(0xC13B0 + GameOffset); // D2Game.6FCF13B0
static D2Game6FCF14D0	D2_AI_CheckWalkablePath  = (D2Game6FCF14D0)		(0xC14D0 + GameOffset); // D2Game.6FCF14D0
static D2Game6FCF1E80	D2_Game6FCF1E80			 = (D2Game6FCF1E80)		(0xC1E80 + GameOffset); // D2Game.6FCF1E80
static D2Game6FCF2110	D2GetNextUnit			 = (D2Game6FCF2110)		(0xC2110 + GameOffset); // D2Game.6FCF2110
static D2Game6FCF2CC0	D2FindNearPet			 = (D2Game6FCF2CC0)		(0xC2CC0 + GameOffset); // D2Game.6FCF2CC0
static D2Game6FCF2E20	D2_Game6FCF2E20			 = (D2Game6FCF2E20)		(0xC2E20 + GameOffset); // D2Game.6FCF2E20
static D2Game6FCF2E70	D2_Check_AI_State		 = (D2Game6FCF2E70)		(0xC2E70 + GameOffset); // D2Game.6FCF2E70
static D2Game6FCF2F30	D2SetFearStateCurse		 = (D2Game6FCF2F30)		(0xC2F30 + GameOffset); // D2Game.6FCF2F30
static D2Game6FC7EC00   D2LevelupUnit			 = (D2Game6FC7EC00)		(0x4EC00 + GameOffset); // D2Game.6FC7EC00
static D2Game6FC7EA50   D2SetPlayerStartStats	 = (D2Game6FC7EA50)		(0x4EA50 + GameOffset); // D2Game.6FC7EA50
static D2Game6FCBD820   D2CheckPlayerInteracting = (D2Game6FCBD820)		(0x8D820 + GameOffset); // D2Game.6FCBD820
static D2Game6FCF8330	D2LaunchShockFieldMissile = (D2Game6FCF8330)	(0xC8330 + GameOffset); // D2Game.6FCF8330
static D2Game6FCF5E20	D2GetMissileIDFromSkillID = (D2Game6FCF5E20)    (0xC5E20 + GameOffset); // D2Game.6FCF5E20
static D2Game6FCF5EE0	D2GetNBRMissilesFromSkill = (D2Game6FCF5EE0)	(0xC5EE0 + GameOffset); // D2Game.6FCF5EE0
static D2Game6FCFBE80	SrvDoFunc9				 = (D2Game6FCFBE80)		(0xCBE80 + GameOffset); // D2Game.6FCFBE80
static D2Game6FCF6D70	D2FireChaosMissile		 = (D2Game6FCF6D70)		(0xC6D70 + GameOffset); // D2Game.6FCF6D70
static D2Game6FCFD3C0	D2_Game6FCFD3C0			 = (D2Game6FCFD3C0)		(0xCD3C0 + GameOffset); // D2Game.6FCFD3C0
static D2Game6FCFD9B0	SrvDoFunc78				 = (D2Game6FCFD9B0)		(0xCD9B0 + GameOffset); // D2Game.6FCFD9B0
static D2Game6FCFE0E0	D2SetAurastatsFromSkills = (D2Game6FCFE0E0)		(0xCE0E0 + GameOffset); // D2Game.6FCFE0E0
static D2Game6FCFE200	D2SetPassiveStatsFromSkills = (D2Game6FCFE200)  (0xCE200 + GameOffset); // D2Game.6FCFE200
static D2Game6FCFEDD0	D2Game_6FCFEDD0			 = (D2Game6FCFEDD0)		(0xCEDD0 + GameOffset); // D2Game.6FCFEDD0
static D2Game6FCFF2E0   D2ReleaseRabiesMissile	 = (D2Game6FCFF2E0)		(0xCF2E0 + GameOffset); // D2Game.6FCFF2E0
static D2Game6FCFB070	D2Game_6FCFB070			 = (D2Game6FCFB070)		(0xCB070 + GameOffset); // D2Game.6FCFB070
static D2Game6FD00370	D2CalcMeleeDamageWithcheck = (D2Game6FD00370)	(0xD0370 + GameOffset); // D2Game.6FD00370
static D2Game6FD025E0   D2ConsumeBloodMana       = (D2Game6FD025E0)		(0xD25E0 + GameOffset); // D2Game.6FD025E0
static D2Game6FD02E10	D2AddStatOverTime		 = (D2Game6FD02E10)		(0xD2E10 + GameOffset); // D2Game.6FD02E10
static D2Game6FD03E40	D2UseItemFunc			 = (D2Game6FD03E40)		(0xD3E40 + GameOffset); // D2Game.6FD03E40
static D2Game6FD0C2E0	D2SetSummonResistance	 = (D2Game6FD0C2E0)		(0xDC2E0 + GameOffset); // D2Game.6FD0C2E0
static D2Game6FD0C3A0	D2SetUnitComponent		 = (D2Game6FD0C3A0)		(0xDC3A0 + GameOffset); // D2Game.6FD0C3A0
static D2Game6FD0C530	D2SetSummonPassiveStats  = (D2Game6FD0C530)		(0xDC530 + GameOffset); // D2Game.6FD0C530
static D2Game6FD0CB10	D2SetSummonBaseStats	 = (D2Game6FD0CB10)		(0xDCB10 + GameOffset); // D2Game.6FD0CB10
static D2Game6FD0FE50	D2SetTempStats			 = (D2Game6FD0FE50)		(0xDFE50 + GameOffset); // D2Game.6FD0FE50
static D2Game6FD0FE80	D2SrvHitCheck			 = (D2Game6FD0FE80)		(0xDFE80 + GameOffset); // D2Game.6FD0FE80
static D2Game_6FD0FE80	D2SrvHitCheckv2			 = (D2Game_6FD0FE80) 	(0xDFE80 + GameOffset); // D2Game.6FD0FE80
static D2Game6FD10200	D2SrvHit1Check			 = (D2Game6FD10200)		(0xE0200 + GameOffset); // D2Game.6FD10200 Note: wrapper of D2SrvHitCheck
static D2Game6FD10360   D2SetConfuseState		 = (D2Game6FD10360)		(0xE0360 + GameOffset); // D2Game.6FD10360
static D2Game6FD10C90	D2AuraUseMana			 = (D2Game6FD10C90)		(0xE0C90 + GameOffset); // D2Game.6FD10C90
static D2Game6FD10E50	D2EndState				 = (D2Game6FD10E50)		(0xE0E50 + GameOffset); // D2Game.6FD10E50
static D2Game6FD10EC0	D2SetCurseState			 = (D2Game6FD10EC0)		(0xE0EC0 + GameOffset); // D2Game.6FD10EC0
static D2Game6FD115E0	D2LaunchMissileonGround	 = (D2Game6FD115E0)		(0xE15E0 + GameOffset); // D2Game.6FD115E0
static D2Game6FD11C90   D2UpdateState			 = (D2Game6FD11C90)		(0xE1C90 + GameOffset); // D2Game.6FD11C90
static D2Game6FD11D90	D2SetElenDamageData		 = (D2Game6FD11D90)		(0xE1D90 + GameOffset); // D2Game.6FD11D90
static D2Game6FD123D0	SrvDoFunc2				 = (D2Game6FD123D0)		(0xE23D0 + GameOffset); // D2Game.6FD123D0
static D2Game6FD12950	D2SkillFunc_1			 = (D2Game6FD12950)		(0xE2950 + GameOffset); // D2Game.6FD12950 - SKILLS_CastStart
static D2Game6FD12BA0	D2SrvSkill				 = (D2Game6FD12BA0)		(0xE2BA0 + GameOffset); // D2Game.6FD12BA0 - SKILLS_CastFinish
static D2Game6FD13220	D2GetSkillDelay			 = (D2Game6FD13220)		(0xE3220 + GameOffset); // D2Game.6FD13220
static D2Game6FD13260	D2SkillFunc_2			 = (D2Game6FD13260)		(0xE3260 + GameOffset); // D2Game.6FD13260 - SKILLS_SetNextActivation
static D2Game6FD136E0	D2AddNewSkill			 = (D2Game6FD136E0)		(0xE36E0 + GameOffset); // D2Game.6FD136E0
static D2Game6FD13800	D2_Game6FD13800			= (D2Game6FD13800)		(0xE3800 + GameOffset); // D2Game.6FD13800
static D2Game6FD13B20	D2CreateLinkPortal		= (D2Game6FD13B20)		(0xE3B20 + GameOffset); // D2Game.6FD13B20
static D2Game6FD13DF0	D2CreatePortal			 = (D2Game6FD13DF0)		(0xE3DF0 + GameOffset); // D2Game.6FD13DF0
//static D2Game6FD13DF0_2	D2CreatePortalv2		 = (D2Game6FD13DF0_2)	(0xE3DF0 + GameOffset); // D2Game.6FD13DF0
static D2Game6FD14170	D2LaunchMissile			 = (D2Game6FD14170)		(0xE4170 + GameOffset); // D2Game.6FD14170
static D2Game6FD14430	D2SummonPet				 = (D2Game6FD14430)		(0xE4430 + GameOffset); // D2Game.6FD14430
static D2Game6FD14C30	D2RefreshPassiveAndSkills = (D2Game6FD14C30)	(0xE4C30 + GameOffset); // D2Game.6FD14C30
static D2Game6FD14C60	D2_Game6FD14C60			 = (D2Game6FD14C60)		(0xE4C60 + GameOffset); // D2Game.6FD14C60
static D2Game6FD14DD0	D2SetElementalDmgFromSkill = (D2Game6FD14DD0)	(0xE4DD0 + GameOffset); // D2Game.6FD14DD0
static D2Game6FD14EC0	D2SetPhysicalDmgFromSkill = (D2Game6FD14EC0)	(0xE4EC0 + GameOffset); // D2Game.6FD14EC0
static D2Game6FD15580	D2GetSummonIDFromptSkills = (D2Game6FD15580)	(0xE5580 + GameOffset); // D2Game.6FD15580
static D2Game6FD155E0	D2ProcessSkillAttack	 = (D2Game6FD155E0)		(0xE55E0 + GameOffset); // D2Game.6FD155E0
static D2Game6FD156A0	D2SetAuraEvent			 = (D2Game6FD156A0)		(0xE56A0 + GameOffset); // D2Game.6FD156A0
static D2Game6FD156F0	D2LaunchInfernoMissile	 = (D2Game6FD156F0)		(0xE56F0 + GameOffset); // D2Game.6FD156F0
static D2Game6FD15940	D2SetInfernoStateandSkillInfo = (D2Game6FD15940) (0xE5940 + GameOffset); // D2Game.6FD15940
static D2Game6FD1C8F0   D2RndMinMaxValue         = (D2Game6FD1C8F0)     (0xEC8F0 + GameOffset); // D2Game.6FD1C8F0
static D2Game6FD1C9A0   D2CalcPercent            = (D2Game6FD1C9A0)     (0xEC9A0 + GameOffset); // D2Game.6FD1C9A0 
static D2Game6FD1CACF	D2Sprintf				 = (D2Game6FD1CACF)		(0xECACF + GameOffset); // D2Game.6FD1CACF
static D2Game6FD1CAA0   D2CreateTempFileBuffert  = (D2Game6FD1CAA0)		(0xECAA0 + GameOffset); // D2Game.6FD1CAA0
static D2Game6FD1CB30	D2AddNametoPlayerData	 = (D2Game6FD1CB30)		(0xECB30 + GameOffset); // D2Game.6FD1CB30
static D2Game6FD1D660	D2ReverseName			 = (D2Game6FD1D660)		(0xED660 + GameOffset); // D2Game.6FD15940
static D2Game6FD1D690	D2FileClose				 = (D2Game6FD1D690)		(0xED690 + GameOffset); // D2Game.6FD1D690
static D2Game6FD1D70D	D2SaveFile				 = (D2Game6FD1D70D)		(0xED70D + GameOffset); // D2Game.6FD1D70D
static D2Game6FD1D877	D2FileOpen				 = (D2Game6FD1D877)		(0xED877 + GameOffset); // D2Game.6FD1D877

//D2Client
static D2Client6FAA1000	D2LoadDc6				= (D2Client6FAA1000)	(0x01000 + ClientOffset); // D2Client.6FAA1000
static D2Client6FAA1140 D2FreeImageCellsAndUnit = (D2Client6FAA1140)    (0x01140 + ClientOffset); // D2Client.6FAA1140
static D2Client6FAA4280 D2SetUnitLightRadius	= (D2Client6FAA4280)	(0x04280 + ClientOffset); // D2Client.6FAA4280
static D2Client6FAA43A0	D2SetLightRadiusEx		= (D2Client6FAA43A0)	(0x043A0 + ClientOffset); // D2Client.6FAA43A0
static D2Client6FAA8B30 D2CheckRunButton        = (D2Client6FAA8B30)    (0x08B30 + ClientOffset); // D2Client.6FAA8B30
static D2Client6FAA9AC0 D2GetskilldelayTimeout  = (D2Client6FAA9AC0)	(0x09AC0 + ClientOffset); // D2Client.6FAA9AC0
static D2Client6FAABF30 D2MenuInit				= (D2Client6FAABF30)	(0x0BF30 + ClientOffset); // D2Client.6FAABF30
static D2Client6FAABF80 D2GetClientGameFrame    = (D2Client6FAABF80)    (0x0BF80 + ClientOffset); // D2Client.6FAABF80
static D2Client6FAABFF0 D2GetGameType			= (D2Client6FAABFF0)    (0x0BFF0 + ClientOffset); // D2Client.6FAABFF0
static D2Client6FAAC020 D2CheckBloodAllowed		= (D2Client6FAAC020)	(0x0C020 + ClientOffset); // D2Client.6FAAC020
static D2Client6FAAC080 D2CheckHirelingAlive	= (D2Client6FAAC080)	(0x0C080 + ClientOffset); // D2Client.6FAAC080
static D2Client6FAAC090 D2GetCltDifficulty		= (D2Client6FAAC090)	(0x0C090 + ClientOffset); // D2Client.6FAAC090
static D2Client6FAAD990 D2SendSrvMsg_3			= (D2Client6FAAD990)	(0x0D990 + ClientOffset); // D2Client.6FAAD990
static D2Client6FAADA20 D2SendSrvMsg_5			= (D2Client6FAADA20)	(0x0DA20 + ClientOffset); // D2Client.6FAADA20
static D2Client6FAADA40 D2_Client6FAADA40		= (D2Client6FAADA40)	(0x0DA40 + ClientOffset); // D2Client.6FAADA40
static D2Client6FAADA70 D2SendNpcPacket			= (D2Client6FAADA70)	(0x0DA70 + ClientOffset); // D2Client.6FAADA70 D2_6FAADA70
static D2Client6FAB0C00 D2GetCharacterName		= (D2Client6FAB0C00)	(0x10C00 + ClientOffset); // D2Client.6FAB0C00
static D2Client6FAB5750 D2GetScreenMenuPanels	= (D2Client6FAB5750)	(0x15750 + ClientOffset); // D2Client.6FAB5750
static D2Client6FAB5A80 D2GetLastClientKill		= (D2Client6FAB5A80)	(0x15A80 + ClientOffset); // D2Client.6FAB5A80
static D2Client6FAB5A90 D2_Client6FAB5A90		= (D2Client6FAB5A90)	(0x15A90 + ClientOffset); // D2Client.6FAB5A90
static D2Client6FABA950	D2CltGetTargetFirstPointXY = (D2Client6FABA950) (0x1A950 + ClientOffset); // D2Client.6FABA950
static D2Client6FABABC0	D2CltCastCurseMissile	= (D2Client6FABABC0)	(0x1ABC0 + ClientOffset); // D2Client.6FABABC0
static D2Client6FABC6B0 D2CltSkill				= (D2Client6FABC6B0)	(0x1C6B0 + ClientOffset); // D2Client.6FABC6B0 SKILLS_ClientCastFinish (cltDoFunc)
static D2Client6FABD4D0 D2LaunchClientNova		= (D2Client6FABD4D0)	(0x1D4D0 + ClientOffset); // D2Client.6FABD4D0
static D2Client6FABDB00 D2LauchClientShockFieldMissile = (D2Client6FABDB00) (0x1DB00 + ClientOffset); // D2Client.6FABDB00
static D2Client6FABF670	D2GetClientMissileFromSkill = (D2Client6FABF670) (0x1F670 + ClientOffset); // D2Client.6FABF670
static D2Client6FABF730 D2GetNbrOfMissilesFromSkill = (D2Client6FABF730) (0x1F730 + ClientOffset); // D2Client.6FABF730
static D2Client6FAC85F0	D2LaunchCltCorpseExpWrapper = (D2Client6FAC85F0) (0x285F0 + ClientOffset); // D2Client.6FAC85F0
static D2Client6FAC8940	D2LaunchCltCorpseExpSkill = (D2Client6FAC8940)	(0x28940 + ClientOffset); // D2Client.6FAC8940		
static D2Client6FACA1D0	D2LaunchClientInfernoMissile = (D2Client6FACA1D0) (0x2A1D0 + ClientOffset); // D2Client.6FACA1D0
static D2Client6FACFCD0 D2PrintText				= (D2Client6FACFCD0)	(0x2FCD0 + ClientOffset); // D2Client.6FACFCD0
static D2Client6FAD10D0 D2GetChanceToHit		= (D2Client6FAD10D0)	(0x310D0 + ClientOffset); // D2Client.6FAD10D0
static D2Client6FADD360 D2CreateItemName		= (D2Client6FADD360)	(0x3D360 + ClientOffset); // D2Client.6FADD360
static D2Client6FAD4B60 D2GetItemGUID			= (D2Client6FAD4B60)	(0x34B60 + ClientOffset); // D2Client.6FAD4B60
static D2Client6FAD56B0 D2GetItemNameFromGUID	= (D2Client6FAD56B0)	(0x356B0 + ClientOffset); // D2Client.6FAD56B0
static D2Client6FAD6BE0	D2CreateMenu			= (D2Client6FAD6BE0)	(0x36BE0 + ClientOffset); // D2Client.6FAD6BE0
static D2Client6FAD6DC0 D2RemoveMenu			= (D2Client6FAD6DC0)	(0x36DC0 + ClientOffset); // D2Client.6FAD6DC0
static D2Client6FAD6EB0 D2Menu					= (D2Client6FAD6EB0)	(0x36EB0 + ClientOffset); // D2Client.6FAD6EB0
static D2Client6FAD7150 D2ShowMenu				= (D2Client6FAD7150)	(0x37150 + ClientOffset); // D2Client.6FAD7150
static D2Client6FAD7180 D2AddNpcMenuItem		= (D2Client6FAD7180)	(0x37180 + ClientOffset); // D2Client.6FAD7180
static D2Client6FAE8F10 D2Client_6FAE8F10		= (D2Client6FAE8F10)	(0x48F10 + ClientOffset); // D2Client.6FAE8F10
static D2Client6FAE9690 D2SetVendorButton_RepairAll = (D2Client6FAE9690)	(0x49690 + ClientOffset); // D2Client.6FAE9690
static D2Client6FAE96D0 D2SetVendorButton_Repair = (D2Client6FAE96D0)	(0x496D0 + ClientOffset); // D2Client.6FAE96D0
static D2Client6FAE9710 D2SetVendorButton_Buy	= (D2Client6FAE9710)	(0x49710 + ClientOffset); // D2Client.6FAE9710
static D2Client6FAE9750 D2SetVendorButton_Sell	= (D2Client6FAE9750)	(0x49750 + ClientOffset); // D2Client.6FAE9750
static D2Client6FAE9790 D2SetVendorButton_Gamble = (D2Client6FAE9790)	(0x49790 + ClientOffset); // D2Client.6FAE9790
static D2Client6FAEB910 D2CheckInventoryMode	= (D2Client6FAEB910)	(0x4B910 + ClientOffset); // D2Client.6FAEB910
static D2Client6FAEB930 D2GetInventoryMode		= (D2Client6FAEB930)	(0x4B930 + ClientOffset); // D2Client.6FAEB930
static D2Client6FAEB940 D2_Client6FAEB940		= (D2Client6FAEB940)	(0x4B940 + ClientOffset); // D2Client.6FAEB940
static D2Client6FAEC510 D2OpenStash				= (D2Client6FAEC510)	(0x4C510 + ClientOffset); // D2Client.6FAEC510
static D2Client6FAED1C0 D2ClientParseServerMessageWrapper = (D2Client6FAED1C0) (0x4D1C0 + ClientOffset); // D2Client.6FAED1C0
static D2Client6FAEED80 D2PanelCharSoundButtonClick = (D2Client6FAEED80) (0x4ED80 + ClientOffset); // D2Client.6FAEED80
static D2Client6FAEEEA0 D2PanelSkillSoundButtonClick = (D2Client6FAEEEA0) (0x4EEA0 + ClientOffset); // D2Client.6FAEEEA0
static D2Client6FAEEFD0 D2PanelCharButtonClick  = (D2Client6FAEEFD0)    (0x4EFD0 + ClientOffset); // D2Client.6FAEEFD0
static D2Client6FAEF130 D2PanelSkillButtonClick  = (D2Client6FAEF130)   (0x4F130 + ClientOffset); // D2Client.6FAEF130
static D2Client6FAF3160 D2CreateStatlistDesc	= (D2Client6FAF3160)	(0x53160 + ClientOffset); // D2Client.6FAF3160
static D2Client6FAF3950 D2GetVendorIDFromClient = (D2Client6FAF3950)	(0x53950 + ClientOffset); // D2Client.6FAF3950
static D2Client6FAF3DC0 D2_Client6FAF3DC0		= (D2Client6FAF3DC0)	(0x53DC0 + ClientOffset); // D2Client.6FAF3DC0
static D2Client6FAF6340 D2Client_6FAF6340		= (D2Client6FAF6340)	(0x56340 + ClientOffset); // D2Client.6FAF6340
static D2Client6FAF65C0 D2MainNPCMenu			= (D2Client6FAF65C0)	(0x565C0 + ClientOffset); // D2Client.6FAF65C0
static D2Client6FAF75A0 D2TravelClientWarrivAct1Check = (D2Client6FAF75A0) (0x575A0 + ClientOffset); // D2Client.6FAF75A0
static D2Client6FAF7740 D2TravelClientCheck		= (D2Client6FAF7740)	(0x57740 + ClientOffset); // D2Client.6FAF7740
static D2Client6FAF7950 D2FlushClientVendorInv  = (D2Client6FAF7950)	(0x57950 + ClientOffset); // D2Client.6FAF7950
static D2Client6FAF9CB0	D2ShowNPCResOption		= (D2Client6FAF9CB0)	(0x59CB0 + ClientOffset); // D2Client.6FAF9CB0
static D2Client6FAFA230 D2SetNPCUnitFlag		= (D2Client6FAFA230)	(0x5A230 + ClientOffset); // D2Client.6FAFA230
static D2Client6FAFA6E0 D2CltCheckTransmission	= (D2Client6FAFA6E0)	(0x5A6E0 + ClientOffset); // D2Client.6FAFA6E0
static D2Client6FAFB200 D2GetItemCost			= (D2Client6FAFB200)	(0x5B200 + ClientOffset); // D2Client.6FAFB200
static D2Client6FAFB6E0 D2_Client6FAFB6E0		= (D2Client6FAFB6E0)	(0x5B6E0 + ClientOffset); // D2Client.6FAFB6E0
static D2Client6FAFB710 D2ConfirmDialog			= (D2Client6FAFB710)	(0x5B710 + ClientOffset); // D2Client.6FAFB710
static D2Client6FAFBB10 D2_Client6FAFBB10		= (D2Client6FAFBB10)	(0x5BB10 + ClientOffset); // D2Client.6FAFBB10
static D2Client6FAFBB50 D2CheckNPCActive		= (D2Client6FAFBB50)	(0x5BB50 + ClientOffset); // D2Client.6FAFBB50
static D2Client6FAFBB60 D2Client_6FAFBB60		= (D2Client6FAFBB60)	(0x5BB60 + ClientOffset); // D2Client.6FAFBB60
static D2Client6FAFC0B0 D2CltCheckActiveNPC		= (D2Client6FAFC0B0)	(0x5C0B0 + ClientOffset); // D2Client.6FAFC0B0 
static D2Client6FAFC130 D2DrawVendorTabNames	= (D2Client6FAFC130)	(0x5C130 + ClientOffset); // D2Client.6FAFC130 
static D2Client6FAFE850 D2DrawPanelSkillIcon	= (D2Client6FAFE850)	(0x5E850 + ClientOffset); // D2Client.6FAFE850
static D2Client6FAFF0B0 D2DisplayOrbTextDisplay = (D2Client6FAFF0B0)	(0x5F0B0 + ClientOffset); // D2Client.6FAFF0B0
static D2Client6FAFFE60 D2DisplayRunText		= (D2Client6FAFFE60)	(0x5FE60 + ClientOffset); // D2Client.6FAFFE60 
static D2Client6FAFF2D0	D2GetCurrentValueFromStat = (D2Client6FAFF2D0)  (0x5F2D0 + ClientOffset); // D2Client.6FAFF2D0
static D2Client6FAFF480 D2DrawAllUIPanel        = (D2Client6FAFF480)    (0x5F480 + ClientOffset); // D2Client.6FAFF480
static D2Client6FAFFCA0 D2DrawExperienceBar		= (D2Client6FAFFCA0)    (0x5FCA0 + ClientOffset); // D2Client.6FAFFCA0
static D2Client6FAFFF90 D2CheckMouseXYCoordinates = (D2Client6FAFFF90)  (0x5FF90 + ClientOffset); // D2Client.6FAFFF90
static D2Client6FB001B0 D2DrawMiniPanelButton	= (D2Client6FB001B0)	(0x601B0 + ClientOffset); // D2Client.6FB001B0
static D2Client6FB002B0 D2DrawBeltUI			= (D2Client6FB002B0)	(0x602B0 + ClientOffset); // D2Client.6FB002B0
static D2Client6FB00990 D2DrawBeltItemDisplay	= (D2Client6FB00990)	(0x60990 + ClientOffset); // D2Client.6FB00990
static D2Client6FB00C10 D2SetCltDestLevelAndDestObjID = (D2Client6FB00C10) (0x60C10 + ClientOffset); // D2Client.6FB00C10
static D2Client6FB10970 D2SetColorForString		= (D2Client6FB10970)	(0x70970 + ClientOffset); // D2Client.6FB10970
static D2Client6FB16200 D2GetPlayerARFromSkill	= (D2Client6FB16200)	(0x76200 + ClientOffset); // D2Client.6FB16200
static D2Client6FB177C0 D2AddSkillTabText		= (D2Client6FB177C0)	(0x777C0 + ClientOffset); // D2Client.6FB177C0
static D2Client6FB18AC0 D2DrawSkillIcon			= (D2Client6FB18AC0)    (0x78AC0 + ClientOffset); // D2Client.6FB18AC0
static D2Client6FB18700 D2SkillsGetIconsEx      = (D2Client6FB18700)    (0x78700 + ClientOffset); // D2Client.6FB18700
static D2Client6FB18770 D2CheckIconBlockedEx    = (D2Client6FB18770)	(0x78770 + ClientOffset); // D2Client.6FB18770
static D2Client6FB18790 D2BookHoverDesc			= (D2Client6FB18790)	(0x78790 + ClientOffset); // D2Client.6FB18790
static D2Client6FB18940 D2SkillsHoverDesc		= (D2Client6FB18940)    (0x78940 + ClientOffset); // D2Client.6FB18940
static D2Client6FB1C600 D2ClientMsgTop			= (D2Client6FB1C600)	(0x7C600 + ClientOffset); // D2Client.6FB1C600
static D2Client6FB1CBD0 D2SetScrollEndFunc		= (D2Client6FB1CBD0)	(0x7CBD0 + ClientOffset); // D2Client.6FB1CBD0
static D2Client6FB1CC20	D2ScrollText			= (D2Client6FB1CC20)	(0x7CC20 + ClientOffset); // D2Client.6FB1CC20
static D2Client6FB1E6B0 D2GethTextList			= (D2Client6FB1E6B0)	(0x7E6B0 + ClientOffset); // D2Client.6FB1E6B0
static D2Client6FB1E710 D2_Client6FB1E710		= (D2Client6FB1E710)	(0x7E710 + ClientOffset); // D2Client.6FB1E710
static D2Client6FB20100 D2LoadCellFile			= (D2Client6FB20100)	(0x80100 + ClientOffset); // D2Client.6FB20100 D2GetRecipeScrollGfx
static D2Client6FB20430 D2GetHighQualityName	= (D2Client6FB20430)	(0x80430 + ClientOffset); // D2Client.6FB20430
static D2Client6FB20600 D2AddUnitDisplay		= (D2Client6FB20600)	(0x80600 + ClientOffset); // D2Client.6FB20600
static D2Client6FB23230 D2GetUIToggle			= (D2Client6FB23230)	(0x83230 + ClientOffset); // D2Client.6FB23230 D2GetUIToggle
static D2Client6FB23260 D2SendUImsg				= (D2Client6FB23260)	(0x83260 + ClientOffset); // D2Client.6FB23260
static D2Client6FB238B0 D2Client_6FB238B0		= (D2Client6FB238B0)	(0x838B0 + ClientOffset); // D2Client.6FB238B0
static D2Client6FB24110 D2GetBtnGfxData			= (D2Client6FB24110)	(0x84110 + ClientOffset); // D2Client.6FB24110
static D2Client6FB25A10	D2Client_6FB25A10		= (D2Client6FB25A10)	(0x85A10 + ClientOffset); // D2Client.6FB25A10
static D2Client6FB25BA0 D2SetContainerGUI		= (D2Client6FB25BA0)	(0x85BA0 + ClientOffset); // D2Client.6FB25BA0
static D2Client6FB269F0	D2GetClientUnit_1		= (D2Client6FB269F0)	(0x869F0 + ClientOffset); // D2Client.6FB269F0
static D2Client6FB26A60	D2GetClientUnit_2		= (D2Client6FB26A60)	(0x86A60 + ClientOffset); // D2Client.6FB26A60
static D2Client6FB26AD0	D2GetOwnerFromCltMissile = (D2Client6FB26AD0)	(0x86AD0 + ClientOffset); // D2Client.6FB26AD0
static D2Client6FB26C90 D2HfindRoom				= (D2Client6FB26C90)	(0x86C90 + ClientOffset); // D2Client.6FB26C90
static D2Client6FB283D0	D2GetClientPlayer		= (D2Client6FB283D0)	(0x883D0 + ClientOffset); // D2Client.6FB283D0
static D2Client6FB283E0 D2CheckPlayerDead		= (D2Client6FB283E0)	(0x883E0 + ClientOffset); // D2Client.6FB283E0
static D2Client6FB284A0 D2GetUnitPalShift		= (D2Client6FB284A0)	(0x884A0 + ClientOffset); // D2Client.6FB284A0
static D2Client6FB29370	D2GetClientRoom			= (D2Client6FB29370)	(0x89370 + ClientOffset); // D2Client.6FB29370
static D2Client6FB296A0 D2GetTargetClientUnit	= (D2Client6FB296A0)	(0x896A0 + ClientOffset); // D2Client.6FB296A0
static D2Client6FB296E0 D2RemoveUnitLightMap	= (D2Client6FB296E0)	(0x896E0 + ClientOffset); // D2Client.6FB296E0
static D2Client6FB297F0	D2GetNpcName			= (D2Client6FB297F0)	(0x897F0 + ClientOffset); // D2Client.6FB297F0
static D2Client6FB29D80	D2CheckEnemyClient		= (D2Client6FB29D80)	(0x89D80 + ClientOffset); // D2Client.6FB29D80
static D2Client6FB2A8E0 D2DropItemOnGround		= (D2Client6FB2A8E0)	(0x8A8E0 + ClientOffset); // D2Client.6FB2A8E0
static D2Client6FB2B360	D2UpdateClientUnit		= (D2Client6FB2B360)	(0xBB360 + ClientOffset); // D2Client.6FB2B360
static D2Client6FB2E780 D2UpdateClientStat		= (D2Client6FB2E780)	(0x8E780 + ClientOffset); // D2Client.6FB2E780
static D2Client6FB2FB90 D2CltCheckItemUsable	= (D2Client6FB2FB90)	(0x8FB90 + ClientOffset); // D2Client.6FB2FB90
static D2Client6FB35D10 D2LaunchCltMissileEffect = (D2Client6FB35D10)	(0x95D10 + ClientOffset); // D2Client.6FB35D10
static D2Client6FB3E070 D2InitClientMissile		= (D2Client6FB3E070)	(0x9E070 + ClientOffset); // D2Client.6FB3E070
static D2Client6FB45300	MonUmod9_CltFireExp		= (D2Client6FB45300)	(0xA5300 + ClientOffset); // D2Client.6FB45300
static D2Client6FB454A0	D2Client_QuestCheck		= (D2Client6FB454A0)	(0xA54A0 + ClientOffset); // D2Client.6FB454A0
static D2Client6FB456D0	MonUmod_CltNova			= (D2Client6FB456D0)	(0xA56D0 + ClientOffset); // D2Client.6FB456D0
static D2Client6FB45800	MonUmod_CltDurielDead	= (D2Client6FB45800)	(0xA5800 + ClientOffset); // D2Client.6FB45800
static D2Client6FB45910	MonUmod_CltPoisonHit	= (D2Client6FB45910)	(0xA5910 + ClientOffset); // D2Client.6FB45910
static D2Client6FB45B20	MonUmod17_CltChargeBolt	= (D2Client6FB45B20)	(0xA5B20 + ClientOffset); // D2Client.6FB45B20
static D2Client6FB45B60	MonUmod_CltScarabBolt	= (D2Client6FB45B60)	(0xA5B60 + ClientOffset); // D2Client.6FB45B60
static D2Client6FB45E10	MonUmod_CltFireSpikes	= (D2Client6FB45E10)	(0xA5E10 + ClientOffset); // D2Client.6FB45E10
static D2Client6FB46110	MonUmod_CltSpawnWorms	= (D2Client6FB46110)	(0xA6110 + ClientOffset); // D2Client.6FB46110
static D2Client6FB464A0	D2GetGreetingSound		= (D2Client6FB464A0)	(0xA64A0 + ClientOffset); // D2Client.6FB464A0
static D2Client6FB46930	D2SetObjectLightRadius	= (D2Client6FB46930)	(0xA6930 + ClientOffset); // D2Client.6FB46930
static D2Client6FB478D0	D2DrawDoorOverlay		= (D2Client6FB478D0)	(0xA78D0 + ClientOffset); // D2Client.6FB478D0
static D2Client6FB4D3A0	D2StatesClientAddUpdate = (D2Client6FB4D3A0)    (0xAD3A0 + ClientOffset); // D2Client.6FB4D3A0
static D2Client6FB4D640 D2StatesClientRemoveUpdate = (D2Client6FB4D640) (0xAD640 + ClientOffset); // D2Client.6FB4D640
static D2Client6FB4D830	D2StatesSetStat			= (D2Client6FB4D830)	(0xAD830 + ClientOffset); // D2Client.6FB4D830
static D2Client6FB4D920 D2StatesAddCallback = (D2Client6FB4D920)		(0xAD920 + ClientOffset); // D2Client.6FB4D920
static D2Client6FB4DA00 D2StatesRemoveCallback = (D2Client6FB4DA00)		(0xADA00 + ClientOffset); // D2Client.6FB4DA00
static D2Client6FB4DB40 D2DrawUnit				= (D2Client6FB4DB40)	(0xADB40 + ClientOffset); // D2Client.6FB4DB40
static D2Client6FB4EE00 D2SetPathType			= (D2Client6FB4EE00)	(0xAEE00 + ClientOffset); // D2Client.6FB4EE00
static D2Client6FB4F410 D2NPCCancel				= (D2Client6FB4F410)	(0xAF410 + ClientOffset); // D2Client.6FB4F410
static D2Client6FB50370 D2Client_6FB50370		= (D2Client6FB50370)	(0xB0370 + ClientOffset); // D2Client.6FB50370
static D2Client6FB55820 D2PlaySound				= (D2Client6FB55820)	(0xB5820 + ClientOffset); // D2Client.6FB55820
static D2Client6FB57480 D2UpdatesWinSoundMsgManage = (D2Client6FB57480) (0xB7480 + ClientOffset); // D2Client.6FB57480
static D2Client6FB57500 D2UpdatesWinMsgManage	= (D2Client6FB57500)	(0xB7500 + ClientOffset); // D2Client.6FB57500
static D2Client6FB57BC0 D2GetMouseXCoordinate	= (D2Client6FB57BC0)	(0xB7BC0 + ClientOffset); // D2Client.6FB57BC0
static D2Client6FB57BD0 D2GetMouseYCoordinate	= (D2Client6FB57BD0)	(0xB7BD0 + ClientOffset); // D2Client.6FB57BD0
static D2Client6FB575E0 D2GetTargetItem		    = (D2Client6FB575E0)	(0xB75E0 + ClientOffset); // D2Client.6FB575E0
static D2Client6FB5A320 D2DrawInventoryItem		= (D2Client6FB5A320)	(0xBA320 + ClientOffset); // D2Client.6FB5A320
static D2Client6FB5B0F0 D2DrawBox				= (D2Client6FB5B0F0)	(0xBB0F0 + ClientOffset); // D2Client.6FB5B0F0
static D2Client6FB5BA70	D2SetPrgOverlay			= (D2Client6FB5BA70)	(0xBBA70 + ClientOffset); // D2Client.6FB5BA70
static D2Client6FB5BEC0	D2RemovePrgOverlay		= (D2Client6FB5BEC0)	(0xBBEC0 + ClientOffset); // D2Client.6FB5BEC0
static D2Client6FB5C080	D2UnitGFXInit			= (D2Client6FB5C080)	(0xBC080 + ClientOffset); // D2Client.6FB5C080 D2Client_6FB5C080
static D2Client6FB5EF30 D2ReturnString			= (D2Client6FB5EF30)	(0xBEF30 + ClientOffset); // D2Client.6FB5EF30


//Fog
// static D2Fog2722		FogCheckLegalRoom		= (D2Fog2722)			(FogOffset + 0x0DD90);	// 1.10
/*static D2Fog2727		FogAssert				= (D2Fog2727)			(FogOffset + 0x0ED30);	// D2Fog #10023
static D2Fog272D		FogLogMsg				= (D2Fog272D)			(FogOffset + 0x120A0);	// D2Fog #10029
static D2Fog273A		FogMemAlloc				= (D2Fog273A)			(FogOffset + 0x08F50);	// D2Fog #10042
static D2Fog273B		FogMemDeAlloc			= (D2Fog273B)			(FogOffset + 0x08F90);	// D2Fog #10043
static D2Fog273C		FogMemPoolAlloc			= (D2Fog273C)			(FogOffset + 0x0BFB0);	// D2Fog #10044
static D2Fog273D		D2Malloc				= (D2Fog273D)			(FogOffset + 0x08FF0);	// D2Fog #10045
static D2Fog273E		D2Free					= (D2Fog273E)			(FogOffset + 0x09030);  // D2Fog #10046
static D2Fog2783		FogGetPath				= (D2Fog2783)			(FogOffset + 0x11900);	// D2Fog #10115
static D2Fog27E3		FogUnknown				= (D2Fog27E3)			(FogOffset + 0x0B720);	// D2Fog #10211
static D2Fog27E9		FogGetRowFromText		= (D2Fog27E9)			(FogOffset + 0x0BC20);	// D2Fog #10217
*/

static D2Fog2727		FogAssert				= (D2Fog2727)			(FogOffset + 0x0ED30);	// D2Fog #10023
static D2Fog2728		FogPacketAssert			= (D2Fog2728)			(FogOffset + 0x0ED60);	// D2Fog #10024
static D2Fog2729		D2SetWindowState		= (D2Fog2729)			(FogOffset + 0x0ED90);	// D2Fog #10025
static D2Fog272D		FogLogMsg				= (D2Fog272D)			(FogOffset + 0x120A0);	// D2Fog #10029
static D2Fog273A		FogMemAlloc				= (D2Fog273A)			(FogOffset + 0x08F50);	// 1.10
static D2Fog273B		FogMemDeAlloc			= (D2Fog273B)			(FogOffset + 0x08F90);	// 1.10
static D2Fog273C		FogMemPoolAlloc			= (D2Fog273C)			(FogOffset + 0x0BFB0);  // D2Fog #10044
static D2Fog273D		D2Malloc				= (D2Fog273D)			(FogOffset + 0x08FF0);	// D2Fog #10045
static D2Fog273E		FogMemPoolDeAlloc		= (D2Fog273E)			(FogOffset + 0x09030);	// 1.10
static D2Fog2783		FogGetPath				= (D2Fog2783)			(FogOffset + 0x11900);	// D2Fog #10115
static D2Fog278E		FogInitBuffer			= (D2Fog278E)			(FogOffset + 0x03650);  // D2Fog #10126
static D2Fog278F		FogNextBit				= (D2Fog278F)			(FogOffset + 0x036A0);  // D2Fog #10127
static D2Fog2790		FogWrite				= (D2Fog2790)			(FogOffset + 0x036C0);  // D2Fog #10128
static D2Fog2791		FogReadSigned			= (D2Fog2791)			(FogOffset + 0x03850);	// D2Fog #10129
static D2Fog2792		FogReadValue			= (D2Fog2792)			(FogOffset + 0x03840);  // D2Fog #10130
static D2Fog27E3		FogUnknown				= (D2Fog27E3)			(FogOffset + 0x0B720);	// 1.10
static D2Fog27E9		FogGetRowFromText		= (D2Fog27E9)			(FogOffset + 0x0BC20);	// 1.10
static D2Fog27F3		FogCheckExpansion		= (D2Fog27F3)			(FogOffset + 0x0D730);	// D2Fog #10227
static D2Fog280C		FogGetBit				= (D2Fog280C)			(FogOffset + 0x039A0);  // D2Fog #10252



//D2Lang
static D2Lang10003		D2GetStringFromCode		= (D2Lang10003)			(LangOffset + 0x3BC0);	// 1.10
static D2Lang10004		D2GetStringFromIndex	= (D2Lang10004)			(LangOffset + 0x3740);	// 1.10
static D2Lang10006		D2GetLang				= (D2Lang10006)			(LangOffset + 0x3FB0);	// 1.10
static D2Lang10007		D2LoadForeignStrTable	= (D2Lang10007)			(LangOffset + 0x3DF0);	// D2Lang.6FC13DF0
static D2Lang10013		D2GetStringAndNum		= (D2Lang10013)			(LangOffset + 0x3960);	// 1.10
static D2L_UnicodeWidth	D2UnicodeWidth			= (D2L_UnicodeWidth)	(LangOffset + 0x29B0);	// 1.10
static D2L_ToUnicode	D2ToUnicode				= (D2L_ToUnicode)		(LangOffset + 0x2A40);	// 1.10
static D2L_StrCat		D2StrCat				= (D2L_StrCat)			(LangOffset + 0x13F0);	// 1.10
static D2L_StrCpy		D2StrCpy				= (D2L_StrCpy)			(LangOffset + 0x14A0);	// 1.10
static D2L_SPrintF		D2SPrintF				= (D2L_SPrintF)			(LangOffset + 0x1670);	// 1.10
static D2L_Win2Unicode	D2Win2Unicode			= (D2L_Win2Unicode)		(LangOffset + 0x1BD0);	// 1.10
static D2Lang6FC12E60	D2GetStrFromIndex		= (D2Lang6FC12E60)		(LangOffset + 0x2E60);	// 1.10
static D2Lang6FC13640	D2LoadTblFile			= (D2Lang6FC13640)		(LangOffset + 0x3640);	// 1.10
static D2Lang6FC13A90	D2GetString				= (D2Lang6FC13A90)		(LangOffset + 0x3A90);	// 1.10

//D2Gfx
static D2Gfx2713		D2GetRenderMode         = (D2Gfx2713)           (GfxOffset + 0x3AC0);	// D2Gfx.10003 6FA73AC0
static D2Gfx2715		GetResolutionMode		= (D2Gfx2715)			(GfxOffset + 0x3AE0);	// D2Gfx.10005 
static D2Gfx2749        D2DrawLine				= (D2Gfx2749)			(GfxOffset + 0x3DE0);	// D2Gfx.10057
static D2Gfx2758        D2DrawImageToWindow     = (D2Gfx2758)           (GfxOffset + 0x42A0);	// D2Gfx.10072
static D2Gfx275A        D2DrawVerticalCropImage = (D2Gfx275A)			(GfxOffset + 0x4300);	// D2Gfx.10074
static D2Gfx10027		D2GetWindowHandle		= (D2Gfx10027)			(GfxOffset + 0x49C0);	// 1.10

//D2Net
static D2Net2715		D2SendNetMessage		= (D2Net2715)			(NetOffset + 0x01760);	// D2Net.6FC01760
static D2Net6FC01810	D2NetInitClientPacketRecive = (D2Net6FC01810)	(NetOffset + 0x1810);	// D2Net #6FC01810
static D2Net272E		D2GetPacketSizeToClient = (D2Net272E)			(NetOffset + 0x1B60);	// #10030 // 6FC01B60
static D2Net2716		D2NetSendPacketToClient = (D2Net2716)		    (NetOffset + 0x22B0);	// D2Net #10006 // 6FC022B0


//D2Win
static D2Win10034       D2GetNearestPaletteIndex = (D2Win10034)         (WinOffset + 0x0EA80); // D2Win.6F8AEA80 #10034
static D2Win10046		D2DrawWinTextTitle		= (D2Win10046)			(WinOffset + 0x0FFB0); // D2Win.6F8AFFB0 #10046 
static D2Win10047		D2MallocUIWinBox		= (D2Win10047)			(WinOffset + 0x0FE30); // D2Win.6F8AFE30 #10047
static D2Win10051		D2DrawWinText			= (D2Win10051)			(WinOffset + 0x10040); // D2Win.6F8B0040 // D2DisplayTextGameSelectScreen
static D2Win10117		D2DrawText				= (D2Win10117)			(WinOffset + 0x0AD40); // D2Win.6F8AAD40 #10117
static D2Win10121		D2UniStrLen				= (D2Win10121)			(WinOffset + 0x0A2A0); // D2Win.6F8AA2A0 #10121
static D2Win10127		D2SetFont				= (D2Win10127)			(WinOffset + 0x09FF0); // D2Win.6F8A9FF0 #10127
static D2Win10129		D2AddUniStringToDisplay = (D2Win10129)			(WinOffset + 0x0AC60); // D2Win.6F8AAC60
static D2Win10131		D2GetPixelRect			= (D2Win10131)			(WinOffset + 0x0B260); // D2Win.6F8AB260
static D2Win10152		D2SetAnimGridX			= (D2Win10152)			(WinOffset + 0x04540); // D2Win.6F8A4540
static D2Win10153		D2SetAnimGridY			= (D2Win10153)			(WinOffset + 0x04570); // D2Win.6F8A4570
static D2Win10156		D2SetAnimGridBitmask	= (D2Win10156)			(WinOffset + 0x045D0); // D2Win.6F8A45D0
static D2Win10161		D2GetCustomTitles		= (D2Win10161)			(WinOffset + 0x04C70); // D2Win.6F8A4C70

static D2Win6F8B2399    D2LoadMPQFile			= (D2Win6F8B2399)		(WinOffset + 0x12399); // D2Win.6F8B2399


//D2Storm
static D2Storm401		D2CreateNewStruct		= (D2Storm401)			(StormOffset + 0x1B130); // Storm.6FFCB130
static D2Storm511		D2FreeWinMessage		= (D2Storm511)			(StormOffset + 0x1D110); // Storm.6FFCD110

//D2Cmp
static D2Cmp2734		D2GetGFXCellStrc		= (D2Cmp2734)			(CmpOffset + 0x02540); // D2Cmp.10036
static D2Cmp2735		D2GetGfxCellWidth		= (D2Cmp2735)			(CmpOffset + 0x025E0); // D2Cmp.10037

//D2Launch


static D2Launch6FA14520 D2GetClassString		= (D2Launch6FA14520)	(LaunchOffset + 0x04520); // D2Launch.6FA14520
static D2Launch6FA23890 D2LaunchInitPlayer		= (D2Launch6FA23890)	(LaunchOffset + 0x13890); // D2Launch.6FA23890

/*=================================================================*/
/* D2 Variable prototypes		                                   */
/*=================================================================*/

#define VARIABLE(R, N) typedef R *N

// D2Game
VARIABLE(DWORD*,		Cltn);
VARIABLE(DWORD,		    HireT);
VARIABLE(DWORD,			gpServerEvent);
VARIABLE(BOOL,			gpSetSaveFiles);
VARIABLE(int,			gdwGameSeed);





// D2Client
VARIABLE(DWORD,			ScrH);
VARIABLE(DWORD,			ScrW);
VARIABLE(DWORD,			BoxX);
VARIABLE(DWORD,			BoxY);
VARIABLE(DWORD,			MsgMenu);
VARIABLE(char,			CharName);
VARIABLE(DWORD,			Uk1);
VARIABLE(DWORD,			Uk2);
VARIABLE(DWORD,			Uk3);
VARIABLE(DWORD,			pNPCTransactionState);
VARIABLE(DWORD,			Uk5);
VARIABLE(DWORD,			Uk6);
VARIABLE(DWORD,			NpcU);
VARIABLE(DWORD,			NpcT);
VARIABLE(DWORD,			NpcI);
VARIABLE(Menu*,			NpcM);
VARIABLE(NpcMenuData,	NpcS1);
VARIABLE(DWORD,			NpcS2);
VARIABLE(Menu*,			NpcS3);
VARIABLE(DWORD,			HireN);
VARIABLE(DWORD,			HireR);
VARIABLE(DWORD,			HireOpt);
VARIABLE(void*,			QuestP);
VARIABLE(DWORD,			SellB);
VARIABLE(DWORD,			ObjUID);
VARIABLE(short int,		ClassPt);
VARIABLE(int,			BorderW);
VARIABLE(int,			BorderH);
VARIABLE(DWORD,			VendorButtonUI);
VARIABLE(Unit*,			ptItem);
VARIABLE(int,			BeltP);
VARIABLE(DWORD,			ActiveTabP);
VARIABLE(DWORD,			Skilltree_CloseP);
VARIABLE(DWORD,			SkilldescDisp);
VARIABLE(DWORD,			LastKillID);
VARIABLE(DWORD,			MenuHide1);
VARIABLE(DWORD,			MenuHide2);
VARIABLE(DWORD,			MenuHide3);
VARIABLE(DWORD,			MenuHide4);
VARIABLE(DWORD,			MenuHide5);
VARIABLE(DWORD,			gnInventoryMode);
//VARIABLE(gfxData*,		Gfx_Recipe);
VARIABLE(D2CellFileStrc*, RecipeBackground);
VARIABLE(D2RosterStrc*, gpPetRoster);
VARIABLE(D2PlayerRosterStrc*, gpPlayerRoster);
VARIABLE(DWORD, KashyaMNBR);
VARIABLE(DWORD, KashyaHMFunc);
VARIABLE(DWORD, KashyaHMStrkey);
VARIABLE(DWORD, KashyaDMFunc);
VARIABLE(DWORD, KashyaDMStrkey);
VARIABLE(DWORD,	SetANpcUniID);
VARIABLE(Unit*,	Sacp);
VARIABLE(DWORD, APannelCharBtn);
VARIABLE(DWORD, APannelSkillBtn);
VARIABLE(D2UnitListStrc*, gpClientUnitList);
VARIABLE(D2PalEntryStrc*, gpMonsterPalettes);
VARIABLE(BOOL, gpRandMonPaletteLoaded);
VARIABLE(DWORD, gpRandMonPalette);
//VARIABLE(D2PaletteStrc, gpRandMonPalette);
VARIABLE(D2CellFileStrc*, gpHlthmanaGfx);
VARIABLE(D2CellFileStrc*, gpOverlapGfx);
VARIABLE(DWORD, gpRedUIPalette);
VARIABLE(DWORD, gpYellowUIPalette);
VARIABLE(DWORD, gpBlueUIPalette);
VARIABLE(D2CellFileStrc*, gpSkillLevelBTN);
VARIABLE(D2CellFileStrc*, gp800BorderFrame);
VARIABLE(D2CellFileStrc*, gpCtrlPnl7);
VARIABLE(D2CellFileStrc*, gp800CtrlPnl7);
VARIABLE(D2CellFileStrc*, gpRunButton);
VARIABLE(BOOL, gpMouseLeftClick);
VARIABLE(D2CellFileStrc*, gpMiniPanelBtn);
VARIABLE(DWORD, gpUk_6FBB5EDC);
VARIABLE(DWORD, gbBeltHoverOpen);
VARIABLE(DWORD, gbBeltPermOpen);
VARIABLE(int, gbBeltUk1);
VARIABLE(D2CellFileStrc*, gbCltrlPnl_PopBelt);
VARIABLE(DWORD, gbResMode);
VARIABLE(Unit*, gpBeltItem);
VARIABLE(Unit*, gpBeltItem2);
VARIABLE(BYTE, gpUIBeltRowColor1);
VARIABLE(BYTE, gpUIBeltRowColor2);
VARIABLE(BYTE, gpUIBeltRowColor3);
VARIABLE(BYTE, gpUIBeltRowColor4);
VARIABLE(DWORD, gpItemGUID);
VARIABLE(DWORD, gnBeltItemSlot);
VARIABLE(int, gnBeltHoverX);
VARIABLE(int, gnBeltHoverY);
VARIABLE(DWORD, gnCltLevelID);
VARIABLE(DWORD, gnCltObjectID);
VARIABLE(D2CellFileStrc*, gpLevelSocketGfx);
VARIABLE(void*,	cltLvlMap);
VARIABLE(DWORD, gpWindowstate);
VARIABLE(DWORD, gpWindowstate);
VARIABLE(BYTE, gpCltDifficulty);
VARIABLE(DWORD, gpVendorItemCost);
VARIABLE(BYTE, gbVendorItemCost);
VARIABLE(DWORD, gbVendorPosXBtn);
VARIABLE(DWORD, gbVendorPosYBtn);
VARIABLE(DWORD, gbTickCount);
VARIABLE(DWORD, gbUnitUnID);
VARIABLE(DWORD, gbUnitID);
VARIABLE(Menu*, gbResultsDialog);
VARIABLE(Menu*, gbWaitDialog);
VARIABLE(Menu*, gbConfirmDialog);
VARIABLE(Menu*, gbMnuUk1);
VARIABLE(Menu*, gbInterruptDialog);
VARIABLE(Unit*, gbTargetingItem);
VARIABLE(DWORD, uk7);
VARIABLE(DWORD, gbMnuTickCount);
VARIABLE(DWORD, uk8);
VARIABLE(DWORD, uk9);
VARIABLE(DWORD, uk10);
VARIABLE(DWORD, uk11);
VARIABLE(DWORD, gnSetVendorPage);
VARIABLE(DWORD, uk12);
VARIABLE(DWORD, uk13);
VARIABLE(DWORD, uk14);
VARIABLE(DWORD, gnInvMode);
VARIABLE(DWORD, uk15);
VARIABLE(DWORD*, uk16);


// D2Common
VARIABLE(D2SkillsTXT*,		SkillPt);
VARIABLE(DWORD,				SkillNum);
VARIABLE(DWORD,				MonS);
VARIABLE(DWORD,				SupU);
VARIABLE(D2SkillDescTXT*,	SkillDescPt);
VARIABLE(D2ShrinesTXT*,		D2TableShrineTXTpt);
VARIABLE(void*,				levelBinT);



// Fog
VARIABLE(DWORD,			Fog2813);

// Net
VARIABLE(void*,	vNetMsgType1);
VARIABLE(void*,	vNetMsgType2);

// D2Launch
VARIABLE(DWORD,	gpCharMnuAnimPosY);
VARIABLE(D2LaunchPlayerInit*, gpLaunchPlayerInit);
VARIABLE(D2MenuObject*, gpMenuObjectList1);
VARIABLE(D2MenuObject*, gpMenuObjectList2);
VARIABLE(D2MenuObject*, gpMenuObjectList3);
VARIABLE(D2MenuObject*, gpMenuObjectList4);
VARIABLE(D2MenuObject*, gpMenuObjectList5);
VARIABLE(D2MenuObject*, gpMenuObjectList6);
VARIABLE(D2MenuObject*, gpMenuObjectList7);
VARIABLE(D2MenuObject*, gpMenuObjectList8);
VARIABLE(D2MenuObject*, gpMenuObjectList9);
VARIABLE(DWORD, gpMenuMaxCharClasses);
VARIABLE(DWORD, gpStartupMode);
VARIABLE(DWORD, gp6FA3CA4C);
VARIABLE(DWORD, gp6FA3CA68);
VARIABLE(DWORD, gp6FA33224);




/*=================================================================*/
/* D2 Variable pointers			                                   */
/*=================================================================*/


// D2Game
static Cltn			Clients				= (Cltn)			(0x113FB8 + GameOffset); // D2Game.6FD43FB8
static DWORD		sgptDataTables		= (DWORD)			(0xF7060 + GameOffset);  //  D2Game.6FD27060
static HireT		HireTXT				= (HireT)			(0x4A000 + GameOffset);  // D2Game.6FC7A000
static gpServerEvent GetServerEvent     = (gpServerEvent)   (0x115830 + GameOffset);  // D2Game.6FD45830
static gpSetSaveFiles D2SetSaveFiles	= (gpSetSaveFiles)  (0x100E08 + GameOffset);  // D2Game.6FD30E08 
static gdwGameSeed	D2GetGameSeed		= (gdwGameSeed)		(0x11DC40 + GameOffset);  // D2Game.6FD30E08 


// D2Client
static ScrH			ScreenHeight		= (ScrH)		    (0xD40F0 + ClientOffset); // D2Client.6FB740F0
static ScrW			ScreenWidth			= (ScrW)			(0xD40EC + ClientOffset); // D2Client.6FB740EC
//static NpcS1		NpcMenuStruct		= (NpcS1)		0x6FB7BD70;					//1.10
//static NpcS1		NpcMenuStruct		= (NpcS1)		0x6FBA7D7D;					//1.10
//static NpcS2		NpcDataNbr			= (NpcS2)		0x6FB7C4C0;					//1.10
//static NpcS2		NpcDataNbr			= (NpcS2)		0x6FBA7DAD;					//1.10
//static NpcS2		NpcDataNbr			= (NpcS2)		0x6FBA7E0D;					//1.10 - max nbr of menues
static NpcS3		NPCCreatedMenu		= (NpcS3)		    (0x115D3F + ClientOffset); // D2Client.6FBB5D3F
static BoxX			TextBoxX			= (BoxX)		    (0x119F54 + ClientOffset); // D2Client.6FBB9F54
static BoxY			TextBoxY			= (BoxY)		    (0xED220 + ClientOffset); // D2Client.6FB8D220
static MsgMenu		MessageMenu			= (MsgMenu)		    (0x11A6C8 + ClientOffset); // D2Client.6FBBA6C8
static CharName		CharacterName		= (CharName)	    (0x107810 + ClientOffset); // D2Client.6FBA7810
static Uk1			Unknown1			= (Uk1)			    (0x121AF4 + ClientOffset); // D2Client.6FBC1AF4
static Uk2			Unknown2			= (Uk2)			    (0x121AFC + ClientOffset); // D2Client.6FBC1AFC
static NpcU			ActiveNpcUniqueID	= (NpcU)		    (0x115CF5 + ClientOffset); // D2Client.6FBB5CF5
static NpcT			ActiveNpcType		= (NpcT)		    (0x115CF9 + ClientOffset); // D2Client.6FBB5CF9
static NpcI			ActiveNpcID			= (NpcI)		    (0x115CFD + ClientOffset); // D2Client.6FBB5CFD
static NpcM			ActiveNpcMenu		= (NpcM)		    (0x115D3B + ClientOffset); // D2Client.6FBB5D3B
static HireN		HirelingName		= (HireN)		    (0xDA5BC + ClientOffset); // D2Client.6FB7A5BC
static HireR		HireResurrectCost	= (HireR)		    (0x115D9C + ClientOffset); // D2Client.6FBB5D9C
static HireOpt		HirelingOption		= (HireOpt)		    (0xDBF29 + ClientOffset); // D2Client.6FB7BF29
static QuestP		QuestData			= (QuestP)		    (0x115D13 + ClientOffset); // D2Client.6FBB5D13
static SellB		VendorButtons		= (SellB)		    (0xD95B0 + ClientOffset); // D2Client.6FB795B0
static ObjUID		ActiveContainerUID	= (ObjUID)		    (0x11A984 + ClientOffset); // D2Client.6FBBA984
static ClassPt		ClassPointer		= (ClassPt)		    (0x107894 + ClientOffset); // D2Client.6FBA7894
static BorderW		BorderWidth			= (BorderW)		    (0x11A748 + ClientOffset); // D2Client.6FBBA748
static BorderH		BorderHeight		= (BorderH)		    (0x11A74C + ClientOffset); // D2Client.6FBBA74C
static VendorButtonUI	VendorButton_UI		= (VendorButtonUI) (0x11A978 + ClientOffset); // D2Client.6FBBA978
static ptItem		GetptItem			= (ptItem)	        (0x1158F0 + ClientOffset); // D2Client.6FBB58F0
static BeltP		BeltRows			= (BeltP)		    (0xDCD70 + ClientOffset); // D2Client.6FB7CD70
static ActiveTabP	ActiveTab			= (ActiveTabP)      (0xECF00 + ClientOffset); // D2Client.6FB8CF00
static Skilltree_CloseP Skilltree_Close = (Skilltree_CloseP) (0x119838 + ClientOffset); // D2Client.6FBB9838
static SkilldescDisp SkilldescDisplay	= (SkilldescDisp)   (0x119430 + ClientOffset); // D2Client.6FBB9430
static LastKillID	GetLastKillID		= (LastKillID)      (0xD7084 + ClientOffset); // D2Client.6FB77084
static MenuHide1	GetMenuHide1		= (MenuHide1)       (0xDBFE4 + ClientOffset); // D2Client.6FB7BFE4
static MenuHide2	GetMenuHide2		= (MenuHide2)       (0xDC00B + ClientOffset); // D2Client.6FB7C00B
static MenuHide3	GetMenuHide3		= (MenuHide3)       (0xDC032 + ClientOffset); // D2Client.6FB7C032
static MenuHide4	GetMenuHide4		= (MenuHide4)       (0xDC059 + ClientOffset); // D2Client.6FB7C059
static MenuHide5	GetMenuHide5		= (MenuHide5)       (0xDC33E + ClientOffset); // D2Client.6FB7C33E
static gnInventoryMode  GetInventoryMode = (gnInventoryMode)     (0x1158EC + ClientOffset); // D2Client.6FBB58EC 
static RecipeBackground SetRecipeBackground = (RecipeBackground) (0x115B94 + ClientOffset); // D2Client.6FBB5B94
static gpPetRoster	GetPetRoster		= (gpPetRoster)		(0x10B9A0 + ClientOffset); // D2Client.6FBAB9A0
static gpPlayerRoster GetPlayerRoster	= (gpPlayerRoster)  (0x10B994 + ClientOffset); // D2Client.6FBAB994
static KashyaMNBR KashyaMenuNBR			= (KashyaMNBR)		(0xDBF21 + ClientOffset); // D2Client.6FB7BF21
static KashyaHMFunc KashyaHMenuFunc		= (KashyaHMFunc)	(0xDBF37 + ClientOffset); // D2Client.6FB7BF37
static KashyaHMStrkey KashyaHMenuStrkey	= (KashyaHMStrkey)	(0xDBF29 + ClientOffset); // D2Client.6FB7BF29
static KashyaDMFunc KashyaDMenuFunc		= (KashyaDMFunc)	(0xDBF3B + ClientOffset); // D2Client.6FB7BF3B
static KashyaDMStrkey KashyaDMenuStrkey	= (KashyaDMStrkey)	(0xDBF2B + ClientOffset); // D2Client.6FB7BF2B
static Uk3			Unknown3			= (Uk3)				(0x115C3B + ClientOffset); // D2Client.6FBB5C3B
static pNPCTransactionState	gpNPCTransactionState	= (pNPCTransactionState)	(0x115D01 + ClientOffset); // D2Client.6FBB5D01
static SetANpcUniID	SetActiveNpcUniqueID = (SetANpcUniID)	(0x11A980 + ClientOffset); // D2Client.6FBBA980
static Uk5			gbGambling			= (Uk5)				(0x115D7C + ClientOffset); // D2Client.6FBB5D7C
static Sacp		SetActiveClientPlayer	= (Sacp)			(0x115DA0 + ClientOffset); // D2Client.6FBB5DA0
//static Gfx_Data		D2GetGfxData		= (Gfx_Data)	0x6FBBA748;				//1.10
//static Gfx_Recipe6FBB5B94	D2SetRecipe	= (Gfx_Recipe6FBB5B94) 0x6FBB5B94;			//1.10
static Uk6			Unknown6			= (Uk6)				(0x115DA4 + ClientOffset); // D2Client.6FBB5DA4
static APannelCharBtn ActivePannelCharBtn = (APannelCharBtn) (0x115BBC + ClientOffset); // D2Client.6FBB5BBC
static APannelSkillBtn ActivePannelSkillBtn = (APannelSkillBtn) (0x115BC0 + ClientOffset); // D2Client.6FBB5BC0
static gpClientUnitList GetClientUnitList = (gpClientUnitList) (0x11AA00 + ClientOffset); // D2Client.6FBBAA00
static gpMonsterPalettes	GetMonsterPalettes	  = (gpMonsterPalettes)	 (0x107C10 + ClientOffset); // D2Client.6FBA7C10
static gpRandMonPaletteLoaded GetLoadedRandMonPalette = (gpRandMonPaletteLoaded) (0x109A18 + ClientOffset); // D2Client.6FBA9A18
static gpRandMonPalette	GetRandMonPalette	  = (gpRandMonPalette)	 (0x107C18 + ClientOffset); // D2Client.6FBA7C18
static gpHlthmanaGfx GetHlthmanaGfx = (gpHlthmanaGfx) (0x115E30 + ClientOffset); // D2Client.6FBB5E30 Hlthmana
static gpOverlapGfx GetOverlapGfx = (gpOverlapGfx) (0x115E8C + ClientOffset); // D2Client.6FBB5E8C overlap
static gpRedUIPalette GetRedUIPalette = (gpRedUIPalette) (0x115EE8 + ClientOffset); // D2Client.6FBB5EE8
static gpYellowUIPalette GetYellowUIPalette = (gpYellowUIPalette) (0x115EE9 + ClientOffset); // D2Client.6FBB5EE9
static gpBlueUIPalette GetBlueUIPalette = (gpBlueUIPalette) (0x115EEA + ClientOffset); // D2Client.6FBB5EEA	
static gpSkillLevelBTN GetSkillLevelBTN = (gpSkillLevelBTN) (0x115BB4 + ClientOffset); // D2Client.6FBB5BB4
static gp800BorderFrame Get800BorderFrame = (gp800BorderFrame) (0x115E3C + ClientOffset); // D2Client.6FBB5E3C
static gpCtrlPnl7       GetCtrlPnl7     = (gpCtrlPnl7) (0x115E44 + ClientOffset); // D2Client.6FBB5E44
static gp800CtrlPnl7    Get800CtrlPnl7  = (gp800CtrlPnl7) (0x115E54 + ClientOffset); // D2Client.6FBB5E54
static gpRunButton      GetRunButton    = (gpRunButton) (0x115E48 + ClientOffset); // D2Client.6FBB5E48
static gpMouseLeftClick GetMouseLeftClick = (gpMouseLeftClick) (0x115EE4 + ClientOffset); // D2Client.6FBB5EE4
static gpMiniPanelBtn   GetMiniPanelBtn = (gpMiniPanelBtn) (0x115E18 + ClientOffset); // D2Client.6FBB5E18 
static gpUk_6FBB5EDC	GetUk_6FBB5EDC = (gpUk_6FBB5EDC) (0x115EDC + ClientOffset); // D2Client.6FBB5EDC
static gbBeltHoverOpen  GetBeltHoverOpen = (gbBeltHoverOpen) (0x115EA8 + ClientOffset); // D2Client.6FBB5EA8 
static gbBeltPermOpen  GetBeltPermOpen = (gbBeltPermOpen) (0x115EA4 + ClientOffset); // D2Client.6FBB5EA4
static gbBeltUk1	  GetBeltUk1		= (gbBeltUk1) (0x115EAC + ClientOffset); // D2Client.6FBB5EAC
static gbCltrlPnl_PopBelt GetCltrlPnl_PopBelt	= (gbCltrlPnl_PopBelt) (0x115E1C + ClientOffset); // D2Client.6FBB5E1C
static gbResMode	   GetResMode		= (gbResMode) (0xFA708 + ClientOffset); // D2Client.6FB9A708
static gpBeltItem	   GetBeltItem		= (gpBeltItem) (0x115EB4 + ClientOffset); // D2Client.6FBB5EB4
static gpBeltItem2	   GetBeltItem2		= (gpBeltItem2) (0x115EA0 + ClientOffset); // D2Client.6FBB5EA0
static gpUIBeltRowColor1 GetBeltRowColor1 = (gpUIBeltRowColor1) (0x115E38 + ClientOffset); // D2Client.6FBB5E38	
static gpUIBeltRowColor2 GetBeltRowColor2 = (gpUIBeltRowColor2) (0x115E39 + ClientOffset); // D2Client.6FBB5E39	
static gpUIBeltRowColor3 GetBeltRowColor3 = (gpUIBeltRowColor3) (0x115E3A + ClientOffset); // D2Client.6FBB5E3A	
static gpUIBeltRowColor4 GetBeltRowColor4 = (gpUIBeltRowColor4) (0x115E3B + ClientOffset); // D2Client.6FBB5E3B	
static gpItemGUID        GetItemGUID      = (gpItemGUID) (0xDCD90 + ClientOffset); // D2Client.6FB7CD90	
static gnBeltItemSlot    GetBeltItemSlot  = (gnBeltItemSlot) (0xDCD78 + ClientOffset); // D2Client.6FB7CD78
static gnBeltHoverX		 GetBeltHoverX	  = (gnBeltHoverX) (0xDCD7C + ClientOffset); // D2Client.6FB7CD7C
static gnBeltHoverY		 GetBeltHoverY	  = (gnBeltHoverY) (0xDCD80 + ClientOffset); // D2Client.6FB7CD80
static gnCltLevelID		 GetCltLevelID	  = (gnCltLevelID) (0x115EF0 + ClientOffset); // D2Client.6FBB5EF0
static gnCltObjectID	 GetCltObjectID	  = (gnCltObjectID) (0x115EF4 + ClientOffset); // D2Client.6FBB5EF4
static gpLevelSocketGfx  GetLevelSocketGfx = (gpLevelSocketGfx) (0x115BB8 + ClientOffset); // D2Client.6FBB5BB8
static cltLvlMap		 cltLvlmapPt	   = (cltLvlMap) (0xF7A17 + ClientOffset); // D2Client.6FB971A7
static gpWindowstate	 GetWindowstate	   = (gpWindowstate) (0xD4C04 + ClientOffset); // D2Client.6FB74C04
static gpCltDifficulty   GetClientDifficulty = (gpCltDifficulty) (0x10795C + ClientOffset); // D2Client.6FBA795C
static gpVendorItemCost  GetRepairCost = (gpVendorItemCost) (0x115D4B + ClientOffset); // D2Client.6FBB5D4B 	
static gbVendorItemCost	 GetVendorItemCost = (gbVendorItemCost) (0x115BD0 + ClientOffset); // D2Client.6FBB5BD0 
static gbVendorPosXBtn   GetVendorPosXBtn = (gbVendorPosXBtn) (0x115D1B + ClientOffset); // D2Client.6FBB5D1B
static gbVendorPosYBtn   GetVendorPosYBtn = (gbVendorPosYBtn) (0x115D1F + ClientOffset); // D2Client.6FBB5D1F
static gbTickCount		 D2GetTickCount = (gbTickCount) (0x115BE1 + ClientOffset); // D2Client.6FBB5BE1
static gbUnitUnID		 GetUnitUnID    = (gbUnitUnID) (0x115BD5 + ClientOffset); // D2Client.6FBB5BD5
static gbUnitID			 GetUnitID		= (gbUnitID) (0x115BD1 + ClientOffset); // D2Client.6FBB5BD1
static gbResultsDialog   gpResultsDialog = (gbResultsDialog) (0x115D33 + ClientOffset); // D2Client.6FBB5D33 
static gbWaitDialog		 gpWaitDialog   = (gbWaitDialog) (0x115D37 + ClientOffset); // D2Client.6FBB5D37 
static gbConfirmDialog	 gpConfirmDialog = (gbConfirmDialog) (0x115D2F + ClientOffset); // D2Client.6FBB5D2F 	
static gbMnuUk1			 gpMnuUk1		= (gbMnuUk1) (0x115D2F + ClientOffset); // D2Client.6FBB5D43 
static gbInterruptDialog gpInterruptDialog = (gbInterruptDialog) (0x115D47  + ClientOffset); // D2Client.6FBB5D47	
static gbTargetingItem	 gpTargetingItem = (gbTargetingItem) (0x111AA4 + ClientOffset); // D2Client.6FBC1AA4
static uk7				 unknown7	     = (uk7) (0x111AA4  + ClientOffset); // D2Client.6FBB5C3B 
static gbMnuTickCount	 gpMnuTickCount  = (gbMnuTickCount) (0x115BD9 + ClientOffset); // D2Client.6FBB5BD9 
static uk8			     unknown8		 = (uk8) (0x115BDD + ClientOffset); // D2Client.6FBB5BDD 		 
static uk9				 unknown9		 = (uk9) (0x115D2F + ClientOffset); // D2Client.6FBB5D2F 
static uk10				 gpMaxVendorTabs = (uk10) (0xD95A8 + ClientOffset); // D2Client.6FB795A8 
static uk11				 unknown11       = (uk11) (0x115940 + ClientOffset); // D2Client.6FBB5940 
static gnSetVendorPage   gpSetVendorPage = (gnSetVendorPage) (0x115900 + ClientOffset); // D2Client.6FBB5900 
static uk12				 unknown12       = (uk12) (0x115B38 + ClientOffset); // D2Client.6FBB5B38 
static uk13				 unknown13       = (uk13) (0x113790 + ClientOffset); // D2Client.6FBB3790  
static uk14				 unknown14       = (uk14) (0x1158E8 + ClientOffset); // D2Client.6FBB58E8 
static gnInvMode	     gpInvMode       = (gnInvMode) (0x1158EC + ClientOffset); // D2Client.6FBB58EC 
static uk15				 unknown15       = (uk15) (0xD95AC + ClientOffset); // D2Client.6FB795AC 
static uk16				 unknown16       = (uk16) (0xD956E + ClientOffset); // D2Client.6FB7956E



// D2Common
static SkillPt				SkillPointer		= (SkillPt)				(0xAA1A0 + CommonOffset);	//1.10
static SkillNum				NumberOfSkills		= (SkillNum)			(0xAA1A8 + CommonOffset);	//1.10
static MonS					MonStats			= (MonS)				(0xAA084 + CommonOffset);	//1.10
static SupU					SuperUniques		= (SupU)				(0xAA0E0 + CommonOffset);	//1.10
static SkillDescPt			SkillDescPointer	= (SkillDescPt)			(0xAA194 + CommonOffset);	//1.10
static D2TableShrineTXTpt	D2TableShrineTXT	= (D2TableShrineTXTpt)	(0xAA69C + CommonOffset);
static levelBinT			levelBinPt			= (levelBinT)			(0xAA260 + CommonOffset);	//1.10

// Fog
static Fog2813		gdwBitMasks			= (Fog2813)		(0x21308 + FogOffset);		// D2Fog.6FF71308

// D2Net
static vNetMsgType1 D2NetMsgType1	= (vNetMsgType1) (0x0B0B4 + NetOffset);			// D2Net.6FC0B0B4
static vNetMsgType2 D2NetMsgType2	= (vNetMsgType2)  (0x0B0A0 + NetOffset);		// D2Net.6FC0B0A0

// D2Launch
static gpCharMnuAnimPosY D2CharMnuAnimPosY = (gpCharMnuAnimPosY) (0x2B894 + LaunchOffset);		// D2Launch.6FA3B894
static gpLaunchPlayerInit D2_LaunchPlayerInit = (gpLaunchPlayerInit) (0x2CA5C + LaunchOffset);	// D2Launch.6FA3CA5C
static gpMenuObjectList1 D2_MenuObjectList1 = (gpMenuObjectList1) (0x2C910 + LaunchOffset);		// D2Launch.6FA3C910
static gpMenuObjectList2 D2_MenuObjectList2 = (gpMenuObjectList2) (0x2C914 + LaunchOffset);		// D2Launch.6FA3C914
static gpMenuObjectList3 D2_MenuObjectList3 = (gpMenuObjectList3) (0x2C918 + LaunchOffset);		// D2Launch.6FA3C918
static gpMenuObjectList4 D2_MenuObjectList4 = (gpMenuObjectList4) (0x2C91C + LaunchOffset);		// D2Launch.6FA3C91C
static gpMenuObjectList5 D2_MenuObjectList5 = (gpMenuObjectList5) (0x2C920 + LaunchOffset);		// D2Launch.6FA3C920
static gpMenuObjectList6 D2_MenuObjectList6 = (gpMenuObjectList6) (0x2C924 + LaunchOffset);		// D2Launch.6FA3C924
static gpMenuObjectList7 D2_MenuObjectList7 = (gpMenuObjectList7) (0x2C928 + LaunchOffset);		// D2Launch.6FA3C928
static gpMenuObjectList8 D2_MenuObjectList8 = (gpMenuObjectList8) (0x2C92C + LaunchOffset);		// D2Launch.6FA3C92C
static gpMenuObjectList9 D2_MenuObjectList9 = (gpMenuObjectList9) (0x2CA70 + LaunchOffset);		// D2Launch.6FA3CA70
static gpMenuMaxCharClasses D2MenuMaxCharClasses = (gpMenuMaxCharClasses) (0x23224 + LaunchOffset);		// D2Launch.6FA33224
static gpStartupMode D2StartupMode = (gpStartupMode) (0x2CD5C + LaunchOffset);			// D2Launch.6FA3CD5C
static gp6FA3CA4C D26FA3CA4C = (gp6FA3CA4C) (0x2CA4C + LaunchOffset);			// D2Launch.6FA3CA4C
static gp6FA3CA68 D26FA3CA68 = (gp6FA3CA68) (0x2CA68 + LaunchOffset);			// D2Launch.6FA3CA68
static gp6FA33224 D26FA33224 = (gp6FA33224) (0x23224 + LaunchOffset);			// D2Launch.6FA33224

// Global variable constants

#endif