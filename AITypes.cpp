#include "Common.hpp"
#include "error.hpp"
#include "d2functions.hpp"
#include "d2customtxt.hpp"
#include "d2wrapper.hpp"
#include "AITypes.hpp"

/*    Date: 2017-07-06
    Author: Thomas Westman
    Function: AITypes SWitch table

    6FCEBBF3   FF2485 04C8CE6F  JMP DWORD PTR DS:[EAX*4+6FCEC804]
*/

const AITypesFN gpfAITypesFN[] =
{
    {(AITYPESFN) (GameOffset + 0xBBD3E)},    // 0  0x6FCEBD3E
    {(AITYPESFN) (GameOffset + 0xBBDC6)},    // 1  0x6FCEBDC6
    {(AITYPESFN) (GameOffset + 0xBBE4C)},    // 2  0x6FCEBE4C
    {(AITYPESFN) (GameOffset + 0xBBDC6)},    // 3  0x6FCEBDC6
    {(AITYPESFN) (GameOffset + 0xBBF5F)},    // 4  0x6FCEBF5F
    {(AITYPESFN) (GameOffset + 0xBC038)},    // 5  0x6FCEC038
    {(AITYPESFN) (GameOffset + 0xBBDC6)},    // 6  0x6FCEC09C
    {(AITYPESFN) (GameOffset + 0xBC09C)},    // 7  0x6FCEC25A
    {(AITYPESFN) (GameOffset + 0xBC665)},    // 8  0x6FCEC665
    {(AITYPESFN) (GameOffset + 0xBC665)},    // 9  0x6FCEC665
    {(AITYPESFN) (GameOffset + 0xBC2DB)},    // 10  0x6FCEC2DB
    {(AITYPESFN) (GameOffset + 0xBC3DC)},    // 11  0x6FCEC3DC
    {(AITYPESFN) (GameOffset + 0xBC520)}    // 12  0x6FCEC520
};

const DWORD gdwAITypesFN = ARRAY(gpfAITypesFN);
