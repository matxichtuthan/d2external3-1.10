/*=================================================================*/
/*  @file Custom.cpp
 *
 *
 *  (c) May 2003 - Allan Jensen.
 */
/*=================================================================*/

#include "d2functions.hpp"
#include "d2customtxt.hpp"
#include "error.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <wchar.h>
#include <wctype.h>
#include "Common.hpp"

DynBook* D2DynBook;

// Class DLClass
DynBook::DynBook()
{
	Init();
}

DynBook::~DynBook()
{

}

void DynBook::Init()
{
	SaveTownID[0] = 1;
	SaveTownID[1] = 40;
	SaveTownID[2] = 75;
	SaveTownID[3] = 103;
	SaveTownID[4] = 109;
	TownFlag = 0;
	LastVisitAct = 0;
	TownDifficulty = 0;
	SaveTownID_NM[0] = 1;
	SaveTownID_NM[1] = 40;
	SaveTownID_NM[2] = 75;
	SaveTownID_NM[3] = 103;
	SaveTownID_NM[4] = 109;
	SaveTownID_HELL[0] = 1;
	SaveTownID_HELL[1] = 40;
	SaveTownID_HELL[2] = 75;
	SaveTownID_HELL[3] = 103;
	SaveTownID_HELL[4] = 109;

}

void DynBook::SetTownID(BYTE Act, DWORD LevelID)
{
	SaveTownID[Act] = LevelID;
}

DWORD DynBook::GetTownID(BYTE Act)
{
	return SaveTownID[Act];
}

void DynBook::SetTownFlag(BYTE Flag)
{
	TownFlag = Flag;
}

BYTE DynBook::GetTownFlag()
{
	return TownFlag;
}

void DynBook::SetLastVisitAct(BYTE act)
{
	LastVisitAct = act;
}

BYTE DynBook::GetLastVisitAct()
{
	return LastVisitAct;
}

void DynBook::SetCurrentTownDifficulty(BYTE Difficulty)
{
	TownDifficulty = Difficulty;
}

BYTE DynBook::GetCurrentTownDifficulty()
{
	return TownDifficulty;
}

void DynBook::SetTownID_NM(BYTE Act, DWORD LevelID)
{
	SaveTownID_NM[Act] = LevelID;
}

DWORD DynBook::GetTownID_NM(BYTE Act)
{
	return SaveTownID_NM[Act];
}

void DynBook::SetTownID_HELL(BYTE Act, DWORD LevelID)
{
	SaveTownID_HELL[Act] = LevelID;
}

DWORD DynBook::GetTownID_HELL(BYTE Act)
{
	return SaveTownID_HELL[Act];
}



void initvar()
{
	D2DynBook = new DynBook;
}


// Made this one for simplicity, instead of rewriting every item code into hex.
DWORD STDCALL Code2Bin(const char* ptCode)
{
	DWORD ItemCode = 0;

	if(strlen(ptCode)!=4)
	{
		ItemCode = Code2Bin("key ");
	}
	else
	{
		for(int i=3;i>=0;i--)
		{
			ItemCode += (DWORD)(ptCode[i]*pow(16.0, (2*i)));
		}
	}

	return ItemCode;
}

bool TestCode(DWORD dwCode, const char* code)
{
	return (dwCode==Code2Bin(code));
}

int GetRand(int nMaxNum)
{
	nMaxNum = (nMaxNum < 0 ? -nMaxNum : nMaxNum);
	int num = (rand()*nMaxNum)/RAND_MAX;
	return (num >= nMaxNum ? 0 : num);
}

/* Code by: Thomas Westam
   Date: 2012-08-17
   Function: Get ptTable	
*/

D2DataTables* D2GetDataTables()
{
	DWORD* ptTbl = (DWORD*)*((DWORD*)sgptDataTables);

	if (!ptTbl)
		return NULL;

	D2DataTables* ptTable = (D2DataTables*)*ptTbl;

	if (!ptTable)
		return NULL;

	return ptTable;
}


/* Code by: Thomas Westam
   Date: 2012-08-17
   Function: Get ptItemStatsTXT	
*/

D2ItemStatCostTXT* D2GetItemStatsTXT(int nStat)
{
	if (nStat < 0 || nStat > (signed)D2GetDataTables()->D2ItemStatCostCount)
		return NULL;

	D2ItemStatCostTXT* ptItemStatCostTXT = D2GetDataTables()->D2ItemStatCost;

	if (!ptItemStatCostTXT)
		return NULL;

	return ptItemStatCostTXT = &ptItemStatCostTXT[nStat];
}

/* Code by: Thomas Westam
   Date: 2012-08-18
   Function: Get ptStatesTXT
*/

D2StatesTXT* D2GetStatesTXT(int nState)
{
	if (nState < 0 || nState > (signed)D2GetDataTables()->D2StatesCount || nState >= STATE_MAX_INDEX)
		return NULL;

	D2StatesTXT* ptStatesTXT = D2GetDataTables()->D2StatesTxt;

	if (!ptStatesTXT)
		return NULL;

	return ptStatesTXT = &ptStatesTXT[nState];
}

/* Code by: Thomas Westam
   Date: 2012-08-26
   Function: Get ptMissilesTXT	
*/

D2MissilesTXT* D2GetMissilesTXT(int MissileID)
{
	if (MissileID < 0 || MissileID > (signed)D2GetDataTables()->D2MissilesCount)
		return NULL;

	D2MissilesTXT* ptMissilesTXT = D2GetDataTables()->D2Missiles;

	if (!ptMissilesTXT)
		return NULL;

	return ptMissilesTXT = &ptMissilesTXT[MissileID];
}

/* Code by: Thomas Westam
   Date: 2012-09-08
   Function: Get ptMonstatsTXT	
*/

D2MonstatsTXT* D2GetMonstatsTXT(int MonstatID)
{
	if (MonstatID < 0 || MonstatID > (signed)D2GetDataTables()->D2MonstatsCount)
		return NULL;

	D2MonstatsTXT* ptMonstatsTXT = D2GetDataTables()->D2Monstats;

	if (!ptMonstatsTXT)
		return NULL;

	return ptMonstatsTXT = &ptMonstatsTXT[MonstatID];
}

/* Code by: Thomas Westam
   Date: 2013-03-10
   Function: Get ptCharstatsTXT
*/

D2CharStatsTXT* D2GetCharStatsTXT(int CharStatID)
{
	if (CharStatID < 0 || CharStatID > (signed)D2GetDataTables()->D2CharStatCount)
		return NULL;

	D2CharStatsTXT* ptCharStatsTXT = D2GetDataTables()->D2CharStat;

	if (!ptCharStatsTXT)
		return NULL;

	return ptCharStatsTXT = &ptCharStatsTXT[CharStatID];
}

/* Code by: Thomas Westam
   Date: 2013-06-03
   Function: Get ptSkillDescTXT
*/


D2SkillDescTXT* D2GetSkillDescTXT(int SkilldescID)
{
	if (SkilldescID < 0 || SkilldescID > (signed)D2GetDataTables()->D2SkillDescCount)
		return NULL;

	D2SkillDescTXT* ptSkillDescTXT = D2GetDataTables()->D2Skilldesc;

	if (!ptSkillDescTXT)
		return NULL;

	return ptSkillDescTXT = &ptSkillDescTXT[SkilldescID];
}

/* Code by: Thomas Westam
   Date: 2016-01-10
   Function: Get ptMonstats2TXT
*/

D2Monstats2TXT* D2GetMonstats2TXT(int MonstatEx)
{
	if (MonstatEx < 0 || MonstatEx > (signed)D2GetDataTables()->D2Monstats2Count)
		return NULL;

	D2Monstats2TXT* ptMonstats2TXT = D2GetDataTables()->D2Monstats2;

	if (!ptMonstats2TXT)
		return NULL;

	return ptMonstats2TXT = &ptMonstats2TXT[MonstatEx];
}


/* Code by: Thomas Westam
   Date: 2014-10-12
   Function: Get ptLevelsTXT
*/


D2LevelsTXT* D2GetLevelsTXT(int LevelID)
{
	if (LevelID < 0 || LevelID > (signed)D2GetDataTables()->D2LevelsCount)
		return NULL;

	D2LevelsTXT* ptLevelsTXT = D2GetDataTables()->D2LevelsTXT;

	if (!ptLevelsTXT)
		return NULL;

	return ptLevelsTXT = &ptLevelsTXT[LevelID];
}


Unit* FASTCALL FindItemInPage(D2Inventory* ptInventory, DWORD dwCode,  BYTE bPage, Unit* ptOldItem/* = NULL*/)
{
	Unit* ptItem = NULL;

	if (ptInventory == NULL){ // Check if ptInventory == NULL
		log_message("FindItemInPage Func: ptInventory == %d", ptInventory);
		return NULL;
	}

	if(ptOldItem)
		ptItem = D2GetNextInventoryItem(ptOldItem);
	else
		ptItem = D2GetInventoryItem(ptInventory);

	ptItem = D2CheckInventoryItem(ptItem);

	while(ptItem)
	{
		if(dwCode == D2GetItemCode(ptItem))
		{
			if(D2GetPage(ptItem) == bPage) return ptItem;
		}

		ptItem = D2GetNextInventoryItem(ptItem);
		ptItem = D2CheckInventoryItem(ptItem);
	}

	// If ptItem == NULL write error	
	
	// log_message("FindItemInPage Func: ptItem == %d", ptItem);

	return NULL;
}

Unit* FASTCALL FindItemInBelt(D2Inventory* ptInventory, DWORD dwCode)
{
	if (ptInventory == NULL){ // Check if ptInventory == NULL
		log_message("FindUdfItemsByQuality Func: ptInventory == %d", ptInventory);
		return NULL;
	}

	Unit* ptItem = D2GetInventoryItem(ptInventory);
	ptItem = D2CheckInventoryItem(ptItem);

	while(ptItem)
	{
		if(D2GetNodePage(ptItem) == NODEPAGE_BELT)
		{
			if(dwCode == D2GetItemCode(ptItem)) return ptItem;
		}
		ptItem = D2GetNextInventoryItem(ptItem);
		ptItem = D2CheckInventoryItem(ptItem);
	}
	return NULL;
}

void DASpawnItem(D2Game* ptGame, Unit* ptUnit, DWORD itemCode, DWORD ilvl, DWORD quality, DWORD page, BYTE CubeClientMsg, BYTE unidItem, BOOL nBeltInv/* = FALSE*/, BOOL notSellable /* = FALSE*/, BOOL nPlayerName /* = FALSE*/)
{
	int dwDummy = 0;
	DWORD unitLevel = 0;
	Unit* ptItem = NULL;
	DWORD maxDurability = 0;
	BOOL nInvItem = 0;
	int Offsets[2];
	DRLGRoom* ptRoom = NULL;

	if (ptGame == NULL){ // Check if ptGame == NULL
		log_message("DASpawnItem Func: ptGame == %d", ptGame);
		return;
	}

	if (ptUnit == NULL){ // Check if ptUnit == NULL
		log_message("DASpawnItem Func: ptUnit == %d", ptUnit);
		return;
	}

	D2ItemsTXT* ptItemsTXT = D2GetItemTXTFromCode(itemCode, &dwDummy);

	if (ptItemsTXT != NULL)
	{
		unitLevel = D2GetUnitLevel(ptUnit, 0);

		if (ilvl == 0) ilvl = unitLevel;

		ptItem = D2CreateItem_Wrapper(ptUnit, D2GetItemIDfromCode(itemCode), ptGame, 4, quality, 0, 1, ilvl, 0, 0, 0);
		
		if (ptItem == NULL){ // Check if ptItem == NULL
			log_message("DASpawnItem Func: ptItem == %d", ptItem);
			return;
		}

		quality = D2GetQuality(ptItem);

		if ((quality == ITEMQUALITY_SET || quality == ITEMQUALITY_UNIQUE) && unidItem == 1) D2SetFlags(ptItem, ITEMFLAG_UNIDENTIFIED, 0);

		if (CubeClientMsg == 1)
		{
			/* DA Code removed
			switch(quality)
			{
				case ITEMQUALITY_CRACKED: D2ClientMsgTop(D2GetStringFromIndex(53694), D2_Orange);
				break;
				case ITEMQUALITY_NORMAL: D2ClientMsgTop(D2GetStringFromIndex(53695), D2_Orange);
				break; 
				case ITEMQUALITY_SUPERIOR: D2ClientMsgTop(D2GetStringFromIndex(53696), D2_Orange);
				break;
				case ITEMQUALITY_MAGIC: D2ClientMsgTop(D2GetStringFromIndex(53697), D2_Orange);
				break;
				case ITEMQUALITY_SET: D2ClientMsgTop(D2GetStringFromIndex(53693), D2_Orange);
				break;
				case ITEMQUALITY_RARE: D2ClientMsgTop(D2GetStringFromIndex(53698), D2_Orange);
				break;
				case ITEMQUALITY_UNIQUE: D2ClientMsgTop(D2GetStringFromIndex(53692), D2_Orange);	
				break;
			}*/

		}

		maxDurability = D2GetMaxDurability(ptItem);
			
		if (maxDurability > 0) D2SetUnitStat(ptItem, STATS_DURABILITY, maxDurability, 0);


		if (notSellable == TRUE){
			D2SetFlags(ptItem,ITEMFLAG_NONSELLABLE,1); // 4096 set non sellable flag
			D2SetFlags(ptItem,ITEMFLAG_FROMPLAYER,1); // 16777216
		}

		int i = 0;

		// add player name

		if (nPlayerName == TRUE)
		{
			D2SetPlayerIDonItem(ptItem,ptUnit->nUnitUnid);
			D2PlayerData* playerdata = D2GetPlayerData(ptUnit);
		
			if (playerdata)
			{
				for (i=0; i<=15; i++)
					ptItem->pItemData->IName[i] = playerdata->CharName[i];
			}
		}


		BOOL beltable = D2Beltable(ptItem);
		BOOL check = FALSE;
		WORD OffsetY = 0;
		WORD OffsetX = 0;

		if (nBeltInv == TRUE && beltable != FALSE)
		{
			check = D2GameAddItemtoBelt(ptUnit->ptGame, ptUnit, ptItem->nUnitUnid, ptUnit->hPath->targetx, 1, &dwDummy);

			if (check == FALSE){
				D2SetPage(ptItem, PAGE_INVENTORY);
				OffsetY = D2GetOffsetYFromUnit(ptItem);
				OffsetX = D2GetOffsetXFromUnit(ptItem);

				nInvItem = D2PlaceItem(__FILE__, __LINE__, ptUnit->ptGame, ptUnit, ptItem->nUnitUnid, OffsetX, OffsetY, 1, 1, 0);

				if (nInvItem == 0)
				{
					D2GetMapOffsetsFromUnit(ptUnit, Offsets);
					ptRoom = D2GetRoomFromUnit(ptUnit);

					D2ServerDropUnitOnGround(ptGame, 0, ptItem, ptRoom, Offsets[0], Offsets[1]);
				}
			}
		}else{
			D2SetPage(ptItem, page);
			
			nInvItem = D2PlaceItem(__FILE__, __LINE__, ptGame, ptUnit, ptItem->nUnitUnid, 0, 0, 1, 1, 0);

			if (nInvItem == 0)
			{
				D2GetMapOffsetsFromUnit(ptUnit, Offsets);
				ptRoom = D2GetRoomFromUnit(ptUnit);

				D2ServerDropUnitOnGround(ptGame, 0, ptItem, ptRoom, Offsets[0], Offsets[1]);
			}
		}
		
	}
}

BOOL FASTCALL ChanceCalc(WORD Pct, DWORD Max/* = 0*/)
{
	if(Max == 0)
		Max = CHANCE_CALC;

	if(Pct >= Max)
		return TRUE;

	int calc1 = GetRand(Max);
	int calc2 = GetRand(Max);

	if(calc1 <= Pct && calc2 <= Pct)
		return TRUE;

	return FALSE;
}

