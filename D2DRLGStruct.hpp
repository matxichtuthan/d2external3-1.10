/*=================================================================*/
/** @file D2DRLGStruct.h
 *  @brief Diablo II DRLG Structures definitions.
 *
 *  (c) Mars 2013 - Thomas Westman.
 */
/*=================================================================*/

#ifndef __D2DRLGSTRUCT_H__INCLUDED__
#define __D2DRLGSTRUCT_H__INCLUDED__

/*=================================================================*/
/*    Room Tiles Structure.                                        */
/*=================================================================*/

struct RoomTile 
{                                 //Offset from Code
};

struct RoomTile2
{
	DWORD		  uk1;			  //+00
	DWORD		  uk2;			  //+04
	WORD		  uk3;			  //+08
	WORD		  uk4;			  //+0A
	DWORD		  uk5;			  //+0C
	DWORD		  uk6;			  //+10
};

struct Coords 
{
	DWORD nPosGameX;	//+00 
	DWORD nPosGameY;	//+04 
	DWORD nSizeGameX;	//+08 
	DWORD nSizeGameY;	//+0c 
	DWORD nPosTileX;	//+10 
	DWORD nPosTileY;	//+14 
	DWORD nSizeTileX;	//+18 
	DWORD nSizeTileY;	//+1c 
};

struct CollisionHdr 
{
}; 

struct D2TileRecord
{
};

struct DRLGDelete
{
};

struct DRLGUnits
{
};

/*=================================================================*/
/*    First Dungeon Room Logical Grid Structure.                   */
/*=================================================================*/

struct D2GFXTileStrc
{
   DWORD _00;
   WORD  _04;
   WORD fMaterial;
   DWORD _08;
   DWORD _0C;
   DWORD _10;
   int nType;
   int nCheck;
   int nIndex;
};

struct D2MapTileStrc
{
	DWORD _00;                 // +000
	DWORD _04;                 // +004
	int nXpos;                 // +008
	int nYpos;                 // +00C
	DWORD _10;                 // +010
	DWORD fFlags;              // +014
	D2GFXTileStrc* pGfxTile;   // +018
	int nRoof;                 // +01C
	DWORD dwLoad;			   // +020
	DWORD _24;				   // +024
	DWORD _28;				   // +028
	DWORD _2C;				   // +02C
};

struct DRLGCoords 
{
    DWORD dwSubtilesLeft;     //+00
    DWORD dwSubtilesTop;      //+04
    DWORD dwSubtilesWidth;    //+08
    DWORD dwSubtilesHeight;   //+0C
    DWORD dwTilesLeft;        //+10
    DWORD dwTilesTop;         //+14
    DWORD dwTilesWidth;       //+18
    DWORD dwTilesHeight;      //+1C
};

struct DRLGRoomTiles
{
	D2MapTileStrc* pWallTiles;		//+00
	int nWalls;						//+04
	D2MapTileStrc* pFloorTiles;		//+08
	int nFloors;					//+0C
};

struct D2RoomCollisionStrc	//sizeof 0x24
{
	DRLGCoords pCoords;			//+00
	WORD* pCollisionMask;		//+20
};

struct DRLGRoom						// DrlgRoom1 
{									//Offset from Code
	union
	{
		struct
		{
			DWORD nSubTileXpos;					//+00 xpos1
			DWORD nSubTileYpos;					//+04 ypos1
			DWORD nSubTileWidth;				//+08 xsize1
			DWORD nSubTileHeight;				//+0C ysize1

			DWORD nTileXpos;                  //+10 xpos2
			DWORD nTileYpos;                  //+14 ypos2
			DWORD nTileWidth;                 //+18 xsize2
			DWORD nTileHeight;                //+1C ysize2
		};
		DRLGCoords pCoords;					//+00
	};
	DRLGRoomTiles*  pTileGrid;			//+20 D2TileRecord*	pTileGrid;
    DRLGRoom** ppRoomsNear;				//+24 void*			ptRoomsNear;
	DWORD nRoomsNear;					//+28 void*			nRoomsNear;
    Unit* pUnitFirst;				//+2C Unit*			pUnitFirst; 
    Unit* pUnitUpdate;			//+30 Unit*			pUnitUpdate;
	D2RoomCollisionStrc* pCollisionGrid;//+34 Coords*		pCoords;
	DRLGRoomEx*	pRoomEx;				//+38 DrlgRoom2*	pRoom2;
	D2Seed seed;						//+3C D2Seed 			seed; _40;
	DRLGDelete* pDelete;				//+44
	DWORD fRoomFlags;					//+48
	Client** pClients;					//+4C Client**		pClients;
	int nMaxClients;					//+50
	int nClients;						//+54
	int nTileCount;						//+58 nUnk;
	int nAllies;						//+5C
	DWORD dwFallenGUID[4];				//+60 _60; _64; GUID[4]
	DWORD dw[2];						//+70 continue.. GUID[4]
    DRLGUnits* pUnits;					//+78		
    DRLGRoom* pNext;					//+7C DrlgRoom1*		pNext;
};


/*=================================================================*/
/*   Second Dungeon Room Logical Grid Structure.                   */
/*=================================================================*/

struct DrlgRoom2					// DLRGRoomEx
{									//Offset from Code
	DrlgLevel*		pDrlgLevel;     //+00
	DWORD			xPos;           //+04
	DWORD			yPos;           //+08
	DWORD			unknown1[3];	//+0A
	DWORD			flags;			//+14
	DWORD			unknown2[2];	//+18
	DWORD			nPresetType;    //+1c
	DWORD*			nPresetType2No; //+20
	DWORD			unknwon3[39];   //Unknown Data at +24
	DrlgRoom2**		paRoomsNear;    //+c0
	DWORD			nRoomsNear;     //+c4
	RoomTile*		pRoomTiles;     //+c8
	PresetUnit*		pPresetUnits;   //+cc
	DWORD			unknown4[1];    //Unknown Data at +d0
	D2Seed			seed;           //+d4
	DWORD			unknown5[2];    //Unknown Data at +d8
	DRLGRoom*		pRoom1;         //+e4
	DrlgRoom2*		pNext;          //+e8
};

/*=================================================================*/
/*   Level Info for Dungeon Room Logical Grid Structure.           */
/*=================================================================*/

struct DrlgLevel 
{                               //Offset from Code
	DrlgMisc*   pDrlgMisc;      //+00
	DWORD       nLevelNo;       //+04
	DWORD       unknown1[10];   //Unknown Data at +08
	DrlgRoom2*  pRoom2First;    //+30
};

/*=================================================================*/
/*   Path Structure.                                               */
/*=================================================================*/



struct D2Path					//sizeof 0x200 
{                               //Offset from code.
	WORD		unknown_1;		//+00
	WORD		xpos;			//+02
	WORD		unknown_2;		//+04
	WORD		ypos;			//+06
	BYTE		unknown_3[4];	//+08
	DWORD       targetx;        //+0C
	DWORD       targety;        //+10
	DWORD       unknown_4;		//+14
	WORD        unknown_18;		//+18
	WORD		unknown_5;		//+1A
	DRLGRoom*  pRoom1;         //+1C
	DWORD       unknown_6[0x0E];//+20 +
	Unit*       pTargetUnit;    //+58
    DWORD       targetType;     //+5C
    DWORD       targetUNID;     //+60
};


struct D2StaticPath							  //sizeof 0x20
{
    DRLGRoom*                pRoom;               //+00
    int						 nTargetX;            //+04
    int						 nTargetY;            //+08
    int						 nXpos;               //+0C
    int						 nYpos;               //+10
    DWORD                    dwUnk[2];            //+14
    BYTE                     nDirection;          //+1C
	bool					 bUpdatePath;		  //+1D
	WORD					 wUnk;				  //+1E
};


// New room structs


struct DRLGPresetUnit {
   DWORD flags;
   DWORD dw1;
   void* ptr;
   int y;
   int classNo;
   DWORD dw2;
   DRLGPresetUnit* pNext;
   int x;
   DWORD eType;
};


struct Adjacent {
   union {
      DRLGRoom *pRoom;
      DRLGRoomEx *pRoomEx;
   };
};

struct DRLGRoomEx{
   DWORD _000;               // +00
   void* ptr2;               // +04 - some ptr
   DWORD losType;            // +08
   DWORD _00C;               // +0C - a bool or int
   DWORD adjacentNo;         // +10
   DWORD _014;               // +14
   DRLGRoomEx* pOtherEx;      // +18
   void* pLevel;         // +1C
   DWORD x;               // +20
   DWORD y;               // +24
   DWORD w;               // +28
   DWORD h;               // +2C
   union {
      DRLGRoomEx** pAdjRoom;
      Adjacent* pAdjacent;
   };                     // +30
   DRLGPresetUnit* pPresets;   // +34
   DRLGRoomEx* pNext;         // +38
   void* ptr[32];            // +3C
   DWORD _BC;               // +BC
   DWORD RoomFlagsEx2;         // +C0
   DRLGRoomEx* pOtherEx2;      // +C4
   void* ptr3;               // +C8
   DWORD _CC;               // +CC - also some sort of flag
   DWORD nSeed;            // +D0
   DWORD nLowSeed;            // +D4
   DWORD nHighSeed;         // +D8
   DWORD* presetTypeNo;      // +DC
   DWORD RoomFlagsEx;         // +E0
   void* pTiles;         // +E4
   DRLGRoom* pRoom;         // +E8
};

/*=================================================================*/
/*   Misc Info for Dungeon Room Logical Grid Structure.            */
/*=================================================================*/

struct DrlgMisc 
{								//Offset from Code
	DrlgLevel*  pLevelFirst;    //+00
	DWORD       unknown1[2];    //Unknown data at +04
	DRLGAct*    pDrlgAct;       //+0c
	DWORD       unknown2[249];  //Unknown data at +10
	DWORD       nStaffTombLvl;  //+3f4
	DWORD       nBossTombLvl;   //+3f8
};

/*=================================================================*/
/*   Act Info for Dungeon Room Logical Grid Structure.            */
/*=================================================================*/

struct DRLGAct 
{	                            //Offset from Code
	DWORD       unknown1[2];    //Unknown data at +00
	DrlgMisc*   pDrlgMisc;      //+08
};


#endif
