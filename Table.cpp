#include "Common.hpp"
#include "error.hpp"
#include "d2functions.hpp"
#include "d2customtxt.hpp"
#include "d2wrapper.hpp"
#include "table.hpp"


UtilityTXT*			UtilityTXTRecord = NULL;
DWORD				UtilityTXTRecordNbr = 0;

TownsTXT*			TownsTXTRecord = NULL;
DWORD				TownsTXTRecordNbr = 0;

void STDCALL CustomTable()
{
	FogLogMsg("*** D2external: Loading Custom Tables");
	LoadUtility();
	LoadTowns();
	FogLogMsg("*** D2external: Done Loading Custom Tables");
}

void STDCALL InitBinCreation(DWORD GameType, DWORD uk1, DWORD uk2)
{
	// Create Blizzard Text Files
	D2InitCreateBinFiles(GameType, uk1, uk2);

	// Custom text files
	CustomTable();
}

void STDCALL UnloadCustomBin()
{
	FogLogMsg("*** D2external: Unloading Custom Tables");
	UnloadUtility();
	UnloadTowns();
	FogLogMsg("*** D2external: Done Unloading Custom Tables");
}

void STDCALL UnloadAllBinImages()
{
	// Unload all Blizzard Bin Images
	D2UnloadAllBinImages();

	// Unload custom text files
	UnloadCustomBin();
}


void STDCALL LoadUtility()
{
	CREATE_TABLE_DESCRIPTION(7);

	ADD_UBYTE_FIELD		("hcidx", 0x00, 0);
	ADD_DWORD_FIELD		("value", 0x01, 1);
	ADD_UBYTE_FIELD		("Intensity", 0x05, 2);
	ADD_UBYTE_FIELD		("red", 0x06, 3);
	ADD_UBYTE_FIELD		("green", 0x07, 4);
	ADD_UBYTE_FIELD		("blue", 0x08, 5);

	ADD_TERM_FIELD		(6);

	BUILD_BIN(UtilityTXT, "Utility", 0x09);

}

void STDCALL UnloadUtility()
{
	UNLOAD_BIN(UtilityTXT);
}



void STDCALL LoadTowns()
{
	CREATE_TABLE_DESCRIPTION(3);

	ADD_UBYTE_FIELD		("Act", 0x00, 0);
	ADD_UBYTE_FIELD		("LevelID", 0x01, 1);
	ADD_TERM_FIELD		(2);

	BUILD_BIN(TownsTXT, "Towns", 0x02);

}

void STDCALL UnloadTowns()
{
	UNLOAD_BIN(TownsTXT);
}