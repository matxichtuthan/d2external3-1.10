#include "common.hpp"
#include "error.hpp"
#include "d2functions.hpp"
#include "d2wrapper.hpp"
#include "D2Struct.hpp"
#include "AI.hpp"

// Table struct done by Necrolis

const D2MonAIStrc gpfMonsterAI[] =
{
	{AI_DEFAULT,	NULL,             (MONAI) &AI_NONE, NULL			 },			//None 0x6FC35710 
	{AI_DEFAULT,	NULL,             (MONAI) &AI_IDLE, NULL             },			//Idle 0x6FCE7C10
	{AI_BASIC,		NULL,             (MONAI)0x6FCD1750,NULL             },			//Skeleton
	{AI_BASIC,		NULL,             (MONAI)0x6FCD1880,NULL             },			//Zombie
	{AI_BASIC,		NULL,             (MONAI)0x6FCD1990,NULL             },			//Bighead
	{AI_BASIC,		NULL,             (MONAI)0x6FCD1BA0,NULL             },			//BloodHawk
	{AI_BASIC,		NULL,             (MONAI)0x6FCD1D50,NULL             },			//Fallen
	{AI_BASIC,		NULL,             (MONAI)0x6FCD2220,NULL             },			//Brute
	{AI_BASIC,		NULL,             (MONAI)0x6FCD2370,NULL             },			//SandRaider
	{AI_BASIC,		NULL,             (MONAI)0x6FCD27A0,NULL             },			//Wraith
	{AI_BASIC,		NULL,             (MONAI)0x6FCD2850,NULL             },			//CorruptRogue
	{AI_BASIC,		NULL,             (MONAI)0x6FCD2A00,NULL             },			//Baboon
	{AI_BASIC,		NULL,             (MONAI)0x6FCD4390,NULL             },			//Goatman
	{AI_BASIC,		NULL,             (MONAI)0x6FCD2FF0,NULL             },			//FallenShaman
	{AI_BASIC,		NULL,             (MONAI)0x6FCD2E80,NULL             },			//QuillRat
	{AI_MULTILAYER,	NULL,             (MONAI)0x6FCD3540,(MONAI)0x6FCD34A0},			//SandMaggot
	{AI_BASIC,		NULL,             (MONAI)0x6FCD3900,NULL             },			//ClawViper
	{AI_BASIC,		NULL,             (MONAI)0x6FCD3E70,NULL             },			//SandLeaper
	{AI_BASIC,		NULL,             (MONAI)0x6FCD4050,NULL             },			//PantherWoman
	{AI_BASIC,		NULL,             (MONAI)0x6FCD4390,NULL             },			//Swarm
	{AI_BASIC,		NULL,             (MONAI)0x6FCD4440,NULL             },			//Scarab
	{AI_BASIC,		NULL,             (MONAI)0x6FCD4720,NULL             },			//Mummy
	{AI_BASIC,		NULL,             (MONAI)0x6FCD48B0,NULL             },			//GreaterMummy
	{AI_BASIC,		NULL,             (MONAI)0x6FCD4DD0,NULL             },			//Vulture
	{AI_BASIC,		NULL,             (MONAI)0x6FCD5850,NULL             },			//Mosquito
	{AI_BASIC,		NULL,             (MONAI)0x6FCD5A60,NULL             },			//WillOWisp
	{AI_BASIC,		NULL,             (MONAI)0x6FCD69E0,NULL             },			//Arach
	{AI_BASIC,		NULL,             (MONAI)0x6FCD6D60,NULL             },			//ThornHulk
	{AI_BASIC,		NULL,             (MONAI)0x6FCD6FD0,NULL             },			//Vampire
	{AI_BASIC,		NULL,             (MONAI)0x6FCD7760,(MONAI)0x6FCD76F0},			//BatDemon
	{AI_BASIC,		NULL,             (MONAI)0x6FCD7BA0,NULL             },			//Fetish
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE73A0,NULL             },			//NpcOutOfTown
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE6660,NULL             },			//Npc
	{AI_BASIC,		NULL,             (MONAI)0x6FCD7EB0,NULL             },			//HellMeteor
	{AI_BASIC,		NULL,             (MONAI)0x6FCD8090,NULL             },			//Andariel
	{AI_BASIC,		NULL,             (MONAI)0x6FCD8260,NULL             },			//CorruptArcher
	{AI_BASIC,		NULL,             (MONAI)0x6FCD85B0,NULL             },			//CorruptLancer
	{AI_BASIC,		NULL,             (MONAI)0x6FCD88C0,NULL             },			//SkeletonBow
	{AI_BASIC,		NULL,             (MONAI)0x6FCD8A60,NULL             },			//MaggotLarva
	{AI_BASIC,		NULL,             (MONAI)0x6FCD8B60,NULL             },			//PinHead
	{AI_BASIC,		NULL,             (MONAI)0x6FCD8D20,NULL             },			//MaggotEgg
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE6F80,NULL             },			//Towner
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE9890,NULL             },			//Vendor
	{AI_BASIC,		(MONAI)0x6FCD8E10,(MONAI)0x6FCD8E30,NULL             },			//FoulCrowNest
	{AI_BASIC,		NULL,             (MONAI)0x6FCD8FE0,NULL             },			//Duriel
	{AI_BASIC,		(MONAI)0x6FCD8E10,(MONAI)0x6FCD91F0,NULL             },			//Sarcophagus
	{AI_BASIC,		NULL,             (MONAI)0x6FCD93A0,NULL             },			//ElementalBeast
	{AI_BASIC,		NULL,             (MONAI)0x6FCD94D0,NULL             },			//FlyingScimitar
	{AI_BASIC,		NULL,             (MONAI)0x6FCD9640,NULL             },			//ZakarumZealot
	{AI_BASIC,		NULL,             (MONAI)0x6FCD9A10,NULL             },			//ZakarumPriest
	{AI_BASIC,		NULL,             (MONAI)0x6FCDA300,NULL             },			//Mephisto
	{AI_BASIC,		NULL,             (MONAI)0x6FCE8950,(MONAI)0x6FCE82F0},			//Diablo
	{AI_HIDDEN,		NULL,             (MONAI)0x6FCDAAA0,(MONAI)0x6FCDA910},			//FrogDemon
	{AI_BASIC,		NULL,             (MONAI)0x6FCDAFC0,NULL             },			//Summoner
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE6DC0,NULL             },			//NpcStationary
	{AI_BASIC,		NULL,             (MONAI)0x6FCDB3E0,NULL             },			//Izual
	{AI_ADVANCED,	NULL,             (MONAI)0x6FCDB720,NULL             },			//Tentacle
	{AI_ADVANCED,	NULL,             (MONAI)0x6FCDBAA0,NULL             },			//TentacleHead
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE7AC0,NULL             },			//Navi
	{AI_BASIC,		(MONAI)0x6FCE58D0,(MONAI)0x6FCE58E0,NULL             },			//BloodRaven
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE77A0,NULL             },			//GoodNpcRanged
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE39E0,(MONAI)0x6FCDC170},			//Hireable
	{AI_BASIC,		NULL,             (MONAI)0x6FCE7A60,NULL             },			//TownRogue
	{AI_BASIC,		NULL,             (MONAI)0x6FCDBCE0,NULL             },			//GargoyleTrap
	{AI_BASIC,		NULL,             (MONAI)0x6FCDBF20,NULL             },			//SkeletonMage
	{AI_BASIC,		NULL,             (MONAI)0x6FCDC1C0,(MONAI)0x6FCDC170},			//FetishShaman
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCDC600,NULL             },			//SandMaggotQueen
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE2960,NULL             },			//NecroPet
	{AI_BASIC,		NULL,             (MONAI)0x6FCDC840,NULL             },			//VileMother
	{AI_BASIC,		NULL,             (MONAI)0x6FCDCBF0,NULL             },			//VileDog
	{AI_BASIC,		NULL,             (MONAI)0x6FCDCCD0,NULL             },			//FingerMage
	{AI_BASIC,		NULL,             (MONAI)0x6FCDD060,NULL             },			//Regurgitator
	{AI_BASIC,		NULL,             (MONAI)0x6FCDD790,NULL             },			//DoomKnight
	{AI_BASIC,		NULL,             (MONAI)0x6FCDD850,NULL             },			//AbyssKnight
	{AI_BASIC,		NULL,             (MONAI)0x6FCDDB10,NULL             },			//OblivionKnight
	{AI_BASIC,		NULL,             (MONAI)0x6FCDE150,NULL             },			//QuillMother
	{AI_BASIC,		NULL,             (MONAI)0x6FCDE2B0,NULL             },			//EvilHole
	{AI_ADVANCED,	NULL,             (MONAI)0x6FCDE4D0,NULL             },			//Trap-Missile
	{AI_BASIC,		NULL,             (MONAI)0x6FCDE570,NULL             },			//Trap-RightArrow
	{AI_BASIC,		NULL,             (MONAI)0x6FCDE710,NULL             },			//Trap-LeftArrow
	{AI_ADVANCED,	NULL,             (MONAI)0x6FCDE8B0,NULL             },			//Trap-Poison
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE7070,NULL             },			//JarJar
	{AI_BASIC,		NULL,             (MONAI)0x6FCDE9E0,NULL             },			//InvisoSpawner
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCDEAF0,NULL             },			//MosquitoNest
	{AI_DEFAULT,	(MONAI)0x6FCDEC70,(MONAI)0x6FCDECE0,NULL             },			//BoneWall
	{AI_BASIC,		NULL,             (MONAI)0x6FCDED10,NULL             },			//HighPriest
	{AI_ADVANCED,	NULL,             (MONAI)0x6FCE98E0,NULL             },			//Hydra
	{AI_BASIC,		NULL,             (MONAI)0x6FCDE960,NULL             },			//Trap-Melee
	{AI_BASIC,		NULL,             (MONAI)0x6FCE9AF0,NULL             },			//7TIllusion
	{AI_BASIC,		NULL,             (MONAI)0x6FCDF780,NULL             },			//Megademon
	{AI_BASIC,		NULL,             (MONAI)0x6FCE4FF0,NULL             },			//Griswold
	{AI_BASIC,		NULL,             (MONAI)0x6FCE9BA0,NULL             },			//DarkWanderer
	{AI_BASIC,		NULL,             (MONAI)0x6FCDE8B0,NULL             },			//Trap-Nova
	{AI_BASIC,		NULL,             (MONAI)0x6FCDFA50,NULL             },			//ArcaneTower
	{AI_BASIC,		NULL,             (MONAI)0x6FCDF410,NULL             },			//DesertTurret
	{AI_BASIC,		NULL,             (MONAI)0x6FCDFB80,NULL             },			//PantherJavelin
	{AI_BASIC,		NULL,             (MONAI)0x6FCDFD50,NULL             },			//FetishBlowgun
	{AI_BASIC,		NULL,             (MONAI)0x6FCE28A0,NULL             },			//Spirit
	{AI_BASIC,		NULL,             (MONAI)0x6FCE28F0,NULL             },			//Smith
	{AI_BASIC,		NULL,             (MONAI)0x6FCE9980,NULL             },			//TrappedSoul
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE7C10,NULL             },			//Buffy
	{AI_DEFAULT,	(MONAI)0x6FCE9CE0,(MONAI)0x6FCE9D00,NULL             },			//AssassinSentry
	{AI_DEFAULT,	(MONAI)0x6FCE9FB0,(MONAI)0x6FCE9FD0,NULL             },			//BladeCreeper
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCEA2A0,NULL             },			//InvisoPet
	{AI_DEFAULT,	(MONAI)0x6FCE9CE0,(MONAI)0x6FCEA490,NULL             },			//DeathSentry
	{AI_ADVANCED,	(MONAI)0x6FCEA680,(MONAI)0x6FCEA6D0,NULL             },			//ShadowWarrior
	{AI_ADVANCED,	(MONAI)0x6FCEAFE0,(MONAI)0x6FCEB240,NULL             },			//ShadowMaster
	{AI_ADVANCED,	(MONAI)0x6FCED140,(MONAI)0x6FCED190,NULL             },			//Raven
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCED540,NULL             },			//DruidWolf
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCEE250,NULL             },			//Totem
	{AI_ADVANCED,	(MONAI)0x6FCECC40,(MONAI)0x6FCECC50,NULL             },			//Vines
	{AI_ADVANCED,	(MONAI)0x6FCECC40,(MONAI)0x6FCECE50,NULL             },			//CycleOfLife
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCEDF70,NULL             },			//DruidBear
	{AI_BASIC,		NULL,             (MONAI)0x6FCE0220,NULL             },			//SiegeTower
	{AI_BASIC,		NULL,             (MONAI)0x6FCE0050,NULL             },			//ReanimatedHorde
	{AI_BASIC,		NULL,             (MONAI)0x6FCE0610,NULL             },			//SiegeBeast
	{AI_BASIC,		NULL,             (MONAI)0x6FCE0A50,NULL             },			//Minion
	{AI_BASIC,		NULL,             (MONAI)0x6FCE0C10,NULL             },			//SuicideMinion
	{AI_BASIC,		NULL,             (MONAI)0x6FCE0CD0,NULL             },			//Succubus
	{AI_BASIC,		NULL,             (MONAI)0x6FCE0FE0,NULL             },			//SuccubusWitch
	{AI_BASIC,		NULL,             (MONAI)0x6FCE1550,NULL             },			//Overseer
	{AI_BASIC,		(MONAI)0x6FCD8E10,(MONAI)0x6FCE1B90,NULL             },			//MinionSpawner
	{AI_BASIC,		(MONAI)0x6FCE2080,(MONAI)0x6FCE2090,NULL             },			//Imp
	{AI_BASIC,		NULL,             (MONAI)0x6FCE2570,NULL             },			//Catapult
	{AI_BASIC,		NULL,             (MONAI)0x6FCE25D0,NULL             },			//FrozenHorror
	{AI_BASIC,		NULL,             (MONAI)0x6FCE2760,NULL             },			//BloodLord
	{AI_BASIC,		NULL,             (MONAI)0x6FCEE6F0,NULL             },			//CatapultSpotter
	{AI_ADVANCED,	(MONAI)0x6FC35710,(MONAI)0x6FCEE450,NULL             },			//NpcBarb
	{AI_ADVANCED,	(MONAI)0x6FC35710,(MONAI)0x6FCEEE60,(MONAI)0x6FCDC170},			//Nihlathak
	{AI_BASIC,		(MONAI)0x6FCE5610,(MONAI)0x6FCE5640,NULL             },			//GenericSpawner
	{AI_BASIC,		NULL,             (MONAI)0x6FCEEAD0,NULL             },			//DeathMauler
	{AI_ADVANCED,	NULL,             (MONAI)0x6FCEEC00,NULL             },			//Wussie
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCEF330,NULL             },			//AncientStatue
	{AI_ADVANCED,	NULL,             (MONAI)0x6FCEFBC0,NULL             },			//Ancient
	{AI_ADVANCED,	(MONAI)0x6FCA4640,(MONAI)0x6FCEFBF0,NULL             },			//BaalThrone
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCCD520,(MONAI)0x6FCCD470},			//BaalCrab
	{AI_BASIC,		NULL,             (MONAI)0x6FCF0180,NULL             },			//BaalTaunt
	{AI_BASIC,		NULL,             (MONAI)0x6FCF05B0,NULL             },			//PutridDefiler
	{AI_BASIC,		NULL,             (MONAI)0x6FCF0090,NULL             },			//BaalToStairs
	{AI_BASIC,		NULL,             (MONAI)0x6FCF02D0,NULL             },			//BaalTentacle
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCCEB70,(MONAI)0x6FCCD470},			//BaalCrabClone
	{AI_BASIC,		NULL,             (MONAI)0x6FCF0420,NULL             },			//BaalMinion
	{AI_BASIC,		NULL,             (MONAI)0x6FCD3B90,NULL             },			//ClawViperEx
	{AI_ADVANCED,	(MONAI)0x6FCEB1B0,(MONAI)0x6FCEB240,NULL             },			//ShadowMasterNoInit
	{AI_DEFAULT,	NULL,             NULL,				NULL             },			//UberIzual
	{AI_DEFAULT,	NULL,             (MONAI)0x6FCE7C10,NULL             },			//UberBaal
	{AI_BASIC,		NULL,             (MONAI)0x6FCD1660,NULL             },			//UberMephisto
	{AI_BASIC,		(MONAI)0x6FCE4B90,(MONAI)0x6FCE4CC0,NULL             },			//UberDiablo
};

const DWORD gdwfMonsterAI = ARRAY(gpfMonsterAI);


/*
	Coded by: Thomas Westman
    Date: 2017-07-14
	Function: AI_NONE

	Hardest function I ever coded!
*/

void FASTCALL AI_NONE(D2Game* ptGame, Unit* ptUnit, AIParam* ptAIParam)
{
	return ;
}

/*
	Coded by: Thomas Westman
    Date: 2017-07-14
	Function: AI_IDLE
*/

void FASTCALL AI_IDLE(D2Game* ptGame, Unit* ptUnit, AIParam* ptAIParam)
{
	if (ptGame == NULL){ // Check if ptGame == NULL
		  log_message("AI_IDLE(): ptGame == %d", ptGame);
		return ;
	}

	if (ptUnit == NULL){ // Check if ptUnit == NULL
		  log_message("AI_IDLE(): ptUnit == %d", ptUnit);
		return ;
	}

	if (ptAIParam == NULL){ // Check if ptPlayer == NULL
		  log_message("AI_IDLE(): ptAIParam == %d", ptAIParam);
		return ;
	}

	DRLGRoom* ptRoom = D2GetRoomFromUnit(ptUnit);

	if (ptRoom == NULL){ // Check if ptRoom == NULL
		  log_message("AI_IDLE(): ptRoom == %d", ptRoom);
		return ;
	}

	D2_AI_Stall_Time(ptGame, ptUnit, 200);
}

