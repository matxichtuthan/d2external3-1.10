/*=================================================================*/
/** @file D2CustomTXTStruct.hpp
 *  @brief Diablo II Extra table
 *
 *  (c) January 2002 - Joel Falcou for The Phrozen Keep.
 */
/*=================================================================*/

#ifndef __D2CUSTOMTXTSTRUCT_HPP__INCLUDED__
#define __D2CUSTOMTXTSTRUCT_HPP__INCLUDED__

/* TXT reading structure */

struct BINField
{
   char*	fieldName;  
   DWORD	type;  
   DWORD	strLength;  
   DWORD	offset;  
   DWORD	lookup; 
};

/**********************
     Utility.txt
 **********************/

#pragma pack(push, 1)
struct UtilityTXT
{
	BYTE	m_hcIdx;
	DWORD	m_Value;
	BYTE	m_Intensity;
	BYTE	m_red;
	BYTE	m_green;
	BYTE	m_blue;
};
#pragma pack(pop)

extern UtilityTXT*		UtilityTXTRecord;
extern DWORD			UtilityTXTRecordNbr;

UtilityTXT* STDCALL GetUtilityRecord(BYTE hcIdx);


/**********************
     Towns.txt
 **********************/

#pragma pack(push, 1)
struct TownsTXT
{
	BYTE	m_Act;
	BYTE	m_LevelID;
};
#pragma pack(pop)

extern TownsTXT*		TownsTXTRecord;
extern DWORD			TownsTXTRecordNbr;

TownsTXT* STDCALL GetTownsRecord(BYTE Act);

#endif
