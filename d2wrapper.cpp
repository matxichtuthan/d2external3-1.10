/*=================================================================*/
/** @file D2DLL.cpp
 *  @brief Main Diablo II extra DLL handler.
 *
 *  This is the sources of the D2DLL class.
 */
/*=================================================================*/

// Some needed standard header.
#include <stdio.h>
#include <memory.h>
#include <direct.h>

// Core Class Headers
#include "error.hpp"
#include "d2wrapper.hpp"
#include "Common.hpp"

/* Beginning of Custom Header */

#include "Table.hpp"
#include "Utility.hpp"
#include "Town.hpp"
#include "AITypes.hpp"
#include "Unit.hpp"
#include "AI.hpp"

/*=================================================================*/
/** @brief D2DLL Wrapper constructor.
 *
 *  Nothing spicy here. You could use this constructor to pre-calculate
 *  some constants or displaying a nice header in your DebugLog file.
 *
 */
/*=================================================================*/

void STDCALL init( const char* pLogfilePath )
{
	char lLogFile[1024] = "";
    DWORD dw = 0;

	_getcwd( application_path, MAX_PATH );

	if( application_path[strlen(application_path)] != '\\')
    {
		application_path[strlen(application_path) + 1] = 0;
		application_path[strlen(application_path)] = '\\';
	}

	sprintf_s(lLogFile, "%s%s", application_path, pLogfilePath);

    set_logfile( lLogFile );
    log_info("*----------------------------------------------------------------*");
    log_info("|                                                                |");
    log_info("|    DIABLO 2 - Dreamland Loader Ver 0.1, For 1.10               |");
	log_info("|    by Allan Jensen 2002-2003                                   |");
    log_info("|                                                                |");
    log_info("|                                                                |");
	log_info("|    DLL structure addapted from Joel FALCOU                     |");
    log_info("|    DLL Wrapper adapted from Sir-General HeavenStorm Mod.       |");
    log_info("|                                                                |");
    log_info("*----------------------------------------------------------------*");
    log_info("");
	log_info( "Starting Diablo II.\n\n");

	log_info("*----------------------------------------------------------------*");
	log_info("| Dll addreses                                                   |");
	log_info("*----------------------------------------------------------------*");
	log_message("| D2Game.dll Load Address:    0x%X                         |", GameOffset);
	log_message("| D2Common.dll Load Address:  0x%X                          |", CommonOffset);
	log_message("| D2Lang.dll Load Address:    0x%X                         |", LangOffset);
	log_message("| Fog.dll Load Address:       0x%X                         |", FogOffset);
	log_message("| D2Gfx.dll Load Address:     0x%X                         |", GfxOffset);
	log_message("| D2Client.dll Load Address:  0x%X                         |", ClientOffset);
	log_message("| D2Net.dll Load Address:     0x%X                         |", NetOffset);
	log_message("| D2Launch.dll Load Address:  0x%X                         |", LaunchOffset);
	log_message("| D2Win.dll Load Address:     0x%X                         |", WinOffset);
	log_info("*----------------------------------------------------------------*");

	log_info("\n\n");
	
	LOAD_LIBRARY( "D2Launch.dll", "d2wrapper.cpp" );
	LOAD_LIBRARY( "D2Client.dll", "d2wrapper.cpp" );
	LOAD_LIBRARY( "D2Net.dll", "d2wrapper.cpp");
	LOAD_LIBRARY( "D2Win.dll", "d2wrapper.cpp" );

	HOOK_LIBRARY( "D2Game.dll",		(GameOffset+0x1000),	0xF7000, "d2wrapper.cpp" );
	HOOK_LIBRARY( "D2Launch.dll",	(LaunchOffset+0x1000), 0x21000, "d2wrapper.cpp" );
    HOOK_LIBRARY( "D2Common.dll",	(CommonOffset+0x1000), 0x8E000, "d2wrapper.cpp" );
	HOOK_LIBRARY( "D2Client.dll",	(ClientOffset+0x1000), 0xCD000, "d2wrapper.cpp" );
	HOOK_LIBRARY( "D2Lang.dll",		(LangOffset+0x1000), 0x0C000, "d2wrapper.cpp" );
	HOOK_LIBRARY( "D2Net.dll",	(NetOffset+0x1000), 0x07000, "d2wrapper.cpp" );
	HOOK_LIBRARY( "Fog.dll",		(FogOffset+0x1000), 0x21000, "d2wrapper.cpp" );
	HOOK_LIBRARY( "D2Win.dll",	(WinOffset+0x1000), 0x1A000, "d2wrapper.cpp" );

    log_info( "\nPatching DLL ..." );
    init_patch();
    log_info( "\nDLL patched sucessfully. Entering Diablo II." );
}

void STDCALL release()
{
}

void STDCALL set_module( HANDLE pModule )
{
    if( pModule != INVALID_HANDLE_VALUE )
    {
        module_handle = pModule;
    }
    else
    {
        log_error( __FILE__, "Invalid Module Handle used for initialization" );
    }
}

void STDCALL init_patch()
{

//	D2Client.dll patches
	CALL(InitBinCreation, (ClientOffset + 0x21C7));					// 6FAA21C7 #D2Common.10576
	CALL(UnloadAllBinImages, (ClientOffset + 0x21FF));				// 6FAA21FF #D2Common.10575
	CALL(SetPlayerLightRadius, (ClientOffset + 0xA89A2));			// D2SetUnitLightRadius (player default radius)
	CALL(GetTownIDFromAct, (ClientOffset + 0xA6B26));				// 6FB46B26 #D2Common.10085
	CALL(CheckIfTownID, (ClientOffset + 0x1B398));					// 6FABB398 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x1B6A7));					// 6FABB6A7 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x1C27C));					// 6FABC27C #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x1F4C6));					// 6FABF4C6 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x1F4D6));					// 6FABF4D6 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x1F526));					// 6FABF526 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x1F536));					// 6FABF536 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x215BF));					// 6FAC15BF #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x2210F));					// 6FAC210F #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x22CD4));					// 6FAC2CD4 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x27EE3));					// 6FAC7EE3 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x28C57));					// 6FAC8C57 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x2936E));					// 6FAC936E #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x2A52E));					// 6FACA52E #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x2A542));					// 6FACA542 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x2A7C8));					// 6FACA7C8 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x2ADC7));					// 6FACADC7 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x2AF27));					// 6FACAF27 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x5E78A));					// 6FAFE78A #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x627EA));					// 6FB027EA #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x63164));					// 6FB03164 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x639A6));					// 6FB039A6 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x63CC1));					// 6FB03CC1 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x80E1B));					// 6FB20E1B #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x89DD1));					// 6FB29DD1 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x8A5F8));					// 6FB2A5F8 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x8A633));					// 6FB2A633 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x90150));					// 6FB30150 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x90B2B));					// 6FB30B2B #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x968C3));					// 6FB368C3 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x9D831));					// 6FB3D831 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0x9D9D6));					// 6FB3D9D6 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0xA8919));					// 6FB48919 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0xA8EAF));					// 6FB48EAF #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0xA9132));					// 6FB49132 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0xA9E3D));					// 6FB49E3D #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0xAA328));					// 6FB4A328 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0xAA33C));					// 6FB4A33C #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0xAA412));					// 6FB4A412 #D2Common.10082
	CALL(CheckIfTownID, (ClientOffset + 0xB07F5));					// 6FB507F5 #D2Common.10082


//	D2Game.dll patches
	CALL(GetTownIDFromAct, (GameOffset + 0x20B8));					// 6FC320B8 #D2Common.10085
	CALL(GetTownIDFromAct, (GameOffset + 0xC131));					// 6FC3C131 #D2Common.10085
	CALL(GetTownIDFromAct, (GameOffset + 0xC543));					// 6FC3C543 #D2Common.10085
	CALL(GetTownIDFromAct, (GameOffset + 0x413EC));					// 6FC713EC #D2Common.10085
	CALL(GetTownIDFromAct, (GameOffset + 0x468B6));					// 6FC768B6 #D2Common.10085
	CALL(GetTownIDFromAct, (GameOffset + 0x567FB));					// 6FC867FB #D2Common.10085
	CALL(GetTownIDFromAct, (GameOffset + 0xD2CCA));					// 6FD02CCA #D2Common.10085
	CALL(CheckIfTownID, (GameOffset + 0x1FCBE));					// 6FC4FCBE #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x26CDF));					// 6FC56CDF #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x2758E));					// 6FC5758E #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x2AD54));					// 6FC5AD54 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x3016C));					// 6FC6016C #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x30184));					// 6FC60184 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x334E4));					// 6FC634E4 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x38889));					// 6FC68889 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x38A0E));					// 6FC68A0E #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x38ADD));					// 6FC68ADD #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x38C53));					// 6FC68C53 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x44172));					// 6FC74172 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x445F6));					// 6FC745F6 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x490CF));					// 6FC790CF #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x4A7D3));					// 6FC7A7D3 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x4B1E7));					// 6FC7B1E7 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x4F562));					// 6FC7F562 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x4F663));					// 6FC7F663 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x4F6B2));					// 6FC7F6B2 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x4F7C2));					// 6FC7F7C2 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x4FA75));					// 6FC7FA75 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x4FADF));					// 6FC7FADF #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x513DD));					// 6FC813DD #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x54FCD));					// 6FC84FCD #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x5F3EA));					// 6FC8F3EA #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x5F401));					// 6FC8F401 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x612EB));					// 6FC912EB #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x61307));					// 6FC91307 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x8FEE7));					// 6FCBFEE7 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0x91284));					// 6FCC1284 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xB2AC2));					// 6FCE2AC2 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xB2AE8));					// 6FCE2AE8 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xB356C));					// 6FCE356C #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xB3922));					// 6FCE3922 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xB3948));					// 6FCE3948 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xB3D09));					// 6FCE3D09 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xB77DA));					// 6FCE77DA #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xB795D));					// 6FCE795D #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xB81D3));					// 6FCE81D3 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xB9E9C));					// 6FCE9E9C #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xBCCD3));					// 6FCECCD3 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xBD680));					// 6FCED680 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xBDB10));					// 6FCEDB10 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xC1021));					// 6FCF1021 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xC1A96));					// 6FCF1A96 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xC1FEE));					// 6FCF1FEE #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xC2327));					// 6FCF2327 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xC2C6B));					// 6FCF2C6B #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xC50A8));					// 6FCF50A8 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xC50B8));					// 6FCF50B8 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xC5143));					// 6FCF5143 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xC5153));					// 6FCF5153 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xC880B));					// 6FCF880B #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xCABBE));					// 6FCFABBE #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xCAC59));					// 6FCFAC59 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xCCDE8));					// 6FCFCDE8 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xCD71D));					// 6FCFD71D #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xCFB41));					// 6FCFFB41 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xCFC16));					// 6FCFFC16 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xCFD42));					// 6FCFFD42 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xCFE3C));					// 6FCFFE3C #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xD2CD4));					// 6FD02CD4 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xD9932));					// 6FD09932 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xDAF49));					// 6FD0AF49 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xDAF9B));					// 6FD0AF9B #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xDB0C9));					// 6FD0B0C9 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xDB105));					// 6FD0B105 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xDBBF1));					// 6FD0BBF1 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xDBDDD));					// 6FD0BDDD #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xDE126));					// 6FD0E126 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xDE307));					// 6FD0E307 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xDE5D4));					// 6FD0E5D4 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xDFB78));					// 6FD0FB78 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xDFFAD));					// 6FD0FFAD #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE0505));					// 6FD10505 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE29D7));					// 6FD129D7 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE2C45));					// 6FD12C45 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE31AC));					// 6FD131AC #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE3E36));					// 6FD13E36 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE5CB0));					// 6FD15CB0 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE5CC9));					// 6FD15CC9 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE5E32));					// 6FD15E32 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE676B));					// 6FD1676B #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE677F));					// 6FD1677F #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE6CBB));					// 6FD16CBB #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE6EFE));					// 6FD16EFE #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE768F));					// 6FD1768F #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE76A3));					// 6FD176A3 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE7D99));					// 6FD17D99 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xE9191));					// 6FD19191 #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xEAD3C));					// 6FD1AD3C #D2Common.10082
	CALL(CheckIfTownID, (GameOffset + 0xEB3CC));					// 6FD1B3CC #D2Common.10082
	CALL(Unit_GetTargetUnit, (GameOffset + 0x275B7));			// Adress: 6FC575B7   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x27BB8));			// Adress: 6FC57BB8   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x27BD6));			// Adress: 6FC57BD6   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x2ADB9));			// Adress: 6FC5ADB9   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x32D9C));			// Adress: 6FC62D9C   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x33921));			// Adress: 6FC63921   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x339E6));			// Adress: 6FC639E6   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x33AD6));			// Adress: 6FC63AD6   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x349CD));			// Adress: 6FC649CD   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x34AEB));			// Adress: 6FC64AEB   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x35CEC));			// Adress: 6FC65CEC   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x3D6D6));			// Adress: 6FC6D6D6   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x3DAF3));			// Adress: 6FC6DAF3   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x51D70));			// Adress: 6FC81D70   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x51E2E));			// Adress: 6FC81E2E   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x58651));			// Adress: 6FC88651   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x58867));			// Adress: 6FC88867   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0x9E8B4));			// Adress: 6FCCE8B4   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xA34D8));			// Adress: 6FCD34D8   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xA8DDB));			// Adress: 6FCD8DDB   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xAB7AF));			// Adress: 6FCDB7AF   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xB7E25));			// Adress: 6FCE7E25   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xB81BD));			// Adress: 6FCE81BD   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xBB361));			// Adress: 6FCEB361   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xBB632));			// Adress: 6FCEB632   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC3158));			// Adress: 6FCF3158   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC317E));			// Adress: 6FCF317E   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC32CD));			// Adress: 6FCF32CD   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC35AE));			// Adress: 6FCF35AE   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC36FE));			// Adress: 6FCF36FE   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC381E));			// Adress: 6FCF381E   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC392D));			// Adress: 6FCF392D   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC3C4E));			// Adress: 6FCF3C4E   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC40DE));			// Adress: 6FCF40DE   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC430D));			// Adress: 6FCF430D   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC4A6E));			// Adress: 6FCF4A6E   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC5096));			// Adress: 6FCF5096   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC5131));			// Adress: 6FCF5131   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC5175));			// Adress: 6FCF5175   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC52C5));			// Adress: 6FCF52C5   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC5301));			// Adress: 6FCF5301   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC5815));			// Adress: 6FCF5815   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC6056));			// Adress: 6FCF6056   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC642F));			// Adress: 6FCF642F   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC7803));			// Adress: 6FCF7803   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC7B35));			// Adress: 6FCF7B35   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC815E));			// Adress: 6FCF815E   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC8BA9));			// Adress: 6FCF8BA9   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC9BAF));			// Adress: 6FCF9BAF   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xC9CC5));			// Adress: 6FCF9CC5   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xCA546));			// Adress: 6FCFA546   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xCAC3B));			// Adress: 6FCFAC3B   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xCAE54));			// Adress: 6FCFAE54   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xCAEDD));			// Adress: 6FCFAEDD   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xCB279));			// Adress: 6FCFB279   CALL D2Game.6FCBC900
	CALL(Unit_GetTargetUnit, (GameOffset + 0xCB403));			// Adress: 6FCFB403   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCB612));			// Adress: 6FCFB612   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCB673));			// Adress: 6FCFB673   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCB8F0));			// Adress: 6FCFB8F0   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCBE8A));			// Adress: 6FCFBE8A   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCC07B));			// Adress: 6FCFC07B   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCC92B));			// Adress: 6FCFC92B   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCCBB9));			// Adress: 6FCFCBB9   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCCF17));			// Adress: 6FCFCF17   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCD8AC));			// Adress: 6FCFD8AC   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCDC5C));			// Adress: 6FCFDC5C   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCE96E));			// Adress: 6FCFE96E   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCEB06));			// Adress: 6FCFEB06   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCED0A));			// Adress: 6FCFED0A   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCF025));			// Adress: 6FCFF025   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCF409));			// Adress: 6FCFF409   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xCF529));			// Adress: 6FCFF529   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD037F));			// Adress: 6FD0037F   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD07AC));			// Adress: 6FD007AC   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD0A38));			// Adress: 6FD00A38   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD0C2C));			// Adress: 6FD00C2C   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD0E07));			// Adress: 6FD00E07   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD1513));			// Adress: 6FD01513   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD1A05));			// Adress: 6FD01A05   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD20DC));			// Adress: 6FD020DC   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD252B));			// Adress: 6FD0252B   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD2719));			// Adress: 6FD02719   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD3DA7));			// Adress: 6FD03DA7   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD5DD7));			// Adress: 6FD05DD7   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD5EEB));			// Adress: 6FD05EEB   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD5F29));			// Adress: 6FD05F29   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD620C));			// Adress: 6FD0620C   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD62E9));			// Adress: 6FD062E9   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD68F4));			// Adress: 6FD068F4   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD6A66));			// Adress: 6FD06A66   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD6E03));			// Adress: 6FD06E03   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD74B7));			// Adress: 6FD074B7   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD792B));			// Adress: 6FD0792B   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD79AF));			// Adress: 6FD079AF   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD7AAB));			// Adress: 6FD07AAB   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD86BB));			// Adress: 6FD086BB   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD8A32));			// Adress: 6FD08A32   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD8F0F));			// Adress: 6FD08F0F   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD9131));			// Adress: 6FD09131   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD94CB));			// Adress: 6FD094CB   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD98F1));			// Adress: 6FD098F1   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD9C4A));			// Adress: 6FD09C4A   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xD9CED));			// Adress: 6FD09CED   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDA1DA));			// Adress: 6FD0A1DA   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDA522));			// Adress: 6FD0A522   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDA555));			// Adress: 6FD0A555   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDA625));			// Adress: 6FD0A625   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDA765));			// Adress: 6FD0A765   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDA8A2));			// Adress: 6FD0A8A2   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDAF33));			// Adress: 6FD0AF33   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDAF7E));			// Adress: 6FD0AF7E   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDB0B3));			// Adress: 6FD0B0B3   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDB0F2));			// Adress: 6FD0B0F2   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDB123));			// Adress: 6FD0B123   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDB194));			// Adress: 6FD0B194   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDBBAA));			// Adress: 6FD0BBAA   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDCC24));			// Adress: 6FD0CC24   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDCFDA));			// Adress: 6FD0CFDA   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDD0E5));			// Adress: 6FD0D0E5   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDD7BD));			// Adress: 6FD0D7BD   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDD97B));			// Adress: 6FD0D97B   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDDB45));			// Adress: 6FD0DB45   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xDE7EE));			// Adress: 6FD0E7EE   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE0385));			// Adress: 6FD10385   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE14E8));			// Adress: 6FD114E8   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE1618));			// Adress: 6FD11618   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE180B));			// Adress: 6FD1180B   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE20AC));			// Adress: 6FD120AC   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE2156));			// Adress: 6FD12156   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE22D3));			// Adress: 6FD122D3   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE2379));			// Adress: 6FD12379   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE2433));			// Adress: 6FD12433   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE2499));			// Adress: 6FD12499   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE263A));			// Adress: 6FD1263A   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE2A1A));			// Adress: 6FD12A1A   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE2DBB));			// Adress: 6FD12DBB   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE3005));			// Adress: 6FD13005   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE3168));			// Adress: 6FD13168   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE4026));			// Adress: 6FD14026   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE47B1));			// Adress: 6FD147B1   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE53AF));			// Adress: 6FD153AF   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE5B4C));			// Adress: 6FD15B4C   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE671A));			// Adress: 6FD1671A   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE6DF2));			// Adress: 6FD16DF2   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE733B));			// Adress: 6FD1733B   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE8390));			// Adress: 6FD18390   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE84FB));			// Adress: 6FD184FB   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE86A0));			// Adress: 6FD186A0   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE94D4));			// Adress: 6FD194D4   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xE9CF1));			// Adress: 6FD19CF1   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xEA500));			// Adress: 6FD1A500   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnit, (GameOffset + 0xEAABB));			// Adress: 6FD1AABB   CALL D2Game.6FCBC900
CALL(Unit_GetTargetUnitv2, (GameOffset + 0x275A9));			// Adress: 6FC575A9   CALL D2Game.6FCBC7E0
CALL(Unit_GetTargetUnitv2, (GameOffset + 0x34EDD));			// Adress: 6FC64EDD   CALL D2Game.6FCBC7E0
CALL(Unit_GetTargetUnitv2, (GameOffset + 0x4F78F));			// Adress: 6FC7F78F   CALL D2Game.6FCBC7E0
CALL(Unit_GetTargetUnitv2, (GameOffset + 0x50F1B));			// Adress: 6FC80F1B   CALL D2Game.6FCBC7E0
CALL(Unit_GetTargetUnitv2, (GameOffset + 0x8C90B));			// Adress: 6FCBC90B   CALL D2Game.6FCBC7E0
CALL(Unit_GetTargetUnitv2, (GameOffset + 0x8C95D));			// Adress: 6FCBC95D   CALL D2Game.6FCBC7E0

	*(DWORD*)(GameOffset + 0xC07B6) = (DWORD)gpfMonsterAI;			// 6FCF07B6  (ID * 4) + (GameOffset + 0x10F990) D2Game.6FD3F990
	*(DWORD*)(GameOffset + 0xC07BE) = (DWORD)gpfMonsterAI;			// 6FCF07BE  (ID * 4) + (GameOffset + 0x10F990) D2Game.6FD3F990
	*(DWORD*)(GameOffset + 0xC08B0) = (DWORD)gpfMonsterAI;			// 6FCF08B0  (ID * 4) + (GameOffset + 0x10F990) D2Game.6FD3F990
	*(DWORD*)(GameOffset + 0xC08B7) = (DWORD)gpfMonsterAI;			// 6FCF08B7  (ID * 4) + (GameOffset + 0x10F990) D2Game.6FD3F990
	*(DWORD*)(GameOffset + 0xC09AD) = (DWORD)gpfMonsterAI;			// 6FCF09AD  (ID * 4) + (GameOffset + 0x10F990) D2Game.6FD3F990

// *(DWORD*)(GameOffset + 0xBBBF6) = (DWORD)gpfAITypesFN;			// 6FCEBBF6 (ID * 4) + (GameOffset + 0xBC804) 6FCEC804

//	D2Common.dll patches
CALL(CheckIfTownID, (CommonOffset + 0x68A20));					// 67A20 #D2Common.10082
CALL(CheckIfTownID, (CommonOffset + 0x73F33));					// 72F33 #D2Common.10082
CALL(CheckIfTownID, (CommonOffset + 0x7CB51));					// 7BB51 #D2Common.10082
CALL(CheckIfTownID, (CommonOffset + 0x7CE60));					// 7BE60 #D2Common.10082

}