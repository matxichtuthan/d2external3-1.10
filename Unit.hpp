#ifndef __UNIT_HPP__INCLUDED__
#define __UNIT_HPP__INCLUDED__

Unit* STDCALL Unit_GetTargetUnit (D2Game* ptGame, Unit* ptUnit);
Unit* STDCALL Unit_GetTargetUnitv2 (D2Game* ptGame, Unit* ptUnit);

#endif