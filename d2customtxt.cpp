#include "common.hpp"
#include "error.hpp"
#include "d2functions.hpp"
#include "d2customtxt.hpp"
#include "table.hpp"

UtilityTXT* STDCALL GetUtilityRecord(BYTE hcIdx)
{
	if(UtilityTXTRecord)
	{
		DWORD i = 0;
		while(i < UtilityTXTRecordNbr)
		{
			if(UtilityTXTRecord[i].m_hcIdx == hcIdx)
				return &UtilityTXTRecord[i];

			i++;
		}
	}
	return 0;
}

TownsTXT* STDCALL GetTownsRecord(BYTE Act)
{
	if(TownsTXTRecord)
	{
		DWORD i = 0;
		while(i < TownsTXTRecordNbr)
		{
			if(TownsTXTRecord[i].m_Act == Act)
				return &TownsTXTRecord[i];

			i++;
		}
	}
	return 0;
}
