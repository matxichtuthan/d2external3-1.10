/*=================================================================*/
/** @file D2SkillsTxt .h
 *  @brief Diablo II Constants.
 *
 *  (c) January 2013 - Thomas Westman
 */
/*=================================================================*/

#ifndef __D2TXTSTRUCT_H__INCLUDED__
#define __D2TXTSTRUCT_H__INCLUDED__


#pragma pack(1)
struct D2SkillsTXT
{
	int		SkillID;			//+000 - WORD
	union
	{
		struct 
		{
			DWORD fDecquant:1;           // 0
			DWORD fLob:1;                // 1
			DWORD fProgressive:1;        // 2
			DWORD fFinishing:1;          // 3
			DWORD fPassive:1;            // 4
			DWORD fAura:1;               // 5
			DWORD fPeriodic:1;           // 6
			DWORD fPrgStack:1;           // 7
			DWORD fInTown:1;             // 8 - probably #1 instead of fLob
			DWORD fKick:1;               // 9
			DWORD fInGame:1;             // a
			DWORD fRepeat:1;             // b
			DWORD fStSoundSuccessOnly:1; // c
			DWORD fStSoundDelay:1;       // d
			DWORD fWeaponSnd:1;          // e
			DWORD fImmediate:1;          // f
			DWORD fNoAmmo:1;             // 10
			DWORD fEnhanceable:1;        // 11
			DWORD fDurability:1;         // 12
			DWORD fUseAttackRate:1;      // 13
			DWORD fTargetableOnly:1;     // 14
			DWORD fSearchEnemyXY:1;      // 15
			DWORD fSearchEnemyNear:1;    // 16
			DWORD fSearchOpenXY:1;       // 17
			DWORD fTargetCorpse:1;       // 18
			DWORD fTargetPet:1;          // 19
			DWORD fTargetAlly:1;         // 1a
			DWORD fTargetItem:1;         // 1b
			DWORD fAttackNoMana:1;       // 1c
			DWORD fItemTgtDo:1;          // 1d
			DWORD fLeftSkill:1;          // 1e
			DWORD fInterrupt:1;          // 1f
		}fSkillFlags;			// +004
		DWORD fFlags;
	};
	union
	{
		struct
		{
			DWORD fTgtPlaceCheck:1;      // 20
			DWORD fItemCheckStart:1;     // 21
			DWORD fItemCltCheckStart:1;  // 22
			DWORD fGeneral:1;            // 23
			DWORD fScroll:1;             // 24
			DWORD fUseManaOnDo:1;        // 25
			DWORD fWarp:1;               // 26
		}fSkillFlagsEx;         // +008
		DWORD fFlagsEx;
	};
	char	CharClass;			// +00C
	BYTE	_00D[3];			// +00D
	char	nPlrMode;           // +010
    char	nMonMode;           // +011
    char	nSeqTransMode;      // +012
    char	nPlrSkillSeq;       // +013
    BYTE	nMasteryCheck;      // +014
    bool	bTargetDead;        // +015
    short	nSeqInput;          // +016
    short	nWeapType[3];       // +018
    short	nWeapType2[3];      // +01E - for the second hand, when using a two weapon attack
    short	nWeapExclude[2];    // +024
    short	nWeapExclude2[2];   // +028
    short	nStartFunc;         // +02C     
    short	nFinishFunc;        // +02E
    short	nProgFunc[3];       // +030
    short	_36;                // +036
    DWORD	dwProgCalc[3];      // +038
    short	nProgDamage;        // +044
	short	nMissile[4];        // +046 +46 Missile +48 MissileA +4A MissileB +4C MissileC
	short	srvoverlay;			// +04E
	DWORD	aurafilter;			// +050
	short	aurastat[6];		// +054 +54 Aurastat1, +56 Aurastat2, +58 Aurastat3, 5A Aurastat4, 5C Aurastat5, 5E Aurastat6 
	DWORD	auralencalc;		// +060
	DWORD	aurarangecalc;		// +064
	DWORD	aurastatcalc[6];	// +068 +68 AurastatCalc1, +6C Aurastatcalc2, +70 AurastatCalc3, +74 AurastatCalc4, +78 AurastatCalc5, +7C AurastatCalc6
	short	aurastate;			// +080
	short	auratargetstate;	// +082
	WORD	auraevent[3];		// +084 +84 Auraevent1, +86 Auraevent2, +88 Auraevent3
	WORD	auraeventfunc[3];	// +08A +8A AuraeventFunc1, +8C AuraEventFunc2, +8E AuraEventFunc3
	short	nAuraTargetEvent;	// +090
	short	nAuraTargetEventFunc; // +092
	short	nPassiveState;		// +094
	short	nPassiveWeaponType;	// +096
	short	nPassiveStat[5];	// +098 +98 PassiveStat1, +9A passivestat2, +9C passivestat3, +9E passivestat4. +A0 passivestat5
	WORD	_0A2;				// +0A2
	DWORD	passivecalc[5];		// +0A4 +A4 passiveCalc1, +A8 passivecalc2, +AC passivecalc3, +B0 passivecalc4, +B4 passivecalc5
	short	passiveevent;		// +0B8
	short	passiveeventfunc;	// +0BA
	short	summon;				// +0BC
	char	pettype;			// +0BE
	char	summode;			// +0BF
	DWORD	petmax;				// +0C0
	short	nPetSkill[5];       // +0C4 +C4 PetSkill1 +C6 PEtSkill2 +C8 PetSkill3 +CA PetSkill4 +CC PetSkill5 
	WORD	_0CE;	            // +0CE
    DWORD	dwPetSkillLevel[5]; // +0D0
    short	nPetBossMod;        // +0E4
    short	nPetOverlay;        // +0E6
	short	cltmissile[5];		// +0E8 +E8 clitmissile1 +EA cltMissile2 +EC cltMissile3, +EE cltMissile4 +F0 cltMissile5
    short	nClientStartFunc;   // +0F2
    short	nClientFinishFunc;  // +0F4
    short	nClientProgFunc[3]; // +0F6
    short	nStartSound;        // +0FC
    short	nStartSoundClass;   // +0FE
    short	nFinishSound[3];    // +100
    short	nCastOverlay;       // +106
    short	nTargetOverlay;     // +108
    short	nTargetSound;       // +10A
	short	prgoverlay;			// +10C
	short	prgsound;			// +10E
	short	nClientOverlay[2];	// +110 +110 cltOverlaya +112 cltOverlayb
	DWORD	dwClientCalc[3];    // +114 +114 cltcalc1 +118 cltcalc2 +11C cltcalc3
	WORD	ItemTarget;			// +120
	WORD	ItemCastSound;		// +122
	WORD	ItemCastOverlay;	// +124
    WORD	_126;               // +126
	DWORD	perdelay;			// +128
	WORD	maxlvl;				// +12C
	WORD	ResultFlags;		// +12E
	DWORD	HitFlags;			// +130
	int		HitClass;			// +134
	DWORD	calc[4];			// +138 +138 Calc1, +13C Calc2 +140 Calc3 +144 Calc4
	DWORD	Param[8];			// +148 +148 Param1, +14C param2 +150 Param3 +154 Param4 +158 Param5 +15C Param6 +160 Param7 +164 Param8
	short	weapsel;			// +168
	short	ItemEffect;			// +16A
	short	ItemCltEffect;		// +16C
	WORD	nPad2;              // +16E
	DWORD	skpoints;			// +170
    short	nReqLevel;          // +174 
    short	nReqStr;            // +176
    short	nReqDex;            // +178
    short	nReqEnr;            // +17A
    short	nReqVit;            // +17C
	short	nReqSkill[3];       // +17E +17E reqskill1 +180 reqskill2 +182 reqskill3
	short	startmana;			// +184
	short	minmana;			// +186
	short	manashift;			// +188
	short	Mana;				// +18A
	short	LvlMana;			// +18C
	char	Attackrank;			// +18E
	BYTE	LineOfSight;		// +18F
	DWORD	Delay;				// +190
	short	SkillDesc;			// +194
    WORD	_196;	            // +196
    int		nToHitBonus;		// +198
    int		nToHitBonusPerLevel; // +19C
    DWORD	dwToHitBonusCalc;    // +1A0
    char	nHitShift;           // +1A4
    BYTE	nSrcDamage;          // +1A5
    WORD	_1A6;	             // +1A6
    int		nMinDamage;          // +1A8
    int		nMaxDamage;          // +1AC
    int		nMinDamagePerLvl[5]; // +1B0
    int		nMaxDamagePerLvl[5]; // +1C4
    DWORD	dwSynergy;           // +1D8
	char	EType;				 // +1DC  
	BYTE	_1DD[3];			 // +1DD
	int		nEleMinDamage;		 // +1E0
	int		nEleMaxDamage;		 // +1E4
	int		nEleMinDamagePerLvl[5];	// +1E8 +1E8 EminLev1 +1EC EminLev2 +1F0 EminLev3 +1F4 EminLev4 +1F8 EminLev5 
	int		nEleMaxDamagePerLvl[5]; // +1FC +1FC EMaxLev1 +200 EMaxLev2 +204 EMaxLev3 +208 EMaxLev4 +20C EMaxLev5
    DWORD	dwEleSynergy;        // +210
	int		nEleLength;          // +214
    int		nEleLengthPerLvl[3]; // +218
    DWORD	dwEleLengthCalc;     // +224
    short	nShapeRestrict;      // +228
    short	nRestrictState[3];   // +22A
    short	nAIType;			 // +230
    short	nAIBonus;			 // +232
    int		nPriceAdd;           // +234
    int		nPriceMul;           // +238
};
#pragma pack()


#pragma pack(1)
struct D2SkillDescTXT
{
	// BYTE	unknown_1[0x08];	//+00
	short	nDesc;              // +00
	char	nSkillTab;          // +02
	char	nSkillRow;          // +03
	char	nSkillCol;          // +04
	char	nListRow;           // +05
	char	nListPool;          // +06
	BYTE	nIcon;              // +07
	short	nSkillName;			// +08
    short nSkillShortDesc;      // +0A
    short nSkillLongDesc;       // +0C
    short nSkillShortName;      // +0E - the name displayed in the character stats screen
    short nManaCostStr;         // +10
    short nDamageFunc;          // +12
    short nToHitFunc;           // +14
    WORD _16;                    // +16
    DWORD dwDamageCalc[2];      // +18
    char nElementProg[3];       // +20
    BYTE _23;                    // +23
    DWORD dwProgDamageMin[3];   // +24
    DWORD dwProgDamageMax[3];   // +30
    short nMissile[3];          // +3C
    char nDescLine[17];         // +42
    BYTE _53[1];                // +53
    short nDescText[17];        // +54
    short nDescTextB[17];       // +76
    DWORD dwDescCalc[17];       // +98
    DWORD dwDescCalcB[17];      // +DC
};
 #pragma pack()

// #pragma pack(1)
/*
struct D2SkillDescTXT
{
	BYTE	unknown_1[0x08];	//+00
	WORD	SkillName;			//+08
	BYTE	unknown_2[0x07];	//+0A
	WORD	SkillDesc;			//+11
	BYTE	unknown[0x10C];		//+00
};*/
// #pragma pack()


// #pragma pack(1)
struct D2BooksTXT
{
	WORD	Name_Unused;		//+00
	WORD	SkillIcon;			//+02
	DWORD	pSpell;				//+04
	DWORD	ScrollSkill;		//+08
	DWORD	BookSkill;			//+0C
	DWORD	BaseCost;			//+10
	DWORD	CostPerCharge;		//+14
	DWORD	ScrollSpellCode;	//+18
	DWORD	BookSpellCode;		//+1C
};
// #pragma pack()

// #pragma pack(1)
struct D2ItemsTXT
{
	char	FlippyFile[0x20];		//+00
	char	InvFile[0x20];			//+20
	char	UniqueInvFile[0x20];	//+40
	char	SetInvFile[0x20];		//+60
	DWORD	ItemCode;				//+80
	DWORD	NormCode;				//+84
	DWORD	UberCode;				//+88
	DWORD	HellCode;				//+8C
	DWORD	AlternateGfx;			//+90
	DWORD	pSpell;					//+94
	WORD	State;					//+98
	WORD	cState1;				//+9A
	WORD	cState2;				//+9C
	WORD	Stat1;					//+9E
	WORD	Stat2;					//+A0
	WORD	Stat3;					//+A2
	int		Calc1;					//+A4
	int		Calc2;					//+A8
	int		Calc3;					//+AC
	DWORD	Len;					//+B0
	WORD	SpellDesc;				//+B4
	WORD	SpellDescStr;			//+B6
	DWORD	SpellDescCalc;			//+B8
	DWORD	BetterGem;				//+BC
	DWORD	WClass;					//+C0
	DWORD	TwoHandedWClass;		//+C4
	DWORD	TMogType;				//+C8
	DWORD	MinAC;					//+CC
	DWORD	MaxAC;					//+D0
	DWORD	GambleCost;				//+D4
	DWORD	Speed;					//+D8
	DWORD	BitField;				//+DC
	DWORD	Cost;					//+E0
	DWORD	MinStack;				//+E4
	DWORD	MaxStack;				//+E8
	DWORD	SpawnStack;				//+EC
	DWORD	GemOffset;				//+F0
	WORD	NameStr;				//+F4
	WORD	Version;				//+F6
	WORD	AutoPrefix;				//+F8
	WORD	MissileType;			//+FA
	BYTE	Rarity;					//+FC
	BYTE	Level;					//+FD
	BYTE	MinDam;					//+FE
	BYTE	MaxDam;					//+FF
	BYTE	MinMisDam;				//+100
	BYTE	MaxMisDam;				//+101
	BYTE	TwoHandMinDam;			//+102
	BYTE	TwoHandMaxDam;			//+103
	BYTE	RangeAdder;				//+104
	BYTE	NotUsed1;				//+105
	WORD	StrBonus;				//+106
	WORD	DexBonus;				//+108
	WORD	RegStr;					//+10A
	WORD	RegDex;					//+10C
	BYTE	Absorbs;				//+10E
	BYTE	InvWidth;				//+10F
	BYTE	InvHeight;				//+110
	BYTE	Block;					//+111
	BYTE	Durability;				//+112
	BYTE	NoDurability;			//+113
	BYTE	Missile;				//+114
	BYTE	Component;				//+115
	BYTE	RArm;					//+116
	BYTE	LArm;					//+117
	BYTE	Torso;					//+118
	BYTE	Legs;					//+119
	BYTE	RSpad;					//+11A
	BYTE	LSpad;					//+11B
	BYTE	TwoHanded;				//+11C
	BYTE	Usable;					//+11D
	WORD	Type;					//+11E
	WORD	Type2;					//+120
	BYTE	SubType;				//+122
	BYTE	NotUsed2;				//+123
	WORD	DropSound;				//+124
	WORD	UseSound;				//+126
	BYTE	DropSfxFrame;			//+128
	BYTE	Unique;					//+129
	BYTE	Quest;					//+12A
	BYTE	QuestDiffCheck;			//+12B
	BYTE	Transparent;			//+12C
	BYTE	TransTbl;				//+12D
	BYTE	NotUsed3;				//+12E
	BYTE	LightRadius;			//+12F
	BYTE	Belt;					//+130
	BYTE	AutoBelt;				//+131
	BYTE	Stackable;				//+132
	BYTE	Spawnable;				//+133
	BYTE	SpellIcon;				//+134
	BYTE	DurWarning;				//+135
	BYTE	QntWaning;				//+136
	BYTE	HasInv;					//+137
	BYTE	GemSockets;				//+138
	BYTE	TransmoGrify;			//+139
	BYTE	TMogMin;				//+13A
	BYTE	TMogMax;				//+13B
	BYTE	HitClass;				//+13C
	BYTE	OneOrTwoHanded;			//+13D
	BYTE	GemApplyType;			//+13E
	BYTE	LevelReq;				//+13F
	BYTE	MagicLvl;				//+140
	BYTE	Transform;				//+141
	BYTE	InvTrans;				//+142
	BYTE	CompactSave;			//+143
	BYTE	SkipName;				//+144
	BYTE	NotUsed4;				//+145
	BYTE	VendorMin[17];			//+146
	BYTE	VendorMax[17];			//+157
	BYTE	VendorMagicMin[17];		//+168
	BYTE	VendorMagicMax[17];		//+179
	BYTE	VendorMagicLvl[17];		//+18A
	BYTE	NotUsed5;				//+19B
	DWORD	NightmareUpgrade;		//+19C
	DWORD	HellUpgrade;			//+1A0
	BYTE	PermStoreItem;			//+1A4
	BYTE	Multibuy;				//+1A5
};
// #pragma pack()

#pragma pack(push, 1)
struct D2RunesTXT
{
	char	Name[0x40];				//+00
	char	RuneName[0x40];			//+40
	BYTE	Complete;				//+80
	BYTE	Server;					//+81
	BYTE	Unknown[4];				//+82
	WORD	Itypes[6];				//+86
	WORD	Etypes[3];				//+92
	int		Runes[6];				//+98
	DWORD	t1code1;				//+B0
	DWORD	t1param1;				//+B4
	DWORD	t1min1;					//+B8
	DWORD	t1max1;					//+BC
	DWORD	t1code2;				//+C0
	DWORD	t1param2;				//+C4
	DWORD	t1min2;					//+C8
	DWORD	t1max2;					//+CC
	DWORD	t1code3;				//+D0
	DWORD	t1param3;				//+D4
	DWORD	t1min3;					//+D8
	DWORD	t1max3;					//+DC
	DWORD	t1code4;				//+E0
	DWORD	t1param4;				//+E4
	DWORD	t1min4;					//+E8
	DWORD	t1max4;					//+EC
	DWORD	t1code5;				//+F0
	DWORD	t1param5;				//+F4
	DWORD	t1min5;					//+F8
	DWORD	t1max5;					//+FC
	DWORD	t1code6;				//+100
	DWORD	t1param6;				//+104
	DWORD	t1min6;					//+108
	DWORD	t1max6;					//+10C
	DWORD	t1code7;				//+110
	DWORD	t1param7;				//+114
	DWORD	t1min7;					//+118
	DWORD	t1max7;					//+11C
	// Custom Added
	BYTE	quality;				//+120
};
#pragma pack(pop)

// #pragma pack(1)
struct D2RuneStruct
{
	int			nNbrOfRuneWords;
	D2RunesTXT*	ptRunes;
};
// #pragma pack()


struct D2ObjectsTXT
{
	char	szName[64];				//+000
	wchar_t	wszName[64];			//+040 
	char	szToken[3];				//+0C0 
	char	nSpawnMaxx;				//+0C3 
	char	nSelectable[8];			//+0C4 
	int		nTrapChance;            // +0CC
    int		nSizeX;                 // +0D0
    int		nSizeY;                 // +0D4
	int		nFrameCount[8];         // +0D8 +D8 nFrameCount[0] +DC nFramecount[1]
	short	nFrameDelta[8];			// +0F8
	bool	bLoopMode[8];			// +108
    char	nLightRadius[8];		// +110
    bool	bBlockLight[8];			// +118
    bool	bHasCollision[8];		// +120 +120 bHasCollision[0] +121 bHasCollision[1]
	bool	bAttackable;			// +128
    char	nStartFrame[8];			// +129
    char	nOrderFlag[8];			// +131
    char	nEnvironEffect;			// +139
    char	bDoor;					// +13A
    char	bVis;					// +13B
    char	nDirection;				// +13C
    char	bPreOperate;			// +13D
    char	nTransLevel;			// +13E
    bool	bHasMode[8];			// +13F
    BYTE	_147;					// +147
    int		nOffsetX;               // +148
    int		nOffsetY;               // +14C
    bool	bDraw;					// +150
    bool	bHasComp[16];			// +151
    char	nCompCount;				// +161
    char	nSpaceX;				// +162
    char	nSpaceY;				// +163
    BYTE	nRed;					// +164
    BYTE	nGreen;					// +165
    BYTE	nBlue;					// +166
    char	nSubClass;				// +167
    int		nNameOffset;			// +168
    BYTE	_16C;	                // +16C
	bool	bMonsterOk;				// +16D
    char	nOperateRange;			// +16E
    char	nShrineFunc;			// +16F
    char	nAct;					// +170
	bool	bLockable;				// +171
    bool	bGore;					// +172
    bool	bRestore;				// +173
    bool	bVirginRestore;			// +174
    bool	bSync;					// +175
    BYTE	_176[2];				// +176
    int		nObjParams[8];          // +178
    char	nFrontX;				// +198
    char	nFrontY;				// +199
    char	nBackX;					// +19A
    char	nBackY;					// +19B
    char	nDamagePercent;			// +19C
    char	nCollisionSub;			// +19D
    BYTE	_19E[2];				// +19E
    int		nLeft;                  // +1A0
    int		nTop;                   // +1A4
    int		nWidth;                 // +1A8
    int		nHeight;                // +1AC
    bool	bBeta;					// +1B0
    char	nInitFunc;				// +1B1
    char	nPopulateFunc;			// +1B2
    char	nOperateFunc;			// +1B3
    char	nClientFunc;			// +1B4
    bool	bOverlay;				// +1B5
    bool	bBlockMissile;			// +1B6
    bool	bDrawUnder;				// +1B7
    bool	bOpenWarp;				// +1B8
    BYTE	_1B9[3];				// +1B9
    int		nAutoMapCell;           // +1BC
};


/*
#pragma pack(1)
struct D2ObjectsTXT
{
	char	szName[64];				//+000
	wchar_t	wszName[64];			//+040 
	char	szToken[3];				//+0C0 
	char	nSpawnMaxx;				//+0C3 
	char	nSelectable[8];			//+0C4 
	int		nTrapChance;            // +0CC
    int		nSizeX;                 // +0D0
    int		nSizeY;                 // +0D4
	int		nFrameCount[8];         // +0D8
	short	nFrameDelta[8];			// +0F8
	bool	bLoopMode[8];			// +108
    char	nLightRadius[8];		// +110
    bool	bBlockLight[8];			// +118
    bool	bHasCollision[8];		// +120
	bool	bAttackable;			// +128
    char	nStartFrame[8];			// +129
    char	nOrderFlag[8];			// +131
    char	nEnvironEffect;			// +139
    char	bDoor;					// +13A
    char	bVis;					// +13B
    char	nDirection;				// +13C
    char	bPreOperate;			// +13D
    char	nTransLevel;			// +13E
    bool	bHasMode[8];			// +13F
    BYTE	_147;					// +147
    int		nOffsetX;               // +148
    int		nOffsetY;               // +14C
    bool	bDraw;					// +150
    bool	bHasComp[16];			// +151
    char	nCompCount;				// +161
    char	nSpaceX;				// +162
    char	nSpaceY;				// +163
    BYTE	nRed;					// +164
    BYTE	nGreen;					// +165
    BYTE	nBlue;					// +166
    char	nSubClass;				// +167
    int		nNameOffset;			// +168
    BYTE	_16C;	                // +16C
	bool	bMonsterOk;				// +16D
    char	nOperateRange;			// +16E
    char	nShrineFunc;			// +16F
    char	nAct;					// +170
	bool	bLockable;				// +171
    bool	bGore;					// +172
    bool	bRestore;				// +173
    bool	bVirginRestore;			// +174
    bool	bSync;					// +175
    BYTE	_176[2];				// +176
    int		nObjParams[8];          // +178
    char	nFrontX;				// +198
    char	nFrontY;				// +199
    char	nBackX;					// +19A
    char	nBackY;					// +19B
    char	nDamagePercent;			// +19C
    char	nCollisionSub;			// +19D
    BYTE	_19E[2];				// +19E
    int		nLeft;                  // +1A0
    int		nTop;                   // +1A4
    int		nWidth;                 // +1A8
    int		nHeight;                // +1AC
    bool	bBeta;					// +1B0
    char	nInitFunc;				// +1B1
    char	nPopulateFunc;			// +1B2
    char	nOperateFunc;			// +1B3
    char	nClientFunc;			// +1B4
    bool	bOverlay;				// +1B5
    bool	bBlockMissile;			// +1B6
    bool	bDrawUnder;				// +1B7
    bool	bOpenWarp;				// +1B8
    BYTE	_1B9[3];				// +1B9
    int		nAutoMapCell;           // +1BC
};
#pragma pack()
*/

// #pragma pack(1)
/*
struct D2ObjectsTXT
{
	char	Name[0x40];				//+000
	char	Text[0x80];				//+040
	char	Token[0x03];			//+0C0
	BYTE	SpawnMax;				//+0C3
	BYTE	Selectable0;			//+0C4
	BYTE	Selectable1;			//+0C5
	BYTE	Selectable2;			//+0C6
	BYTE	Selectable3;			//+0C7
	BYTE	Selectable4;			//+0C8
	BYTE	Selectable5;			//+0C9
	BYTE	Selectable6;			//+0CA
	BYTE	Selectable7;			//+0CB
	BYTE	TrapProp;				//+0CC
	DWORD	SizeX;					//+0D0
	DWORD	SizeY;					//+0D4
	DWORD	FrameCnt0;				//+0D8
	DWORD	FrameCnt1;				//+0DC
	DWORD	FrameCnt2;				//+0E0
	DWORD	FrameCnt3;				//+0E4
	DWORD	FrameCnt4;				//+0E8
	DWORD	FrameCnt5;				//+0EC
	DWORD	FrameCnt6;				//+0F0
	DWORD	FrameCnt7;				//+0F4
	WORD	FrameDelta0;			//+0F8
	WORD	FrameDelta1;			//+0FA
	WORD	FrameDelta2;			//+0FC
	WORD	FrameDelta3;			//+0FE
	WORD	FrameDelta4;			//+100
	WORD	FrameDelta5;			//+102
	WORD	FrameDelta6;			//+104
	WORD	FrameDelta7;			//+106
	BYTE	CycleAnim0;				//+108
	BYTE	CycleAnim1;				//+109
	BYTE	CycleAnim2;				//+10A
	BYTE	CycleAnim3;				//+10B
	BYTE	CycleAnim4;				//+10C
	BYTE	CycleAnim5;				//+10D
	BYTE	CycleAnim6;				//+10E
	BYTE	CycleAnim7;				//+10F
	BYTE	Lit0;					//+110
	BYTE	Lit1;					//+111
	BYTE	Lit2;					//+112
	BYTE	Lit3;					//+113
	BYTE	Lit4;					//+114
	BYTE	Lit5;					//+115
	BYTE	Lit6;					//+116
	BYTE	Lit7;					//+117
	BYTE	BlocksLight0;			//+118
	BYTE	BlocksLight1;			//+119
	BYTE	BlocksLight2;			//+11A
	BYTE	BlocksLight3;			//+11B
	BYTE	BlocksLight4;			//+11C
	BYTE	BlocksLight5;			//+11D
	BYTE	BlocksLight6;			//+11E
	BYTE	BlocksLight7;			//+11F
	BYTE	HasCollision0;			//+120
	BYTE	HasCollision1;			//+121
	BYTE	HasCollision2;			//+122
	BYTE	HasCollision3;			//+123
	BYTE	HasCollision4;			//+124
	BYTE	HasCollision5;			//+125
	BYTE	HasCollision6;			//+126
	BYTE	HasCollision7;			//+127
	BYTE	IsAttackable0;			//+128
	BYTE	Start0;					//+129
	BYTE	Start1;					//+12A
	BYTE	Start2;					//+12B
	BYTE	Start3;					//+12C
	BYTE	Start4;					//+12D
	BYTE	Start5;					//+12E
	BYTE	Start6;					//+12F
	BYTE	Start7;					//+130
	BYTE	OrderFlag0;				//+131
	BYTE	OrderFlag1;				//+132
	BYTE	OrderFlag2;				//+133
	BYTE	OrderFlag3;				//+134
	BYTE	OrderFlag4;				//+135
	BYTE	OrderFlag5;				//+136
	BYTE	OrderFlag6;				//+137
	BYTE	OrderFlag7;				//+138
	BYTE	EnvEffect;				//+139
	BYTE	IsDoor;					//+13A
	BYTE	BlocksVis;				//+13B
	BYTE	Orientation;			//+13C
	BYTE	PreOperate;				//+13D
	BYTE	Trans;					//+13E
	BYTE	Mode0;					//+13F
	BYTE	Mode1;					//+140
	BYTE	Mode2;					//+141
	BYTE	Mode3;					//+142
	BYTE	Mode4;					//+143
	BYTE	Mode5;					//+144
	BYTE	Mode6;					//+145
	BYTE	Mode7;					//+146
	BYTE	NotUsed_1;				//+147
	DWORD	XOffset;				//+148
	DWORD	YOffset;				//+14C
	BYTE	Draw;					//+150
	BYTE	HD;						//+151
	BYTE	TR;						//+152
	BYTE	LG;						//+153
	BYTE	RA;						//+154
	BYTE	LA;						//+155
	BYTE	RH;						//+156
	BYTE	LH;						//+157
	BYTE	SH;						//+158
	BYTE	S1;						//+159
	BYTE	S2;						//+15A
	BYTE	S3;						//+15B
	BYTE	S4;						//+15C
	BYTE	S5;						//+15D
	BYTE	S6;						//+15E
	BYTE	S7;						//+15F
	BYTE	S8;						//+160
	BYTE	TotalPieces;			//+161
	BYTE	XSpace;					//+162
	BYTE	YSpace;					//+163
	BYTE	Red;					//+164
	BYTE	Green;					//+165
	BYTE	Blue;					//+166
	BYTE	SubClass;				//+167
	DWORD	NameOffset;				//+168
	BYTE	NotUsed_2;				//+16C
	BYTE	MonsterOK;				//+16D
	BYTE	OperateRange;			//+16E
	BYTE	ShrineFunction;			//+16F
	BYTE	Act;					//+170
	BYTE	Lockable;				//+171
	BYTE	Gore;					//+172
	BYTE	Restore;				//+173
	BYTE	RestoreVirgins;			//+174
	BYTE	Sync;					//+175
	WORD	NotUsed_3;				//+176
	DWORD	Param0;					//+178
	DWORD	Param1;					//+17C
	DWORD	Param2;					//+180
	DWORD	Param3;					//+184
	DWORD	Param4;					//+188
	DWORD	Param5;					//+18C
	DWORD	Param6;					//+190
	DWORD	Param7;					//+194
	BYTE	nTgtFX;					//+198
	BYTE	nTgtFY;					//+199
	BYTE	nTgtBX;					//+19A
	BYTE	nTgtBY;					//+19B
	BYTE	Damage;					//+19C
	BYTE	CollisionSubst;			//+19D
	WORD	NotUsed_4;				//+19E
	DWORD	Left;					//+1A0
	DWORD	Top;					//+1A4
	DWORD	Width;					//+1A8
	DWORD	Height;					//+1AC
	BYTE	Beta;					//+1B0
	BYTE	InitFn;					//+1B1
	BYTE	PopulateFn;				//+1B2
	BYTE	OperateFn;				//+1B3
	BYTE	ClientFn;				//+1B4
	BYTE	Overlay;				//+1B5
	BYTE	BlockMissile;			//+1B6
	BYTE	DrawUnder;				//+1B7
	BYTE	OpenWarp;				//+1B8
	WORD	NotUsed_5;				//+1B9
	DWORD	AutoMap;				//+1BC
};
*/
// #pragma pack()

// #pragma pack(1)
/*
struct D2ShrinesTXT
{
	BYTE	Code;					//+00
	BYTE	NotUsed1[3];			//+01
	DWORD	Arg1;					//+04
	DWORD	Arg2;					//+08
	DWORD	DurInFrames;			//+0C
	BYTE	ResetTimeInMinutes;		//+10
	BYTE	Rarity;					//+11
	char	ViewName[0x1F];			//+12
	char	NiftyPhrase[0x7F];		//+32
	BYTE	EffectClass;			//+B2
	BYTE	NotUsed2;				//+B3
	DWORD	LevelMin;				//+B4
};
*/
// #pragma pack()

// #pragma pack(1)
struct D2CharStatsTXT
{
	wchar_t	Class_t[0x10];			//+00
	char	Class[0x10];			//+20
	BYTE	Str;					//+30
	BYTE	Dex;					//+31
	BYTE	Int;					//+32
	BYTE	Vit;					//+33
	BYTE	Stamina;				//+34
	BYTE	HpAdd;					//+35
	BYTE	PercentStr;				//+36
	BYTE	PercentInt;				//+37
	BYTE	PercentDex;				//+38
	BYTE	PercentVit;				//+39
	BYTE	ManaRegen;				//+3A
	BYTE	NotUsed1;				//+3B
	DWORD	ToHitFactor;			//+3C
	BYTE	WalkVelocity;			//+40
	BYTE	RunVelocity;			//+41
	BYTE	RunDrain;				//+42
	BYTE	LifePerLevel;			//+43
	BYTE	StaminaPerLevel;		//+44
	BYTE	ManaPerLevel;			//+45
	BYTE	LifePerVitality;		//+46
	BYTE	StaminaPerVitality;		//+47
	BYTE	ManaPerMagic;			//+48
	BYTE	BlockFactor;			//+49
	BYTE	NotUsed2[2];			//+4A
	DWORD	BaseWClass;				//+4C
	BYTE	StatPerLevel;			//+50
	BYTE	NotUsed3;				//+51
	WORD	StrAllSkills;			//+52
	WORD	StrSkillTab1;			//+54
	WORD	StrSkillTab2;			//+56
	WORD	StrSkillTab3;			//+58
	WORD	StrClassOnly;			//+5A
	DWORD	Item1;					//+5C
	BYTE	Item1Loc;				//+60
	BYTE	Item1Count;				//+61
	BYTE	NotUsed4[2];			//+62
	DWORD	Item2;					//+64
	BYTE	Item2Loc;				//+68
	BYTE	Item2Count;				//+69
	BYTE	NotUsed5[2];			//+6A
	DWORD	Item3;					//+6C
	BYTE	Item3Loc;				//+70
	BYTE	Item3Count;				//+71
	BYTE	NotUsed6[2];			//+72
	DWORD	Item4;					//+74
	BYTE	Item4Loc;				//+78
	BYTE	Item4Count;				//+79
	BYTE	NotUsed7[2];			//+7A
	DWORD	Item5;					//+7C
	BYTE	Item5Loc;				//+80
	BYTE	Item5Count;				//+81
	BYTE	NotUsed8[2];			//+82
	DWORD	Item6;					//+84
	BYTE	Item6Loc;				//+88
	BYTE	Item6Count;				//+89
	BYTE	NotUsed9[2];			//+8A
	DWORD	Item7;					//+8C
	BYTE	Item7Loc;				//+90
	BYTE	Item7Count;				//+91
	BYTE	NotUsed10[2];			//+92
	DWORD	Item8;					//+94
	BYTE	Item8Loc;				//+98
	BYTE	Item8Count;				//+99
	BYTE	NotUsed11[2];			//+9A
	DWORD	Item9;					//+9C
	BYTE	Item9Loc;				//+A0
	BYTE	Item9Count;				//+A1
	BYTE	NotUsed12[2];			//+A2
	DWORD	Item10;					//+A4
	BYTE	Item10Loc;				//+A8
	BYTE	Item10Count;			//+A9
	BYTE	NotUsed13[2];			//+AA
	WORD	StartSkill;				//+AC
	WORD	Skill1;					//+AE
	WORD	Skill2;					//+B0
	WORD	Skill3;					//+B2
	WORD	Skill4;					//+B4
	WORD	Skill5;					//+B6
	WORD	Skill6;					//+B8
	WORD	Skill7;					//+BA
	WORD	Skill8;					//+BC
	WORD	Skill9;					//+BE
	WORD	Skill10;				//+C0
	BYTE	NotUSed14[2];			//+C2
};
// #pragma pack()

// #pragma pack(1)
struct D2ItemTypesTXT
{
	DWORD			Code;				//+00
	WORD			Equiv1;				//+04
	WORD			Equiv2;				//+06
	BYTE			Repair;				//+08
	BYTE			Body;				//+09
	BYTE			BodyLoc1;			//+0A
	BYTE			BodyLoc2;			//+0B
	WORD			Shoots;				//+0C
	WORD			Quiver;				//+0E
	BYTE			Throwable;			//+10
	BYTE			Reload;				//+11
	BYTE			ReEquip;			//+12
	BYTE			AutoStack;			//+13
	BYTE			Magic;				//+14
	BYTE			Rare;				//+15
	BYTE			Normal;				//+16
	BYTE			Charm;				//+17
	BYTE			Gem;				//+18
	BYTE			BeltAble;			//+19
	BYTE			MaxSock1;			//+1A
	BYTE			MaxSock25;			//+1B
	BYTE			MaxSock40;			//+1C
	BYTE			TreasureClass;		//+1D
	BYTE			Rarity;				//+1E
	BYTE			StaffMods;			//+1F
	BYTE			CostFormula;		//+20
	BYTE			Class;				//+21
	BYTE			StorePage;			//+22
	BYTE			VarInvGfx;			//+23
	char			InvGfx1[0x20];		//+24
	char			InvGfx2[0x20];		//+44
	char			InvGfx3[0x20];		//+64
	char			InvGfx4[0x20];		//+84
	char			InvGfx5[0x20];		//+A4
	char			InvGfx6[0x20];		//+C4
 };
// #pragma pack()

// #pragma pack(1)
struct D2OpStatStrc
{
	short nOpBase;				// +00
	short nOpStat;				// +02
	BYTE nOp;					// +04
	BYTE nOpParam;				// +05
};
// #pragma pack()

// #pragma pack(1)
struct D2ItemStatCostTXT
{
    int				Stat;				//+000
	DWORD			BitField;			//+004 Flags can be found in D2Constants.hpp
	char			SendBits;			//+008
	char			SendParamBits;		//+009
	char			CSVBits;			//+00A
	char			CSVParam;			//+00B
	int				Divide;				//+00C
	int				Multiply;			//+010
	int				Add;				//+014
	char			ValShift;			//+018
	char			SaveBits;			//+019
	char			SaveBits109;		//+01A
	BYTE			NotUsed1;			//+01B
	int				SaveAdd;			//+01C
	int				SaveAdd109;			//+020
	char			SaveParamBits;		//+024
	BYTE			uk3[0x7];			//+025 
	int				MinAccr;			//+02C
	bool			Encode;				//+030
	BYTE			NotUsed2;			//+031
	short			MaxStat;			//+032
	short			DescPriority;		//+034
	char			DescFunc;			//+036
	char			DescVal;			//+037
	short			DescStrPos;			//+038
	short			DescStrNeg;			//+03A
	short			DescStr2;			//+03C
	short			DGrp;				//+03E
	char			DGrpFunc;			//+040
	char			DGrpVal;			//+041
	short			DGrpStrPos;			//+042
	short			DGrpStrNeg;			//+044
	short			DGrpStr2;			//+046
	short			ItemVent[2];		//+048
	short			ItemVentFunc[2];	//+04C
	bool			KeepZero;			//+050
	bool			bBaseOf;			//+051 unknown data, filled during init Data1
	bool			bTargetOf;			//+052 unknown data, filled during init Data2
	bool			bSync;				//+053 unknown data, filled during init Data3
	char			Op;					//+054
	char			OpParam;			//+055
	short			OpBase;				//+056
	short			OpStat[3];			//+058
	// BYTE			Data4[0xE2];		//+05E unknown data, filled during init
	short			nBaseOf[64];		//+05E
	D2OpStatStrc	pStat[16];			//+0DE
	BYTE			_13E;				//+13E
	BYTE			_13F;				//+13F
	DWORD			Stuff;				//+140
 };
// #pragma pack()


// #pragma pack(1)
struct D2LvlWarpTXT
{
	BYTE unknown_0[0x14];	// Unknown
	DWORD ExitWalkX;		// +14
	DWORD ExitWalkY;		// +18
};
// #pragma pack()

// #pragma pack(1)
struct D2GemTXT
{
    char     name[20];			//+00  
    char     letter[6];			//+20  
    BYTE     uk1[2];			//+26  
	BYTE     uk2[0xC];          //+2C
    DWORD    code;              //+28(B)
    BYTE     nummods;			//+2E  
    BYTE     transform;         //+2F  
    int	     weaponmod1code;	//+30  
    DWORD    weaponmod1param;	//+34  
    DWORD    weaponmod1min;     //+38  
    DWORD    weaponmod1max;     //+3C  
    int		 weaponmod2code;    //+40  
    DWORD    weaponmod2param;	//+44  
    DWORD    weaponmod2min;     //+48  
    DWORD    weaponmod2max;     //+4C  
    int		 weaponmod3code;    //+50  
    DWORD    weaponmod3param;	//+54  
    DWORD    weaponmod3min;     //+58  
    DWORD    weaponmod3max;     //+5C  
    int		 helmmod1code;      //+60  
    DWORD    helmmod1param;     //+64  
    DWORD    helmmod1min;		//+68  
    DWORD    helmmod1max;		//+6C  
    int		 helmmod2code;		//+70  
    DWORD    helmmod2param;     //+74  
    DWORD    helmmod2min;		//+78  
    DWORD    helmmod2max;		//+7C  
    int      helmmod3code;      //+80  
    DWORD    helmmod3param;     //+84  
    DWORD    helmmod3min;		//+88  
    DWORD    helmmod3max;		//+8C  
    int      shieldmod1code;    //+90  
    DWORD    shieldmod1param;	//+94  
    DWORD    shieldmod1min;     //+98  
    DWORD    shieldmod1max;     //+9C  
    int      shieldmod2code;    //+A0  
    DWORD    shieldmod2param;	//+A4  
    DWORD    shieldmod2min;     //+A8  
    DWORD    shieldmod2max;     //+AC  
    int      shieldmod3code;    //+B0  
    DWORD    shieldmod3param;	//+B4  
    DWORD    shieldmod3min;     //+B8  
    DWORD    shieldmod3max;     //+BC  
};
// #pragma pack()

// #pragma pack(1)
struct D2Properties
{
	WORD	code;	// 0x00	
	BYTE	set1;	// 0x02	
	BYTE	set2;	// 0x03	
	BYTE	set3;	// 0x04	
	BYTE	set4;	// 0x05	
	BYTE	set5;	// 0x06	
	BYTE	set6;	// 0x07	
	BYTE	set7;	// 0x08	
	BYTE	uk1;	// 0x09
	WORD	val1;	// 0x0a	
	WORD	val2;	// 0x0c	
	WORD	val3;	// 0x0e	
	WORD	val4;	// 0x10	
	WORD	val5;	// 0x12	
	WORD	val6;	// 0x14	
	WORD	val7;	// 0x16	
	BYTE	func1;	// 0x18	
	BYTE	func2;	// 0x19	
	BYTE	func3;	// 0x1a	
	BYTE	func4;	// 0x1b	
	BYTE	func5;	// 0x1c	 
	BYTE	func6;	// 0x1D	
	BYTE	func7;	// 0x1E	
	BYTE	uk2;	// 0x1F
	WORD	stat1;	// 0x20	
	WORD	stat2;	// 0x22	
	WORD	stat3;	// 0x24	
	WORD	stat4;	// 0x26	
	WORD	stat5;	// 0x28	
	WORD	stat6;	// 0x2A	
	WORD	stat7;	// 0x2C	
};
// #pragma pack()

// #pragma pack(1)
struct D2UniqueItemsTXT //size=0x14C (332)  
{  
     WORD     uniqueId;          //+00  
     BYTE     uk1[0x20];          //+02  
     WORD     uniqueNameId;     //+22  
     BYTE     uk2[0x08];          //+24  
     union {  
          BYTE     flag;          //+2C  
          struct {  
               BYTE ukf:2;  
               BYTE carry1:1;  
          };  
     };  
     BYTE     uk3[0x11F];          //+2D  
};
// #pragma pack()

// #pragma pack(1)
struct D2MonstatsTXT
{
    short nMonster;	        // +000
    short nBase;            // +002
    short nNextInClass;     // +004
    short nNameString;      // +006
    short nDescString;      // +008
    BYTE  _0A;				// +00A
	BYTE  nCompMode;		// +00B - only 12
	union
	{
		struct
		{
			BYTE MonsterFlags1;  // +00C
			BYTE MonsterFlags2;  // +00D
			BYTE MonsterFlags3;  // +00E
			BYTE MonsterFlags4;  // +00F
		}fMonsterFlags;
		
		DWORD MonsterFlags;	// +00C;
	};
	DWORD dwMonToken;       // +010
    short nMonSounds;       // +014
    short nMonSoundsBoss;   // +016
    short nMonStatsEx;      // +018
    short nMonProp;         // +01A
    short nMonType;         // +01C
    short nMonAI;           // +01E
    short nSpawn;           // +020
    char nOffsetX;          // +022
    char nOffsetY;          // +023
    char nSpawnMode;        // +024
    BYTE _25[1];            // +025
    short nMinion[2];       // +026
    BYTE _2A[2];            // +02A
    char nMinionMin;        // +02C
    char nMinionMax;        // +02D
    char nMonRarity;        // +02E
    char nGroupMin;         // +02F
    char nGroupMax;         // +030
    char SparsePopulatePct; // +031
    short nWalkVel;         // +032
    short nRunVel;          // +034
    BYTE _36[4];            // +036
    short nMissile[8];      // +03A - A1, A2, S1, S2, S3, S4, SC, SQ
    BYTE nChainMax;         // +04A - the total number in this chain
    char nChain;			// +04B - code generated value, index in class-link-chain
    char nAlignType;        // +04C
    char nPalShift;         // +04D
    char nThreatLevel;      // +04E
    BYTE nAITickInterval[3];// +04F - delay between calling AiFunc
    BYTE nAIActivateDist[3];// +052 - distance at which the AiFunc is first called
    BYTE _55;               // +055
    short nAIParam[8][3];   // +056
    short nTreasure[3][4];  // +086
    char QuestNo;           // +09E
    char QuestFlag;         // +09F
    char DrainRes[3];       // +0A0
    char BlockChance[3];    // +0A3
    char CriticalChance;    // +0A6
    BYTE _0A7;				// +0A7
    short SkillDamageNo;    // +0A8
    short mLvl[3];          // +0AA
    short LifeMin[3];       // +0B0
	short LifeMax[3];		// +0B6
    short Defense[3];       // +0BC
    short ToHitA1[3];		// +0C2
    short ToHitA2[3];		// +0C8
    short ToHitS1[3];		// +0CE
    short Experience[3];    // +0D4
	WORD A1MinD[3];			// +0DA
	WORD A1MaxD[3];			// +0E0
	WORD A2MinD[3];			// +0E6
	WORD A2MaxD[3];			// +0EC
	WORD S1MinD[3];			// +0F2
	WORD S1MaxD[3];			// +0F8
    char EleMode[3];        // +0FE
    char EleType[3];        // +101
    char EleChance[3][3];   // +104
    BYTE _10D[1];           // +10D
	WORD EMinD[3][3];		// +10E [pos][diff]
	WORD EMaxD[3][3];		// +120
    short EleLength[3][3];  // +132
    short PhyRes[3];        // +144
    short MagRes[3];        // +14A
    short FireRes[3];       // +150
    short LightRes[3];      // +156
    short ColdRes[3];       // +15C
    short PoisRes[3];       // +162
    char ColdEffect[3];     // +168
    BYTE _16B[1];           // +16B
    BOOL bSendSkills;       // +16C
    short nSkill[8];        // +170
    BYTE nSkillMode[8];     // +180
    WORD nMonSkillSeq[8];   // +188 - code generated data, skill seq related
    BYTE nSkillLevel[8];    // +198
    int HpRegen;            // +1A0 - negative value leads to assertion
    char nSplEndDeath;      // +1A4
    char nSplModeChart;     // +1A5
    char nSplEndGeneric;    // +1A6
    char nSplClientEnd;     // +1A7
};
// #pragma pack()

// #pragma pack(1) 
struct D2Monstats2TXT //
{

   short	nMonStatsId;               			// +0x00000 - func 1a9a09c
   BYTE     pad0002;
   BYTE     pad0003;
	union
	{
		struct
		{
			BYTE MonsterFlags1;  // +00C
			BYTE MonsterFlags2;  // +00D
			BYTE MonsterFlags3;  // +00E
			BYTE MonsterFlags4;  // +00F
		}fMonsterFlags;
		
		DWORD MonsterFlags;	// +00C;
	};

   //DWORD dwFlag04;
            // 0x00000001 - NoGfxHitTest         -  0
            // 0x00000002 - NoMap                -  1
            // 0x00000004 - NoOvly               -  2
            // 0x00000008 - IsSel                -  3
            // 0x00000010 - AlSel                -  4
            // 0x00000020 - NoSel                -  5
            // 0x00000040 - ShiftSel             -  6
            // 0x00000080 - CorpseSel            -  7
            // 0x00000100 - Revive               -  8
            // 0x00000200 - IsAtt                -  9
            // 0x00000400 - Small                - 10
            // 0x00000800 - Large                - 11
            // 0x00001000 - Soft                 - 12
            // 0x00002000 - Critter              - 13
            // 0x00004000 - Shadow               - 14
            // 0x00008000 - NoUniqueShift        - 15
            // 0x00010000 - CompositeDeath       - 16
            // 0x00020000 - Inert                - 17
            // 0x00040000 - ObjCol               - 18
            // 0x00080000 - DeadCol              - 19
            // 0x00100000 - UnflatDead           - 20
   char     SizeX;            	// +0x00008
   char     SizeY;            	// +0x00009
   char     SpawnCol;         	// +0x0000A
   char     Height;           	// +0x0000B
   char     OverlayHeight;    	// +0x0000C
   char     PixHeight;        	// +0x0000D
   char     MeleeRng;         	// +0x0000E
   BYTE     pad000F;
   int		BaseW;            	// +0x00010
   char     HitClass;         	// +0x00014
   BYTE     pad0015;
   BYTE     pad0016;
   BYTE     pad0017;
   BYTE     pad0018;
   BYTE     pad0019;
   BYTE     pad001A;
   BYTE     pad001B;
   BYTE     pad001C;
   BYTE     pad001D;
   BYTE     pad001E;
   BYTE     pad001F;
   BYTE     pad0020;
   BYTE     pad0021;
   BYTE     pad0022;
   BYTE     pad0023;
   BYTE     pad0024;
   BYTE     pad0025;
   int		HDv;            	// +0x00026 - func 1a1bfe0
   int		TRv;            	// +0x00032 - func 1a1bfe0
   int		LGv;            	// +0x0003E - func 1a1bfe0
   int		Rav;            	// +0x0004A - func 1a1bfe0
   int		Lav;            	// +0x00056 - func 1a1bfe0
   int		RHv;            	// +0x00062 - func 1a1bfe0
   int		LHv;            	// +0x0006E - func 1a1bfe0
   int		SHv;            	// +0x0007A - func 1a1bfe0
   int		S1v;            	// +0x00086 - func 1a1bfe0
   int		S2v;            	// +0x00092 - func 1a1bfe0
   int		S3v;            	// +0x0009E - func 1a1bfe0
   int		S4v;            	// +0x000AA - func 1a1bfe0
   int		S5v;            	// +0x000B6 - func 1a1bfe0
   int		S6v;            	// +0x000C2 - func 1a1bfe0
   int		S7v;            	// +0x000CE - func 1a1bfe0
   int		S8v;            	// +0x000DA - func 1a1bfe0
   BYTE     pad00E6;
   BYTE     pad00E7;
   DWORD dwFlagE8;
            // 0x00000001 - HD                   -  0
            // 0x00000002 - TR                   -  1
            // 0x00000004 - LG                   -  2
            // 0x00000008 - RA                   -  3
            // 0x00000010 - LA                   -  4
            // 0x00000020 - RH                   -  5
            // 0x00000040 - LH                   -  6
            // 0x00000080 - SH                   -  7
            // 0x00000100 - S1                   -  8
            // 0x00000200 - S2                   -  9
            // 0x00000400 - S3                   - 10
            // 0x00000800 - S4                   - 11
            // 0x00001000 - S5                   - 12
            // 0x00002000 - S6                   - 13
            // 0x00004000 - S7                   - 14
            // 0x00008000 - S8                   - 15
   char     TotalPieces;      	// +0x000EC
   BYTE     pad00ED;
   BYTE     pad00EE;
   BYTE     pad00EF;
   DWORD dwFlagF0;
            // 0x00000001 - MDT                  -  0
            // 0x00000002 - MNU                  -  1
            // 0x00000004 - MWL                  -  2
            // 0x00000008 - MGH                  -  3
            // 0x00000010 - MA1                  -  4
            // 0x00000020 - MA2                  -  5
            // 0x00000040 - MBL                  -  6
            // 0x00000080 - MSC                  -  7
            // 0x00000100 - MS1                  -  8
            // 0x00000200 - MS2                  -  9
            // 0x00000400 - MS3                  - 10
            // 0x00000800 - MS4                  - 11
            // 0x00001000 - MDD                  - 12
            // 0x00002000 - MKB                  - 13
            // 0x00004000 - MSQ                  - 14
            // 0x00008000 - MRN                  - 15
   char     DDT;              	// +0x000F4
   char     DNU;              	// +0x000F5
   char     DWL;              	// +0x000F6
   char     DGH;              	// +0x000F7
   char     DA1;              	// +0x000F8
   char     DA2;              	// +0x000F9
   char     DBL;              	// +0x000FA
   char     DSC;              	// +0x000FB
   char     DS1;              	// +0x000FC
   char     DS2;              	// +0x000FD
   char     DS3;              	// +0x000FE
   char     DS4;              	// +0x000FF
   char     DDD;              	// +0x00100
   char     DKB;              	// +0x00101
   char     DSQ;              	// +0x00102
   char     DRN;              	// +0x00103
   DWORD dwFlag104;
            // 0x00000010 - A1mv                 -  4
            // 0x00000020 - A2mv                 -  5
            // 0x00000080 - SCmv                 -  7
            // 0x00000100 - S1mv                 -  8
            // 0x00000200 - S2mv                 -  9
            // 0x00000400 - S3mv                 - 10
            // 0x00000800 - S4mv                 - 11
   char     InfernoLen;       	// +0x00108
   char     InfernoAnim;      	// +0x00109
   char     InfernoRollback;  	// +0x0010A
   BYTE		ResurrectMode;    	// +0x0010B - func 1a99634
   WORD		ResurrectSkill;   	// +0x0010C - func 1a9a1a4
   WORD     HtTop;            	// +0x0010E
   WORD     HtLeft;           	// +0x00110
   WORD     HtWidth;          	// +0x00112
   WORD     HtHeight;         	// +0x00114
   BYTE     pad0116;
   BYTE     pad0117;
   DWORD    AutomapCel;       	// +0x00118
   char     LocalBlood;       	// +0x0011C
   char     Bleed;            	// +0x0011D
   char     Light;            	// +0x0011E
   char     Light_r;          	// +0x0011F
   char     Light_g;          	// +0x00120
   char     Light_b;          	// +0x00121
   char     Utrans;           	// +0x00122
   char     Utrans_N_;        	// +0x00123
   char     Utrans_H_;        	// +0x00124
   BYTE     pad0125;
   BYTE     pad0126;
   BYTE     pad0127;
   DWORD	Heart;            	// +0x00128
   DWORD	BodyPart;         	// +0x0012C
   char     Restore;          	// +0x00130
   BYTE     pad0131;
   BYTE     pad0132;
   BYTE     pad0133;
};
// #pragma pack()

// #pragma pack(1)
struct AffixTXT
{
    char szName[32];            // +00
    BYTE _20[2];                // +20
    short GameVers;             // +22
    D2Properties PropList[3];   // +24
    char BoolSpawnable;         // +
    BYTE _55[1];                // +
    short ColorNo;              // +
    int qLvl;                   // +
    int AffixGroup;             // +
    int qLvlMax;                // +
    char BoolRare;              // +42
    char LvlReq;                // +
    char BoolClassOnly;         // +
    char ClassNo;               // +
    char LvlReqClassOnly;       // +
    BYTE _69[1];                // +
    short ItemType[7];          // +
    short ExcludeType[5];       // +
    BYTE AffixFreq;             // +
    BYTE _83[1];                // +
    int PriceDiv;               // +
    int PriceMul;               // +
    int PriceAdd;               // +
}; 
// #pragma pack()

// #pragma pack(1)
struct SuperUniquesTXT
{
   WORD     Superunique;      	// +0x00000 - func 1a9a0e0
   WORD     Name;             	// +0x00002 - func 19f94d0
   DWORD    Class;            	// +0x00004 - func 1a9a084
   DWORD    hcIdx;            	// +0x00008
   DWORD    Mod1;             	// +0x0000C
   DWORD    Mod2;             	// +0x00010
   DWORD    Mod3;             	// +0x00014
   DWORD    MonSound;         	// +0x00018 - func 1a9a090
   DWORD    MinGrp;           	// +0x0001C
   DWORD    MaxGrp;           	// +0x00020
   char     AutoPos;          	// +0x00024
   char     EClass;           	// +0x00025
   char     Stacks;           	// +0x00026
   char     Replaceable;      	// +0x00027
   char     Utrans;           	// +0x00028
   char     Utrans_N_;        	// +0x00029
   char     Utrans_H_;        	// +0x0002A
   BYTE     pad002B;
   WORD     TC;               	// +0x0002C - func 1a99fc0
   WORD     TC_N_;            	// +0x0002E - func 1a99fc0
   WORD     TC_H_;            	// +0x00030 - func 1a99fc0
   BYTE     pad0032;
   BYTE     pad0033;
};
// #pragma pack()

// #pragma pack(1)
struct HirelingTXT
{
   WORD     Version;          	// +0x00000
   BYTE     pad0002;
   BYTE     pad0003;
   DWORD    Id;               	// +0x00004
   DWORD    Class;            	// +0x00008
   DWORD    Act;              	// +0x0000C
   DWORD    Difficulty;       	// +0x00010
   DWORD    Seller;           	// +0x00014
   DWORD    Gold;             	// +0x00018
   DWORD    Level;            	// +0x0001C
   DWORD    Exp_lvl;          	// +0x00020
   DWORD    Hp;               	// +0x00024
   DWORD    Hp_lvl;           	// +0x00028
   DWORD    Defense;          	// +0x0002C
   DWORD    Def_lvl;          	// +0x00030
   DWORD    Str;              	// +0x00034
   DWORD    Str_lvl;          	// +0x00038
   DWORD    Dex;              	// +0x0003C
   DWORD    Dex_lvl;          	// +0x00040
   DWORD    Ar;               	// +0x00044
   DWORD    Ar_lvl;           	// +0x00048
   DWORD    Share;            	// +0x0004C
   DWORD    Dmg_min;          	// +0x00050
   DWORD    Dmg_max;          	// +0x00054
   DWORD    Dmg_lvl;          	// +0x00058
   DWORD    Resist;           	// +0x0005C
   DWORD    Resist_lvl;       	// +0x00060
   DWORD    Defaultchance;    	// +0x00064
   DWORD    Head;             	// +0x00068
   DWORD    Torso;            	// +0x0006C
   DWORD    Weapon;           	// +0x00070
   DWORD    Shield;           	// +0x00074
   DWORD	Skill1;           	// +0x00078 - tabl 6fdf0aec  (+00B9C)
   DWORD	Skill2;           	// +0x0007C - tabl 6fdf0aec  (+00B9C)
   DWORD	Skill3;           	// +0x00080 - tabl 6fdf0aec  (+00B9C)
   DWORD	Skill4;           	// +0x00084 - tabl 6fdf0aec  (+00B9C)
   DWORD	Skill5;           	// +0x00088 - tabl 6fdf0aec  (+00B9C)
   DWORD	Skill6;           	// +0x0008C - tabl 6fdf0aec  (+00B9C)
   DWORD    Chance1;          	// +0x00090
   DWORD    Chance2;          	// +0x00094
   DWORD    Chance3;          	// +0x00098
   DWORD    Chance4;          	// +0x0009C
   DWORD    Chance5;          	// +0x000A0
   DWORD    Chance6;          	// +0x000A4
   DWORD    Chanceperlvl1;    	// +0x000A8
   DWORD    Chanceperlvl2;    	// +0x000AC
   DWORD    Chanceperlvl3;    	// +0x000B0
   DWORD    Chanceperlvl4;    	// +0x000B4
   DWORD    Chanceperlvl5;    	// +0x000B8
   DWORD    Chanceperlvl6;    	// +0x000BC
   char     Mode1;            	// +0x000C0
   char     Mode2;            	// +0x000C1
   char     Mode3;            	// +0x000C2
   char     Mode4;            	// +0x000C3
   char     Mode5;            	// +0x000C4
   char     Mode6;            	// +0x000C5
   char     Level1;           	// +0x000C6
   char     Level2;           	// +0x000C7
   char     Level3;           	// +0x000C8
   char     Level4;           	// +0x000C9
   char     Level5;           	// +0x000CA
   char     Level6;           	// +0x000CB
   char     Lvlperlvl1;       	// +0x000CC
   char     Lvlperlvl2;       	// +0x000CD
   char     Lvlperlvl3;       	// +0x000CE
   char     Lvlperlvl4;       	// +0x000CF
   char     Lvlperlvl5;       	// +0x000D0
   char     Lvlperlvl6;       	// +0x000D1
   DWORD	Hiredesc;         	// +0x000D2 - tabl 6fdf0008  (+000B8)
   char     Namefirst[32];    	// +0x000D3
   char     Namelast[33];     	// +0x000F3
   BYTE     pad0114;		    // +0x000F4
   BYTE     pad0115;			// +0x000F5
   BYTE     pad0116;			// +0x000F6
   BYTE     pad0117;			// +0x000F7
   DWORD	pad0118;			// +0x000F8
   DWORD	pad0119;			// +0x000FC
   DWORD	pad0120;			// +0x00100
   DWORD	pad0121;			// +0x00104
   DWORD	pad0122;			// +0x00108
   DWORD	pad0123;			// +0x0010C
   DWORD	pad0124;			// +0x00110
   WORD		pad0125;			// +0x00114 // NameFirst
   WORD		pad0126;			// +0x00116 // NameLast
};
// #pragma pack()

// #pragma pack(1)
struct MonlvlTXT
{
	int Defense[2][3];		// +00
	int ToHit[2][3];			// +18
	int Life[2][3];			// +30
	int Damage[2][3];			// +48
	int Experience[2][3];		// +60
};
// #pragma pack()

// #pragma pack(1)
struct D2MissilesTXT {
   WORD		Missile;          			// +0x00000 
   BYTE     pad0002;					// +0x00002
   BYTE     pad0003;					// +0x00003
   DWORD	dwFlag04;					// +0x00004
            // 0x00000001 - LastCollide          -  0
            // 0x00000002 - Explosion            -  1
            // 0x00000004 - Pierce               -  2
            // 0x00000008 - CanSlow              -  3
            // 0x00000010 - CanDestroy           -  4
            // 0x00000020 - ClientSend           -  5
            // 0x00000040 - GetHit               -  6
            // 0x00000080 - SoftHit              -  7
            // 0x00000100 - ApplyMastery         -  8
            // 0x00000200 - ReturnFire           -  9
            // 0x00000400 - Town                 - 10
            // 0x00000800 - SrcTown              - 11
            // 0x00001000 - NoMultiShot          - 12
            // 0x00002000 - NoUniqueMod          - 13
            // 0x00004000 - Half2HSrc            - 14
            // 0x00008000 - MissileSkill         - 15
   WORD     PCltDoFunc;       			// +0x00008
   WORD     PCltHitFunc;      			// +0x0000A
   WORD     PSrvDoFunc;       			// +0x0000C
   WORD     PSrvHitFunc;      			// +0x0000E
   WORD     PSrvDmgFunc;      			// +0x00010
   WORD		TravelSound;      			// +0x00012 
   WORD		HitSound;         			// +0x00014 
   WORD		ExplosionMissile; 			// +0x00016 
   WORD		SubMissile[3];      		// +0x00018 SubMissile1 +18 SubMissile2 +1A SubMissile3 +1C
   WORD		CltSubMissile[3];   		// +0x0001E CltSubMissile1 +1E CltSubMissile2 +20 CltSubMissile3 +22
   WORD		HitSubMissile[4];   		// +0x00024 HitSubMissile1 +24 HitSubMissile2 +26 HitSubMissile3 +28 HitSubMissile4 +2A
   WORD		CltHitSubMissile[4];		// +0x0002C CltHitSubMissile1 +2C CltHitSubMissile2 +2e CltHitSubMissile3 +30 CltHitSubMissile4 +32
   WORD		ProgSound;        			// +0x00034 
   WORD		ProgOverlay;      			// +0x00036 
   DWORD    Param[5];           		// +0x00038 par1 +38 par2 +3C par3 +40 par4 +44 par5 +48
   DWORD    SHitPar[3];         		// +0x0004C SHitPar1 +4C SHitPar2 +50 SHitPar3 +54 
   DWORD    CltParam[5];        		// +0x00058 CltParam1 +58 CltParam2 +5C CltParam3 +60 CltParam4 +64 CltParam5 +68 
   DWORD    CHitPar[3];         		// +0x0006C CHitPar1 +6C CHitPar2 +70 CHitPar3 +74
   DWORD    DParam[2];          		// +0x00078 DParam1 +78 DParam2 +7C
   DWORD	SrvCalc1;         			// +0x00080 
   DWORD	CltCalc1;         			// +0x00084 
   DWORD	SHitCalc1;        			// +0x00088 
   DWORD	CHitCalc1;        			// +0x0008C 
   DWORD	DmgCalc1;         			// +0x00090 
   char     HitClass;         			// +0x00094
   BYTE     pad0095;					// +0x00095
   WORD     Range;            			// +0x00096
   WORD     LevRange;         			// +0x00098
   char     Vel;              			// +0x0009A
   char     VelLev;           			// +0x0009B
   char     MaxVel;           			// +0x0009C
   BYTE     pad009D;					// +0x0009D
   WORD     Accel;            			// +0x0009E
   WORD     Animrate;         			// +0x000A0
   WORD     Xoffset;          			// +0x000A2
   WORD     Yoffset;          			// +0x000A4
   WORD     Zoffset;          			// +0x000A6
   DWORD	HitFlags;         			// +0x000A8
   WORD     ResultFlags;      			// +0x000AC
   char     KnockBack;        			// +0x000AE
   BYTE     pad00AF;					// +0x000AF
   DWORD    MinDamage;        			// +0x000B0
   DWORD    MaxDamage;        			// +0x000B4
   DWORD    MinLevDam[5];       		// +0x000B8 MinLevDam1 +B8 MinLevDam2 +BC MinLevDam3 +C0 MinLevDam4 +C4 MinLevDam5 +C8
   DWORD    MaxLevDam[5];       		// +0x000CC MaxLevDam1 +CC MaxLevDam2 +D0 MaxLevDam3 +D4 MaxLevDam4 +D8 MaxLevDam5 +DC 
   DWORD	DmgSymPerCalc;    			// +0x000E0 
   BYTE		EType;            			// +0x000E4 
   BYTE     pad00E5;					// +0x000E5
   BYTE     pad00E6;					// +0x000E6
   BYTE     pad00E7;					// +0x000E7
   DWORD    EMin;             			// +0x000E8
   DWORD    EMax;             			// +0x000EC
   DWORD    MinELev[5];         		// +0x000F0 MinELev1 +F0 MinELev2 +F4 MinELev3 +F8 MinELev4 +FC MinELev +100
   DWORD    MaxELev[5];         		// +0x00104 MaxELev1 +104 MaxELev2 +108 MaxELev3 +10C MaxELev4 +110 MaxELev5 +114
   DWORD	EDmgSymPerCalc;   			// +0x00118  
   DWORD    ELen;             			// +0x0011C
   DWORD    ELevLen[3];         		// +0x00120 ELevLen1 +120 ELevLen2 +124 ELevLen3 +128
   char     CltSrcTown;       			// +0x0012C
   char     SrcDamage;        			// +0x0012D
   char     SrcMissDmg;       			// +0x0012E
   char     Holy;             			// +0x0012F
   char     Light;            			// +0x00130
   char     Flicker;          			// +0x00131
   char     Red;              			// +0x00132
   char     Green;            			// +0x00133
   char     Blue;             			// +0x00134
   char     InitSteps;        			// +0x00135
   char     Activate;         			// +0x00136
   char     LoopAnim;         			// +0x00137
   char     CelFile[64];      			// +0x00138
   char     AnimLen;          			// +0x00178
   BYTE     pad0179;					// +0x00179
   BYTE     pad017A;					// +0x0017A
   BYTE     pad017B;					// +0x0017B
   DWORD    RandStart;        			// +0x0017C
   char     SubLoop;          			// +0x00180
   char     SubStart;         			// +0x00181
   char     SubStop;          			// +0x00182
   char     CollideType;      			// +0x00183
   char     Collision;        			// +0x00184
   char     ClientCol;        			// +0x00185
   char     CollideKill;      			// +0x00186
   char     CollideFriend;    			// +0x00187
   char     NextHit;          			// +0x00188
   char     NextDelay;        			// +0x00189
   char     Size;             			// +0x0018A
   char     ToHit;            			// +0x0018B
   char     AlwaysExplode;    			// +0x0018C
   char     Trans;            			// +0x0018D
   char     Qty;              			// +0x0018E
   BYTE     pad018F;					// +0x0018F
   DWORD    SpecialSetup;     			// +0x00190
   WORD		Skill;            			// +0x00194 
   char     HitShift;         			// +0x00196
   BYTE     pad0197;					// +0x00197
   BYTE     pad0198;					// +0x00198
   BYTE     pad0199;					// +0x00199
   BYTE     pad019A;					// +0x0019A
   BYTE     pad019B;					// +0x0019B
   DWORD    DamageRate;       			// +0x0019C
   char     NumDirections;    			// +0x001A0
   char     AnimSpeed;        			// +0x001A1
   char     LocalBlood;       			// +0x001A2
   BYTE     pad01A3;					// +0x001A3
}; // End                           +0x001A4
// #pragma pack()

// #pragma pack(1)
struct D2StatesTXT				//sizeof 0x3C
{
    short nState;               // +00
    short nOverlay[4];          // +02 +02 nOverlay1 +04 nOverlay2 +06 nOverlay3 +08 nOverlay4
    short nInitOverlay;         // +0A
    short nEndOverlay;          // +0C
    short nProgOverlay;         // +0E
    DWORD fStateFlags;			// +10
    DWORD fStateFlagsEx;		// +14
    short nStat;				// +18
    WORD nInitFunc;				// +1A
    WORD nEndFunc;				// +1C
    short nStateGroup;          // +1E
    BYTE nColorPriority;        // +20
    char nColorShift;			// +21
    BYTE nRed;                  // +22
    BYTE nGreen;                // +23
    BYTE nBlue;                 // +24
    BYTE _25;	                // +25
    short nInitSound;           // +26
    short nEndSound;            // +28
    short nColorShiftItemType;  // +2A
    char nColor;                // +2C
    char nShapeShiftUnitType;   // +2D
    short nShapeShiftUnitClass; // +2E
    WORD nClientEvent;			// +30
    WORD nClientEventFunc;		// +32
    WORD nClientActiveFunc;		// +34
    WORD nActiveFunc;			// +36
    short nSkill;               // +38
    short nMissile;             // +3A				
};
// #pragma pack()

// #pragma pack(1)
struct D2DifficultyLevelsTXT
{
    int ResistPenalty;          // +00
    int ExpPenalty;             // +04
	union
	{
		struct
		{
			int ExceptionalOdds;        // +08
			int ExceptionalOddsMagic;   // +0C
		};
		
		DWORD dwExceptionalOdds[2];
	};
    int MonSkillBonus;          // +10
    int MonFreezeDivisor;       // +14
    int MonChillDivisor;        // +18
    int MonAiCurseDivisor;      // +1C
	union
	{
		struct
		{
			int EliteOdds;              // +20
			int EliteOddsMagic;         // +24	
		};

		DWORD dwEliteOdds[2];
	};
    int LifeStealDivisor;       // +28
    int ManaStealDivisor;       // +2C
    int BossMonsterDamage;      // +30
    int ChampMonsterDamage;     // +34
    int HirelingBossPenalty;    // +38
    int MonCorpseExplosionCap;  // +3C
    int StaticFieldCap;         // +40
	union
	{
		struct
		{
			int GambleRare;             // +44
			int GambleSet;              // +48
			int GambleUnique;           // +4C
			int GambleExceptional;      // +50
			int GambleElite;            // +54
		};
		int nGamble[5];
	};
};
// #pragma pack()

struct D2OverlayTXT		//sizeof 0x84
{
    short nOverlay;         // +00
    char szFileName[64];    // +02
    short wGameVersion;     // +42
    int nFrameCount;        // +44
    BOOL bPreDraw;          // +48
    int nGroupSize;         // +4C
    char nDirection;        // +50
    bool bOpenBeta;			// +51
    bool bBeta;				// +52
    BYTE _53;				// +53
    int nOffsetX;           // +54
    int nOffsetY;           // +58
    int nHeight[4];         // +5C
    int nAnimRate;          // +6C
    int nInitRadius;        // +70
    int nEndRadius;         // +74
    int nLoopDelay;         // +78
    char nTransLevel;       // +7C
    BYTE nRed;              // +7D
    BYTE nGreen;            // +7E
    BYTE nBlue;             // +7F
    char nDirectionCount;   // +80
    bool bLocalBlood;		// +81
	WORD _82;				// +82
};

struct D2LevelsTXT		   //sizeof 0x220
{
    short nLevel;               // +000
    char nPalette;              // +002
    char nAct;                  // +003
    char nTeleportMode;         // +004
    bool bRain;                    // +005 - d2common.#10632
    bool bMud;                    // +006 - d2common.#10633
    bool bPerspective;            // +007 - d2common.#10634
    bool bInside;                // +008
    bool bDrawEdge;                // +009
    WORD _00A;                    // +00A
    int nWarpDistance;          // +00C
    short nAreaLevel[3];        // +010 - d2common.#11247
    short nAreaLevelEx[3];      // +016 - d2common.#11247
    int nMonDen[3];             // +01C
    char nBossMin[3];           // +028
    char nBossMax[3];           // +02B
    char nMonWander;            // +02E
    char nMonSpcWalk;           // +02F - don't set on levels, or experience bugs
    char nQuest;                // +030
    bool bRanged;                // +031
    char nSpawnCount;           // +032 - can only spawn 13 kinds at the same time
    char nMonCount;             // +033 - code generated value
    WORD _034;                  // +034
    short nMonsterPicks[25];    // +036
    short nMonster[25];         // +068
    short nBossMonster[25];     // +09A
    short nCritter[4];          // +0CC
    short CritterPercent[4];    // +0D4
    short nCritterMax;          // +0DC
    BYTE _0DE[6];               // +0DE
    char nWaypoint;             // +0E4
    char nObjGroup[8];          // +0E5
    char nObjGroupPercent[8];   // +0ED
    char szName[40];            // +0F5
    char szWarpName[40];        // +11D
    char szEntryFile[40];       // +145
    BYTE _16D;                    // +16E
    wchar_t wszName[40];        // +16E
    wchar_t wszWarpName[40];    // +1BE
    WORD _20E;                    // +20E
    int nTheme;                 // +210
    BOOL bFloorFilter;          // +214
    BOOL bBlankScreen;          // +218
    char nSoundEnviron;         // +21C
    BYTE nPad[3];               // +21D
};

struct D2LevelDefsTXT      //sizeof 0x9C
{
    int nQuestFlag;        // +00
    int nQuestFlagEx;      // +04
    int nAutomapLayer;     // +08
    int nSizeX[3];         // +0C
    int nSizeY[3];         // +18
    int nOffsetX;          // +24
    int nOffsetY;          // +28
    BOOL bDepend;          // +2C
    int nDRLGType;         // +30
    int nLevelType;        // +34
    int nSubType;          // +38
    int nSubTheme;         // +3C
    int nSubWaypoint;      // +40
    int nSubShrine;        // +44
    int nVis[8];           // +48
    int nWarp[8];           // +68
    BYTE nIntensity;       // +88
    BYTE nRed;             // +89
    BYTE nGreen;           // +8A
    BYTE nBlue;            // +8B
    BOOL bPortal;          // +8C
    BOOL bPosition;        // +90
    BOOL bSaveMonsters;    // +94
    BOOL bLineOfSight;     // +98
};

struct D2PetTypeTXT			//sizeof 0xE0
{
    int nPet;				// +00
    DWORD fPetFlags;		// +04
    short nPetGroup;        // +08
    short nBaseMax;         // +0A
    short nPetDescName;		// +0C
    char nIconType;         // +0E
    char szIcons[5][32];    // +0F
    BYTE _AF[3];            // +AF
    short nMonster[4];		// +B2
	BYTE _BA[38];			// +BA
};

struct D2LvlPrestTXT
{
};

struct D2DataTables
{
	BYTE			unknown_000[0x00BC]; // 0x0000
	D2StatesTXT*	D2StatesTxt;		 // +000BC States.txt
	DWORD			D2StatesLink;		 // +000C0
	DWORD			D2StatesCount;		 // +000C4
	BYTE			unknown1[0x00D8];	 // +000C8
	HirelingTXT*	D2Hireling;			 // +001A0 Hireling.txt
	DWORD			D2HirelingCount;	 // +001A4
	BYTE			unknown6[0x08D0];	 // +001A8
	D2MonstatsTXT*  D2Monstats;			 // +00A78 Monstats.txt
	DWORD			D2MonstatsLinkTable; // +00A7C fog.10211
	DWORD			D2MonstatsCount;	 // +00A80 
	BYTE			unknown5[0x0C];		 // +00A84 old: 0xE0
	D2Monstats2TXT* D2Monstats2;		 // +00A90
	DWORD			D2Monstats2LinkTable; // +00A94
	DWORD			D2Monstats2Count;	 // +00A98
	BYTE			unknown_A9C[0xC8];	 // +00A9C old: 0xE0
	D2MissilesTXT*	D2Missiles;			 // +00B64
	DWORD			D2MissilesLink;		 // +00B68
	DWORD			D2MissilesCount;	 // +00B6C
	MonlvlTXT*		D2MonLvl;			 // +00B70 MonLvl.txt
	DWORD			D2MonLvlCount;		 // +00B74
	DWORD*			D2MonSeqTXT;		 // +00B78
	DWORD			D2MonSeqLink;		 // +00B7C
	DWORD			D2MonSeq;			 // +00B80
	DWORD*			D2pMonSequences;	 // +00B84 created from MonSeq.txt after reading it.
	DWORD			D2nMonSequences;	 // +00B88
 	D2SkillDescTXT*	D2Skilldesc;         // +00B8C Skilldesc.txt
	DWORD			D2SkillDescLink;	 // +00B90
	DWORD			D2SkillDescCount;	 // +00B94
	D2SkillsTXT*	D2SkillsTXT;		 // +00B98
	DWORD			D2SkillsTXTLink;	 // +00B9C
	DWORD			D2SkillsCount;		 // +00BA0
	//BYTE			unknown4[0x20];		 // +00BA4 
	BYTE			unknown4[0x14];		 // +00BA4 
	D2OverlayTXT*	D2Overlay;			 // +00BB8
	DWORD			D2OverlayLink;		 // +00BBC
	DWORD			D2OverlayCount;		 // +00BC0
    D2CharStatsTXT*	D2CharStat;			 // +00BC4 CharStats.txt
	DWORD			D2CharStatCount;	 // +00BC8
	D2ItemStatCostTXT* D2ItemStatCost;	 // +00BCC Itemstatcost.txt
	BYTE            uk7[4];				 // +00BD0 
	DWORD           D2ItemStatCostCount; //+00BD4 
	BYTE			unknown2[0x10];		 // +00BD8 
	D2PetTypeTXT*	D2PetType;			 // +00BE8
	DWORD			D2PetTypeLink;		 // +00BEC
	DWORD			D2PetTypeCount;		 // +00BF0
	BYTE			unknownBF4[0x18];	 // +00BF4
	DWORD*			D2Sets;				 // +00C0C Sets.txt
	DWORD			D2SetsCount;		 // +00C10
	DWORD			unknown3;			 // +00C14
	DWORD*			D2SetItems;			 // +00C18 SetItems.txt
	DWORD			D2SetItemsCOunt;	 // +00C1C
	BYTE			uk10[0x4];			 // +00C20
    D2UniqueItemsTXT* D2UniqueItems;     // +00C24 UniqueItems.txt 
    DWORD           D2UniqueItemsCount;  // +00C28 
	BYTE			uk_C2C[0x2C];		 // +00C2C
	D2LevelsTXT*	D2LevelsTXT;		 // +00C58
	DWORD			D2LevelsCount;		 // +00C5C
	D2LevelDefsTXT* D2LevelDefsTXT;		 // +00C60 leveldefs.txt
	D2LvlPrestTXT*  D2LvlPrestTXT;		 // +00C64 lvlprest.txt
	int				nLvelPreset;		 // +00C68 # of lvlprest records
	int				nStuff;				 // +00C6C stuff value (ItemStatCost.txt + 0x140, recordNo1)
	int				cStuff;				 // +00C70 2 ^ stuff - 1 (used as a controller field for opstats and other special stats)
	BYTE			uk_C74[0x58];		 // +00C74 "00C64 old uk_C64[0x3C4]" 3B4 new: 0x58
	DWORD			D2InventoryCount;	 // +00CCC 
	D2InventoryTXT*	 D2InventoryTXT;		 // +00CD0
	BYTE			uk_CD4[0x354];		 // +00CD4 
	D2LvlWarpTXT*	D2WarpTXT;			 // +01028 nec's +00CD4
	DWORD			D2WarpCount;		 // +0102C nec's +00CD8
	BYTE			uk_1030[0x64];		 // +01030
	D2ShrinesTXT*	D2ShrinesTXT;		 // +01094
	DWORD			D2ShrineCount;		 // +01098
};

// #pragma pack(1)
/*
struct D2DataTables
{
	BYTE			unknown_000[0x00BC]; // 0x000
	D2StatesTXT*	D2StatesTxt;		 // +000BC States.txt
	DWORD			D2StatesLink;		 // +000C0
	DWORD			D2StatesCount;		 // +000C4
	BYTE			unknown1[0x00D8];	 // +000C8
	HirelingTXT*	D2Hireling;			 // +001A0 Hireling.txt
	DWORD			D2HirelingCount;	 // +001A4
	BYTE			unknown6[0x08D0];	 // +001A8
	D2MonstatsTXT*  D2Monstats;			 // +00A78 Monstats.txt
	DWORD			D2MonstatsLinkTable; // +00A7C fog.10211
	DWORD			D2MonstatsCount;	 // +00A80 
	BYTE			unknown5[0xE0];		 // +00A84
	D2MissilesTXT*	D2Missiles;			 // +00B64
	DWORD			D2MissilesLink;		 // +00B68
	DWORD			D2MissilesCount;	 // +00B6C
	MonlvlTXT*		D2MonLvl;			 // +00B70 MonLvl.txt
	DWORD			D2MonLvlCount;		 // +00B74
	DWORD*			D2MonSeqTXT;		 // +00B78
	DWORD			D2MonSeqLink;		 // +00B7C
	DWORD			D2MonSeq;			 // +00B80
	DWORD*			D2pMonSequences;	 // +00B84 created from MonSeq.txt after reading it.
	DWORD			D2nMonSequences;	 // +00B88
 	D2SkillDescTXT*	D2Skilldesc;         // +00B8C Skilldesc.txt
	DWORD			D2SkillDescLink;	 // +00B90
	DWORD			D2SkillDescCount;	 // +00B94
	D2SkillsTXT*	D2SkillsTXT;		 // +00B98
	DWORD			D2SkillsTXTLink;	 // +00B9C
	DWORD			D2SkillsCount;		 // +00BA0
	BYTE			unknown4[0x14];		 // +00BA4 
	D2OverlayTXT*	D2Overlay;			 // +00BB8
	DWORD			D2OverlayLink;		 // +00BBC
	DWORD			D2OverlayCount;		 // +00BC0
    D2CharStatsTXT*	D2CharStat;			 // +00BC4 CharStats.txt
	DWORD			D2CharStatCount;	 // +00BC8
	D2ItemStatCostTXT* D2ItemStatCost;	 // +00BCC Itemstatcost.txt
	BYTE            uk7[4];				 // +00BD0 
	DWORD           D2ItemStatCostCount; //+00BD4 
	BYTE			unknown2[0x10];		 // +00BD8 
	DWORD*			D2PetType;			 // +00BE8
	DWORD			D2PetTypeLink;		 // +00BEC
	DWORD			D2PetTypeCount;		 // +00BF0
	BYTE			unknownBF4[0x18];	 // +00BF4
	DWORD*			D2Sets;				 // +00C0C Sets.txt
	DWORD			D2SetsCount;		 // +00C10
	DWORD			unknown3;			 // +00C14
	DWORD*			D2SetItems;			 // +00C18 SetItems.txt
	DWORD			D2SetItemsCOunt;	 // +00C1C
	BYTE			uk10[0x4];			 // +00C20
    D2UniqueItemsTXT* D2UniqueItems;     // +00C24 UniqueItems.txt 
    DWORD           D2UniqueItemsCount;  // +00C28 
};
*/
// #pragma pack()



#endif
