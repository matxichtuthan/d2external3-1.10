#include "Common.hpp"
#include "error.hpp"
#include "d2functions.hpp"
#include "d2customtxt.hpp"
#include "d2wrapper.hpp"
#include "Town.hpp"


/* Get TownID from Act
   Coded by: Thomas Westman
   Date: 2013-03-03
*/

DWORD STDCALL GetTownIDFromAct(BYTE Act)
{
	if (Act <0 || Act > 4)
	{
		FogAssert("bAct < NUM_ACTS", __FILE__, __LINE__);
		exit(-1);
	}

	DWORD TownID;

	TownsTXT* ptTownsTXT = GetTownsRecord(Act);

	if (!ptTownsTXT){
		log_message("GetTownIDFromAct Func: ptTownsTXT == %d", ptTownsTXT);
		return 1;
	}

	TownID = ptTownsTXT->m_LevelID;
	TownID = TownID & 255;

	return TownID;
}

/* Check if room is Town
   Coded by: Thomas Westman
   Date: 2013-03-03
*/

BOOL STDCALL CheckIfTownID(DRLGRoom* ptRoom)
{
	if (!ptRoom)
	{
		log_message("CheckIfTownID Func: ptTownsTXT == %d", ptRoom);
		return FALSE;
	}

	DWORD LevelID = D2GetLevelIDFromRoom(ptRoom);
	BYTE Act = D2GetActfromLevelID(LevelID);

	DWORD TownID = GetTownIDFromAct(Act);

	if (LevelID == TownID)
		return TRUE;

	return FALSE;
}