#ifndef __TOWN_HPP__INCLUDED__
#define __TOWN_HPP__INCLUDED__

DWORD STDCALL GetTownIDFromAct(BYTE Act);
BOOL STDCALL CheckIfTownID(DRLGRoom* ptRoom);

#endif