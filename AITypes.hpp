#ifndef __AITYPES_HPP__INCLUDED__
#define __AITYPES_HPP__INCLUDED__

typedef void (FASTCALL * AITYPESFN) ();

struct AITypesFN
{
    AITYPESFN ptAITypesFN;
};

extern const AITypesFN gpfAITypesFN[];
extern const DWORD gdwAITypesFN;

#endif