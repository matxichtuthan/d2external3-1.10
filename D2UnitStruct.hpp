/*=================================================================*/
/** @file D2UnitStruct.h
 *  @brief Diablo II Unit Structures definitions.
 *
 *  (c) January 2002 - Joel Falcou for The Phrozen Keep.
 */
/*=================================================================*/

#ifndef __D2UNITSTRUCT_H__INCLUDED__
#define __D2UNITSTRUCT_H__INCLUDED__

#define PETFLAG_ALIVE 0x00000001
#include "d2wrapper.hpp"

/*=================================================================*/
/*    Preset Unit Structures (used by DrlgRoom2).                  */
/*=================================================================*/

#pragma pack(1)
struct D2CoordStrc
{
	int nXpos;
	int nYpos;
};
#pragma pack()

#pragma pack(1)
struct PresetUnit 
{                                   //Offset from Code
};
#pragma pack()

#pragma pack(1)
struct D2SkillSeq
{
};
#pragma pack()

#pragma pack(1)
struct D2AnimData
{
};
#pragma pack()

#pragma pack(1)
struct D2GfxInfo
{
}; 
#pragma pack()

#pragma pack(1)
struct D2HoverText
{
};
#pragma pack()

#pragma pack(1)
struct D2Combat
{
    D2Game*			ptGame;				// +00
    int				nAttackerType;		// +04
    DWORD			dwAttackerGUID;		// +08
    int				nDefenderType;		// +0C
    DWORD			dwDefenderGUID;		// +10
	DWORD HitFlags;						// +14
	union
	{

		struct
		{
			BYTE fResultFlags1;	// +18
			BYTE fResultFlags2;	// +19
		};
		WORD ResultFlags;	// +18
	};	
	WORD fExtra;			// +1A
	DWORD PhysicalDamage;	// +1C
	DWORD DamagePercent;	// +20
	DWORD FireDamage;		// +24
	DWORD BurnDamage;		// +28
	DWORD BurnLength;		// +2C
	DWORD LightningDamage;	// +30
	DWORD MagicDamage;		// +34
	DWORD ColdDamage;		// +38
	DWORD PoisonDamage;		// +3C
	DWORD PoisonLength;		// +40
	DWORD ColdLength;		// +44
	DWORD FreezeLength;		// +48 
	DWORD LifeSteal;		// +4C
	DWORD ManaSteal;		// +50
	DWORD StaminaSteal;		// +54
	DWORD StunLength;		// +58
	DWORD AbsorbHeal;		// +5C
	DWORD FinalDamage;		// +60
	DWORD uk4;				// +64
	DWORD PiercePercent;	// +68
	DWORD DamageRatio;		// +6C Field from Missiles.txt, 1024ths
	BYTE  uk5[4];			// +70
	DWORD HitClass;			// +74
	bool  bSetHitClass;		// +78
	char  nConvType;        // +79
    BYTE  _66[2];           // +7A
    int   nConvPct;         // +7C
    BYTE  _6C[4];           // +80
    D2Combat* ptNext;		// +84
};
#pragma pack()


#pragma pack(1)
struct PacketList
{
};
#pragma pack()

#pragma pack(1)
struct D2Event
{
};
#pragma pack()

#pragma pack(1)
struct UnitMsg	//sizeof 0x14
{
	UnitMsg*	pNext;			//+00
	int nMsgType;				//+04
	union
	{
		struct
		{
			DWORD dwGUID;				//+08
			int nStat;					//+0C
			DWORD dwValue;				//+10
		}pStatUpdate;				//+08
		struct
		{
			int nType;					//+08
			DWORD dwGUID;				//+0C
			int nPercent;				//+10
		}pHealthUpdate;				//+08
		struct						 
		{
			WORD wMonster;				//+08	
		}pGFXLoad;					//+08
	};
};
#pragma pack()


#pragma pack(1)
struct CmdCurrent
{
	DWORD uk1;			// +00
	DWORD uk2;			// +04
	DWORD uk3;			// +08
	DWORD nUnitType;	// +0c
	DWORD nUniqueID;	// +10
};
#pragma pack()

struct D2AnimMode
{
};

struct D2QuestChain
{
};

struct D2TimeoutInfoEx
{
};

struct D2ItemEventStrc							//sizeof 0x20
{
    short                 nEvent;			// +00
    WORD                  fFlags;			// +02
    int                   nType;            // +04
	DWORD				  dwGUID;			// +08 - generic arg1, can also be state id
    DWORD                 dwStat;           // +0C - generic arg2, can also be skill id or another encoded arg
    DWORD				  dwArg;            // +10 - number three of the above ^
    DWORD	              pfFunc;           // +14 ITEMEVENT
    D2ItemEventStrc*		  pPrev;            // +18
    D2ItemEventStrc*		  pNext;            // +1C
};

struct D2ClientItemEvent					//sizeof 0x20
{
    short					nEvent;			  // +00
    WORD					fFlags;			  // +02
    int						nType;            // +04
	DWORD					dwArg;			  // +08
    int						nSkill;           // +0C
    int						nLevel;           // +10
    DWORD					pfFunc;           // +14 CLTITEMEVENT
    D2ClientItemEvent*		pPrev;            // +18
    D2ClientItemEvent*		pNext;            // +1C
};

//#pragma pack(1)
struct Unit							// size of 0xF4
{	
									//Offset from Code.
	DWORD		nUnitType;			//+00 Unit Type
	DWORD		nUnitId;			//+04 Unit ID
    union 
    {
        D2PoolManager*  pMemPool;   //+08 
        DWORD			NameRelated;
    };
    DWORD		nUnitUnid;			//+0C Unique ID
	DWORD		nAnimMode;			//+10 Current animation mode
    union 
    {
		D2PlayerData*     pPlayerData;
		D2MonsterData*    pMonsterData;
		D2ObjectData*     pObjectData;
		D2MissileData*	  pMissileData;
		D2ItemData*       pItemData;
		D2TileData*       pTileData;
	};				                //+14 ptUnitData
    DWORD		nAct;		        //+18 Act
	DRLGAct*	ptAct;				//+1C ptAct
	DWORD		LoSeed;				//+20
	DWORD		HiSeed;				//+24
	DWORD		StartLoSeed;		//+28
	// Path*		hPath;				//+2C
	union
    {
        D2Path*				hPath;             
        D2StaticPath*		pStaticPath;
    }; // +2C
	D2SkillSeq*	pSequence;			//+30
    DWORD		nSeqFrameCount;     //+34 number of frames in the sequence ( 256ths )
	DWORD       nSeqCurrentFrame;   //+38
	union
	{
		int         nAnimRate32;        //+3C
		D2AnimMode*	pAnimInfo;			//+3C
	};
	DWORD		eSeqMode;           //+40 the unit mode for the current sequence frame
    int			nFrame;             // 44 current animation frame ( 256ths )
    int			nFrameCount;        // 48 total number of frames in the current animation ( 256ths )
    WORD		nAnimRate;		    // 4C anim speed for the current animation
    BYTE		bAction;            // 4E last trigger frame event that occurred
    BYTE		_4F;                // 4F
	D2AnimData* nAnimData;			//+50 AnimData.D2 record address for current mode
    D2GfxInfo*  hGfx;               // 54 unit gfx structure controlling things like z-offset, COF cache etc
    DWORD		nHitOutLineFrame;   // 58 how many frames will the game draw an outline around the unit ( Direct3D / Glide only )
	union
	{
		D2StatsListEx* ptStatsList;		//+5C ptStatsList
		D2StatsList*   ptD2StatsList;		//+5C ptStatsList
	};
	D2Inventory* ptInventory;		//+60 ptInventory
    union
    {
        D2LightMapStrc*     pLightMap;
        DWORD               dwInteractGUID;
		DWORD				dwSpawnFrame;
    }; //+64
    union
    {
        int                 nLightRadius;
        int                 nInteractType;
		int					nSpawnCounter;
    };                                          //+68
    union
    {
        bool                bInteracting;   
        char                nPL2Shift; 
    };                                          //+6C
    BYTE                    _6D;                //+6D 
    short                   fUpdateType;        //+6E
	union
	{
		Unit*				pUpdateUnit;        //+70
		bool				bDelayedSFX;		
	};
	union
	{
		D2QuestChain*       pQuestRecord;       //+74
		int					nSFXMode;			
	};
	union
	{
		DWORD                   bSparkyChest;       //+78 BOOL                    bSparkyChest
		void*					pSFX;			
	};
	union
	{
		D2TimeoutInfoEx*		pTimeInfo;          //+7C
		DWORD					dwSFXTicks;
	};
    union
    {
        D2Game*					ptGame;             //+80 
		DWORD					dwAsyncSFXTicks;	
        void*                   pSoundSync;
    };
    DWORD                   dwSFXStepsTicks;    //+84
    BOOL					bSound;				//+88
	WORD					nClickXpos;			//+8C
	WORD					nClickYpos;			//+8E
	union
	{
		D2ItemEventStrc*    pItemEvents;        //+90 stores events on unit
		D2ClientItemEvent*  pClientItemEvents;	//+90
	};
	DWORD		PrevChainUnitType;	//+94 Used when launching missiles
	DWORD		PrevChainUnitUnid;	//+98 Used when launching missiles
    int			eKillerType;        // 9C
	DWORD		nKillerId;          // A0 type and guid of the unit that killed or attacked this unit
                                    // needs UNITFLAGEX_GOTKILLED to be queried
    D2HoverText* pHover;            // A4 hover text buffer to send to client, needs UNITFLAG_HOVERMSG
	D2SkillData* pSkillData;		//+A8 ptInfo
	D2Combat*	pCombat;            // AC stored damage set usually by SkillSt and dispatched by SkillDo
    DWORD		dwHitClass;         // B0 hitclass of the last attack that hit this unit
	DWORD		unknown_37;			//+B4
	DWORD		nDropItemCode;		//+B8
	DWORD		unknown_39;			//+BC
	DWORD		unknown_40;			//+C0
	DWORD		nFlags;				//+C4 Flags
	DWORD		nLODFlag;			//+C8 LODFlag
	DWORD		unknown_41;			//+CC
    DWORD		nTargetId;          //+D0 index in the AiTarget list stored by pGame and used
                                    // by monsters to pick targets. old: Client# (0B at start) still vallid?
	DWORD		dwClientTicks;		//+D4 Used with doors (some kind of randomization), GetTickCount() 
    union
    {
         DWORD dwServerTicks;            // time stamp used solely by doors server-side
		 void*  pParticleStream;
         PacketList* hClientPacket;         // not really utilized client-side packet dispatch list
                                    // server-side this is in pUnit.pPlayerData.pClient.pPacketList

    };                              // D8
    D2Event*	pServerEvent;               // DC server-side time based events set on this unit
    Unit*		pNextUnitToUpdate;          // E0 next unit in the update list linked in this room
    Unit*		pNextUnitById;              // E4 next unit in the matching unit list stored by pGame
    Unit*		pNextUnitInRoom;            // E8 next unit inside this room
    UnitMsg*	pMessage;                  // EC first unit message to dispatch ( circular list, beware )
    UnitMsg*	pLastMessage;               // F0 last unit message to dispatch ( circular list, beware )
	#if (__UNITCORE_EXTRA__)
		DWORD	fExtraFlags;				// +F4
		DWORD	dwConjureTimer;				// +F8
		DWORD	dwConjureDecay;				// +FC
		DWORD	dwLichboneID;				// +100
		DWORD	dwLichboneLvl;				// +104
		DWORD	dwLichboneExp;				// +108
		DWORD	dwLichboneHealTimer;		// +10C
		DWORD	dwLichboneHealDecay;		// +110
		int		dwLichboneHealchance;		// +114
		DWORD	dwLichboneHealtreshold;		// +118
		DWORD	dwLichboneHeallthreshold;	// +11C
		DWORD	dwLichboneFlag;				// +120
		DWORD	dwConjuredFlag;				// +124
	#endif

	// DWORD		unknown_50;			//+F4
};
//#pragma pack()


#pragma pack(1)
struct AIStruct
{
/*+00 = 0 on spawn, g�ti breyst seinna 
+04 = pfnFunction, ptr, see ai table.txt at +08 (if 0 and +00 = 0, then 6FCD3AD0, then 6FCCA0D0) 
+0c = -1 on spawn 
+10 = 1 on spawn 
+14 = 0 on spawn 
+18 = 0 on spawn 
+1c = 0 on spawn 
+28 = if minion, then ptGame 
+2c = if minion, then owner id 
+30 = if minion, then owner type */
};
#pragma pack()

#pragma pack(1)
struct Pets
{
	DWORD			fFlags;				//+00
	DWORD			dwGUID;				//+04
	DWORD			dwSeed;				//+08
	DWORD			dwName;				//+0C
	int				nType;				//+10
	Pets*			ptNext;				//+14
};
#pragma pack()

#pragma pack(1)
struct PetList
{
	Pets*			ptPets;				//+00
	int				nPets;				//+04
	int				nMax;				//+08
};	
#pragma pack()

#pragma pack(1)
struct PlayerPets
{
	PetList*		ptPetList;			//+00
};
#pragma pack()

struct D2WaypointStrc		//sizeof 0x18
{
    WORD fFlags[12];
};

#pragma pack(1)
struct D2PlayerData 
{
	char	CharName[0x10];	//+00	Player Name
	//LPVOID	ptQuest[3];		//+10	Quest Pointers for each difficulty
	D2QuestFlagStrc*  ptQuestData[3];  //+10 pQuestData
	//D2QuestFlagStrc* ptQuest[3];		//+10	Quest Pointers for each difficulty
	LPVOID  ptHistory[3];	//+1C   History Pointer for waypoints for each difficulty
	BYTE	unknown1[0xC];  //+28
	void*	ptArenaUnit;	//+34	ptArena for the Unit
	BYTE	unknown[0x4];	//+38
	WORD    MPSourcePortalUniqueID; // +3C Source Portal Unique_ID
	WORD	unknown4;		//+3E
	WORD	MPDestPortalUniqueID; // +40 Destination Portal Unique_ID
	BYTE	unknown3[0x02];	//+42 
	PlayerPets*	ptPlayerPets; //+44 Pet Type Chain: PlayerData->ptPlayerPets->ptPetList[nPetType].ptPets
	BYTE	ptObjectUnID;	//+48	Object UniqueID for TownPortals	
	BYTE	unknown2[0x53]; //+49
	Client*	ptClient;		//+9C	ptClient

};
#pragma pack()


#pragma pack(1)
struct D2TileData
{
    int             nWarpID;        //+00
    int             nXSelect;       //+04
    int             nYSelect;       //+08
    int             nXSelect2;      //+0C
    int             nYSelect2;      //+10
    int             nXExitWalk;     //+14
    int             nYExitWalk;     //+18
    int             nXOffset;       //+1C
    int             nYOffset;       //+20
    int             nLitVersion;    //+24
    int             nTiles;         //+28
    int             nDirection;     //+2C   b/l/r
};
#pragma pack()

#pragma pack(1)
struct AiPath
{
};
#pragma pack()

#pragma pack(1)
struct NpcInteract
{
};
#pragma pack()

#pragma pack(1)
struct RMCoordList
{
};
#pragma pack()

#pragma pack(1)
struct D2MonsterData
{
   D2MonstatsTXT* pRecord;      // +00

   union
   {
      struct
      {
         BYTE HD;            // +04
         BYTE TR;            // +05
         BYTE LG;            // +06
         BYTE RA;            // +07
         BYTE LA;            // +08
         BYTE RH;            // +09
         BYTE LH;            // +0A
         BYTE SH;            // +0B
         BYTE S1;            // +0C
         BYTE S2;            // +0D
         BYTE S3;            // +0E
         BYTE S4;            // +0F
         BYTE S5;            // +10
         BYTE S6;            // +11
         BYTE S7;            // +12
         BYTE S8;            // +13
      };

      BYTE composit[16];       // +04
   };

   WORD   NameSeed;            // +14
   BYTE   MonFlags;            // +16
   BYTE   HitFlag;             // +17 - flags checked when the unit gets hit
   DWORD  dwDuriel;            // +18
   BYTE   MonUMods[9];         // +1C
   BYTE   _0x25;               // +25
   short   eSuperUniqueId;     // +26

   union
   {
      UnitAi* pAi;				// +28 - pAiGeneral
      WORD wCltInit;
   };

   union
   {
      AiPath* pAiPath;         // +2C - pMonMode // on client this holds the name
      wchar_t* wszCltName;
   };
   
   NpcInteract* ptNpcInter;      // +30 - used for something else by client-critters
   int      ai_type_other;         // +34 - see AiTarget.cpp
   int      ai_type;            // +38
   DWORD   nOwnerId;            // +3C - set on client
   int      nNecroPetVar;         // +40 - set to 100 for necropets

   DWORD   _0x44;               // +44
   DWORD   _0x48;               // +48
   DWORD   _0x4C;               // +4c
   RMCoordList* pSight;         // +50
   int      eLastMode;            // +54   // actually mon mode!
   DWORD   MonRegIdx;            // +58
   DWORD   SummonerFlags;         // +5C
};
#pragma pack()

// #pragma pack(1)
struct D2ObjectData 
{								// Offset from Code
	D2ObjectsTXT* ptObjTXT;		// +00
    union 
    {
        DWORD       fDWORD;
		struct
		{
			WORD		  fFlags;
			short		  nTrap;
		};
        BYTE          fChestFlags;		// the high bit says its locked, the other 7 bits are trap id
        struct
        {
            WORD          nPortalLevel; //used to be BYTE
            BYTE          fPortalFlags; // 3 is link, 5 is in use
        };
        short         nShrineType;
    };                                  // +04
    D2ShrinesTXT*	  ptShrineRecord;   // +08
    DWORD             dwOpGUID;         // +0C
    BOOL              bPermanent;       // +10 - coords???
    DWORD             nUnk;             // +14
	D2CoordStrc		  pDestRoom;		// +18
	D2CoordStrc		  pDestPortal;		// +20
    char              szPerant[16];     // +28

	// BYTE value;					// +04
	// +4 BYTE DestLevel on objects
	// ptPortal +94 object type
	// ptPortal +98 Dest Object UniqueID

};


//#pragma pack()

#pragma pack(1)
struct D2ItemData 
{							// Offset from Code

	DWORD	quality;			// +00
	DWORD	lowSeed;			// +04
	DWORD	highSeed;			// +08
	DWORD	PlayerID;			// +0c #10734 / #10735 (ptItem->ptInventory->ptPlayer->0C) 
	DWORD	startLowSeed;		// +10
	DWORD	cmdFlags;			// +14
	DWORD	flags;				// +18
	DWORD	GUID1;				// +1C Global Unique ID 1
	DWORD	GUID2;				// +20 Global Unique ID 2
	DWORD	GUID3;				// +24 Global Unique ID 3
	DWORD	OtherID;			// +28 MonsterID, set/hi/lo/uni, player class on ear
	DWORD	iLvl;				// +2C
	WORD	wVersion;			// +30
	WORD	wRName1;			// +32
	WORD	wRName2;			// +34
	WORD	wAutoPref;			// +36

	union
	{
		struct
		{
			WORD	wPref;		// +38 magPrefix,Runeword!!,... 
			WORD	wUnk3A;		// +3A
			WORD	wUnk3B;		// +3C
		};
		WORD	rPrefix[3];		// +38 affixes on rares
	};

	union
	{
		struct
		{
			WORD	wSuff;		// +3E
			WORD	wUnk4A;		// +40
			WORD	wUnk4B;		// +42
		};
		WORD	rSuffix[3];		// +3E
	};

	BYTE EquipPos;				// +44 01 = head, 02 = neck, 03 = tors, 04 = rarm, 05 = larm, 06 = lrin, 07 = rrin, 08 = belt ,09 = feet, 0a = glov, 0b = ralt, 0c = lalt, 00 = grid (none)
	BYTE StorePage;				// +45 0 = equip/belt 1 = inventory 2 = ? 3 = ? 4 = cube 5 = stash 6 = ?  7 = ?
	BYTE unk46;					// +46 set at 10722, called from d2game
	BYTE unk47;					// +47 set at 10723, called from ""
	BYTE pLvl;					// +48 level of ear
	BYTE varGfx;				// +49
	BYTE IName[18];				// +4A -> 5B   inscribed/ear name
	D2Inventory*   ptInventory;	// +5C ptInventory
	Unit *prev;					// +60 prev item unit
	Unit *next;					// +64 next item unit
	DWORD unk68;				// +68
	Unit *unk6C;				// +6C same as prev ?
	DWORD *unk70[4];			// +70
};
#pragma pack()

#pragma pack(1)
struct MissilesData
{
/*
+04 flags 
+08 0 on start 
+0c -1 on start 
+10 Range
+20 nDirection 
+28 Vel/MaxVel/VelLev/ or LevRange
+2c range? 
+30 level 
+34 number of pierces (byte) 
+35 (byte) 
+36 (byte) 
+37 (byte) 
+38 range-activate */
};
#pragma pack()

struct D2EventStrc
{
	BYTE nType;				// +00 - type of event
	BYTE nUnused;			// +01
	WORD fFlags;			// +02 - flags
	DWORD dwFrame;			// +04 - frame to trigger on
	Unit* pUnit;			// +08 - base of chain unit
	DWORD dwGUID;			// +0C
	int nUnitType;			// +10 - type of unit
	DWORD dwArg;			// +14
	DWORD dwArgEx;			// +18
	D2EventStrc* pNext;		// +1C - next event in node
	D2EventStrc* pPrev;		// +20 - prev event in node
	D2EventStrc* pUnitNext;	// +24 - next event on unit
	D2EventStrc* pUnitPrev;	// +28 - prev event on unit
	//EVENTCALLBACK pfFunc;	// +2C
	DWORD pfFunc;			// +2C
};

struct D2EventArrayStrc			// sizeof = 0x7088
{
	D2EventStrc pEvents[600];	// +0x0000
	D2EventStrc* pLast;			// +0x7080
	D2EventArrayStrc* pNext;	// +0x7084
};

struct D2EventControlStrc				// sizeof = 0xA20
{
	DWORD nList;						// +0x000 - current list
	D2EventStrc* pStart[5][64];			// +0x004 - start nodes for frame % 64
	D2EventStrc* pEnd[5][64];			// +0x504 - end nodes for frame % 64
	D2EventStrc* pEmpty[5];				// +0xA04 - linked list for events per unit type?
	D2EventStrc* pCurrent;				// +0xA18 - next list to dispatch?
	D2EventArrayStrc* pEventCache;		// +0xA1C - base of structure
};

struct D2InactiveControlStrc
{
};

struct D2NPCMessageStrc						// sizeof 0x0C
{
    int nNPC;								// +00
    short nStringIndex;						// +04
	short nPad;								// +06
    BOOL nMenu;								// +08
};

struct D2NPCMessageTableStrc						//sizeof 0xC4
{
    D2NPCMessageStrc pMessages[0x10];			//+00
    int nMessages;							//+C0
};

struct D2ScrollTextStrc         // sizeof = 0xC
{
   short nMessage;				// +0
   WORD	wUnused;				// +2
   BYTE nMenu;					// +4
   BYTE nPad[3];				// +5
   D2ScrollTextStrc* pNext;		// +8
};

struct D2ScrollTextControlStrc   // sizeof = 0xC
{
   D2PoolManager* pMemPool;		// +0
   BYTE nMessages;				// +4 - bMenu
   BYTE nPad[3];                // +5
   D2ScrollTextStrc* pFirst;    // +8 - max of 8 nodes
};


struct D2UnitNodeStrc
{
};

struct D2NPCDataStrc
{
};

struct D2ArenaStrc
{
};

struct D2PartyPlayerStrc
{
	DWORD dwGUID;
	D2PartyPlayerStrc* pNext;
};

struct D2PartyDataExStrc
{
	WORD wParty;
	WORD wUnused;
	D2PartyPlayerStrc* pPlayers;
	D2PartyDataExStrc* pPrev;
};

struct D2PartyStrc
{
	int nParties;
	D2PartyDataExStrc* pParties;
};

struct D2PartyListStrc         
{
   DWORD                     dwGUID;            // +00
   union
   {
	   struct 
	   {
		   DWORD          fLoot:1;
		   DWORD          fNoHear:1;
		   DWORD          fSquelched:1;
		   DWORD          fHostile:1;
	   }fStatusFlags;                               // +04
	   DWORD fFlags;
   };
   DWORD                     nHostileTimestamp; // +08
   union
   {
	   struct
	   {
		   DWORD          fInParty:1;
		   DWORD          fInvitedYou:1;
		   DWORD          fYouInvited:1;
	   }fPartyStatus;                               // +0C
	   DWORD fStatus;
   };
   D2PartyListStrc*          pNext;             // +10
};


struct D2MonRegInfoStrc // size 0x34
{
    WORD nMonster;      // +000
    BYTE nRarity;       // +002
    BYTE _003;          // +003
    DWORD _004;         // +004
    DWORD _008;         // +008
    DWORD _00C;         // +00C
    DWORD _010;         // +010
    DWORD _014;         // +014
    DWORD _018;         // +018
    DWORD _01C;         // +01C
    DWORD _020;         // +020
    DWORD _024;         // +024
    DWORD _028;         // +028
    DWORD _02C;         // +02C
    DWORD _030;         // +030
};

struct D2MonRegStrc             // size 0x2E4
{
    BYTE nAct;					// +000
    BYTE _001;					// +001
    BYTE _002;					// +002
    BYTE _003;					// +003
    DWORD nPopulatedCount;		// +004 - incremented by some lookup func (D2Game.6FC8EC40)
    DWORD _008;					// +008
    DWORD nRoomCount;			// +00C - value returned by D2Common.#10479, number of empty rooms
    BYTE nMonCountEx;			// +010
    BYTE nTotalRarity;			// +011
    BYTE nInfoCount;			// +012
    BYTE _0x13;					// +013
    D2MonRegInfoStrc pInfo[13];	// +014
    DWORD nMonDen;				// +2B8
    BYTE nMonUMin;				// +2BC
    BYTE nMonUMax;				// +2BD
    BYTE nMonWndr;				// +2BE
    BYTE _2BF;					// +2BF
    DWORD nLevel;				// +2C0
    DWORD _2C4;					// +2C4
    DWORD nBossCount;			// +2C8 - how many uniques were spawned
    DWORD nMonCount;			// +2CC
    DWORD nKillCount;			// +2D0
    DWORD _2D4;					// +2D4 -1
    bool bQuest;				// +2D8
    BYTE _2D9;					// +2D9
    BYTE _2DA;					// +2DA
    BYTE _2DB;					// +2DB
    DWORD nMonLvl1;				// +2DC
    DWORD nMonLvl2;				// +2E0
};

struct D2ObjectRegionStrc 
{
	union
	{
		struct
		{
			DWORD                dwLoSeed;    // +00
			DWORD                dwHiSeed;    // +04
		};

		D2Seed		 pSeed;		  // +00
	};
    int                  nTypes[5];   // +08
   /*
         type 0 unknown - 1 record [none]
         type 1 magic shrines - 7 records [16, 17, 18, 19, 20, 21, 22] [16 is never picked]
         type 2 health boost / exchange - 2 records [2, 4] = 4 unused
         type 3 mana boost / exchange - 2 records [3, 5] = 5 unused
         type 4 booster shrines - 11 records  [1,6,7,8,9,10,11,12,13,14,15]
   */
    void*                pTypeList[5];// +20
   /*
         this is a list of pointers to a list of each type
         below this point is a huge list of pointers to data,
         one for each of D2s 1024 levels, in addition to other
         stuff, that I didn't really bother to examine.
   */
};

#pragma pack(1)
struct D2Game
{
	// BYTE	unknown_0[0x18];
	DWORD					_00[6];				// +00 +04 +08 +0C +10 +14
	//+00 is some sort of table - probably pGameTbl, passed from D2Client or D2Server...
	//+04 is some sort of seed - seed from the D2Client Game Marshal iirc
	//+08 to +14 is some stuff inited by the module(D2Client, D2Server) Marshalling the Game(s)
	LPCRITICAL_SECTION		pSyncLock;			// +18
	// DWORD	_ptLock	;						// +18 Unknown
	D2PoolManager* MemoryPool;					// +1C Memory Pool (??)
	// BYTE	unknown_1[0x0C];		
	int						nClientRelated;		// +20
	DWORD					dwUnk;				// +24
	WORD					wGameNumber;		// +28
	// DWORD	UniqueID;				// +2C
	// DWORD	UnitType;				// +30
	char                    szGameName[14];     // +2A 
    char                    szGameServerIp[16];	// +38
    char                    szAccountName[16];	// +48
    char                    szRealmName[18];	// +58 
    BYTE                    nGameType;          // +6A
    WORD                    _6B;	            // +6B
	// BYTE	unknown_18[0x39];
	BYTE					Difficulty;			// +6D Difficulty 0,1,2
	WORD					unknown_2;			// +6E Cube puts 4 here
	DWORD					LODFlage;			// +70 0 = Normal, 1 = LOD
	//DWORD	unknown_3;				// +74 0 at start
    DWORD					nGameMode;			// +74 0 at start-> ladder, normal etc...
	//DWORD	unknown_4;				// +78
	WORD                    nItemFormat;        // +78 
	WORD                    _7A;				// +7A
	//DWORD	unknown_5;				// +7C
	//DWORD	unknown_6;				// +80
	//DWORD	unknown_7;				// +84
    D2Seed		            pInitSeed;          // +7C - LoSeed set when GameSeed is created, HiSeed set when ObjectRegion is created
    BOOL                    bInitSeed;          // +84
	Client*					ptClient;			// +88 ptClientLastJoined
	//DWORD	unknown_19;				// +8C Unknown
	int                     nClientCount;       // +8C
	//int						PlayersSpawnedCounter;	// +90 # of players spawned counter
	int						nSpawnCount[6];
	//int                     nPlayerCount;       // +90 "0"
	//DWORD	MonstersSpawnedCounter;	// +94 # of monsters spawned counter 
	//DWORD   ObjectsSpawnedCounter;	// +98 # of objects spawned counter
	//DWORD   MissilesSpawnedCounter;	// +9C # of missiles spawned counter
	//DWORD   ItemsSpawnedCounter;	// +A0 # of items spawned counter 
	//DWORD   TilesSpawnedCounter;	// +A4 # of tiles spawned counter 
    //int                     nMonsterCount;      // +94 "1"
    //int                     nObjectCount;       // +98 "2"
    //int                     nMissileCount;      // +9C "3"
    //int                     nItemCount;         // +A0 "4"
    //int                     nWarpTileCount;     // +A4 "5"
	DWORD					CurrentFrameCounter;	// +A8 Current Frame (FrameCounter)
//	DWORD	unknown_9;				// +AC
//	DWORD	unknown_10;				// +B0
//	DWORD	unknown_11;				// +B4
    DWORD                   dwCycleLastSecond;  // +AC
    DWORD                   dwCycleCount;       // +B0
    DWORD                   dwTickCount;        // +B4
//	DWORD	unknown_12;				// +B8
    D2EventControlStrc*     pEventControl;      // +B8
//	DWORD	unknown_13;				// +BC
	DRLGAct*                pAct[5];            // +BC
//	DWORD	unknown_14;				// +C0
//	DWORD	unknown_15;				// +C4
//	DWORD	unknown_16;				// +C8
//	DWORD	unknown_17;				// +CC is used in spawn dead bodies code
	D2Seed					pGameSeed;			// +D0
	D2InactiveControlStrc*  pInactive[5];		// +D8
//	void*	ptArena;			// +1D28 ptArena
	DWORD                   dwMonSeed;          // +EC
	D2MonRegStrc*           pMonRegion[1024];   // +F0
	D2ObjectRegionStrc*     pObjectRegion;      // +10F0
	D2QuestControlStrc*     pQuestControl;      // +10F4
	D2UnitNodeStrc*         pNodeList[10];      // +10F8
    union
    {
        Unit*				pUnitList[5][128];  // +1120
        struct
        {
            Unit*           pPlayerList[128];   // +1120
            Unit*           pMonsterList[128];  // +1320
            Unit*           pObjectList[128];   // +1520
            Unit*           pItemList[128];     // +1720
            Unit*           pMissileList[128];  // +1920
        };
    };
	Unit*					pWarpTileList;      // +1B20
	DWORD                   fUniqueSpawn[128];  // +1B24 
	D2NPCDataStrc*          pVendorControl;     // +1D24
	D2ArenaStrc*            pArenaControl;      // +1D28
	D2PartyStrc*            pPartyControl;      // +1D2C
	DWORD                   fBossSpawn[64];     // +1D30
    DWORD                   nPathCount[17];		// +1D70
    DWORD                   nPathCountTotal;    // +1DB4
    BOOL					bUpdateStatsTick;	// +1DB8 - this is mainly for the D2GS
	DWORD					dwStatsTick;		// +1DBC
	DWORD					dwLastUpdateTick;	// +1DC0
	DWORD					dwThreadPriority;	// +1DC4 - this is mainly for the D2GS
    DWORD                   dwUpdateTick;		// +1DC8
    DWORD                   dwUnknown;			// +1DCC - this is always zero, its for D2GS anyways
	DWORD					dwServerGame;		// +1DD0
	D2Game*					pPrev;				// +1DD4
	D2Game*					pNext;				// +1DD8	
	DWORD					dwException;		// +1DDC - has for exceptions 1 = NULL deref, 2 = 0xC0000001 deref, 3 = Assert with blank Msg, 4 = Alloc 1.7Gb of Mem(lol)
	DWORD					_1DE0[2];			// +1DE0 - these are unused, atleast in D2Game, haven't check D2Server, but v1.00 ain't reliable

};
#pragma pack()

#pragma pack(1)
struct Client
{
	DWORD	ClientNbr;			// +000 Client #
	DWORD	ConnectionState;	// +004 Connection State (4 = connected)
	DWORD	unknown_1;			// +008
	BYTE	unknown_2;			// +00C
	char	CharName[0x10];		// +00D PlayerName
	BYTE	unknown_3[0x157];	// +01D
	Unit*	ptPlayer;			// +174 ptPlayer this Client Belongs To
	DWORD   dwSave;             // +178
    BYTE*   pSaveGame;          // +17C
    size_t  nSaveOpenSize;		// +180
    size_t  nSaveCurrentSize;	// +184
    size_t  nFileSize;			// +188
    void*   pSavePacketList;    // +18C
    DWORD   _190[2];            // +190
	int		nAttachedUnitType;	// +198
	DWORD	dwAttachedUnitGUID;	// +19C
    DWORD   _1A0[2];            // +1A0
    D2Game* ptGame;             // +1A8
};
#pragma pack()

/*
struct D2ClientStrc										// sizeof 0x510
{
    DWORD                   nClient;					// +000
    int                     nClientStatus;              // +004
    BYTE                    nClass;						// +008
    BYTE                    _009;                       // +009
	union
	{
		WORD                    fFileFlags;                 // +00A
		short					nLevelExpLayer;				// +00A
	};
	BYTE                    nCharTemplate;              // +00C
    char                    szCharName[16];             // +00D
    char                    szAccName[64];				// +01D
	BYTE					nPad[3];					// +05D
    D2UnitStrc*             pClientContainer;           // +060
    BYTE                    _01D[8];                    // +064
	DWORD					dwRealm;					// +068
    DWORD                   nStartSkill;                // +070
	DWORD					_074[2];					// +074
	DWORD					dwMapSeed;					// +07C
    BYTE                    _080[236];                  // +080
    int                     nPlayerType;                // +16C
    DWORD                   dwPlayerGUID;               // +170
    D2UnitStrc*             pPlayer;                    // +174
    DWORD                   dwSave;                     // +178
    BYTE*                   pSaveGame;                  // +17C
    size_t                  nSaveOpenSize;				// +180
    size_t                  nSaveCurrentSize;			// +184
    size_t                  nFileSize;					// +188
    D2PacketListStrc*       pSavePacketList;            // +18C
    DWORD                    _190[2];                   // +190
	int						nAttachedUnitType;			// +198
	DWORD					dwAttachedUnitGUID;			// +19C
    DWORD                    _1A0[2];                   // +1A0
    D2GameStrc*             pGame;                      // +1A8
    DWORD                   nAct;                       // +1AC 
    DWORD                   dwRoomTicker;               // +1B0
    DRLGRoom*               pRoom;                      // +1B4
    D2PacketListStrc*       pDispatchList;              // +1B8
    D2PacketListStrc*       pPacketList;                // +1BC
    D2PacketListStrc*       pEmpty;                     // +1C0
    DWORD                   _1C4[16];                   // +1C4 
    D2PacketListStrc*       pDispatchListEx;            // +204
	DWORD					_208[115];					// +208
    DWORD					fClientFlags;               // +3D4
    DWORD                   dwTimeOut;                  // +3D8
    D2ClientHotKeyStrc      pHotKeys[16];               // +3DC
	BOOL					bWeaponSwap;				// +45C
	DWORD					_460[8];					// +460
	BYTE					nSaveField;					// +480
	BYTE					nInit;						// +481
	BYTE					nInitEx;					// +482
	BYTE					_483;						// +483
	DWORD					dwGolemGUID;				// +484
	DWORD					dwCreateTime;				// +488
	DWORD					_48C[7];					// +48C 
    D2ClientStrc*           pNext;						// +4A8
    D2ClientStrc*           pServerNext;				// +4AC
    D2ClientStrc*           pNextInListByName;			// +4B0
    BYTE                    uk4B0[76];					// +4B4
	DWORD					dwHeartbeatTick;			// +500 - if > 0xA you get dc'ed
	DWORD					nExpLoss;					// +504
    DWORD                   nExp;						// +508
    DWORD                   dw50C;						// +50C - set to a var on init
//    DWORD                    dw510;                     // +510 - 1.11b+ stuff??
//    DWORD                    dw514;                     // +514 
};
*/

/* Quest structs */

#pragma pack(1)
struct D2QuestGUIDStrc		//sizeof 0x82
{
	DWORD dwPlayerGUID[32];	//+00
	short nGUIDCount;		//+80
};
#pragma pack()

struct D2QuestFlagStrc
{
};

struct D2QuestDataEx
{
	BYTE uk1[283];
	BYTE uk2;			// +11B

};

struct D2QuestTimerStrc							// sizeof 0x14
{
    DWORD					pfUpdate;           //+00
    D2QuestDataStrc*        pQuest;             //+04
    DWORD                   dwTicks;            //+08
    DWORD                   dwTimeout;          //+0C
    D2QuestTimerStrc*       pNext;              //+10
};

//#pragma pack(1)
struct D2QuestControlStrc
{
    D2QuestDataStrc*    pLastQuest;         //+00
    BOOL                bExecuting;         //+04
    BOOL                bPickedSet;         //+08
    D2QuestFlagStrc*    pQuestFlags;        //+0C
    D2QuestTimerStrc*   pTimer;             //+10
    DWORD               dwTick;             //+14
    D2Seed		        pSeed;              //+18
	BYTE				nTrigger;			//+20
    BYTE				nPad[3];			//+21
};
//#pragma pack()

//#pragma pack(1)
struct D2QuestDataStrc									// sizeof 0xF4
{
    int nQuestChain;									// +00 - internal
    D2Game* pGame;										// +04
    char nAct;											// +08
    bool bInit;											// +09 - bNotIntro, set to false for intro quests
    bool bActive;										// +0A - confirmed
    BYTE fLastState;									// +0B - previous quest state
    BYTE fState;										// +0C - main quest state
    BYTE fInitState;									// +0D - init state
    WORD wUnused;										// +0E 
    int nSequence;										// +10
    union
    {
        DWORD dwFlags;									// +14
        BYTE fFlags;									// +14
    };
    D2QuestDataEx* pQuestDataEx;						// +18 - union of 0x29 structs
	D2QuestGUIDStrc pQuestGUID;                         // +1C
    WORD wUnusedEx;										// +9E 
    QUESTCALLBACK pfCallback[MAX_QUEST_CALLBACKS];		// +A0 0x0F +A0 0x0F MAX_QUEST_CALLBACKS QUESTCALLBACK pfCallback[MAX_QUEST_CALLBACKS]
    D2NPCMessageTableStrc* pNPCMessages;				// +DC
    int nQuest;											// +E0 - index in quest flag bit array
    QUESTSTATUS pfStatusFilter;								// +E4 
    QUESTACTIVE pfActiveFilter;							// +E8
    QUESTSEQ pfSeqFilter;									// +EC 
    D2QuestDataStrc* pPrev;								// +F0
};
//#pragma pack()

//#pragma pack(1)
struct D2QuestArgStrc
{
    D2Game* pGame;							// +00
    int nEvent;                             // +04
	Unit* pTarget;							// +08
    Unit* pPlayer;							// +0C
    DWORD dw10;                             // +10

    union
    {
        struct
        {
            D2ScrollTextControlStrc* pTextControl;      // +14
            DWORD dw18;									// +18
        };

        struct
        {
            short nNPC;						// +14
            WORD w3;                        // +16

            short nMessageIndex;            // +18
            WORD w4;                        // +1A
        };

        struct
        {
            int nOldLevel;                  // +14
            int nNewLevel;                  // +18
        };
    };
};
//#pragma pack()

/*=================================================================*/
/*   Item Location Structure.                                     */
/*=================================================================*/

#pragma pack(1)
struct LocInfo
{                       //Offset from Code
/*
    DWORD noneLoc;      //+00
    DWORD headLoc;      //+04
    DWORD neckLoc;      //+08
    DWORD torsoLoc;     //+0c
    DWORD rightArmLoc;  //+10
    DWORD leftArmLoc;   //+14
    DWORD rightRingLoc; //+18
    DWORD leftRingLoc;  //+1c
    DWORD beltLoc;      //+20
    DWORD feetLoc;      //+24
    DWORD glovesLoc;    //+28
*/
};
#pragma pack()

/*=================================================================*/
/*   Inventory Node Structure.                                     */
/*=================================================================*/

#pragma pack(1)
struct Node
{                   //Offset from Code.
/*
    DWORD null;     //+00
    ItemData* pItem;    //+04
    DWORD unID;     //+08
    DWORD page;     //+0c
    Node* nextNode; //+10
*/
};
#pragma pack()

/*=================================================================*/
/*   Inventory Structure.                                          */
/*=================================================================*/
#pragma pack(1)
struct D2Inventory 
{                               //Offset from Code.
    DWORD		nIdentifier;	//+00 1020304
    DWORD		unknown_1;		//+04 ptGame + 1C
	Unit*		ptUnit;			//+08 ptUnit
	DWORD		unknown_2;		//+0C
	DWORD		unknown_3;		//+10
	DWORD		unknown_4;		//+14
	DWORD		unknown_5;		//+18
	DWORD		unknown_6;		//+1C
	DWORD		unknown_7;		//+20
	DWORD		nUnitUnid;		//+24 Unit unid
	// +28 Socket_Counter
	// +2c ptInventory2
	// +30 ptNode next?
	// +34 ptCorpse
	// +3C next ptCorpse Unique_id
};
#pragma pack()

/*=================================================================*/
/*  Skill Structure.                                               */
/*=================================================================*/
#pragma pack(1)
struct Skills
{
							
    D2SkillsTXT*  skillTxt;	//+00
    Skills* pNextSkill;     //+04
    DWORD   mode;           //+08
    DWORD   fFlags;			//+0C unknown1[3]
	DWORD   _1[2];			//+10
    DWORD   TargetOffsetX;	//+18 
    DWORD   TargetOffsetY;	//+1c 
    DWORD   TargetUnitID;	//+20 
    DWORD   nPar4;			//+24 unknown2
    DWORD   slvl;           //+28
    DWORD   slvlBonus;      //+2c
    DWORD   nQuantity;      //+30 unknown3
    DWORD   dwOwnerGUID;    //+34 - old state -1 = Native Skill
    int     nCharges;       //+38
	DWORD	dwUnk;			//+3C
};


#pragma pack()
/*=================================================================*/
/*  Skills Info Structure.                                         */
/*=================================================================*/
#pragma pack(1)
struct D2SkillData						// renamed D2Info
{										// Offset from Code.
	D2PoolManager*	 pMemPool;;			// +00 ptGame + 1C prev unknown_1
	union
	{
		Skills*			 pFirstSkill;	// +04
		Skills*			 pStartSkill;	// +04
	};
	Skills*			pLeftSkill;			//+08
	Skills*			pRightSkill;		//+0C
	Skills*			pCurrentSkill;		//+10
	D2LightMapStrc* pSkillLight;		//+14
/*		
    DWORD   gameRelated;    //+00
    Skills* ptSkill;        //+04
    Skills* ptLeftSkill;    //+08
    Skills* ptRightSkill;   //+0c
    Skills* ptCurrentSkill; //+10
*/
};
#pragma pack()

/*=================================================================*/
/*   Data Structure for image data								   */
/*=================================================================*/

struct D2GFXCellStrc		   //sizeof 0x2C
{
	union
	{
		DWORD dwOffset;            //+00
		D2GFXCellStrc* pNext;	   //+00
	};
    DWORD nWidth;              //+04
    DWORD nHeight;             //+08
    DWORD nXOffset;            //+0C
    DWORD nYOffset;            //+10
    DWORD _2;				   //+14
    D2GFXCellStrc* pParent;    //+18
    DWORD nLength;             //+1C
    BYTE nUnk[12];			   //+20
};

struct D2CellFileStrc			//sizeof nFileSize + 0x320 = Raw File
{
	DWORD			 nVersion;		//+00
	DWORD			 fCellFlags;	//+04
	int				 nEncodeType;	//+08
	DWORD			 nTerminator;	//+0C
	size_t			 nDirections;	//+10
	size_t			 nFrames;		//+14
    D2GFXCellStrc*	 pFirstFrame;	//+18       &pFirstFrame = pFrameList*
};

struct D2GFXDataStrc			//sizeof 0x48
{
    D2GFXCellStrc* pCellInit;	//+00 
    D2CellFileStrc* pCellFile;	//+04 
    DWORD nFrame;				//+08
    DWORD nDirection;			//+0C
    int nMaxDirections;			//+10
    int nMaxFrames;				//+14
	DWORD fFlags;				//+18
    BYTE fState;				//+1C
	union
	{
		BYTE nComponent;			//+1D
		BYTE fItemFlags;			//+1D
	};
    BYTE bUnk1E;				//+1E - padding no doubt
    BYTE bUnk1F;				//+1F
    int nUnitType;				//+20
    int nUnitIndex;				//+24
    int nMode;					//+28
    int nOverlay;				//+2C
	union
	{
		// [token][component][type][mode][hitclass]
		struct
		{
			DWORD dwToken;				//+30
			DWORD dwComponent;			//+34
			DWORD dwArmorType;			//+38 - lit, med , hvy
			DWORD dwMode;				//+3C
			DWORD dwHitClass;			//+40
		};
		DWORD dwName[5];				//+30
	};
    const char* pName;				//+44
};

#pragma pack(1)
struct D2StatsList
{
	DWORD		nUnitId;			//+00
	Unit*		ptUnit;				//+04
	DWORD		nUnitType;			//+08
	DWORD		nItemNum;			//+0C
	DWORD		Charges;			//+10
	DWORD		id;					//+14
	DWORD		uk18;               //+18  
    BYTE		uk2[0x08];          //+1C  
    Stats*		ptBaseStatsTable;   //+24  
    WORD		nbBaseStats;        //+28  
    WORD		sizeOfBaseStatsTable; //+2A ??  
    D2StatsList*	ptStats;            //+2C  
    BYTE		uk3[0x04];          //+30  
    D2StatsList*	ptItemStats;        //+34  
    BYTE		uk4[0x04];          //+38  
    D2StatsList*	ptAffixStats;       //+3C  
    D2StatsList*	ptNextStats2;       //+40  
    union  
     {  
          Unit*     ptChar;         //+44  
          Unit*     ptItem;  
     };  
    Stats*     ptStatsTable;       //+48  
     WORD     nbStats;              //+4C  
     WORD     sizeOfStatsTable;     //+4E ??  
     BYTE     uk5[0x8];             //+50  
     BYTE*     unknow0;             //+58 (sizeof(*unknow0)=0x30 (calculated)  
     DWORD     unknow1;             //+5C (=0)  
     DWORD     unknow2;             //+60 (=0)  
};
#pragma pack()


/*=================================================================*/
/*  Stat/State Structures                                          */
/*=================================================================*/

#pragma pack(1)
struct D2StatInfo
{
	D2Stats*		ptStats;		//+00
	WORD			nStats;			//+04
	WORD			nStatSize;		//+06
};
#pragma pack()

//#pragma pack(1)
struct StatsList							//sizeof 0x3C
{
	DWORD*			MemoryPool;				//+00
	Unit*			ptUnitAttached;			//+04
	int				nOwnerType;				//+08
	DWORD			dwOwnerGUID;			//+0C
	DWORD			fFlags;					//+10
	int				nState;					//+14
	DWORD			dwTimeout;				//+18
	int				nSkill;					//+1C
	int				nLevel;					//+20
	D2StatInfo		sStatInfo;				//+24 
	D2StatsListEx*	ptPrevLink;				//+2C
	D2StatsListEx*	ptNextLink;				//+30
	D2StatsListEx*	ptParent;				//+34 - Parent statlist. It's pActiveMods points to 'this' Null when ptUnit04 is NULL. Should be equal to ptUnit04+5C
	DWORD			pfExpire;				//+38
};
//#pragma pack()

//#pragma pack(1)
struct D2StatsListEx : public StatsList		//sizeof 0x64
{
	D2StatsListEx*	ptAffixStats;			//+3C 
	D2StatsListEx*	ptInactiveAffixStats;	//+40 
	Unit*			ptOwner;				//+44 
	D2StatInfo		sStatInfoAccr;			//+48 
	D2StatInfo		sStatInfoSync;			//+50  - sStatInfoMod - Stats to update client(s). Sent through D2Common.#10513 (Filled when stat changes and ISC.Saved = 1)
	DWORD*			ptStateFlags;			//+58 
	DWORD			pfCallback;				//+5C 
	D2Game*			ptGame;					//+60 
};
//#pragma pack()

#pragma pack(1)
struct D2Stats
{
	union
	{
		struct
		{
			WORD	nParam;			//+00
			WORD	nIndex;			//+02
		};
		DWORD		dwStat;			//+00
	};
	int		nValue;				//+04
};
#pragma pack()

#pragma pack(1)
struct D2StatsCompbound
{
	DWORD StatsCompbound[34];
};
#pragma pack()

/*=================================================================*/
/*   Data Structure to Update Client                               */
/*=================================================================*/

#pragma pack(1)
struct DataPacket
{
	BYTE mType;			// +00 Packet Type
	BYTE mFunc;			// +02 Function to Call
	BYTE mSize;			// +04 Size of the Packet
	DWORD mReserved;	// +06 Used with items
	BYTE mUnitType;		// +0A Always '0'
	DWORD mPlayerID;	// +0C The Player ID (ptUnit + 08h)
	DWORD mItemID;		// +10 The Item ID
	DWORD mMod1_ID;		// +14 The Stat/Mod ID
	DWORD mParam1;		// +18 3 Paramters
	DWORD mParam2;		// +1C
	DWORD mParam3;		// +20
};
#pragma pack()

/*=================================================================*/
/*   Data Structure to Add Mods to Items                           */
/*=================================================================*/
#pragma pack(1)
struct ItemMod
{
	DWORD mID;			// +00 Mod ID, get it from properties.txt
	DWORD mParam;		// +04 First Property
	DWORD mMinimum;		// +08 Second Property, Min Value
	DWORD mMaximum;		// +0C Third Property, Max Value
};
#pragma pack()

/*=================================================================*/
/*   Data Structure for menus			                           */
/*=================================================================*/
#pragma pack(1)
struct Menu
{
};
#pragma pack()
/*=================================================================*/
/*   Data Structure for Cube Input and Output                      */
/*=================================================================*/
struct CubeInput
{
	BYTE	Flag1;			// +00	Flags
	BYTE	Flag2;			// +01	Flags
	WORD	ID;				// +02	ItemNbr, ItemType
	WORD	unknown;		// +04
	BYTE	Quality;		// +06	Item Quality
	BYTE	Quantity;		// +07	Quantity
};

struct CubeOutput
{
	BYTE	Flag1;			// +00 Flags
	BYTE	Flag2;			// +01 Flags
	WORD	ID;				// +02 ItemNbr
	WORD	unknown1;		// +04
	BYTE	Quality;		// +06 Item Quality
	BYTE	Quantity;		// +07 Quantity
	BYTE	OutputItem;		// +08 Flag to show what to Output
	BYTE	unused[3];		// +09 Overwritten in cube
	WORD	Prefix;			// +0C Prefix to Add to item
	DWORD	unknown2;		// +0E
	WORD	Suffix;			// +12 Suffix to Add to item
	DWORD	unknown3;		// +14
};



/*=================================================================*/
/*   Data Structure for monsters		                           */
/*=================================================================*/
#pragma pack(1)
struct Operate2
{
	D2Game*		ptGame;			//+00
	DWORD		uk1;			//+04 
	DWORD		uk2;			//+08 
	DWORD		UnitID;			//+0C
	DWORD		AnimationMode;	//+10
	DWORD		uk3;			//+14
};
#pragma pack()

#pragma pack(1)
struct Operate3
{
	DWORD		pointX;			//+00
	DWORD		pointY;			//+04
	DWORD		uk1;			//+08
	DWORD		PathType;		//+0C
};
#pragma pack()



/*=================================================================*/
/*   Data Structure for automodifier	                           */
/*=================================================================*/
#pragma pack(1)
struct ItemOperate
{
	Unit*		ptItem;			//+00
};
#pragma pack()

/*=================================================================*/
/*   Data Structure for NPC menus		                           */
/*=================================================================*/
#pragma pack(1)
struct NpcMenuData
{
	DWORD		NpcID;				//+00
	DWORD		NbrMenuItems;		//+04
	WORD		MenuItemTxt[5];		//+08
	DWORD		MenuItemFunc[5];	//+12
	bool		bEnabled;			//+26
};
#pragma pack()

/*=================================================================*/
/*   Data Structure for Vendor screens							   */
/*=================================================================*/
#pragma pack(1)
struct VLItems
{
	BYTE		VendorMin;			//+00
	BYTE		VendorMax;			//+01
	BYTE		VendorMagicMin;		//+02
	BYTE		VendorMagicMax;		//+03
	DWORD		ItemCode;			//+04
	DWORD		VendorMagicLvl;		//+08
};
#pragma pack()

#pragma pack(1)
struct VendorLst
{
	VLItems*	LimitItems;			//+00
	DWORD		LimitCount;			//+04
	DWORD*		PermItems;			//+08
	DWORD		PermCount;			//+0C
};
#pragma pack()

#pragma pack(1)
struct sWinMessage  
{  
     void* msg;						//+00
     DWORD type;					//+04
	 DWORD uk1;						//+08
     WORD  posX;					//+0C
	 WORD  posY;					//+0E
     DWORD uk3;						//+10
     DWORD uk4;						//+14	
     DWORD managed;					//+18
     DWORD uk5;						//+1C
};
#pragma pack()

// Object Struct
// Code removed from the func. so not used
#pragma pack(1)
struct objStruct
{
	BYTE value;					//+00
	D2ObjectsTXT* ptObjTXT;		//+04
	D2Game* ptGame;					//+08
	DWORD maxValue;					//+0C
};
#pragma pack()

#pragma pack(1)
struct D2ItemDropStrc // size 0x21 D2ItemDropStrc D2UpdateItemParam
{
/*	BYTE uk1;				   // +00
	BYTE uk2;				   // +01 (used)
	WORD uk3;				   // +02
	BYTE uk4;				   // +04
	BYTE uk5;				   // +05
	BYTE uk6;				   // +06
	BYTE uk7;		           // +07
*/
	Unit* pReciever;           //+00 - need debug this my code says different then nec's struct
	D2Seed* pSeed;             //+04 - need debug this my code says different then nec's struct
	D2Game*	ptGame;			   // +08
	DWORD nItemLvl;			   // +0C item level
	DWORD dw_10;			   // +10
    DWORD nItemId;			   // +14
	DWORD nAnimMode;           // +18 always 3, 4 for starter item, 1 for tmog
//	DWORD PointX2;			   // +1C
//	DWORD PointY2;			   // +20
    union
	{
		struct
		{
			DWORD       nXpos;                  //+1C
			DWORD       nYpos;                  //+20
		};
		D2CoordStrc		pCoords;				//+1C
	};
	DRLGRoom* ptRoom;							// +24
	WORD        fSpawnInit;						//+28
	//DWORD uk10;								// +28 Unknown 2
	WORD	nItemFormat;						 // +2A
	//DWORD uk11;				   // +2C Unknown
	BOOL		bForce;                 //+2C
    DWORD       nQuality;               //+30
	DWORD       nQuantity;  			//+34
	DWORD		nMinDur;				//+38
	DWORD		nMaxDur;				//+3C
	int			nItemIndex;				//+40
	DWORD		fFlags;					//+44 - itemflag override (used when force is true)
	DWORD		dwSeed;					//+48 - overrides the seed, used when force is true
	DWORD		dwItemSeed;				//+4C - overrides the item seed when force is true
	DWORD		nEarLevel;				//+50
	DWORD		nQtyOverride;			//+54
	char		szName[16];				//+58
	DWORD		nPrefix[3];				//+68
	DWORD		nSuffix[3];				//+74
	DWORD		fFlagsEx;				//+80
};
#pragma pack()

#pragma pack(1)
struct nClueStruct
{
	DWORD ClueID;				   // +00
};
#pragma pack()

#pragma pack(1)
struct nAnim
{
	DWORD uk1;					   // +00
	DWORD uk2;					   // +04
	DWORD uk3;					   // +08
	DWORD PosX;					   // +0C
	DWORD PosY;					   // +10
};
#pragma pack()

#pragma pack(1)
struct D2DamageData 
{
	DWORD HitFlags;			// +00
	union
	{

		struct
		{
			BYTE fResultFlags1;	// +04
			BYTE fResultFlags2;	// +05
		};
		WORD ResultFlags;	// +04
	};	
	WORD fExtra;			// +06
	/*BYTE ResultFlags;		// +04
	BYTE ResultFlags2;		// +05
	BYTE ResultFlags3;		// +06
	BYTE ResultFlags4;		// +07*/
	DWORD PhysicalDamage;	// +08
	DWORD DamagePercent;	// +0C
	DWORD FireDamage;		// +10
	DWORD BurnDamage;		// +14
	DWORD BurnLength;		// +18
	DWORD LightningDamage;	// +1C
	DWORD MagicDamage;		// +20
	DWORD ColdDamage;		// +24
	DWORD PoisonDamage;		// +28
	DWORD PoisonLength;		// +2C
	DWORD ColdLength;		// +30
	DWORD FreezeLength;		// +34 
	DWORD LifeSteal;		// +38
	DWORD ManaSteal;		// +3C
	DWORD StaminaSteal;		// +40
	DWORD StunLength;		// +44
	DWORD AbsorbHeal;		// +48
	DWORD FinalDamage;		// +4C
	DWORD uk4;				// +50
	DWORD PiercePercent;	// +54
	DWORD DamageRatio;		// +58 Field from Missiles.txt, 1024ths
	BYTE  uk5[4];			// +5C
	DWORD HitClass;			// +60
	bool  bSetHitClass;		// +64
	char  nConvType;        // +65
    BYTE  _66[2];           // +66
    int   nConvPct;         // +68
    BYTE  _6C[4];           // +6C
	//DWORD eType;			// +65; From skills.txt Etype column
	//BYTE  uk6[0x0D];        // +69 
};
#pragma pack()

/*=================================================================*/
/*   Data Structure for textmsglist								   */
/*=================================================================*/
#pragma pack(1)
struct nTextList
{
	DWORD msg;	// +00
};
#pragma pack()

#pragma pack(1)
struct ItemStatPacket      // sizeof 0x0F
{
   BYTE bType;            // 00 - type of packet
   BYTE bSubType;         // 01 - subtype of packet

   /*
      0 = vanilla
      1 = custom
   */

   BYTE bSetStat;         // 02 - whenever or not setstat is called
   DWORD dwGuid;         // 03 - id of item

   WORD eStat;            // 07 - stat id
   WORD eLayer;         // 09 - layer
   int nValue;            // 0B - value
};
#pragma pack()


#pragma pack(1)
struct D2MinionListStrc
{
    DWORD dwGUID;            //+00
    D2MinionListStrc* pNext; //+04
};
#pragma pack()

#pragma pack(1)
struct D2PartyNodeStrc
{
	DWORD dwGUID;
	DWORD fFlags;
	D2PartyNodeStrc* pNext;
};
#pragma pack()

#pragma pack(1)
struct D2PartyDataStrc
{
	D2PartyNodeStrc* pNode;
};
#pragma pack()

#pragma pack(1)
struct D2PlayerRosterStrc         // sizeof = 0x84
{
	char szName[16];             // +00
	DWORD dwGUID;                // +10
	DWORD nLife;		         // +14
	int nArenaScore;             // +18
	int nClass;                  // +1C
	short nLevel;                // +20
	short nParty;                // +22
	int nArea;                   // +24
	int nXpos;                   // +28
	int nYpos;                   // +2C
	DWORD fFlags;                // +30 - eButton
	D2PartyDataStrc* pParty;     // +34
	D2MinionListStrc* pMinions;  // +38
	DWORD _3C;                   // +3C
	DWORD _40;                   // +40
	DWORD _44;                   // +44
	DWORD _48;                   // +48
	DWORD _4C;                   // +4C
	DWORD _50;                   // +50
	DWORD _54;                   // +54
	DWORD _58;                   // +58
	DWORD _5C;                   // +5C
	DWORD _60;                   // +60
	WORD _64;                    // +64
	char szNameEx[16];           // +66 - decoy
	WORD _74;                    // +76
	DWORD _78;                   // +78
	DWORD _7C;                   // +7C
	D2PlayerRosterStrc* pNext;   // +80
};
#pragma pack()

#pragma pack(1)
struct D2RosterStrc            // sizeof = 0x34
{
	int nMonster;                // +00
	int nPetType;                // +04
	DWORD dwGUID;                // +08
	DWORD dwPlayerGUID;          // +0C
	DWORD dw10;                  // +10
	DWORD dw14;                  // +14
	DWORD dw18;                  // +18
	int nLifePercent;            // +1C
	BOOL bDead;                  // +20
	DWORD dwSeed;                // +24
	int nString;	             // +28
	int nMercType;               // +2C
	D2RosterStrc* pNext;         // +30
};
#pragma pack()

#pragma pack(1)
struct D2SaveFile
{
	char  CharName[0x10];  //+14	Player Name

/*	DWORD file_identifier; // +00
	DWORD file_Version;    // +04
	DWORD file_size;	   // +08
	DWORD checksum;		   // +0C
	DWORD ptClient428;	   // +10*/
};
#pragma pack()

#pragma pack(1)
struct D2Hireling
{
	DWORD nameStr;		   // +00
	DWORD unknown1;        // +04
	DWORD ukFlag;		   // +08
	DWORD unknown3;        // +0C
	DWORD hireName;		   // +10
	BYTE  unknown2[0x0E];  // +14
	BYTE  act;			   // +22
};
#pragma pack()

struct D2HirelingWrapper
{
	D2Hireling* ptHireling;  // +00
	DWORD uk1;				 // +04 flags?
};

#pragma pack(1)
struct AutoMapInfo
{
   DWORD first;
   DWORD last;
};
#pragma pack()

#pragma pack(1)
struct MapRect
{
   int x;            // +04
   int w;            // +08
   int y;            // +0C
   int h;            // +10
};
#pragma pack()

#pragma pack(1)
struct AutoMapData
{
//   D2CellFile* hCell;   // +00
   DWORD* hCell; // +00
   MapRect rect;
   int cells_w;
   int cells_h;
};
#pragma pack()

#pragma pack(1)
struct GfxTile
{
   DWORD _00;
   DWORD _04;
   DWORD _08;
   DWORD _0C;
   DWORD _10;
   int tileType;
   int nCheck;
   int nIndex;
};
#pragma pack()

#pragma pack(1)
struct MapTile
{
   DWORD _00;                  // +000
   DWORD _04;                  // +004
   int x;                     // +008
   int y;                     // +00C
   DWORD _10;                  // +010
   DWORD flags;               // +014
   GfxTile* hGfxTile;            // +018
   int nRoof;                  // +01C
};
#pragma pack()

#pragma pack(1)
struct AutoMapCacheCell            // sizeof = 0x14
{
   int layer;                  // +00
   short idx;                  // +04
   short x;                  // +06
   short y;                  // +08
   short flags;               // +0A
   AutoMapCacheCell* pColNext;      // +0C
   AutoMapCacheCell* pRowNext;      // +10
};
#pragma pack()

#pragma pack(1)
struct AutoMapCell
{
   int layer;               // +00 - automap layer from levels.txt
   short idx;               // +04 - frame to draw from cell
   short x;               // +06 - x pos of cell
   short y;               // +08 - y pos of cell
   short flags;            // +0A
   AutoMapCell* pColNext;      // +0C - next col
   AutoMapCell* pRowNext;      // +10 - next row
   AutoMapCell* pNextCache;   // +14
   AutoMapCell* pNextList;      // +18 - previous one allocated
};
#pragma pack()

#pragma pack(1)
struct AutoMapShort
{
   int lvltype;            // +00
   int tiletype;            // +04
   char bCheck;            // +08
   char bFilterMin;         // +09
   char bFilterMax;         // +0A
   BYTE _0B;               // +0B
   int nCellIdx[4];         // +0C
   int nRand;               // +1C
};
#pragma pack()

#pragma pack(1)
struct AutoMapCache                         // sizeof = 0x2804
{
   AutoMapCacheCell cells[ AUTOMAP_CACHE_CELLS ];   // +0000
   AutoMapCache* pNext;                     // +2800
};
#pragma pack()

#pragma pack(1)
struct LightMap {
   DWORD radius1;      // +18 size * 8
   DWORD radius2;      // +1C size * 8
   BYTE intensity;      // +24
   BYTE red;         // +25
   BYTE green;         // +26
   BYTE blue;         // +27
};
#pragma pack()

#pragma pack(1)
struct UnitAi
{
	DWORD specialState; // +000
	DWORD AIFunctions; // +004
	DWORD AIFlags;	   // +008
	DWORD OwnerGUID;   // +00C the global unique identifier of the boss or minion owner
	DWORD eOwnerType;  // +010 Boss or minion unit type
	DWORD dwArgs[3];   // +014 custom data used by ai func to store counters as example
					   // +014 TargetUniqueID, +1C Unit Range = 1
    CmdCurrent *pCmdCurrent; // +020
	DWORD pCmdLast;    // +024
	D2Game* ptGame;	   // +028
	DWORD OwnerGUID2;  // +02C same as +008?? hard to belive!
	DWORD eOwnerType2; // +030 same as +00c?? hard to belive!
	DWORD pMinionList[2]; // +34 minionList
	DWORD eTrapNo;		// +3C used by shadows for summoning traps.
};
#pragma pack()

#pragma pack(1)
struct AIParam {
   UnitAi*			hControl;				//+00
   DWORD			AIThink_Func_Addr;		//+04 Func adress to ai
   Unit*			ptTarget;				//+08
   DWORD			unk2;					//+0C
   DWORD			unk3;					//+10
   int				DistanceToTarget;		//+14
   BOOL				EngagedInCombat;		//+18
   D2MonstatsTXT*	ptMonStatsTXT;			//+1C
   D2Monstats2TXT*	ptMonStats2TXT;			//+20
};
#pragma pack()

#pragma pack(1)
struct AttackParam {
   DWORD			eMode;					// +00
   Unit*			pAttacker;				// +04
   Unit*			pDefender;				// +08
   DWORD			unk1;					// +0C
   DWORD			unk2;					// +10
   BYTE				unk3;					// +14
   BYTE				modeFlag;				// +15 100 when mode exists, 101 otherwise
   BYTE				unk5;					// +16
   BYTE				unk6;					// +17
   DWORD			unk7;					// +18
   DWORD			unk8;					// +1C
};
#pragma pack()

#pragma pack(1)
struct Minion {
	Unit*			ptMinion1;				// +00
	Unit*			ptMinion2;				// +04			
};
#pragma pack()

#pragma pack(1)
struct CurseParam {
	D2Game*			ptGame;					// +00
	Unit*			ptAttacker;				// +04
	BOOL			AI_Disable_Decision;	// +08 dimvision/terror flag
	BOOL			nFlag;					// +0C set all values to signed only 
	DWORD			SkillID;				// +10
	DWORD			SkillLevel;				// +14
	int				AuraLen;				// +18
	int				Aurastats[6];			// +1C aurastats[i]
	int				Aurastatscalc[6];		// +34 aurastatscalc[i]
	int				StateID;				// +4C 
	DWORD			AuraEventsFunc[3];		// +7C AuraEventFuncs[i]
	DWORD			AuraEvents[3];			// +84 AuraEvents[i]
};
#pragma pack()

#pragma pack(1)
struct D2MissileInit{						// Size of 0x5C
	DWORD			nFlag;					// +00
	Unit*			ptAttacker;				// +04
	Unit*			ptMissile;				// +08
	Unit*			ptTarget;				// +0C
	int				MissileID;				// +10
	union
	{
		struct
		{
			int nXpos;					// +14
			int nYpos;					// +18
		};

		D2CoordStrc pCoord;			 //+14
	};
	union
	{
		struct
		{
			int nTargetX;            // +1C
			int nTargetY;            // +20
		};

		D2CoordStrc pTargetCoord;	 //+1C
	};
	int				nGFXArg;				// +24 - used for client only, things like zpos
	DWORD			MissileRange;			// +28 - nec name it as nVelocity
	int				SkillID;				// +2C
	int				SkillLevel;				// +30
	int				nLoops;					// +34
	DWORD			uk7;					// +38 - not used
	DWORD			uk8;					// +3C - not used
	int				nFrame;					// +40
	int				nActivateFrame;			// +44
	int				nAttackBonus;			// +48
	int				nVelocity;              // +4C - nec name it as Range
	int				nLightRadius;			// +50 - used if missile has no own radius
	DWORD*			pfFunc;					// +54
	void*			pExtraArgs;				// +58

};
#pragma pack()

#pragma pack(1)
struct FindNearPet
{
   Unit* pSource;
   Unit* pFoundPet;
   int nDistance;
   int nCount;
};
#pragma pack()

#pragma pack(1)
struct UnitInDistance
{
	int nBool;
	int nDistance;
};
#pragma pack()

#pragma pack(1)
struct UnitnFlag
{
	DWORD nFlag;	// +00 Unit nFlag
};
#pragma pack()

struct D2SummonArg
{
    DWORD			 fSummonFlags;		// +00 - 1 = force coordinates
    Unit*            ptOwner;			// +04
    int              nMonstatID;  		// +08
    int				 nAISpecialState;   // +0C
    int              nMonMode;			// +10
	union
	{
		D2CoordStrc	 pPosition;			// +14
		struct
		{
			int      nXpos;             // +14
			int      nYpos;             // +18
		};
	};
    int              nPetType;			// +1C
    int              nPetCount;         // +20
	int				 nPetMax;			// +24
};

/*=================================================================*/
/*   Data Structure for skills.txt AuraStateEffect struct          */
/*=================================================================*/
#pragma pack(1)
struct D2AuraStateEffect		// size 0x44 // prev named D2TempStat
{
	DWORD StateID;			// +00
	DWORD skillID;			// +04
	DWORD sLvl;				// +08
	DWORD SkillDelay;		// +0C
	DWORD TempStat[5];		// +10 +10 Tempstat1 +14 Tempstat2 +18 Tempstat3 +1C Tempstat4 +20 Tempstat5
	DWORD uk1;				// +24
	DWORD TempCalc[5];		// +28 +28 TempCalc1 +2C TempCalc2 +30 TempCalc3 +34 Tempcalc4 +38 TempCalc5
	DWORD dw_3C;			// +3C
	DWORD counter;			// +40 counter
	DWORD dw_44;			// +44 ptStatsListEx 
	DWORD dw_48;			// +48 possible targetStateID (stores stateID)
	DWORD StateFunction;	// +4C
	DWORD PhysicalDamage;	// +50 dw_50
	DWORD DamagePercent;	// +54 dw_54
	DWORD FireDamage;		// +58 dw_58
	DWORD BurnDamage;		// +5C dw_5C
	union{
		DWORD dw_60;		
		DWORD BurnLength;	
	};						// +60
	
	union{
		StatsList* ptStatsList;
		DWORD LightningDamage; 
	};						// +64
	DWORD MagicDamage;		// +68
	DWORD ColdDamage;		// +6C
	DWORD PoisonDamage;		// +70
	DWORD PoisonLength;		// +74
	DWORD ColdLength;		// +78
	DWORD FreezeLength;		// +7C
	DWORD LifeSteal;		// +80
	DWORD ManaSteal;		// +84
	DWORD StaminaSteal;		// +88
	DWORD StunLength;		// +8C
	DWORD AbsorbHeal;		// +90
};
#pragma pack()

/*=================================================================*/
/*   Data Structure for skills.txt auraState struct                */
/*=================================================================*/
/*
#pragma pack(1)
struct AuraStateEffect
{
	DWORD StateID;		// +00
	DWORD skillID;		// +04
	DWORD uk1;			// +0C
	DWORD uk2;			// +10
	DWORD AuraStat1;	// +14
	DWORD AuraStat2;	// +14
};
#pragma pack()
*/


/*=================================================================*/
/*   Data Structure for shrines						               */
/*=================================================================*/
#pragma pack(1)
struct StateEffect
{
	Unit* ptSource;		// +00
	Unit* ptTarget;		// +04
	DWORD skillID;		// +08
	DWORD skillSLVL;	// +0C
	DWORD duration;		// +10
	DWORD StatID;		// +14
	DWORD StatValue;	// +18
	DWORD StateID;		// +1C
	DWORD EndOffset;	// +20
};
#pragma pack(1)

#pragma pack(1)
struct D2CurseState 
{
   Unit* ptAttacker;			   // +00
   Unit* ptDefender;			   // +04
   int skillNo;					   // +08
   int sLvl;					   // +0c
   int duration;				   // +10
   int statNo;					   // +14
   int statValue;				   // +18
   int stateNo;					   // +1C
   DWORD StateFunction;			   // +20
};
#pragma pack()

/*=================================================================*/
/*   Data Structure for Life and Mana Leech Func				   */
/*=================================================================*/
#pragma pack(1)
struct D2LifeAndManaLeechSkillParam
{
	DWORD skillID;			// +00
	DWORD sLvl;				// +04
	DWORD nCounter;			// +08
};
#pragma pack()

/*=================================================================*/
/*    PACKET STRUCTURES FOR SEND PACKET FUNC	                   */
/*=================================================================*/

struct DAUpdatePlayerPacket
{
	BYTE ntype;			// +00
	BYTE nSubtype;		// +01
	WORD wNull;			//+02 (Reserved)
	DWORD nGuid;		// +04
	DWORD nUnitType;	// +08	
	DWORD nfExtraFlags; // +0C
	WORD nskillID;		// +10
	DWORD sLvl;			// +12
};

struct DAUpdateItemConjureTimer
{
	BYTE ntype;			// +00
	BYTE nSubtype;		// +01
	WORD wNull;			//+02 (Reserved)
	DWORD nGuid;		// +04
	DWORD nUnitType;	// +08	
	DWORD nConjureTimer; // +0C 
};

struct DAUpdateItemModStats
{
	BYTE ntype;				// +00
	BYTE nSubtype;			// +01
	WORD wNull;				//+02 (Reserved)
	DWORD nGuid;			// +04
	DWORD nUnitType;		// +08	
	DWORD nstatID;			// +0C properity stat
	DWORD nValue;			// +10
	DWORD dwLichboneID;		// +14
	DWORD dwLichboneExp;	// +18
	DWORD dwLichboneLvl;	// +1C
	BOOL bNoStat;			// +20
	DWORD dwLichboneTimer;	// +24
};

#pragma pack(1)
struct D2QuestBalloonPacketStrc		//sizeof 0x6
{
    BYTE nType;						//+00
    bool bState;					//+01
    DWORD dwNPCGUID;				//+02
};
#pragma pack()

#pragma pack(1)
struct PlayStat32
{
   BYTE type;
   WORD stat;
   DWORD guid;
   DWORD value;
};
#pragma pack()

#pragma pack(1)
struct MonStat8
{
   BYTE type;
   WORD stat;
   DWORD guid;
   BYTE value;
};
#pragma pack()

#pragma pack(1)
struct MonStat16
{
   BYTE type;
   WORD stat;
   DWORD guid;
   WORD value;
};
#pragma pack()

#pragma pack(1)
struct MonStat32
{
   BYTE type;
   WORD stat;
   DWORD guid;
   DWORD value;
};
#pragma pack()

#pragma pack(1)
struct Gold8
{
   BYTE type;
   BYTE value;
};
#pragma pack()

#pragma pack(1)
struct Exp8
{
   BYTE type;
   BYTE value;
};
#pragma pack()

#pragma pack(1)
struct Exp16
{
   BYTE type;
   WORD value;
};
#pragma pack()

#pragma pack(1)
struct Exp32
{
   BYTE type;
   DWORD value;
};
#pragma pack()

#pragma pack(1)
struct Stat8
{
   BYTE type;
   WORD stat;
   BYTE value;
};
#pragma pack()

#pragma pack(1)
struct Stat16
{
   BYTE type;
   WORD stat;
   WORD value;
};
#pragma pack()

#pragma pack(1)
struct Stat32
{
   BYTE type;		// +00
   WORD stat;		// +04
   DWORD value;		// +08
};
#pragma pack()

#pragma pack(1)
struct UnitMsgStat
{
   UnitMsg* pNext;         // +00
   int eMsgType;         // +04
   DWORD nUnitId;         // +08
   DWORD nStat;         // +0C
   DWORD nValue;         // +10
};
#pragma pack()

#pragma pack(1)
struct D2StateUpdatePacket			//sizeof 0x8 + {var}
{
	BYTE		nType;				//+00
	BYTE		nUnitType;			//+01
	DWORD		dwGUID;				//+02
	BYTE		nSize;				//+06
	WORD		nState;				//+07 - was BYTE
	BYTE		ptStream;			//+09
};
#pragma pack()

#pragma pack(1)
struct D2StateUpdateExPacket		//sizeof 0xFF
{
	BYTE		nType;				//+00
	BYTE		nUnitType;			//+01
	DWORD		dwGUID;				//+02
	BYTE		nSize;				//+06
	WORD		nState;				//+07 - was BYTE
	BYTE		ptStream[246];		//+09
};
#pragma pack()

#pragma pack(1)
struct D2StateDataPacket			//sizeof 0x7 + {var}
{
	BYTE		nType;				//+00
	BYTE		nUnitType;			//+01
	DWORD		dwGUID;				//+02
	BYTE		nSize;				//+06
	BYTE		ptStream;			//+07
};
#pragma pack()

#pragma pack(1)
struct D2StateDataExPacket			//sizeof 0xFF
{
	BYTE		nType;				//+00
	BYTE		nUnitType;			//+01
	DWORD		dwGUID;				//+02
	BYTE		nSize;				//+06
	BYTE		ptStream[248];		//+07
};
#pragma pack()

#pragma pack(1)
struct D2StateTogglePacket			//sizeof 0xC (was 0x8)
{
	BYTE		nType;				//+00
	BYTE		nUnitType;			//+01
	DWORD		dwGUID;				//+02
	WORD		nState;				//+06 - was BYTE
};
#pragma pack()

#pragma pack(1)
struct D2ClientPacketRecive 			// sizeof 210
{
	BYTE nPacketType;					// +000
	BYTE b_01;							// +001
	WORD w_02;							// +002
	BYTE nData[512];					// +004
	DWORD nSize;						// +204
	DWORD nTickCount;					// +208
	D2ClientPacketRecive* pNext;		// +20C
};
#pragma pack()

#pragma pack(1)
struct D2BitBuffer					// sizeof 0x14
{
	BYTE*		ptBuffer;			//+00
	DWORD		nBitsInBuffer;		//+04
	DWORD		nBufferPos;			//+08
	DWORD		nBitsAtPos;			//+0C - num bits already read or written at cur pos
	BOOL		bBufferFull;		//+10 - could be a bit bucket
};
#pragma pack()

#pragma pack(1)
struct D2PathPointStrc
{
	short nXpos;
	short nYpos;
};
#pragma pack()

#pragma pack(1)
struct MissileData //
{
    DWORD                    _00;					// +00 - some type of coords, see #11128, #11131 - #11134
    short					 nStreamMissile;		// +04
	short					 nStreamRange;			// +06
	short                    nActivateFrame;        // +08
    short                    nSkill;                // +0A
    short                    nLevel;                // +0C
    short                    nTotalFrames;          // +0E
    short                    nCurrentFrame;         // +10
    WORD                     _12;                   // +12
    DWORD                    fFlags;				// +14
    int                      nOwnerType;            // +18
    DWORD                    dwOwnerGUID;           // +1C
    int                      nHomeType;             // +20 
    DWORD                    dwHomeGUID;            // +24
    int                      nStatus;				// +28 - used only by heatseeking projectiles
    D2PathPointStrc          pCoords;               // +2C - same, these are not coords, they are missile streams etc, see #11213 & #11214
	void*					 pStream;				// +30
};
#pragma pack()

#pragma pack(1)
struct D2PoolBlockStrc				//sizeof 0x18
{
	BYTE* pCommit;					//+00
	DWORD* pUsage;					//+04
	DWORD nBlocks;					//+08
	D2PoolBlockStrc* pPrev;			//+0C
	D2PoolBlockStrc* pNext;			//+10
	struct D2PoolStrc* pPool;		//+14
};
#pragma pack()

#pragma pack(1)
struct D2PoolStrc				//sizeof 0x30
{
	CRITICAL_SECTION pSync;		//+00
	DWORD nBlockSize;			//+18
	DWORD nBlocks;				//+1C
	DWORD nSize;				//+20
	DWORD nAllocBlock;			//+24
	D2PoolBlockStrc* pBlocks;	//+28
	D2PoolBlockStrc* pTail;		//+2C	
};
#pragma pack()

#pragma pack(1)
struct D2PoolBlockEntryStrc		//sizeof 0x8
{
	D2PoolBlockStrc* pBlock;	//+00
	BYTE* pCommit;				//+04
};
#pragma pack()

#pragma pack(1)
struct D2PoolManager						//sizeof 0x17CC	
{
	BOOL bFreePoolAlloc;					//+00
	CRITICAL_SECTION pSync;					//+04
	DWORD nPools;							//+1C
	D2PoolStrc pPools[MAX_POOLS];			//+20
	DWORD nBlocks;							//+7A0
	DWORD nTotalBlocks;						//+7A4
	D2PoolBlockEntryStrc* pBlocks;			//+7A8
	BYTE* pOverflow[MAX_POOL_OVERFLOW];		//+7AC
	DWORD dwMemory;							//+17A8
	char szName[MAX_POOL_NAME];				//+17AC	
};
#pragma pack()

#pragma pack(1)
struct D2MemoryManagerStrc						//sizeof 0xBEA4
{
	BOOL bInit;									//+00
	CRITICAL_SECTION pSync;						//+04
	D2PoolManager pManagers[MAX_MANAGERS];		//+1C
	DWORD nManagers;							//+BE7C
	DWORD nManagerIndex[8];						//+BE80
	DWORD nFreeManagers;						//+BEA0
};
#pragma pack()

#pragma pack(1)
struct D2UnitListStrc         
{
   Unit* pUnitList[6][128];
};
#pragma pack()

#pragma pack(1)
struct D2PathPointX
{
	WORD PointX;
};
#pragma pack()

#pragma pack(1)
struct D2PathPointY
{
	WORD PointY;
};
#pragma pack()

#pragma pack(1)
struct D2LightMapStrc								//sizeof 0x34	
{
    int                      nUnitType;             //+00
    DWORD                    dwGUID;                //+04
    BOOL                     bNeutral;		        //+08 - actually bool, false for server
    int						 nType;                 //+0C
	D2CoordStrc				 pPosition;				//+10
    int                      nLightRadius;          //+18 - nRadius << 3
    int                      nLightRadiusEx;        //+1C - nRadius << 3
    BOOL					 bDelete;               //+20
    BYTE                     nIntensity;			//+24
    BYTE                     nRed;                  //+25
    BYTE                     nGreen;                //+26
    BYTE                     nBlue;                 //+27
    D2LightMapStrc*          pPrev;                 //+28
    BOOL                     bStaticValid;          //+2C
    DWORD*	                 pStaticMap;            //+30
};
#pragma pack()


/*=================================================================*/
/*   Data Structure for objects                                    */
/*=================================================================*/


struct D2ObjectInitStrc
{
	D2Game*				 ptGame;				// +00
	Unit*				 ptObject;			// +04
	DRLGRoom*            ptRoom;				// +08
	D2ObjectRegionStrc*  ptObjectRegion;		// +0C
	D2ObjectsTXT*		 ptObjectsTXT;			// +10
	int                  nYpos;				// +14
	int                  nXpos;				// +18
};

struct D2ObjectOperateStrc
{
	D2Game*				ptGame;			// +00
	Unit*				ptObject;		// +04
	Unit*				ptPlayer;		// +08
	D2ObjectRegionStrc*	ptObjectRegion;	// +0C
	int					nObjectID;		// +10
};

/*=================================================================*/
/*   Data Structure for objects. this is D2ObjectOperateStrc       */
/*=================================================================*/
#pragma pack(1)
struct Operate
{
	D2Game*		ptGame;		//+00
	Unit*		ptObject;	//+04
	Unit*		ptPlayer;	//+08
	DWORD		Unique_ID;	//+0C	ptGame + 0x10F0
	DWORD		nObjectID;	//+10
};
#pragma pack()

struct LvlWarpWrapper
{
	D2LvlWarpTXT* ptLvlWarpTXT; //+00
};


struct D2UnitFindParam // size 0x0E
{
	DWORD nAuraFilter;	//+00
	DWORD dw_04;		//+04
	DWORD dw_08;		//+08
	DWORD xPos;			//+0C
	DWORD yPos;			//+10
	DWORD Range;		//+14
	DWORD dw_18[8];		//+18

};

struct D2UnitsList // list of units
{
	Unit* ptUnit[1024];	//+00
};

struct D2UnitFind{  
	D2PoolManager*		pMemPool;			//+00
	D2UnitsList*		ptUnitsList;		//+04
	DRLGRoom*			ptRoom;				//+08
	DWORD				nFunc;				//+0C
	D2UnitFindParam*	ptUnitFindParam;	//+10
	DWORD				dw_14;				//+14
	DWORD				nMaxCount;			//+18 
	DWORD				dw_1C;				//+1C
	DWORD				nPosX;				//+20
	DWORD				nPosY;				//+24
	DWORD				nRange;				//+28
	DWORD				dw_2C[5];			//+2C
};

struct D2DrawOverlayParam
{
	DWORD nUnitID;				// +00
	DWORD nMode;				// +04
};

struct D2SourceDmgStrc			// size 0x0E
{
	DWORD nBaseLevel;			// unit level
	DWORD nDmgModifier;			// Dmg Modifier
};

struct D2PaletteStrc				//sizeof 0x100
{	
	BYTE pPalette[256];

	__forceinline operator BYTE*()
	{
		return &pPalette[0];
	}
};

struct D2PaletteCacheStrc			//sizeof 0x1008
{
	int nIndex;						//+00
	D2PaletteStrc pPalette[16];		//+04
	D2PaletteCacheStrc* pNext;		//+1004
};

struct D2PalEntryStrc				//sizeof 0x8
{
	int nMonster;					//+00
	D2PaletteCacheStrc* pPalettes;	//+04
};

struct D2QuantityStrc
{
	DWORD uk1;						// +00
	DWORD uk2;						// +04
	DWORD Quantity;					// +08
};

struct D2MPQArchiveStrc	//sizeof 0x108
{
	typedef BOOL (__stdcall * t_LoadFail)();
	HANDLE hArchive;	//+00
	char szBuffer[260];	//+04
};

struct D2InvBelt // sizeof 0x42
{
	DWORD BeltData[66]; 
};

struct D2InvBoxStrc //sizeof 0x10 
{ 
	int nLeft; //+00 
	int nRight; //+04 
	int nTop; //+08 
	int nBottom; //+0C 
};

struct D2InvBeltItemStrc // sizeof ?
{
	Unit* ptItem; //+00
	DWORD BeltData[66]; 
};

struct D2InvItemReqMet // sizeof ?
{
	BOOL nFlag; // +00
};

struct D2ObjectParam
{
	DWORD nUniqueID;			// +00
	DWORD nMode;				// +04
};

struct D2TCExInfoStrc
{
	DWORD nClassic;			// 00 - prob for classic
	DWORD nProb;			// 04 - proc for lod
	short nItem;			// 08 - hcidx of item (also idx of other tc etc)
	short nMul;				// 0A - also hcidx of unique (also used by ma and mg)
	WORD fFlags;			// 0C
	short nMagic;			// 0E - chance magic
	short nRare;			// 10 - chance rare
	short nSet;				// 12 - chance set
	short nUnique;			// 14 - chance unique
	short nEthereal;		// 16 - chance ethereal
	short nSocketed;		// 18 - chance socketed? no idea...
};

struct D2TCExShortStrc
{
   short                  nGroup;      // +00
   short                  nLevel;      // +02
   int					  nTypes;	   // +04 - how many items in the tc array
   DWORD				  dwClassic;   // +08 - sum prob for classic
   DWORD				  dwProb;	   // +0C - sum prob for expansion  
   int                    nPicks;      // +10 - set to 1 for picks equal 0
   int                    nNoDrop;     // +14
   short				  nUnused;	   // +18
   short                  nMagic;      // +1A
   short                  nRare;       // +1C
   short                  nSet;        // +1E
   short                  nUnique;     // +20
   short                  nSuperior;   // +22 - intended for superior
   short                  nNormal;     // +24 - intended for normal
   short				  nUnused2;	   // +26
   D2TCExInfoStrc*        pInfo;       // +28 - TcExInfo* holds the actual data and chance for all the items in the TC
};

#pragma pack(1)
struct D2LaunchPlayerInit
{
	BYTE b_00[0x100]; // +00 Unknowns
	LPWSTR charName; // +100
	BYTE b_104[0x21C]; // +104
	BYTE nClassID; // +320
	WORD nLevel;  // +321
	BYTE b_323;	  // +323
	
	union
	{
	   struct 
	   {
		   BYTE fGameflag1; // none + newbie
		   BYTE fGameflag2; // hardcore + hasdied
		   BYTE fGameflag3; // expansion
		   BYTE fGameflag4; // not ladder
	   };      // +04
		DWORD nGameFlags; // +324
   };

	DWORD nLadderInfo; // +328 probably ladder related
	BYTE b_32C[0x8]; // +32C
	DWORD nGameType; // +334 
	DWORD* ptGrid;	// +338 Animation Grid / 33c
	// +100 : LPWSTR
	// +320 : classID
	// +324 : GameFlags
	// +328 : Ladder?
};
#pragma pack()

struct D2MenuObject
{
   // +0 eType
   //	+48 MenuDataList? (contains stringdata)
   // +4C
   // +50
   // +54
   // +58
   // +A0 language setting
   // +A4 language setting
};

struct D2MenuObjectList
{
	DWORD ptMenuObjectList[8]; // D2MenuObjectLists
};

struct D2MenuCharAnim
{
	DWORD dw_00;			// +00 // flag or position for text
	DWORD GridYMax;			// +04
	DWORD dw_08;			// +08 possible charanimposx
	DWORD GridXMax;			// default 0x25
	DWORD dw_10;			//
};

struct VendorNPCData
{
	DWORD nUniqueID;
};

/*
struct D2WindowStrc
{
    DWORD nType;                // +00
    D2CellFileStrc* pImage;        // +04
    union
    {
        int nTransLevel;            // +08
        DWORD fFlags;                // +08
    };
    int nXpos;                    // +0C
    int nYpos;                    // +10
    union
    {
        int nTriggerKey;            // +14
        int nWidth;                    // +14
    };
    int nHeight;                // +18
    WINDOWCALLBACK pfCallback;    // +1C - pfDraw
    WINDOWCALLBACK pfTicker;    // +20
    WINDOWCALLBACK pfClickDown;    // +24
    WINDOWCALLBACK pfClickUp;    // +28
    DWORD dw6;                    // +2C
    WINDOWCALLBACK pfTimer;        // +30
    union
    {
        WINDOWCALLBACK pfInput;                // +34
        WINDOWCALLBACK pfTimerCallback;        // +34
    };
    D2WindowStrc* pPrev;        // +3C
    union
    {
        int nFrame;                    // +40
        int nSeconds;                // +40
    };
    union
    {
        DWORD dwStartTick;            // +44
        int nImageFrames;            // +44 - set to 5
    };
    union
    {
        void* pLine;                // +48
        DWORD dwAnimTick;            // +48;
    };
    DWORD dwOldTick;            // +4C
    DWORD dw8;                    // +50
    int nLines;                    // +54
    DWORD dw9;                    // +58
    DWORD fFlagsEx;                // +5C - 0x20 -> fDrawn
    DWORD dw11;                    // +60
    int nColumns;                // +64
    DWORD dw10[11];                // +68
    int nFont;                    // +94
    DWORD dw7[4];                // +98
    WORD fButtonFlags;            // +1EA
    DWORD dw12[33];                // +1EC
    DWORD dwString;                // +270
};
*/

#endif