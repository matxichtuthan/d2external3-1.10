#include "Common.hpp"
#include "error.hpp"
#include "d2functions.hpp"
#include "d2customtxt.hpp"
#include "d2wrapper.hpp"
#include "utility.hpp"


/* Set player default light radius, intensity and color 
   Coded by: Thomas Westman
   Date: 2013-03-02
*/

D2LightMapStrc* FASTCALL SetPlayerLightRadius(Unit* ptUnit, BOOL nFlag, WORD wLight, WORD w255, WORD wRed, WORD wGrn, WORD wBlu)
{

	UtilityTXT* ptUtilityTXT = GetUtilityRecord(1);

	if (!ptUtilityTXT)
	{
		log_message("D2SetPlayerLightRadius Func: ptUtilityTXT == %d", ptUtilityTXT);
		return 0;
	}

	D2LightMapStrc* ptLightMapStrc = D2SetUnitLightRadius(ptUnit, nFlag, (WORD)ptUtilityTXT->m_Value, ptUtilityTXT->m_Intensity, ptUtilityTXT->m_red, ptUtilityTXT->m_green, ptUtilityTXT->m_blue);

	return ptLightMapStrc;
}