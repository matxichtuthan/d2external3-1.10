#ifndef __TABLE_HPP__INCLUDED__
#define __TABLE_HPP__INCLUDED__

#define CREATE_TABLE_DESCRIPTION( S ) BINField  TableDesc[S]; 

/* 0 - null data, used by the End (Term) fields only */
#define ADD_TERM_FIELD(I)               \
TableDesc[I].fieldName = "end";         \
TableDesc[I].type = 0x00;               \
TableDesc[I].strLength = 0x00;          \
TableDesc[I].offset = 0x00;             \
TableDesc[I].lookup = 0x00000000        \

/* 1 - String (needs length field) */
#define ADD_STRING_FIELD( N,L,O,I )     \
TableDesc[I].fieldName = N;             \
TableDesc[I].type = 0x01;               \
TableDesc[I].strLength = L;             \
TableDesc[I].offset = O;                \
TableDesc[I].lookup = 0x00000000        \

/* 2 - DWORD field */
#define ADD_DWORD_FIELD( N,O,I )        \
TableDesc[I].fieldName = N;             \
TableDesc[I].type = 0x02;               \
TableDesc[I].strLength = 0x00;          \
TableDesc[I].offset = O;                \
TableDesc[I].lookup = 0x00000000        \

/* 3 - WORD field  */
#define ADD_WORD_FIELD( N,O,I )         \
TableDesc[I].fieldName = N;             \
TableDesc[I].type = 0x03;               \
TableDesc[I].strLength = 0x00;          \
TableDesc[I].offset = O;                \
TableDesc[I].lookup = 0x00000000        \

/* 4 - BYTE field */
#define ADD_BYTE_FIELD( N,O,I )         \
TableDesc[I].fieldName = N;             \
TableDesc[I].type = 0x04;               \
TableDesc[I].strLength = 0x00;          \
TableDesc[I].offset = O;                \
TableDesc[I].lookup = 0x00000000        \

/* 6 - UBYTE field */
#define ADD_UBYTE_FIELD( N,O,I )        \
TableDesc[I].fieldName = N;             \
TableDesc[I].type = 0x06;               \
TableDesc[I].strLength = 0x00;          \
TableDesc[I].offset = O;                \
TableDesc[I].lookup = 0x00000000        \

/* 9 - three and four letter codes */
#define ADD_STRCODE_FIELD( N,O,I )      \
TableDesc[I].fieldName = N;             \
TableDesc[I].type = 0x09;               \
TableDesc[I].strLength = 0x00;          \
TableDesc[I].offset = O;                \
TableDesc[I].lookup = 0x00000000        \

/* A,B,D,10,11 - Lookup code */
#define ADD_LOOKUP_FIELD( N,O,T,L,I )   \
TableDesc[I].fieldName = N;             \
TableDesc[I].type = T;                  \
TableDesc[I].strLength = 0x00;          \
TableDesc[I].offset = O;                \
TableDesc[I].lookup = L                 \

#define BUILD_BIN( S, F, T ) S##Record = (S*)D2CompileText(0,F,TableDesc,&S##RecordNbr, T);
#define UNLOAD_BIN(S)					\
D2UnloadBinImage(S##Record);			\
S##Record = NULL;						\
S##RecordNbr = 0						\


// Utility.txt
extern UtilityTXT*			UtilityTXTRecord;
extern DWORD				UtilityTXTRecordNbr;

// Towns.txt
extern TownsTXT*			TownsTXTRecord;
extern DWORD				TownsTXTRecordNbr;

/* TXT reading function */

void STDCALL CustomTable();
void STDCALL InitBinCreation(DWORD GameType, DWORD uk1, DWORD uk2);
void STDCALL LoadUtility();
void STDCALL LoadTowns();

/* Unload all custom Bin Images */
void STDCALL UnloadCustomBin();
void STDCALL UnloadAllBinImages();
void STDCALL UnloadUtility();
void STDCALL UnloadTowns();


#endif
